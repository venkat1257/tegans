﻿using eBizSuiteAppModel.Table;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MeBizSuiteAppUI.Content
{
    public partial class Finance : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             if (Session["username"] == null)
                    {
                        Response.Write("<script type=text/javascript> window.parent.location.href ='/EmployeeMaster/Info';</script>");
                    }
                    try
                    {
            if (!IsPostBack)
            {
                string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
                string connectionString = ConfigurationManager.ConnectionStrings["sqlcon"].ToString();
                string providerName = "System.Data.SqlClient";
                eBizSuiteTableEntities dbContext = new eBizSuiteTableEntities(); string color = "Blue";
                int? eID = Convert.ToInt32(Session["eid"]);
                var colortype = dbContext.MYTABLE001.Where(a => a.COLUMN09 == eID).FirstOrDefault();
                if (colortype != null)
                { color = colortype.COLUMN06; }
                string OPUnit = Session["OPUnit"].ToString();
                int OPUnitStatus = 1;
                if (string.IsNullOrEmpty(Session["OPUnitstatus"] as string))
                {
                    OPUnit = OPUnit.Replace(",null", ",");
                    OPUnit = OPUnit.TrimEnd(',');
                    OPUnitStatus = Convert.ToInt32(Session["OPUnitstatus"]);
                }
                if (Request.QueryString["ReportName"] == "InventoryAsset")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                    string Project = Request.QueryString["Project"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/InventoryAsset.rdl";
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    SqlCommand cmd = new SqlCommand("USP_REPORTS_INVENTORYASSET", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    cmd.Parameters.AddWithValue("@ReportName", "InventoryAsset");
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                    cmd.Parameters.AddWithValue("@Project", Project);
                    //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "BankRegister")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string Vendor = Request.QueryString["Bank"];
                    //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                    string Project = Request.QueryString["Project"];
                    //EMPHCS1214	Bank register balance must be cumulative and also operating unit must be added . RECEIPT must be added BY RAJ.Jr 30/9/2015
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/BankRegister.rdl";
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    SqlCommand cmd = new SqlCommand("[usp_FIN_REPORT_BANKREGISTER]", con);
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "BankRegister");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Bank", Vendor);
                    //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                    cmd.Parameters.AddWithValue("@Project", Project);
                    //EMPHCS1214	Bank register balance must be cumulative and also operating unit must be added . RECEIPT must be added BY RAJ.Jr 30/9/2015
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
				//EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
                else if (Request.QueryString["ReportName"] == "CashRegister")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string Vendor = Request.QueryString["Bank"];
                    string Project = Request.QueryString["Project"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/CashRegister.rdl";
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    SqlCommand cmd = new SqlCommand("[usp_FIN_REPORT_CASHREGISTER]", con);
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "CashRegister");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Bank", Vendor);
                    cmd.Parameters.AddWithValue("@Project", Project);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "PayableReport")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string Vendor = Request.QueryString["Vendor"];
                    string Type = Request.QueryString["Type"];
                    //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                    string Project = Request.QueryString["Project"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/AccountPayable.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    SqlCommand cmd = new SqlCommand("GetReportsAPAR", con);
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "PayableReport");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Vendor", Vendor);
                    cmd.Parameters.AddWithValue("@Type", Type);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                    cmd.Parameters.AddWithValue("@Project", Project);
                    //EMPHCS1476 EMPHCS1477 EMPHCS1478 Add Advances to Account Payable By Srinivas
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"]);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "ReceivableReport")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string Customer = Request.QueryString["Customer"];
                    string Type = Request.QueryString["Type"];
					//EMPHCS1827 Adding Category Filter to All Sales Customer Reports by gnaneshwar on 27/10/2016
                    string Category = Request.QueryString["Category"];
                    //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                    string Project = Request.QueryString["Project"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/ReceivableAccountReport.rdl";
                    //EMPHCS1740 : Purchase ,Sales, , Purchase returns & Sales returns Placing LInk to data to open new tab detaile record by GNANESHWAR on 23/5/2015
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    SqlCommand cmd = new SqlCommand("GetReportsAPAR", con);
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "ReceivableReport");
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Customer", Customer);
					//EMPHCS1827 Adding Category Filter to All Sales Customer Reports by gnaneshwar on 27/10/2016
                    cmd.Parameters.AddWithValue("@Category", Category);
                    cmd.Parameters.AddWithValue("@Type", Type);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                    cmd.Parameters.AddWithValue("@Project", Project);

                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    //cmd.Parameters.AddWithValue("@OPUnitStatus", OPUnitStatus);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "ProfitAndLoss" || Request.QueryString["ReportName"] == "ProfitAndLoss1")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    //EMPHCS1630 - PROFIT AND LOSS, TRAIL BALANCE ,BALANCE SHEET COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 26/3/2016
                    Session["FYFDate"] = FDate;
                    Session["FYTDate"] = TDate;
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    Session["OperatingFY"] = OperatingUnit;
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/" + Request.QueryString["ReportName"] + ".rdl";
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    SqlCommand cmd = new SqlCommand("GETFINANCEREPORTS", con);
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "ProfitAndLoss");
                    cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    Session["FDate"] = FDate;
                    Session["TDate"] = TDate;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "TrialBalance")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    //EMPHCS1630 - PROFIT AND LOSS, TRAIL BALANCE ,BALANCE SHEET COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 26/3/2016
                    Session["FYFDate"] = FDate;
                    Session["FYTDate"] = TDate;
                    //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                    string Project = Request.QueryString["Project"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    Session["OperatingFY"] = OperatingUnit;
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/TrialBalance.rdl";
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    SqlCommand cmd = new SqlCommand("GETFINANCEREPORTS", con);
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", Request.QueryString["ReportName"]);
                    cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                    cmd.Parameters.AddWithValue("@ProjectName", Project);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "BalanceSheet" || Request.QueryString["ReportName"] == "BalanceSheet1")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    Session["FYFDate"] = FDate;
                    Session["FYTDate"] = TDate;
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    Session["OperatingFY"] = OperatingUnit;
                    //EMPHCS1630 - PROFIT AND LOSS, TRAIL BALANCE ,BALANCE SHEET COMPLETE DATA DETAIL INFORMATION OF RECORD SHOWN IN NEW TAB by gnaneshwar on 26/3/2016
                    //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                    string Project = Request.QueryString["Project"];
                    ReportViewer1.Reset();
                    ReportViewer1.ProcessingMode = ProcessingMode.Local;
                    ReportViewer1.LocalReport.ReportPath = "Content/"+Request.QueryString["ReportName"]+".rdl";
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    SqlCommand cmd = new SqlCommand("GETFINANCEREPORTS", con);
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "BalanceSheet");
                    cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                    //EMPHCS1382 by GNANESHWAR ON 18/11/2015 Adding Project to Finance Reports in Header Level
                    cmd.Parameters.AddWithValue("@ProjectName", Project);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }

                //EMPHCS1606 rajasekhar reddy patakota 15/03/2016 ARAging Report Changes for rk
                else if (Request.QueryString["ReportName"] == "ARAgeingNew")
                {
                    //string FDate = Request.QueryString["FDate"];
                    //string TDate = Request.QueryString["TDate"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    //EMPHCS1706 InventoyByUOM and AR Aging Repot Chages ading Location And Sales rep by GNANESHWAR ON 22/4/2016
                    string Type = Request.QueryString["sales"];
                    //EMPHCS1606 rajasekhar reddy patakota 15/03/2016 ARAging Report Changes for rk
                    if (OperatingUnit != "" && OperatingUnit != null) OPUnit = OperatingUnit;
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/ARAgeingNew.rdl";
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    //EMPHCS1606 rajasekhar reddy patakota 15/03/2016 ARAging Report Changes for rk
                    SqlCommand cmd = new SqlCommand("AgingREPORTS", con);
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", Request.QueryString["ReportName"]);
                    //cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    //cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@SalesRep", Type);
                    cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                //EMPHCS1615 rajasekhar reddy patakota 16/03/2016 ARAging Details Report Changes for rk
                else if (Request.QueryString["ReportName"] == "ARAgeingDetailesNew")
                {
                    //string FDate = Request.QueryString["FDate"];
                    //string TDate = Request.QueryString["TDate"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    string Type = Request.QueryString["sales"];
					//EMPHCS1805 rajasekhar reddy patakota 27/08/2016 Customer Wise Brand filter in aging reports
                    string Brand = Request.QueryString["Brand"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/ARAgeingDetailesNew.rdl";
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    SqlCommand cmd = new SqlCommand("ARAgingDetailsREPORTS", con);
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", Request.QueryString["ReportName"]);
                    //cmd.Parameters.AddWithValue("@FrDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    //cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@SalesRep", Type);
					//EMPHCS1805 rajasekhar reddy patakota 27/08/2016 Customer Wise Brand filter in aging reports
                    cmd.Parameters.AddWithValue("@Brand", Brand);

                    cmd.Parameters.AddWithValue("@Operating", OperatingUnit);
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                    ColorCodes();
                }
                //EMPHCS1848 : Creating Inventory report and stock register with out price by gnaneshwar on 30/12/2016
                else if (Request.QueryString["ReportName"] == "StockRegister")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string Item = Request.QueryString["Item"];
                    string Brand = Request.QueryString["Brand"];
                    string Type = Request.QueryString["Type"];
                    string Location = Request.QueryString["Location"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    string UPC = Request.QueryString["UPC"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/StockRegister.rdl";
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    SqlCommand cmd = new SqlCommand("USP_REPORTS_STOCKLEDGER", con);
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "StockLedger");
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@Project", "");
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Location", Location);
                    cmd.Parameters.AddWithValue("@Item", Item);
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@Type", Type);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@UPC", UPC);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtCustomers = new DataTable();
                    cmd.CommandTimeout = 0;
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "StockLedger")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string Item = Request.QueryString["Item"];
                    string Brand = Request.QueryString["Brand"];
                    string Type = Request.QueryString["Type"];
                    string Location = Request.QueryString["Location"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    string UPC = Request.QueryString["UPC"];
                    string Project = Request.QueryString["Project"];
                    string Customer = Request.QueryString["Customer"];
                    ReportViewer1.Reset();
                        if (Session["AcOwner"].ToString() == "56597")
                            ReportViewer1.LocalReport.ReportPath = "Content/StockLedgerSVR.rdl";
                        else
                            ReportViewer1.LocalReport.ReportPath = "Content/StockLedger.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    SqlCommand cmd = new SqlCommand("USP_REPORTS_STOCKLEDGER", con);
                        if (Session["AcOwner"].ToString() == "56597")
                            cmd = new SqlCommand("USP_REPORTS_STOCKLEDGERSVR", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "StockLedger");
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    //cmd.Parameters.AddWithValue("@Project", "");
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Location", Location);
                    cmd.Parameters.AddWithValue("@Item", Item);
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@Type", Type);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@UPC", UPC);
                    cmd.Parameters.AddWithValue("@Project", Project);
                    cmd.Parameters.AddWithValue("@Customer", Customer);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtCustomers = new DataTable();
                    cmd.CommandTimeout = 0;
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                }
                    else if (Request.QueryString["ReportName"] == "ServiceStockLedger")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Item = Request.QueryString["Item"];
                        string Brand = Request.QueryString["Brand"];
                        string Type = Request.QueryString["Type"];
                        string Location = Request.QueryString["Location"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string UPC = Request.QueryString["UPC"];
                        string Cust = Request.QueryString["Cust"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/StockLedger.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_STOCKLEDGER", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "StockLedger");
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Project", "");
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Location", Location);
                        cmd.Parameters.AddWithValue("@Item", Item);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@UPC", UPC);
                        cmd.Parameters.AddWithValue("@Customer", Cust);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        cmd.CommandTimeout = 0;
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                else if (Request.QueryString["ReportName"] == "StockLedgerKK")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string Item = Request.QueryString["Item"];
                    string Brand = Request.QueryString["Brand"];
                    string Type = Request.QueryString["Type"];
                    string Location = Request.QueryString["Location"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    string UPC = Request.QueryString["UPC"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/StockLedgerKK.rdl";
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    SqlCommand cmd = new SqlCommand("USP_REPORTS_STOCKLEDGERKK", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "StockLedger");
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@Project", "");
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Location", Location);
                    cmd.Parameters.AddWithValue("@Item", Item);
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@Type", Type);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@UPC", UPC);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtCustomers = new DataTable();
                    cmd.CommandTimeout = 0;
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "StockRegisterKK")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string Item = Request.QueryString["Item"];
                    string Brand = Request.QueryString["Brand"];
                    string Type = Request.QueryString["Type"];
                    string Location = Request.QueryString["Location"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    string UPC = Request.QueryString["UPC"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/StockRegisterKK.rdl";
                    ReportViewer1.LocalReport.EnableHyperlinks = true;
                    SqlConnection con = new SqlConnection(sqlcon);
                    SqlCommand cmd = new SqlCommand("USP_REPORTS_STOCKLEDGERKK", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ReportName", "StockLedger");
                    cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                    cmd.Parameters.AddWithValue("@Project", "");
                    cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@Location", Location);
                    cmd.Parameters.AddWithValue("@Item", Item);
                    cmd.Parameters.AddWithValue("@Brand", Brand);
                    cmd.Parameters.AddWithValue("@Type", Type);
                    cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                    cmd.Parameters.AddWithValue("@UPC", UPC);
                    cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtCustomers = new DataTable();
                    cmd.CommandTimeout = 0;
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "PROFITANDLOSSBYJOBBING")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/ProfiAndLossByJobbing.rdl";
                    SqlConnection con = new SqlConnection(sqlcon);
                    SqlCommand cmd = new SqlCommand("PROFITANDLOSSBYJOBBING", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@OPUNIT", OPUnit);
                    cmd.Parameters.AddWithValue("@ACOWNER", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@FRDATE", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@TODATE", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@OPERATING", OperatingUnit);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                }
                else if (Request.QueryString["ReportName"] == "ProfitAndLossByProduct")
                {
                    string FDate = Request.QueryString["FDate"];
                    string TDate = Request.QueryString["TDate"];
                    string item = Request.QueryString["Item"];
                    string OperatingUnit = Request.QueryString["OperatingUnit"];
                    ReportViewer1.Reset();
                    ReportViewer1.LocalReport.ReportPath = "Content/ProfitAndLossByProduct.rdl";
                    SqlConnection con = new SqlConnection(sqlcon);
                    SqlCommand cmd = new SqlCommand("ProfitAndLossByProduct", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@OPUNIT", OPUnit);
                    cmd.Parameters.AddWithValue("@ACOWNER", Session["AcOwner"]);
                    cmd.Parameters.AddWithValue("@FRDATE", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@TODATE", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                    cmd.Parameters.AddWithValue("@OPERATING", OperatingUnit);
                    cmd.Parameters.AddWithValue("@Item", item);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dtCustomers = new DataTable();
                    da.Fill(dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                    ReportViewer1.LocalReport.DataSources.Add(rds);
                    ReportViewer1.DataBind();
                    ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                }
                    else if (Request.QueryString["ReportName"] == "GSTR1")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        //ReportViewer1.LocalReport.ReportPath = "Content/GSTR-1.rdl";
                        ReportViewer1.LocalReport.ReportPath = "Content/GSTR1.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        //SqlCommand cmd = new SqlCommand("USP_PROC_GSTREPORTS", con);
                        SqlCommand cmd = new SqlCommand("USP_PROC_GSTREPORTSGSTR1", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 0;
                        //cmd.Parameters.AddWithValue("@ReportName", "GSTR1");
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "LotusReport")
                    {
                        string iv = Request.QueryString["iv"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/LotusReport.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        //EMPHCS1606 rajasekhar reddy patakota 15/03/2016 ARAging Report Changes for rk
                        SqlCommand cmd = new SqlCommand("PROFIT_R_LOSS_BY_INVOICEREFERENCE", con);
                        cmd.CommandTimeout = 0;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@REF", iv);
                        cmd.Parameters.AddWithValue("@OPUNIT", OperatingUnit);
                        cmd.Parameters.AddWithValue("@OPERATING", OPUnit);
                        cmd.Parameters.AddWithValue("@ACOWNER", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@DTF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                    }
                    else if (Request.QueryString["ReportName"] == "StockSummary")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Item = Request.QueryString["Item"];
                        string Brand = Request.QueryString["Brand"];
                        string Type = Request.QueryString["Type"];
                        string Location = Request.QueryString["Location"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string UPC = Request.QueryString["UPC"];
                        string Project = Request.QueryString["Project"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/StockSummary.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_STOCKSUMMARY", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "StockSummary");
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Project", Project);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Location", Location);
                        cmd.Parameters.AddWithValue("@Item", Item);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@UPC", UPC);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        cmd.CommandTimeout = 0;
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                    else if (Request.QueryString["ReportName"] == "StockDetailsByItem")
                    {
                        string FDate = Request.QueryString["FDate"];
                        string TDate = Request.QueryString["TDate"];
                        string Item = Request.QueryString["Item"];
                        string Brand = Request.QueryString["Brand"];
                        string Type = Request.QueryString["Type"];
                        string Location = Request.QueryString["Location"];
                        string OperatingUnit = Request.QueryString["OperatingUnit"];
                        string UPC = Request.QueryString["UPC"];
                        string Project = Request.QueryString["Project"];
                        ReportViewer1.Reset();
                        ReportViewer1.LocalReport.ReportPath = "Content/StockDetailsByItem.rdl";
                        ReportViewer1.LocalReport.EnableHyperlinks = true;
                        SqlConnection con = new SqlConnection(sqlcon);
                        SqlCommand cmd = new SqlCommand("USP_REPORTS_STOCKDETAILSBYITEM", con);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ReportName", "StockDetailsByItem");
                        cmd.Parameters.AddWithValue("@OPUnit", OPUnit);
                        cmd.Parameters.AddWithValue("@Project", Project);
                        cmd.Parameters.AddWithValue("@AcOwner", Session["AcOwner"]);
                        cmd.Parameters.AddWithValue("@FromDate", DateTime.ParseExact(FDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@ToDate", DateTime.ParseExact(TDate.Trim(), Session["FormatDate"].ToString(), CultureInfo.InvariantCulture).ToString("MM/dd/yy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("@Location", Location);
                        cmd.Parameters.AddWithValue("@Item", Item);
                        cmd.Parameters.AddWithValue("@Brand", Brand);
                        cmd.Parameters.AddWithValue("@Type", Type);
                        cmd.Parameters.AddWithValue("@OperatingUnit", OperatingUnit);
                        cmd.Parameters.AddWithValue("@UPC", UPC);
                        cmd.Parameters.AddWithValue("@DateF", Session["FormatDate"].ToString());
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataTable dtCustomers = new DataTable();
                        cmd.CommandTimeout = 0;
                        da.Fill(dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Clear();
                        ReportDataSource rds = new ReportDataSource("DataSet1", dtCustomers);
                        ReportViewer1.LocalReport.DataSources.Add(rds);
                        ReportViewer1.DataBind();
                        ReportViewer1.LocalReport.Refresh();
                        ColorCodes();
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = "Page Not Found......";
                Session["MessageFrom"] = msg + "Due to" + ex.Message;
                Session["SuccessMessageFrom"] = "fail";
                Response.Write("<script type=text/javascript> window.parent.location.href ='/EmployeeMaster/Info';</script>");

            }
        }

        public void ColorCodes()
        {
            string color = "Blue";
            string FDate = Request.QueryString["FDate"];
            string TDate = Request.QueryString["TDate"];
            if ((FDate == null && TDate == null) || (FDate == "undefined" && TDate == "undefined"))
           {
                var dt = DateTime.Now.ToString(Session["FormatDate"].ToString());

                FDate = dt;
                TDate = dt;

            }
           
            string sqlcon = ConfigurationManager.ConnectionStrings["sqlcon"].ConnectionString;
            SqlConnection con = new SqlConnection(sqlcon);
            SqlCommand cmdp = new SqlCommand("SELECT m.COLUMN03 AS [COLOR TYPE], m.COLUMN04 AS TABLEHEADERBACKCOLOR, m.COLUMN05 AS TABLEHEADERFORECOLOR,  m.COLUMN06 AS TABLEROWBACKCOLOR1, m.COLUMN07 AS TABLEROWBACKCOLOR2, m.COLUMN08 AS TABLEROWFORECOLOR, m.COLUMN09 AS [HOVER COLOR], m.COLUMN10 AS [TITTLE BAR COLOR],isnull(c8.COLUMN03,' ') Cname FROM MYTABLE003 m left outer join CONTABLE008 c8 on c8.columna03='" + Session["AcOwner"] + "' and (c8.columna02='" + Session["OPUnit1"] + "' or c8.columna02 is null) and isnull(c8.column05,0)=0 and isnull(c8.columna13,0)=0 where m.column03='" + color + "'", con);
            SqlDataAdapter dap = new SqlDataAdapter(cmdp);
            DataTable dtp = new DataTable();
            dap.Fill(dtp);
            ReportDataSource rdsp = new ReportDataSource("DataSet2", dtp);
            ReportViewer1.LocalReport.DataSources.Add(rdsp);
            ReportViewer1.DataBind();
            ReportParameter[] myParams = new ReportParameter[]{
                    new ReportParameter("ReportParameter1", dtp.Rows[0]["TABLEHEADERBACKCOLOR"].ToString()),
                    new ReportParameter("ReportParameter2", dtp.Rows[0]["TABLEROWBACKCOLOR1"].ToString()),
                    new ReportParameter("ReportParameter3", dtp.Rows[0]["TABLEROWBACKCOLOR2"].ToString()),
                    new ReportParameter("ReportParameter4", dtp.Rows[0]["TABLEROWFORECOLOR"].ToString()),
                    new ReportParameter("ReportParameter5", dtp.Rows[0]["Cname"].ToString()),
                    new ReportParameter("ReportParameter6", FDate),
                    new ReportParameter("ReportParameter7", TDate)};
            ReportViewer1.LocalReport.SetParameters(myParams);
            ReportViewer1.LocalReport.Refresh();
        }
    }
}