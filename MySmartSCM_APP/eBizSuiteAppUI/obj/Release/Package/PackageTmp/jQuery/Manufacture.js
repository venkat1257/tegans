


function PAYMENTTERMSCHANGE(tr,fId) {
    $.ajax({
        //EMPHCS729 rajasekhar reddy patakota 01/09/2015 Tax Amount Calculation In Transaction Forms
        url: "/FormBuilding/GetPT?PT" + tr + "",
        type: "POST",
        //EMPHCS729 Rajasekhar Patakota 21/7/2015 Tax Amount Calculation In Transaction Forms
        data: JSON.stringify({ PT: tr }),
        dataType: "json", cache: true,
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            if (result.PT != '') {

                
                if (fId == 1362)
                    $("#FITABLE019COLUMN10").datepicker('setDate', result.PT);
                else if (fId == 1374)
                    $("#PUTABLE001COLUMN08").datepicker('setDate', result.PT);
                else if (fId == 1376)
                    $("#SATABLE015COLUMN08").datepicker('setDate', result.PT);
                else if (fId == 1251)
                    $("#PUTABLE001COLUMN08").datepicker('setDate', result.PT);
                else if (fId == 1287)
                    $("#SATABLE009COLUMN10").datepicker('setDate', result.PT);

                else if (fId == 1293)
                    $("#FITABLE012COLUMN20").datepicker('setDate', result.PT);
                else if (fId == 1273)
                    $("#PUTABLE005COLUMN11").datepicker('setDate', result.PT);
                else if (fId == 1328)
                    $("#PUTABLE001COLUMN08").datepicker('setDate', result.PT);
                else if (fId == 1353)
                    $("#SATABLE005COLUMN08").datepicker('setDate', result.PT);

                else if (fId == 1355)
                    $("#PUTABLE001COLUMN08").datepicker('setDate', result.PT);
                else if (fId == 1283)
                    $("#SATABLE005COLUMN08").datepicker('setDate', result.PT);

            }
        }
    })
   

 
   
}
//EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
function GRNItemCreate(eval, FormID, FormName, Direction) {
    var Reference = $("#PUTABLE022COLUMN04").val();
    var OPU = $("#PUTABLE022COLUMN09").val();
    $.ajax({
        url: "/JobOrder/GRNItemCreate?FormName=" + FormName + "",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({ Item: Reference, Direction: Direction, OPU: OPU }),
        success: function (data) {
            var items = [];
            items.push("<option value=''>" + "--Select--" + "</option>");
            for (var i = 0; i < data.Data1.length; i++) {
                items.push("<option value=" + data.Data1[i].Value + ">" + data.Data1[i].Text + "</option>");
            }
            var tbl = $("#grdData")[0];
            for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                var trw = tbl.rows[irow];
                $(trw).find("#PUTABLE023COLUMN03").html(items.join(' '));
                $(trw).find("#PUTABLE023COLUMN03").val(data.Data2);
                $(trw).find("#PUTABLE023COLUMN04").val(data.Data3);
            }
        }
    });
}
//EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
function GRNtoJobOrderIN(eve, FormName) {
    var PurchaseOrderID = eve;
    if (eve != "null") {
        $.ajax({
            url: "/JobOrder/GRNtoJobOrderIN?FormName=" + FormName + "",
            type: "JSON",
            contentType: "application/json",
            data: JSON.stringify({ GRN: PurchaseOrderID }),
            success: function (data) {
                if (data != "") {
                    $("#PUTABLE001COLUMN05").val(data.Data2);
                    $("#select2-PUTABLE001COLUMN05-container").text(data.cName);
                    $("#PUTABLE001COLUMN24").val(data.Data3);
                    $("#PUTABLE001COLUMN11").val(PurchaseOrderID);
                    $("#SATABLE003COLUMN06").val(data.Data7);
                    $("#SATABLE003COLUMN07").val(data.Data8);
                    $("#SATABLE003COLUMN16").val(data.Data9);
                    $("#SATABLE003COLUMN11").val(data.Data10);
                    $("#SATABLE003COLUMN10").val(data.Data11);
                    $("#SATABLE003COLUMN12").val(data.Data12);
                    $("#SATABLE003COLUMN08").val(data.Data13);
                    //$("#PUTABLE002COLUMN03").val(data.Data4);
                    var tbl = $("#grdData")[0];
                    $('#grdData').remove();
                    $('#itemgrid').html(data.grid);
                    $('#grdData').append(data.remain);
                    var tbl = $("#grdData")[0];
                    var s = 1;
                    for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                        var trw = tbl.rows[irow];
                        $(trw).find("#PUTABLE002COLUMN09").val(0);
                        $(trw).find("#PUTABLE002COLUMN36").val(s);
                        $(trw).find("#PUTABLE002COLUMN36").prop("readonly", true);
                        s = s + 1;
                    }
                    dateupdate(); 
                }
            }
        });
    }
};
//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
function ddlSelectproductionToJOR(eve, FormName) {
    var ProductionID = eve;
    if (eve != "null") {
        $.ajax({
            url: "/JobOrderReceipt/getProductiontoJORDetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ ProductionID: ProductionID }),
            success: function (data) {
                if (data != "") {
                    $('#grdData').remove();
                    $('#itemgrid').html(data.grid);
                    $('#grdData').append(data.remain);
                    $("#PUTABLE003COLUMN08").val(data.Data);
                    $("#PUTABLE003COLUMN05").val(data.Data1);
                    $("#PUTABLE003COLUMN06").val(data.Data2);
                    $("#PUTABLE003COLUMN10").val(data.Data3);
                    $("#PUTABLE003COLUMN12").val(data.Data4);
                    $("#PUTABLE003COLUMN13").val(data.Data5);
                    $("#PUTABLE003COLUMN23").val(eve);
                    $("#PUTABLE003COLUMN20").prop("checked", true);
                    document.getElementById("PUTABLE003COLUMN20").value = true;
                     var tbl = $("#grdData")[0];
                     for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                         var trw = tbl.rows[irow];
                         $(trw).find("#PUTABLE004COLUMN06").attr("readonly", true);
                     }
                }
            }
        });
    }
};

//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
function GetProdEditDetails(prodid, id, FormName) {
    var Transno = $('#SATABLE019COLUMN04').val();
    if (id != "null") {
        $.ajax({
            url: "/Production/GetProdEditDetails?FormName=" + FormName + "",
            type: "JSON",
            contentType: "application/json",
            async:false,
            data: JSON.stringify({ Prodid: prodid, Processid: id, Transno: Transno }),
            success: function (data) {
                if (data.Production != "") {
                    $('#Productionid').attr("value", data.Prodid);
                    //$('#SATABLE019COLUMN04').val(data.Production);
                    $('#grdData').remove();
                    $('#itemgrid').html(data.grid);
                    $('#SATABLE019COLUMN07').val(data.Jobber);
                    $('#SATABLE019COLUMN10').val(data.Customer);
                    $('#SATABLE019COLUMN06').val(data.JobOrder);
                    $('#SATABLE019COLUMN08').val(data.SalesOrder);
                    $('#SATABLE019COLUMN09').val(data.Opportunity);
                    $('#SATABLE019COLUMN11').val(data.Date);
                    $('#SATABLE019COLUMN12').val(data.CurrentStatus);
                    $('#SATABLE019COLUMN13').val(data.SubmittedTo);
                    $('#SATABLE019COLUMN14').val(data.DueDate);
                    $('#SATABLE019COLUMN15').val(data.ExecutedBy);
                    $('#SATABLE019COLUMN16').val(data.HandedoverTo);
                    $('#SATABLE019COLUMN18').val(data.Remarks);
                    $('#SATABLE019COLUMN05').val(data.Issue);
                    $('#SATABLE019COLUMN19').val(data.OperatingUnit);
                    $('#SATABLE019COLUMN20').val(data.Machineype);
                    $('#SATABLE019COLUMN21').val(data.Machine);
                    $('#SATABLE019COLUMN22').val(data.ProcessingIn);
                    $('#SATABLE019COLUMN23').val(data.ProcessingOut);
                    $('#SATABLE019COLUMN24').val(data.ProcessingDuration);
                    var tbl = $("#grdData")[0];
                    var tr = $('#grdData > tbody:last tr:last');
                    $('#grdData tbody').empty();
                    $('#grdData').append(tr);
                    $('#grdData').append(data.grid1);
                    tbl = $("#grdData")[0];
                    if (tbl.rows.length <= 1)
                        $("#grdData tbody").append("No Records Exists");
                    for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                        var trw = tbl.rows[irow];
                        $(trw).find("#SATABLE020COLUMN04").prop("disabled", "disabled");
                        $(trw).find("#SATABLE020COLUMN06").prop("readonly", true);
                    }
                }
                else {
                    $('#Productionid').attr("value", 0);
                    $('#grdData tbody').empty();
                    $("#grdData tbody").append("No Records Exists");
                }
            }
        });
    }
};
//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
function GetProductionDetails(eve, FormName) {
    if (eve != "null") {
        $.ajax({
            url: "/Production/GetIssuetoProdDetails?FormName=" + FormName + "",
            type: "JSON",
            contentType: "application/json",
            async:false,
            data: JSON.stringify({ IssueID: eve }),
            success: function (data) {
                if (data != "") {
                    $("#SATABLE019COLUMN06").val(data.Data1);
                    $("#SATABLE019COLUMN07").val(data.Data2);
                    $("#SATABLE019COLUMN05").val(data.pid);
                    $("#SATABLE019COLUMN19").val(data.Data4);
                    $("#SATABLE019COLUMN10").val(data.Data5);
                    $("#SATABLE019COLUMN09").val(data.Data6);
                    $('#grdData').remove();
                    $('#itemgrid').html(data.grid);
                    var tbl = $("#grdData")[0];
                    var tr = $('#grdData > tbody:last tr:last');
                    $('#grdData tbody').empty();
                    $('#grdData').append(tr);
                    $('#grdData').append(data.grid1);
                    tbl = $("#grdData")[0];
                    for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                        var trw = tbl.rows[irow];
                        $(trw).find("#SATABLE020COLUMN04").prop("disabled", "disabled");
                        $(trw).find("#SATABLE020COLUMN13").prop("disabled", "disabled");
                        $(trw).find("#SATABLE020COLUMN06").prop("readonly", true);
                    }
                }
            }
        });
    }
};

function ProductionQtychk(tr, qty) {
    var RE = /^\d*\.?\d*$/;
    var actual = $(tr).find("#SATABLE020COLUMN06").val();
    if (actual == "" || typeof (actual) == "undefined") actual = 0;
    if (qty == "" || typeof (qty) == "undefined") qty = 0;
    if ((isNaN(parseFloat(qty)) || parseFloat(qty) < 0 || !(RE.test(qty)))) {
        Alert.render('Please Enter Value Greater Than Zero Only!'); $("#alertok").focus();
        $(tr).find("#SATABLE020COLUMN07").val('');
        $("#alertok").on("click", function () {
            $("#dialogbox").dialog("close"); $(tr).find("#SATABLE020COLUMN07").focus();
        })
    }
    else if (parseFloat(qty) > parseFloat(actual)) {
        Alert.render('Actual Qty Should Less Than Expected Qty'); $("#alertok").focus();
        $(tr).find("#SATABLE020COLUMN07").val('');
        $("#alertok").on("click", function () {
            $("#dialogbox").dialog("close"); $(tr).find("#SATABLE020COLUMN07").focus();
        })
    }
}
function ProductionSave() {
    $("#divLoading").show();
    var flag = false;
    var ProdNo = $("#SATABLE019COLUMN04").val();
    if (flag == false) {
        if (ProdNo == "" || ProdNo == null || typeof (ProdNo) == "undefined") {
            flag = true;
            Alert.render("Please Enter Production"); $("#alertok").focus(); $("#divLoading").hide();
            $("#alertok").on("click", function () {
                $("#dialogbox").dialog("close"); $("#SATABLE019COLUMN04").focus(); flag = true; return false;
            })
            flag = true; return false;
        }
    }
    var Jobber = $("#SATABLE019COLUMN07").val();
    if (flag == false) {
        if (Jobber == "" || Jobber == null || typeof (Jobber) == "undefined") {
            flag = true;
            Alert.render("Please Select Jobber"); $("#alertok").focus(); $("#divLoading").hide();
            $("#alertok").on("click", function () {
                $("#dialogbox").dialog("close"); $("#SATABLE019COLUMN07").focus(); flag = true; return false;
            })
            flag = true; return false;
        }
    }
    var Status = $("#SATABLE019COLUMN12").val();
    if (flag == false) {
        if (Status == "" || Status == null || typeof (Status) == "undefined") {
            flag = true;
            Alert.render("Please Select Current Status"); $("#alertok").focus(); $("#divLoading").hide();
            $("#alertok").on("click", function () {
                $("#dialogbox").dialog("close"); $("#SATABLE019COLUMN12").focus(); flag = true; return false;
            })
            flag = true; return false;
        }
    }
    //var Submitted = $("#SATABLE019COLUMN13").val();
    //if (flag == false) {
    //    if (Submitted == "" || Submitted == null || typeof (Submitted) == "undefined") {
    //        flag = true;
    //        Alert.render("Please Select Submitted To"); $("#alertok").focus(); $("#divLoading").hide();
    //        $("#alertok").on("click", function () {
    //            $("#dialogbox").dialog("close"); $("#SATABLE019COLUMN13").focus(); flag = true; return false;
    //        })
    //        flag = true; return false;
    //    }
    //}
    var Opunit = $("#SATABLE019COLUMN19").val();
    if (flag == false) {
        if (Opunit == "" || Opunit == null || typeof (Opunit) == "undefined") {
            flag = true;
            Alert.render("Please Select Operating Unit"); $("#alertok").focus(); $("#divLoading").hide();
            $("#alertok").on("click", function () {
                $("#dialogbox").dialog("close"); $("#SATABLE019COLUMN19").focus(); flag = true; return false;
            })
            flag = true; return false;
        }
    }
    if (flag == false) {
        var tbl = $("#grdData")[0];
        var xmldata = "<ROOT>"; var Gridlength = tbl.rows.length;
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            if ($(trw).find("#checkRow").is(':checked')) {
                var upc = $(trw).find("[id='SATABLE020COLUMN03']").val();
                if (typeof upc == "undefined") upc = "";
                var RefCols = $(trw).find("[id='checkRow']").val();
                if (typeof RefCols == "undefined" || RefCols == "") RefCols = 0;
                var Item = $(trw).find("[id='SATABLE020COLUMN04']").val();
                if (typeof Item == "undefined" || Item == "") Item = 0;
                if (Item == "0") {
                    flag = true;
                    Alert.render("Please Select Item"); $("#alertok").focus(); $("#divLoading").hide();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); $(trw).find("[id='SATABLE020COLUMN04']").focus(); flag = true; return false;
                    })
                    irow = Gridlength;
                    flag = true; return false;
                }
                var Desc = $(trw).find("[id='SATABLE020COLUMN05']").val();
                if (typeof Desc == "undefined") Desc = "";
                Desc = Desc.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;')
                    .replace(/'/g, '&apos;');
                var EQty = $(trw).find("[id='SATABLE020COLUMN06']").val();
                if (typeof EQty == "undefined" || EQty == "") EQty = 0;
                var AQty = $(trw).find("[id='SATABLE020COLUMN07']").val();
                if (typeof AQty == "undefined" || AQty == "") AQty = 0;
                var Units = $(trw).find("[id='SATABLE020COLUMN13']").val();
                if (typeof Units == "undefined" || Units == "") Units = 0;
                var Prints = $(trw).find("[id='SATABLE020COLUMN14']").val();
                if (typeof Prints == "undefined" || Prints == "") Prints = "";
                if (AQty == "0"&&EQty != "0") {
                    flag = true;
                    Alert.render("Please Enter Actual Quantity"); $("#alertok").focus(); $("#divLoading").hide();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); $(trw).find("[id='SATABLE020COLUMN07']").focus(); flag = true; return false;
                    })
                    irow = Gridlength;
                    flag = true; return false;
                }
                var Remarks = $(trw).find("[id='SATABLE020COLUMN08']").val();
                if (typeof Remarks == "undefined") Remarks = "";
                Remarks = Remarks.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;')
                    .replace(/'/g, '&apos;');

                xmldata = xmldata + "<Column><UPC>" + upc + "</UPC>";
                xmldata = xmldata + "<RefCols>" + RefCols + "</RefCols>";
                xmldata = xmldata + "<Item>" + Item + "</Item>";
                xmldata = xmldata + "<Desc>" + Desc + "</Desc>";
                xmldata = xmldata + "<Units>" + Units + "</Units>";
                xmldata = xmldata + "<Prints>" + Prints + "</Prints>";
                xmldata = xmldata + "<EQty>" + EQty + "</EQty>";
                xmldata = xmldata + "<AQty>" + AQty + "</AQty>";
                xmldata = xmldata + "<Remarks>" + Remarks + "</Remarks></Column>";
            }
        }
        xmldata = xmldata + "</ROOT>";
        if (xmldata != "<ROOT></ROOT>" && flag != true) {
            var dataparam = { list: xmldata };
            $.ajax({
                url: "/Production/PRDSaveGrid",
                type: "POST",
                data: JSON.stringify(dataparam),
                dataType: "html",
                cache: true,
                async: false,
                contentType: "application/json; charset=utf-8",
                success: function (resultG) {
                    //document.forms[0].submit();                    
                    var formData = $("#saveformdata").serialize();
                    $.ajax({
                        url: "/Production/NewForm",
                        type: "POST",
                        dataType: "json",
                        data: formData,
                        async: true,
                        success: function (data) {
                            debugger
                            var OutParam = data.OutParam;
                            var status = data.status;
                            if (OutParam != "") {
                                MessagesColour(data.MessageFrom, data.SuccessMessageFrom);
                                $('#OutParam').val(OutParam);
				//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                                var id = $("#SATABLE019COLUMN12").val();
                                $("#SATABLE019COLUMN13").find('option[value=' + id + ']').remove();
                                $('#SATABLE019COLUMN12').val(status);
                                $('#SATABLE019COLUMN13').val("");
                                $("#SATABLE019COLUMN24").val("");
                                $("#SATABLE019COLUMN22").val("");
                                $("#SATABLE019COLUMN23").val("");
                                var index = $('#tabs1 a[href="#' + status + '"]').parent().index();
                                var selectedli = $('#tabs1 ul li').eq(index);
                                $("#tabs1 ul li").removeClass("ui-tabs-selected ui-state-active");
                                selectedli.addClass("ui-tabs-selected ui-state-active");
                                var tbl = $("#grdData")[0];
                                for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                                    var trw = tbl.rows[irow];
                                    var item=$(trw).find("#SATABLE020COLUMN04").val();
                                    var actqty = $(trw).find("#SATABLE020COLUMN07").val();
                                    var RefCols = $(trw).find("#checkRow").val();
                                    var remqty = 0;
                                    for (var i = 0; i < data.ActqtyList.length; i++) {
                                        if (data.ActqtyList[i].Item == item && data.ActqtyList[i].IssueNo == RefCols) {
                                            remqty = data.ActqtyList[i].Qty;
                                            if (remqty == "" || typeof (remqty) == "undefined") remqty = 0;
                                            i = data.ActqtyList.length;
                                        }
                                    }
                                    actqty = parseFloat(actqty) + parseFloat(remqty);
                                    $(trw).find("#SATABLE020COLUMN06").val(actqty);
                                    $(trw).find("#SATABLE020COLUMN07").val(actqty);
                                    $(trw).find("#SATABLE020COLUMN08").val("");
                                    $('#grdData input[type=checkbox]:not(:checked)').closest('tr').remove();
                                }
                                $("#divLoading").hide();
                            }
                        }
                    })
                }
            });
        }
    }
}

//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
function ProductionEditSave() {
    $("#divLoading").show();
    var flag = false;
    var ProdNo = $("#SATABLE019COLUMN04").val();
    if (flag == false) {
        if (ProdNo == "" || ProdNo == null || typeof (ProdNo) == "undefined") {
            flag = true;
            Alert.render("Please Enter Production"); $("#alertok").focus(); $("#divLoading").hide();
            $("#alertok").on("click", function () {
                $("#dialogbox").dialog("close"); $("#SATABLE019COLUMN04").focus(); flag = true; return false;
            })
            flag = true; return false;
        }
    }
    var Jobber = $("#SATABLE019COLUMN07").val();
    if (flag == false) {
        if (Jobber == "" || Jobber == null || typeof (Jobber) == "undefined") {
            flag = true;
            Alert.render("Please Select Jobber"); $("#alertok").focus(); $("#divLoading").hide();
            $("#alertok").on("click", function () {
                $("#dialogbox").dialog("close"); $("#SATABLE019COLUMN07").focus(); flag = true; return false;
            })
            flag = true; return false;
        }
    }
    var Status = $("#SATABLE019COLUMN12").val();
    if (flag == false) {
        if (Status == "" || Status == null || typeof (Status) == "undefined") {
            flag = true;
            Alert.render("Please Select Current Status"); $("#alertok").focus(); $("#divLoading").hide();
            $("#alertok").on("click", function () {
                $("#dialogbox").dialog("close"); $("#SATABLE019COLUMN12").focus(); flag = true; return false;
            })
            flag = true; return false;
        }
    }
    //var Submitted = $("#SATABLE019COLUMN13").val();
    //if (flag == false) {
    //    if (Submitted == "" || Submitted == null || typeof (Submitted) == "undefined") {
    //        flag = true;
    //        Alert.render("Please Select Submitted To"); $("#alertok").focus(); $("#divLoading").hide();
    //        $("#alertok").on("click", function () {
    //            $("#dialogbox").dialog("close"); $("#SATABLE019COLUMN13").focus(); flag = true; return false;
    //        })
    //        flag = true; return false;
    //    }
    //}
    var Opunit = $("#SATABLE019COLUMN19").val();
    if (flag == false) {
        if (Opunit == "" || Opunit == null || typeof (Opunit) == "undefined") {
            flag = true;
            Alert.render("Please Select Operating Unit"); $("#alertok").focus(); $("#divLoading").hide();
            $("#alertok").on("click", function () {
                $("#dialogbox").dialog("close"); $("#SATABLE019COLUMN19").focus(); flag = true; return false;
            })
            flag = true; return false;
        }
    }
    if (flag == false) {
        var tbl = $("#grdData")[0];
        var xmldata = "<ROOT>"; var Gridlength = tbl.rows.length;
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            if ($(trw).find("#checkRow").is(':checked')) {
                var upc = $(trw).find("[id='SATABLE020COLUMN03']").val();
                if (typeof upc == "undefined") upc = "";
                var RefCols = $(trw).find("[id='checkRow']").val();
                if (typeof RefCols == "undefined" || RefCols == "") RefCols = 0;
                var IIRefCols = $(trw).find("[id='RefCols']").val();
                if (typeof IIRefCols == "undefined" || IIRefCols == "") IIRefCols = 0;
                var Item = $(trw).find("[id='SATABLE020COLUMN04']").val();
                if (typeof Item == "undefined" || Item == "") Item = 0;
                if (Item == "0") {
                    flag = true;
                    Alert.render("Please Select Item"); $("#alertok").focus(); $("#divLoading").hide();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); $(trw).find("[id='SATABLE020COLUMN04']").focus(); flag = true; return false;
                    })
                    irow = Gridlength;
                    flag = true; return false;
                }
                var Desc = $(trw).find("[id='SATABLE020COLUMN05']").val();
                if (typeof Desc == "undefined") Desc = "";
                Desc = Desc.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;')
                    .replace(/'/g, '&apos;');
                var Units = $(trw).find("[id='SATABLE020COLUMN13']").val();
                if (typeof Units == "undefined" || Units == "") Units = 0;
                var Prints = $(trw).find("[id='SATABLE020COLUMN14']").val();
                if (typeof Prints == "undefined" || Prints == "") Prints = "";
                var EQty = $(trw).find("[id='SATABLE020COLUMN06']").val();
                if (typeof EQty == "undefined" || EQty == "") EQty = 0;
                var AQty = $(trw).find("[id='SATABLE020COLUMN07']").val();
                if (typeof AQty == "undefined" || AQty == "") AQty = 0;
                if (AQty == "0" && EQty != "0") {
                    flag = true;
                    Alert.render("Please Enter Actual Quantity"); $("#alertok").focus(); $("#divLoading").hide();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); $(trw).find("[id='SATABLE020COLUMN07']").focus(); flag = true; return false;
                    })
                    irow = Gridlength;
                    flag = true; return false;
                }
                var Remarks = $(trw).find("[id='SATABLE020COLUMN08']").val();
                if (typeof Remarks == "undefined") Remarks = "";
                Remarks = Remarks.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;')
                    .replace(/'/g, '&apos;');

                xmldata = xmldata + "<Column><UPC>" + upc + "</UPC>";
                xmldata = xmldata + "<RefCols>" + RefCols + "</RefCols>";
                xmldata = xmldata + "<IIRefCols>" + IIRefCols + "</IIRefCols>";
                xmldata = xmldata + "<Item>" + Item + "</Item>";
                xmldata = xmldata + "<Desc>" + Desc + "</Desc>";
                xmldata = xmldata + "<Units>" + Units + "</Units>";
                xmldata = xmldata + "<Prints>" + Prints + "</Prints>";
                xmldata = xmldata + "<EQty>" + EQty + "</EQty>";
                xmldata = xmldata + "<AQty>" + AQty + "</AQty>";
                xmldata = xmldata + "<Remarks>" + Remarks + "</Remarks></Column>";
            }
        }
        xmldata = xmldata + "</ROOT>";
        if (xmldata != "<ROOT></ROOT>" && flag != true) {
            var dataparam = { list: xmldata };
            $.ajax({
                url: "/Production/PRDSaveGrid",
                type: "POST",
                data: JSON.stringify(dataparam),
                dataType: "html",
                cache: true,
                async: false,
                contentType: "application/json; charset=utf-8",
                success: function (resultG) {
                    //document.forms[0].submit();                    
                    var formData = $("#saveformdata").serialize();
                    $.ajax({
                        url: "/Production/ProductionEdit",
                        type: "POST",
                        dataType: "json",
                        data: formData,
                        async: true,
                        success: function (data) {
                            debugger
                            var OutParam = data.OutParam;
                            var status = data.status;
                            if (data != "") {
                                $('#Productionid').attr("value",data.Productionid);
                                MessagesColour(data.MessageFrom, data.SuccessMessageFrom);
                                var id = $("#SATABLE019COLUMN12").val();
                                //$("#SATABLE019COLUMN13").find('option[value=' + id + ']').remove();
                                $('#SATABLE019COLUMN12').val(status);
                                $('#SATABLE019COLUMN13').val("");
                                $("#SATABLE019COLUMN24").val("");
                                $("#SATABLE019COLUMN22").val("");
                                $("#SATABLE019COLUMN23").val("");
                                var index = $('#tabs1 a[href="#' + status + '"]').parent().index();
                                var selectedli = $('#tabs1 ul li').eq(index);
                                $("#tabs1 ul li").removeClass("ui-tabs-selected ui-state-active");
                                selectedli.addClass("ui-tabs-selected ui-state-active");
                                var tbl = $("#grdData")[0];
                                for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                                    var trw = tbl.rows[irow];
                                    var item=$(trw).find("#SATABLE020COLUMN04").val();
                                    var actqty = $(trw).find("#SATABLE020COLUMN07").val();
                                    var RefCols = $(trw).find("#RefCols").val();
                                    var remqty = 0;
                                    for (var i = 0; i < data.ActqtyList.length; i++) {
                                        if (data.ActqtyList[i].Item == item && data.ActqtyList[i].IssueNo == RefCols) {
                                            remqty = data.ActqtyList[i].Qty;
                                            if (remqty == "" || typeof (remqty) == "undefined") remqty = 0;
                                            i = data.ActqtyList.length;
                                        }
                                    }
                                    actqty = parseFloat(actqty) + parseFloat(remqty);
                                    $(trw).find("#SATABLE020COLUMN06").val(actqty);
                                    $(trw).find("#SATABLE020COLUMN07").val(actqty);
                                    $(trw).find("#SATABLE020COLUMN08").val("");
                                    $(trw).find("#checkRow").val("");
                                    $('#grdData input[type=checkbox]:not(:checked)').closest('tr').remove();
                                }
                                $("#divLoading").hide();
                            }
                        }
                    })
                }
            });
        }
    }
}

function MessagesColour(MessageFrom, SuccessMessageFrom) {
    if (MessageFrom != '') {
        $("#messageDiv").text(MessageFrom);
        $('.headerEmtyCont').show();
        var successmsg = SuccessMessageFrom;
        if (successmsg == "Success") {
            document.getElementById('messageDiv').style.color = 'green';
            $('#confirm').show();
        }
        else if (successmsg == "fail") {
            document.getElementById('messageDiv').style.color = 'red';
            $('#fail').show();
        }
        else {
            document.getElementById('messageDiv').style.color = 'red';
            $('#fail').show();
        }
        InfoHeight(1);
        $("#messageDiv").stop(true); $("#messageDiv").slideUp(300); $("#messageDiv").fadeIn();
        $("#messageDivIcon").stop(true); $("#messageDivIcon").slideUp(300); $("#messageDivIcon").fadeIn();
        $.ajax({
            url: "/FormBuilding/SetSess",
            type: "POST",
            contentType: "application/json",
            success: function (data) { }
        });
        $('.headerEmtyCont').fadeOut(5000);
    }
    else {
        $('.headerEmtyCont').hide();
        $('#messageDiv').hide();
        $('#messageDivIcon').hide();
    }
}
//EMPHCS1837 rajasekhar reddy patakota 30/11/2016 Project wise tracking in Opportunity and job order screens
function GetCustomerWiseProject(eve, FormName, FormId) {
    var CustomerId = eve;
    var OPUnit = $("#SATABLE013COLUMN11").val();
    if (typeof ($("#SATABLE005COLUMN24").val()) != "undefined") OPUnit = $("#SATABLE005COLUMN24").val();
    else if (typeof ($("#SATABLE007COLUMN15").val()) != "undefined") OPUnit = $("#SATABLE007COLUMN15").val();
    else if (typeof ($("#PUTABLE003COLUMN13").val()) != "undefined") OPUnit = $("#PUTABLE003COLUMN13").val();
    else if (typeof ($("#SATABLE015COLUMN13").val()) != "undefined") OPUnit = $("#SATABLE015COLUMN13").val();
    else if (typeof ($("#SATABLE009COLUMN14").val()) != "undefined") OPUnit = $("#SATABLE009COLUMN14").val();
    if (CustomerId != "null" && typeof (OPUnit) != "undefined") {
        $.ajax({
            url: "/FormBuilding/GetCustomerWiseProject?FormName=" + FormName + "",
            type: "POST",
            async: false,
            contentType: "application/json",
            data: JSON.stringify({ CustomerId: CustomerId, OPUnit: OPUnit }),
            success: function (data) {
                if (data != "") {
                    var items = [];
                    items.push("<option value=''>" + "--Select--" + "</option>");
                    for (var i = 0; i < data.Data1.length; i++) {
                        items.push("<option value=" + data.Data1[i].Value + ">" + data.Data1[i].Text + "</option>");
                    }
                    if (typeof ($("#SATABLE013COLUMN19").val()) != "undefined") {
                        $("#SATABLE013COLUMN19").html(items.join(' '));
                    }
                    else if (typeof ($("#SATABLE005COLUMN35").val()) != "undefined") {
                        $("#SATABLE005COLUMN35").html(items.join(' '));
                    }
                    else if (typeof ($("#SATABLE007COLUMN22").val()) != "undefined") {
                        $("#SATABLE007COLUMN22").html(items.join(' '));
                    }
                    else if (typeof ($("#PUTABLE003COLUMN23").val()) != "undefined") {
                        $("#PUTABLE003COLUMN23").html(items.join(' '));
                    }
                    else if (typeof ($("#SATABLE015COLUMN26").val()) != "undefined") {
                        $("#SATABLE015COLUMN26").html(items.join(' '));
                    }
                    else if (typeof ($("#SATABLE009COLUMN29").val()) != "undefined") {
                        $("#SATABLE009COLUMN29").html(items.join(' '));
                    }
                }
            }
        });
    }
};
function OpportunitytoJobOrderOUT(eve, FormName) {
    if (eve != "null") {
        $.ajax({
            url: "/JobOrder/OpportunitytoJobOrderOUT?FormName=" + FormName + "",
            type: "JSON",
            contentType: "application/json",
            data: JSON.stringify({ Opportunity: eve }),
            success: function (data) {
                if (data != "") {
                    $("#SATABLE005COLUMN53").val(data.Data2);
                    $("#SATABLE005COLUMN24").val(data.Data3);
                    $("#SATABLE005COLUMN11").val(data.Data1);
                    $("#SATABLE005COLUMN09").val(data.Data4);
                    $("#SATABLE005COLUMN54").val(eve);
                    ////EMPHCS1837 rajasekhar reddy patakota 30/11/2016 Project wise tracking in Opportunity and job order screens
                    GetCustomerWiseProject(data.Data2, FormName, 1283);
                    $("#SATABLE005COLUMN35").val(data.Data14);
                    //$("#SATABLE003COLUMN07").val(data.Data8);
                    //$("#SATABLE003COLUMN16").val(data.Data9);
                    //$("#SATABLE003COLUMN11").val(data.Data10);
                    //$("#SATABLE003COLUMN10").val(data.Data11);
                    //$("#SATABLE003COLUMN12").val(data.Data12);
                    //$("#SATABLE003COLUMN08").val(data.Data13);
                    //$("#PUTABLE002COLUMN03").val(data.Data4);
                    var tbl = $("#grdData")[0];
                    $('#grdData').remove();
                    $('#itemgrid').html(data.grid);
                    $('#grdData').append(data.remain);
                    var tbl = $("#grdData")[0];
                    for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                        var trw = tbl.rows[irow];
                        SOLineQuantityTaxCalculation(trw);
                    }
                    dateupdate(); 
                }
            }
        });
    }
};
//EMPHCS1587 :Adding Advance Tab to Jobber Payment by gnaneshwar on 4/3/2016
function AdvanceChange(trw, chk, FID) {
    //EMPHCS1391	Single Header For Vendor and Customer Payment Srinivas 26/11/2015
    var rem = $(trw).find("#advAmt").text();
    var TranAdv = $(trw).find("#advID").text();
    var ref = '';

    if (FID == 1288) {
        var tbl = $("#grdData")[0];
        var TranAdvID = $(trw).find("#advID").text();
        $.ajax({
            url: "/JobberPayments/GetAdvID",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ tran: TranAdvID, FormId: FID }),
            success: function (data) {
                if (data.AdvID > 0) {
                    for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                        var trv = tbl.rows[irow];
                        var val = parseFloat($(trv).find("#SATABLE012COLUMN05").val());
                        if (val != 0) {
                            var vl = $(trv).find("#SATABLE012COLUMN09").val();
                            var crd = $(trv).find("#SATABLE012COLUMN14").val();
                            var vlDH = $(trv).find("#aSATABLE012COLUMN05").val();
                            var vlD = $(trv).find("#SATABLE012COLUMN05").val();
                            if (vl == '' || typeof (vl) == "undefined") { vl = 0; }
                            if (vlDH == '' || typeof (vlDH) == "undefined") { vlDH = 0; }
                            if (crd == '' || typeof (crd) == "undefined") { crd = 0; }
                            if (vlD == '' || typeof (vlD) == "undefined") { vlD = 0; }
                            var IAdjuItem = $(trw).find("#advAmt").text();
                            //var tax = $(trw).find("#advTax").text();
                            var AdID = $(trw).find("#advID").text();
                            if (chk) {

                                //EMPHCS852	Customer Payment - if i select advance without amount,due amount and advance amout become as NAN.it should be populate 0 if no value  BY Raj.Jr 05/08/2015
                                if (IAdjuItem == "" || IAdjuItem == 0) {
                                    IAdjuItem = 0;
                                }
                                if (val != vl) {debugger
                                    //EMPHCS1391	Single Header For Vendor and Customer Payment Srinivas 26/11/2015
                                    if ((parseFloat(vlDH) - parseFloat(crd) - parseFloat(vl)) <= (parseFloat(rem))) {
                                        rem = (parseFloat(rem)) + (parseFloat(vl) - (parseFloat(vlDH) - parseFloat(crd)));
                                        vl = (parseFloat(vlDH) - parseFloat(crd));
                                        vlDH = 0;
                                        vlD = 0;
                                        ref = 'OPEN';
                                        var c = $(trv).find("#advSATABLE012COLUMN15").val();
                                        if (c == "0" || c == "") {
                                            $(trv).find("#advSATABLE012COLUMN15").val(data.AdvID);
                                        }
                                        else {
                                            $(trv).find("#advSATABLE012COLUMN15").val(c + "," + data.AdvID);
                                        }
                                    }
                                    else {
                                        //EMPHCS1391	Single Header For Vendor and Customer Payment Srinivas 26/11/2015
                                        if ((parseFloat(vlDH) - parseFloat(crd) - parseFloat(vl)) != 0) {
                                            vl = parseFloat(vl) + parseFloat(rem);
                                            vlDH = (parseFloat(vlDH) - parseFloat(crd)) - parseFloat(vl);
                                            vlD = parseFloat(vlD) - parseFloat(vl);
                                            rem = 0;
                                            ref = 'CLOSE';
                                            var c = $(trv).find("#advSATABLE012COLUMN15").val();
                                            if (vl > 0) {
                                                if (c == "0" || c == "") {
                                                    $(trv).find("#advSATABLE012COLUMN15").val(data.AdvID);
                                                }
                                                else {
                                                    $(trv).find("#advSATABLE012COLUMN15").val(c + "," + data.AdvID);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else {
                                var myString = $(trv).find("#advSATABLE012COLUMN15").val();
                                if (myString.search(data.AdvID) == -1) {
                                }
                                else {
                                    if (parseFloat(vlD) == 0) {
                                        vlDH = vl;
                                        vlD = vl;
                                        vl = 0;
                                        //EMPHCS1391	Single Header For Vendor and Customer Payment Srinivas 26/11/2015
                                        //rem = (parseFloat(rem));
                                        ref = 'OPEN';

                                    }
                                    else {
                                        //EMPHCS1391	Single Header For Vendor and Customer Payment Srinivas 26/11/2015
                                        if (parseFloat(rem) <= (parseFloat(vl))) {
                                            vl = (parseFloat(vl) - parseFloat(rem));
                                            rem = 0;
                                            vlDH = (parseFloat(vlDH) - parseFloat(crd)) + parseFloat(rem);
                                            vlD = parseFloat(vlD) + parseFloat(rem);
                                            ref = 'OPEN';
                                        }
                                        else {
                                            var rm = rem;
                                            if (rm == '' || typeof (rm) == "undefined") { rm = 0; }
                                            rem = (parseFloat(rem) - parseFloat(vl));
                                            vl = (0);
                                            vlDH = (parseFloat(vlDH) - parseFloat(crd)) + parseFloat(rem);
                                            vlD = parseFloat(vlD) + parseFloat(rem);
                                            //rem = (parseFloat(rem));
                                            ref = 'OPEN';
                                        }
                                    }
                                    var myString = $(trv).find("#advSATABLE012COLUMN15").val();
                                    if (myString == data.AdvID) {
                                        $(trv).find("#advSATABLE012COLUMN15").val("");
                                    }
                                    else {
                                        var avoid = "," + data.AdvID;
                                        var c = myString.replace(avoid, '');
                                        $(trv).find("#advSATABLE012COLUMN15").val(c);
                                    }
                                }
                            }
                            $(trv).find("#SATABLE012COLUMN09").val(vl);
                            if (irow == 1) $("#SATABLE011COLUMN13").val(0);
                            PaymentDefaultAmountcal(trv);
                            var paymentval = $(trv).find("#SATABLE012COLUMN06").val();
                            var payment = $("#SATABLE011COLUMN13").val();
                            if (payment == '' || typeof (payment) == "undefined") { payment = 0; }
                            if (paymentval == '' || typeof (paymentval) == "undefined") { paymentval = 0; }
                            paymentval = (parseFloat(paymentval) + parseFloat(payment)).toFixed(2);
                            $("#SATABLE011COLUMN13").val(paymentval);
                            //EMPHCS1391	Single Header For Vendor and Customer Payment Srinivas 26/11/2015
                            if (rem == 0)
                                break
                        }
                    }
                }
            }
        });
    }
}

function PaymentDefaultAmountcal(trw) {
    var payment = $(trw).find("#SATABLE012COLUMN06").val();
    var Dueamt = $(trw).find("#SATABLE012COLUMN05").val();
    var Advamt = $(trw).find("#SATABLE012COLUMN09").val();
    if (payment == '' || typeof (payment) == "undefined") { payment = 0; }
    if (Dueamt == '' || typeof (Dueamt) == "undefined") { Dueamt = 0; }
    if (Advamt == '' || typeof (Advamt) == "undefined") { Advamt = 0; }
    payment = (parseFloat(Dueamt) - parseFloat(Advamt)).toFixed(2);
    if (payment <= 0 || payment == "0" || payment == "0.00") payment = "";
    $(trw).find("#SATABLE012COLUMN06").val(payment);
}


function ddlSelectPayBillCustomer(eve, FormName) {
    var SalesOrderID = eve;
    if (eve != "null") {
        $.ajax({
            url: "/JobberPayments/GetJOBillJObberDetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ JOBillID: SalesOrderID }),
            success: function (data) {
                if (data != "") {
                    $("#SATABLE011COLUMN07").val(data.Data);
                    $("#SATABLE011COLUMN05").val(eve);
                    $("#SATABLE011COLUMN09").val(data.Data2);
                    $("#SATABLE011COLUMN10").val(data.Data3);
                    $("#SATABLE011COLUMN12").val(data.Data5);
                    $("#SATABLE011COLUMN11").val(data.Data4);
                    //EMPHCS1391	Single Header For Vendor and Customer Payment Srinivas 26/11/2015
                    //$("#SATABLE011COLUMN13").val(data.Data10);
                    //EMPHCS1125	if we select vendor/customer/jobber dont display operating unit automatically BY RAJ.Jr 12/9/2015
                    // $("#SATABLE011COLUMN14").val(data.Data6);
                    $("#SATABLE011COLUMN15").val(data.Data7);
                    $("#SATABLE011COLUMN16").val(data.Data8);
                    $("#SATABLE011COLUMN17").val(data.Data9);
                    //$("#SATABLE011COLUMN18").val(data.Data11);
                    $("#SATABLE011COLUMN20").val(data.Data12);
                    $('#SATABLE011COLUMN06').find('option').remove();
                    var items = [];
                    items.push("<option value=''>" + "--Select--" + "</option>");
                    for (var i = 0; i < data.Data13.length; i++) {
                        items.push("<option value=" + data.Data13[i].Value + ">" + data.Data13[i].Text + "</option>");
                    }
                    $("#SATABLE011COLUMN06").html(items.join(' '));
                    $("#SATABLE011COLUMN06").val(data.Data14);
                    $('#grdData').remove();
                    $('#grdfData').remove();
                    $('#itemgrid').html(data.grid);
                    $('.gridLevelFB').html(data.grid1);
                    $('#grdData').append(data.remain);
                    $('#grdfData').append(data.grid1);
                    var totpayment = 0; var payment = 0;
                    var tbl = $("#grdData")[0];
                    for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                        var trw = tbl.rows[irow];
                        PaymentDefaultAmountcal(trw);
                        payment = $(trw).find("#SATABLE012COLUMN06").val()
                        if (payment == '' || typeof (payment) == "undefined") { payment = 0; }
                        totpayment = (parseFloat(totpayment) + parseFloat(payment)).toFixed(2);
                        $(trw).find("#SATABLE012COLUMN03").prop("disabled", true);
                        $(trw).find("#SATABLE012COLUMN04").prop("readonly", true);
                        $(trw).find("#SATABLE012COLUMN05").prop("readonly", true);
                        $(trw).find("#SATABLE012COLUMN09").prop("readonly", true);
                    }
                    $("#SATABLE011COLUMN13").val(totpayment);
                    $('#grdData thead tr th:first').html(
           $('<input/>', {
               type: 'checkbox',
               click: function () {
                   var checkboxes = $(this).closest('table').find('tbody tr td input[type="checkbox"]');
                   checkboxes.prop('checked', $(this).is(':checked')); PaymentCalculation();
               }
           })
       );
                    dateupdate();
                    EnableDisable();
                }
            }
        });
    }
};




//jquery Load
function JavascriptFunction(FormName) {
    var url = "/Home/PostMethod?FormName=" + FormName + "";
    $("#divLoading").show();
    $.post(url, null,
            function (data) {
                $("#PID")[0].innerHTML = data;
                $("#divLoading").hide();
            });
}
//EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
function DynamicPopupcancel()
{
    $.ajax({
        url: "/FormBuilding/SetVariable?FormName=" + FormName + "",
        type: "POST",
        contentType: "application/json",
        //data: JSON.stringify({ FormName: FormName, FormId: FormId, opunit: opunit }),
        success: function (data) {
            $('#newMDialog').dialog('close');
            $('#newMDialog').hide();
            $('.ui-dialog').hide();
            $('.ui-dialog-titlebar').hide();
            $('.ui-widget-overlay').hide();
            $('#mImageHide').attr('pattern', 'N');
        }
    });
}
//Dynamic Form Filling
function Dynamicforms(FormName, ide, Ref, FormId) {
    if (FormName == "Item%20Receipt" || FormName == "Item Receipt" || FormName == "Receipt") {
        ddlSelect(ide, FormName);
        EnableDisable(FormId);
    }
	//EMPHCS1823 rajasekhar reddy patakota 10/06/2016 Email template Setup for Transactions
    else if (FormName == "Job%20Order(Out)" || FormName == "Job Order(Out)" || FormName == "Job Order") {
        if (Ref == "JobberConvert")
            OpportunitytoJobOrderOUT(ide, FormName);
        EnableDisable(FormId);
    }
    //EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
    else if (FormName == "Production") {
        if (ide != ""&& ide !=null) {
            GetProductionDetails(ide, FormName);
        }
    }
    else if (FormName == "Job%20Order(In)" || FormName == "Job Order(In)") {
        GRNtoJobOrderIN(ide, FormName);
        EnableDisable(FormId);
    }
    //EMPHCS1784	Job Order for IN Process 15/7/2016
    else if (FormName == "Job%20Order%20Receipt%20IN" || FormName == "Job Order Receipt IN" || FormName == "Receipt") {
        ddlSelect(ide, FormName);
        EnableDisable(FormId);
    }
    else if (FormName == "Job%20Order%20Issue%20IN" || FormName == "Job Order Issue IN" || FormName == "Issue(Out)" || FormName == "Job%20Order%20Issue" || FormName == "Job Order Issue") {
        ddlSelectII(ide, FormName, FormId);
        //EnableDisable(FormId);
    }
    //EMPHCS1784	Job Order for IN Process 15/7/2016
    else if (FormName == "Receipt%20(In)" || FormName == "Receipt(In)" || FormName == "Receipt") {
        ddlSelect(ide, FormName);
        EnableDisable(FormId);
    }
    else if (FormName == "Issue%20(In)" || FormName == "Issue(In)") {
        debugger
        ddlSelectII(ide, FormName, FormId);
        //EnableDisable(FormId);
    }
    else if (FormName == "Quotation%20Comparision" || FormName == "Quotation Comparision") {
        ddlSelectPQDetails(ide, FormName);
        if (ide != null && ide != "") {
            ddlSelectPQItem(null, null, ide, FormName, null);
        } else {
            ddlSelectPQItem(null, 1, ide, FormName, null);
        }
        EnableDisable(FormId);
    }
    else if (FormName == "Sales%20Quotation" || FormName == "Sales Quotation") {
        ddlSelectSQ(ide, FormName, FormId)
        EnableDisable(FormId);
    }
    else if (FormName == "Sales%20Return%20Order" || FormName == "Sales Return Order") {
        if (typeof Ref != "undefined" && Ref != "" && Ref != null) { ddlSelectISAR(ide, FormName, FormId); } else { ddlSelectSAR(ide, FormName, FormId); }
        EnableDisable(FormId);
    }
    else if (FormName == "Purchase%20Return%20Order" || FormName == "Purchase Return Order") {
        debugger
        if (typeof Ref != "undefined" && Ref != "" && Ref != null) { ddlSelectBPOR(ide, FormName); } else { ddlSelectPOR(ide, FormName); }
        EnableDisable(FormId);
    }
    else if (FormName == "Purchase%20Order" || FormName == "Purchase Order") {
        if (typeof Ref != "undefined" && Ref != "" && Ref == "POQuotation" && Ref != null) {
            var QuotationItems = $.session.get("checkedQnos");
            ddlSelectPoQuotation(QuotationItems, FormName, FormId, Ref);
        }
        EnableDisable(FormId);
    }
    else if (FormName == "Sales%20Order" || FormName == "Sales Order") {

        if (typeof Ref != "undefined" && Ref != "" && Ref == "SalesConvert" && Ref != null) {
            getSalesQutationid(FormName);
        }
        EnableDisable(FormId);
    }
    else if (FormName == "Credit%20Memo" || FormName == "Credit Memo") {
        ddlSelectSCM(ide, FormName);
        EnableDisable(FormId);
    }
    else if (FormName == "Refund") {
        ddlSelectPRef(ide, FormName, FormId);
        EnableDisable(FormId);
    }
    else if (FormName == "Job%20Order%20Receipt" || FormName == "Receipt(Out)") {
    	//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
        if (Ref == "Production")
            ddlSelectproductionToJOR(ide, FormName);
        else
            ddlSelectJOR(ide, FormName);
        //EnableDisable(FormId);
    }
    else if (FormName == "Bill") {
        if (typeof Ref != "undefined" && Ref != "" && Ref == "BillQuotation" && Ref != null) {
            var QuotationItems = $.session.get("checkedQnos");
            ddlSelectBillQuotation(QuotationItems, FormName, Ref);
        } else {
            ddlSelect1(ide, FormName);
        }
        EnableDisable(FormId);
    }
    else if (FormName == "Opening%20Balance%20Entry" || FormName == "Opening Balance Entry") {
        ddlSelectOB(FormName);
    }
    else if (FormName == "Payment") {
        ddlSelect2(ide, FormName);
        EnableDisable(FormId);

    }
    else if (FormName == "Item%20Issue" || FormName == "Item Issue" || FormName == "Return Issue" || FormName == "Return%20Issue") {
        ddlSelectII(ide, FormName, FormId);
        //EnableDisable(FormId);
    }
    else if (FormName == "Issue(Out)" || FormName == "Issue(Out)") {
        debugger
        ddlSelectII(ide, FormName, FormId);
        //EnableDisable(FormId);
    }
    else if (FormName == "Resourse%20Consumption" || FormName == "Resourse Consumption") {
        ddlSelectWO(ide, FormName, FormId);
        //EnableDisable(FormId);
    }
    else if (FormName == "Invoice") {
        debugger
        if (typeof Ref != "undefined" && Ref != "" && Ref == "InvoiceConvert" && Ref != null) {
            getInvoiceQutationid(FormName);
        }
        else {
            ddlSelectIV(ide, FormName, FormId);
        }
    }
    else if (FormName == "Bill%20to%20Jobber" || FormName == "Bill to Jobber") {
        debugger

        ddlSelectJOB(ide, FormName);
        EnableDisable(FormId);
    }
    else if (FormName == "Customer%20Payments" || FormName == "Customer Payments") {
        ddlSelectPM(ide, "0", FormName);
        EnableDisable(FormId);
    }
    else if (FormName == "Jobber%20Payments" || FormName == "Jobber Payments") {
        ddlSelectJOPM(ide, "0", FormName);
        EnableDisable(FormId);
    }
    else if (FormName == "Project%20Work%20Order" || FormName == "Project Work Order") {

        if (typeof Ref != "undefined" && Ref != "" && Ref == "MileStone" && Ref != null) {
            ddlSelectProjectWorkOrder(ide, FormName, Ref);
        } else {
            ddlSelectProjectWorkOrder(ide, FormName, null);
        }
        EnableDisable(FormId);
    }
}
//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
//Jquery Date
function dateupdate(seletedDate) {
    if (seletedDate != "") {
        $('.date').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: seletedDate,
            showOn: "button",
            buttonImage: "/images/calendar.png",
            buttonImageOnly: true, readonly: true, showOn: "both"
        });
    }
    else {
        $('.date').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'dd/mm/yy',
            showOn: "button",
            buttonImage: "/images/calendar.png",
            buttonImageOnly: true, readonly: true, showOn: "both"
        });
    }
    //$('.date').datepicker('setDate', new Date());
    $('.ui-datepicker-trigger').css("width", "16px");
    $('.ui-datepicker-trigger').css("width", "16px");
    $('.ui-datepicker-trigger').css("background-color", "#97c6ed");
};

function EnableDisable(sessionformid, sessionOPID) {
    //EMPHCS761 - GNANESHWAR 16/7/2015 Read only Propert appying to fields in all form with out user entaring data
    if (sessionformid == 1283) {
    $("#SATABLE005COLUMN07").prop( "checked", true );$("#SATABLE005COLUMN16").val( "Approved");
    $("#SATABLE005COLUMN29").val(1000 );
    $("#SATABLE005COLUMN31").val(1000);
        //EMPHCS1106 by gnaneshwar on 7/9/2015 dispaying default operating unit in all forms
    if (sessionOPID != "" && sessionOPID != null) {
        $("#SATABLE005COLUMN24").val(sessionOPID);
    }
    $("#SATABLE005COLUMN12").prop("readonly", true);
    $("#SATABLE005COLUMN32").prop("readonly", true);
    $("#SATABLE005COLUMN15").prop("readonly", true);
    $("#PUTABLE001COLUMN12").prop("readonly",true);
    $("#PUTABLE001COLUMN14").prop("readonly",true);
    $("#PUTABLE001COLUMN15").prop("readonly",true);
    var tbl = $("#grdData")[0];
    for (var irow = 1; irow < tbl.rows.length  ; irow++) {
        var trw = tbl.rows[irow];
        $(trw).find("#PUTABLE002COLUMN04").prop("readonly",true);
        $(trw).find("#PUTABLE002COLUMN05").prop("readonly",true);
        $(trw).find("#PUTABLE002COLUMN06").prop("readonly",true);
        $(trw).find("#PUTABLE002COLUMN11").prop("readonly",true);
        $(trw).find("#PUTABLE002COLUMN12").prop("readonly",true);
        $(trw).find("#PUTABLE002COLUMN13").prop("readonly",true);
        //$(trw).find("#aSATABLE006COLUMN25").val((parseFloat($(trw).find("#SATABLE006COLUMN11").val()) - parseFloat($(trw).find("#SATABLE006COLUMN25").val())).toFixed(2));
    }
    }
    //EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
    else if (sessionformid == 1626) {
        if (sessionOPID != "" && sessionOPID != null) {
            $("#SATABLE019COLUMN19").val(sessionOPID);
        }
        $("#SATABLE019COLUMN12").prop("readonly", true);
    }
    //EMPHCS1784	Job Order for IN Process 15/7/2016
    else if (sessionformid == 1586) {debugger
        $("#PUTABLE001COLUMN07").prop("checked", true);
        $("#PUTABLE001COLUMN16").val("Approved");
        if ($("#PUTABLE001COLUMN31").val() == "") {
            $("#PUTABLE001COLUMN31").val(1000);
        }
        $("#PUTABLE001COLUMN29").val(1000);
        if (sessionOPID != "" && sessionOPID != null) {
            $("#PUTABLE001COLUMN24").val(sessionOPID);
        }
        if (Location != "" && Location != null && sessionOPID != null && sessionOPID != "") {
            $("#PUTABLE001COLUMN47").val(Location);
        }
       
        $("#PUTABLE001COLUMN12").prop("readonly", true);
        $("#PUTABLE001COLUMN14").prop("readonly", true);
        $("#PUTABLE001COLUMN15").prop("readonly", true);
        $("#PUTABLE001COLUMN32").prop("readonly", true);

        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            if (tbl.rows.length == 2) {
                $(trw).find("#PUTABLE002COLUMN25").val(1000);
            }
            else if ($(trw).find("#PUTABLE002COLUMN25").val() == 1000) {
                $(trw).find("#PUTABLE002COLUMN25").val(1000);
            }
            $(trw).find("#PUTABLE002COLUMN04").prop("readonly", true);
            $(trw).find("#PUTABLE002COLUMN06").prop("readonly", true);
            $(trw).find("#PUTABLE002COLUMN11").prop("readonly", true);
            $(trw).find("#PUTABLE002COLUMN12").prop("readonly", true);
            $(trw).find("#PUTABLE002COLUMN13").prop("readonly", true);
        }
    }
    else if (sessionformid == 1588) {debugger
        $("#SATABLE007COLUMN20").val(1000);
        if (sessionOPID != "" && sessionOPID != null) {
            $("#SATABLE007COLUMN15").val(sessionOPID);
            //$("#SATABLE007COLUMN13").prop("readonly", true);
        }
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#SATABLE008COLUMN07").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN08").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN10").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN11").prop("readonly", true);
        }
    }
    //EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
    else if (sessionformid == 1605) {
        debugger
        if (sessionOPID != "" && sessionOPID != null) {
            $("#PUTABLE022COLUMN09").val(sessionOPID);
        }
    }
    else if (sessionformid == 1284) {

        $("#SATABLE007COLUMN20").val(1000);
        //EMPHCS1106 by gnaneshwar on 7/9/2015 dispaying default operating unit in all forms
        if (sessionOPID != "" && sessionOPID != null) {
            $("#SATABLE007COLUMN15").val(sessionOPID);
        }
        $("#SATABLE007COLUMN07").prop("readonly", true);
        $("#SATABLE007COLUMN09").prop("readonly", true);
        $("#SATABLE007COLUMN12").prop("readonly", true);
        //$("#SATABLE007COLUMN13").prop("readonly", true);
        $("#SATABLE007COLUMN14").prop("readonly", true);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#SATABLE008COLUMN04").prop("disabled", true);
            $(trw).find("#SATABLE008COLUMN05").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN06").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN07").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN08").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN11").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN10").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN12").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN13").prop("readonly", true);
        }
    }

    else if (sessionformid == 1588) {
        //$("#SATABLE007COLUMN13").prop("readonly", true);
    }

    else if (sessionformid == 1286) {
        $("#PUTABLE003COLUMN17").val(1000);
        //EMPHCS1106 by gnaneshwar on 7/9/2015 dispaying default operating unit in all forms
        if (sessionOPID != "" && sessionOPID != null) {
            $("#PUTABLE003COLUMN13").val(sessionOPID);
        }
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            //EMPHCS995  gnaneshwar 18/8/2015  uom condition checking in Sales Return
            //$(trw).find("#PUTABLE004COLUMN03").prop("disabled", true);
            $(trw).find("#PUTABLE004COLUMN09").prop("readonly", true);
            $(trw).find("#PUTABLE004COLUMN04").prop("readonly", true);

            $(trw).find("#PUTABLE004COLUMN06").prop("readonly", true);
            $(trw).find("#PUTABLE004COLUMN07").prop("readonly", true);
           // $(trw).find("#PUTABLE004COLUMN10").prop("readonly", true);
            $(trw).find("#PUTABLE004COLUMN11").prop("readonly", true);
        }
    }

    else if(sessionformid==1287){
        $("#SATABLE009COLUMN19").val(1000 );
        $("#SATABLE009COLUMN23").val(1000);
        //EMPHCS1106 by gnaneshwar on 7/9/2015 dispaying default operating unit in all forms
        if (sessionOPID != "" && sessionOPID != null) {
            $("#SATABLE009COLUMN14").val(sessionOPID);
        }
        $("#SATABLE009COLUMN07").prop("readonly",true);                         
        $("#SATABLE009COLUMN09").prop("readonly",true);
        $("#SATABLE009COLUMN11").prop("readonly",true);
        $("#SATABLE009COLUMN12").prop("readonly",true);
        $("#SATABLE009COLUMN13").prop("readonly",true);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#SATABLE010COLUMN04").prop("disabled",true);
            $(trw).find("#SATABLE010COLUMN06").prop("readonly",true);
            $(trw).find("#SATABLE010COLUMN09").prop("readonly",true);
            $(trw).find("#SATABLE010COLUMN11").prop("readonly",true);
            $(trw).find("#SATABLE010COLUMN14").prop("readonly",true);
            $(trw).find("#SATABLE010COLUMN12").prop("readonly",true);
        }
    }
    else if (sessionformid == 1288) {

        $("#SATABLE011COLUMN18").val(1000);
        $("#SATABLE011COLUMN13").prop("readonly", true);
        //EMPHCS1106 by gnaneshwar on 7/9/2015 dispaying default operating unit in all forms
        if (sessionOPID != "" && sessionOPID != null) {
            $("#SATABLE011COLUMN14").val(sessionOPID);
        }
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#SATABLE012COLUMN03").prop("disabled", true);
            $(trw).find("#SATABLE012COLUMN04").prop("readonly", true);
            $(trw).find("#SATABLE012COLUMN05").prop("readonly", true);
            $(trw).find("#SATABLE012COLUMN09").prop("readonly", true);
        }
        //EMPHCS1021 rajasekhar reddy patakota 22/8/2015 Account Field value displaying in edit
        //$("#SATABLE011COLUMN08").val(4000 );
    }
else
{
 }


};
//EMPHCS901,EMPHCS902 LOGS CREATION IN TAX CALCULATION FOR PO AND SO done by srinivas 11/8/2015
var logValue = '\r\n';
//tax cals
function SOLineTax(tr) {
    var qty = 0; var price = 0; var discount = 0;
    qty = $(tr).find("[id='SATABLE006COLUMN07']").val();
    price = $(tr).find("[id='SATABLE006COLUMN09']").val();
    discount = $(tr).find("[id='SATABLE006COLUMN10']").val();
    if (qty == "") { qty = 0; }
    if (price == "") { price = 0; }
    if (discount == "" || typeof (discount) == "undefined") { discount = "0"; }
    $(tr).find("#SATABLE006COLUMN25").val((parseFloat(qty) * parseFloat(price)).toFixed(2));
    var linetaxtype = $(tr).find("#SATABLE006COLUMN26").val();
    var TaxCal = $(tr).find("#SATABLE006COLUMN25").val();
    $.ajax({
        url: "/Common/GetTaxAmount",
        type: "POST",
        //EMPHCS729 Rajasekhar Patakota 21/7/2015 Tax Amount Calculation In Transaction Forms
        data: JSON.stringify({ TaxType: linetaxtype, TaxCal: TaxCal, FormName: $("#FormName").val() }),
        dataType: "json", cache: true,
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            if (result.Tax != '') {
                tax = result.Tax; tax = (parseFloat(tax) * parseFloat(0.01));
                var tt1 = result.TaxCal
                tax = (parseFloat(tt1) * parseFloat(tax)).toFixed(2);
                //EMPHCS985 Implement logs for SO and invoice done by rajasekhar 18/8/2015
                logValue += " Line Tax Amount = Total (" + tt1 + ")* Tax (" + tax + ") \r\n";
            } else { tax = 0; }
            $(tr).find("[id='aSATABLE006COLUMN25']").val(tax);
            SOTaxLevelCalculations();
        }
    })
}

//Customer Address Details
function ddlSelectCustomerAddres1(eve, FormName) {
    var CustomerID = eve;
    if (eve != "null") {
        $.ajax({
            url: "/FormBuilding/GetVendorAddress?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ CustomerID: CustomerID }),
            success: function (data) {
                if (data != "") {
                    $("#SATABLE009COLUMN07").val(data.cAddress);
                } else { $("#SATABLE009COLUMN07").val(""); }
            }
        });
    }
};
//Customer Address
function ddlSelectCustomerAdd(eve, FormName) {
    var CustomerId = eve;
    if (eve != "null") {
        $.ajax({
            url: "/FormBuilding/GetVendorAddres?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ CustomerId: CustomerId }),
            success: function (data) {
                if (data != "") {
                    $("#SATABLE009COLUMN05").val(data.cId);
                    $("#SATABLE009COLUMN14").val(data.Data9);
                    $("#SATABLE009COLUMN15").val(data.Data10);
                    $("#SATABLE009COLUMN16").val(data.Data11);
                    $("#SATABLE009COLUMN17").val(data.Data12);
                    $("#SATABLE009COLUMN21").val(data.Data20);
                    $("#SATABLE009COLUMN11").val(data.Data19);
                    $("#SATABLE009COLUMN14").val(data.Data21);
                }
            }
        });
    }
};
// vendor  adress
function ddlSelectVendor(eve, FormName, FormId) {
    debugger
    var vendorId = eve;
    if (eve != "null") {
        $.ajax({
            url: "/Common/GetVendorAddres?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ vendorId: vendorId }),
            success: function (data) {

                if (data != "") {
                     var items = [];
                        items.push("<option value=''>" + "--Select--" + "</option>");
                        for (var i = 0; i < data.item.length; i++) {
                            items.push("<option value=" + data.item[i].Value + ">" + data.item[i].Text + "</option>");
                        }
                        $("#SATABLE003COLUMN11").html(items.join(' '));
                        $("#SATABLE005COLUMN05").val(data.vId);
                        $("#SATABLE003COLUMN06").val(data.Data1);
                        $("#SATABLE003COLUMN07").val(data.Data2);
                        $("#SATABLE003COLUMN08").val(data.Data3);
                        $("#SATABLE003COLUMN09").val(data.Data4);
                        debugger;
                        $("#SATABLE003COLUMN10").val(data.Data5);
                        $("#SATABLE003COLUMN11").val(data.Data6);
                        $("#SATABLE003COLUMN12").val(data.Data7);
                        $("#SATABLE003COLUMN16").val(data.Data8);
                        if(data.Data9!="")
                        $("#SATABLE005COLUMN24").val(data.Data9);
                        $("#SATABLE005COLUMN25").val(data.Data10);
                        $("#SATABLE005COLUMN26").val(data.Data11);
                        $("#SATABLE005COLUMN27").val(data.Data12);

                        $("#SATABLE005COLUMN21").val(data.Data13);
                        $("#SATABLE005COLUMN22").val(data.Data14);
                        $("#SATABLE005COLUMN23").val(data.Data15);
                        
                        $("#SATABLE005COLUMN10").val(data.Data19);
                        $("#SATABLE005COLUMN30").val(data.Data20);
                        //$("#SATABLE005COLUMN24").val(data.Data21);
                        if (data.Data19 != "") {
                            PAYMENTTERMSCHANGE(data.Data19, FormId)
                        } else
                            $("#SATABLE005COLUMN08").val($("#SATABLE005COLUMN06").val());


                        if (data.data17 == "True") {
                            $("#SATABLE003COLUMN17").prop("checked", true);
                        }
                        else { $("#SATABLE003COLUMN17").prop("checked", false); }

                        if (data.data18 == "True") {
                            $("#SATABLE003COLUMN18").prop("checked", true);
                        }
                        else { $("#SATABLE003COLUMN18").prop("checked", false); }
                        EnableDisable(FormId);


                    
                }
            }
        });
    }
};

function ddlChange1(trw, Formid, FormName) {
    var JoItemID = $(trw).find("#PUTABLE004COLUMN03 :selected").val();
    var JO = $("#PUTABLE003COLUMN06 :selected").val();
 if (typeof JoItemID != "undefined") {
        debugger;
        $.ajax({
            url: "/JobOrderReceipt/JOGetItemDetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ ItemID: JoItemID,JO:JO }),
            success: function (data) {
                if (data != "") {
                    debugger

                    $(trw).find("#PUTABLE004COLUMN03").val(data.Data);
                    $(trw).find("#UPCSearch").val(data.Data1);
                    $(trw).find("#MATABLE007COLUMN04").val(data.Data2);
                    $(trw).find("#MATABLE007COLUMN42").val(data.Data3);
                    $(trw).find("#MATABLE007COLUMN43").val(data.Data4);
                    $(trw).find("#MATABLE007COLUMN44").val(data.Data5);
                    $(trw).find("#MATABLE007COLUMN10").val(data.Data6);
                    $(trw).find("#PUTABLE004COLUMN10").val(data.Data7);
		    //EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                    $(trw).find("#PUTABLE004COLUMN10").attr("readonly",false);
                    $(trw).find("#PUTABLE004COLUMN23").val(data.Data8);
                    if ($(trw).find("#PUTABLE004COLUMN17").val() == "")
                        $(trw).find("#PUTABLE004COLUMN17").val(data.Data9);
                    //$(trw).find("#PUTABLE004COLUMN06").val(data.Data10);
                
                    //$(trw).find("#UPCSearch").val("");
                }
                else {
                    Alert.render('There Is No Data For Selected Item'); $("#alertok").focus();
                    $(trw).find("#PUTABLE004COLUMN03").val('');
                    $(trw).find("#UPCSearch").val('');
                    $(trw).find("#MATABLE007COLUMN04").val('');
                    $(trw).find("#MATABLE007COLUMN42").val('');
                    $(trw).find("#MATABLE007COLUMN43").val('');
                    $(trw).find("#MATABLE007COLUMN44").val('');
                    $(trw).find("#MATABLE007COLUMN10").val('');
                    $(trw).find("#PUTABLE004COLUMN10").val('');
                    $(trw).find("#PUTABLE004COLUMN23").val('');
                    $(trw).find("#PUTABLE004COLUMN17").val('');
                    $(trw).find("#PUTABLE004COLUMN06").val('');
                }
            }
        });
    }
}

//Sales Order Line Level Item Details
function ddlChange(trw, Formid, FormName) {
    debugger
    //var trw = $(tr).parent().parent();
    var IAdjuItem = $(trw).find("#FITABLE015COLUMN03 :selected").val();
    var ItemID = $(trw).find("#PUTABLE002COLUMN03 :selected").val();
    var JoItemID = $(trw).find("#PUTABLE004COLUMN03 :selected").val();
    var bItemID = $(trw).find("#PUTABLE006COLUMN04 :selected").val();
    var SOItemID = $(trw).find("#SATABLE006COLUMN03 :selected").val();
    var iSOItemID = $(trw).find("#SATABLE010COLUMN05 :selected").val();
    var OPItemID = $(trw).find("#SATABLE014COLUMN03 :selected").val();
    var SQItemID = $(trw).find("#SATABLE016COLUMN03 :selected").val();
    var Brand = $(trw).find("#MATABLE007COLUMN10 :selected").val();
    var PWOItemID = $(trw).find("#PRTABLE006COLUMN03 :selected").val();
    var RCWOItemID = $(trw).find("#PRTABLE008COLUMN03 :selected").val();
    var GRNItemID = $(trw).find("#PUTABLE023COLUMN03 :selected").val();

    if (typeof Brand != "undefined" && Formid != 1286) {
        debugger
        if (Formid == 1285) {
            var color = ""; var style = ""; var size = ""; var brand = "";
            if ($(trw).find("#MATABLE007COLUMN42").val() != "")
            { color = $(trw).find("#MATABLE007COLUMN42 option:selected").text(); }
            else
            {
                Alert.render('Please Select Color'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $(trw).find("#MATABLE007COLUMN42").focus();
                })
                $(trw).find("#MATABLE007COLUMN42").focus(); return false;
            }
            if ($(trw).find("#MATABLE007COLUMN43").val() != "")
            { style = $(trw).find("#MATABLE007COLUMN43 option:selected").text(); }
            else
            {
                Alert.render('Please Select Style'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $(trw).find("#MATABLE007COLUMN43").focus();
                })
                $(trw).find("#MATABLE007COLUMN43").focus(); return false;
            }
            if ($(trw).find("#MATABLE007COLUMN44").val() != "")
            { size = $(trw).find("#MATABLE007COLUMN44 option:selected").text(); }
            else
            {
                Alert.render('Please Select Size'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $(trw).find("#MATABLE007COLUMN44").focus();
                })
                $(trw).find("#MATABLE007COLUMN44").focus(); return false;
            }
            if ($(trw).find("#MATABLE007COLUMN10").val() != "")
                brand = $(trw).find("#MATABLE007COLUMN10 option:selected").text();

            $.ajax({
                url: "/Common/getMatrixDetails?FormName=" + FormName + "",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({ style: $(trw).find("#MATABLE007COLUMN43").val(), brand: $(trw).find("#MATABLE007COLUMN10").val(), color: $(trw).find("#MATABLE007COLUMN42").val(), size: $(trw).find("#MATABLE007COLUMN44").val() }),
                success: function (data) {
                    if (data.item != "") {
                        $(trw).find("#MATABLE007COLUMN04").val('');
                    }
                    else {
                        $(trw).find("#MATABLE007COLUMN04").val('');
                    }
                }
            });
        }
        else if (Formid == 1261) {
        }
        else {
            debugger;
            $.ajax({
                url: "/FormBuilding/getMatrixDetails?FormName=" + FormName + "",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({ style: $(trw).find("#MATABLE007COLUMN43").val(), brand: $(trw).find("#MATABLE007COLUMN10").val(), color: $(trw).find("#MATABLE007COLUMN42").val(), size: $(trw).find("#MATABLE007COLUMN44").val() }),
                success: function (data) {
                    $(trw).find("#MATABLE007COLUMN04").val(data.icode);
                    $(trw).find("#PUTABLE004COLUMN03 option:contains(" + data.item + ")").attr('selected', 'selected');
                    if (data.item == "")
                        Alert.render('There Is No Item Exists'); $("#alertok").focus();
                    $(trw).find("#PUTABLE004COLUMN03").val(null);
                    return false;
                }
            });
        }
    }
    else if (typeof SOItemID != "undefined") {
        $.ajax({
            url: "/Common/GetItemDetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ ItemID: SOItemID }),
            success: function (data) {
                if (data != "") {
                    debugger
                    $(trw).find("#SATABLE006COLUMN04").val(data.Data);
                    $(trw).find("#SATABLE006COLUMN05").val(data.Data9);
                    $(trw).find("#SATABLE006COLUMN06").val(data.Data2);
                    $(trw).find("#SATABLE006COLUMN09").val(data.Data6);
                    if (data.Data11 != "") {
                        $(trw).find("#SATABLE006COLUMN26").val(data.Data11);
                    }
                    else {
                        $(trw).find("#SATABLE006COLUMN26").val(1000);
                    }
                    $(trw).find("#SATABLE006COLUMN27").val(data.Data7);
                    $(trw).find("#SATABLE006COLUMN07").val('');
                    $(trw).find("#SATABLE006COLUMN05").val(data.Data9);

                    if (Formid == 1283) { $(trw).find("#SATABLE006COLUMN09").val(data.Data4); }
                    $(trw).find("#SATABLE006COLUMN04").prop("readonly", true);
                    $(trw).find("#SATABLE006COLUMN05").prop("readonly", true);
                    $(trw).find("#SATABLE006COLUMN06").prop("readonly", true);
                    $(trw).find("#SATABLE006COLUMN12").attr("readonly", true);
                    $(trw).find("#SATABLE006COLUMN13").attr("readonly", true);
                    $(trw).find("#SATABLE006COLUMN26").attr("readonly", true);
                    $(trw).find("#UPCSearch").val("");
                }
                else {
                    Alert.render('There Is No Data For Selected Item'); $("#alertok").focus();
                    $(trw).find("#SATABLE006COLUMN04").val('');
                    $(trw).find("#SATABLE006COLUMN05").val('');
                    $(trw).find("#SATABLE006COLUMN06").val('');
                    $(trw).find("#SATABLE006COLUMN27").val('');
                    if (Formid == 1283) {
                        $(trw).find("#SATABLE006COLUMN09").val('');
                    }
                }
            }
        });
    }
    else if (typeof PWOItemID != "undefined") {
        $.ajax({
            url: "/FormBuilding/GetItemDetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ ItemID: PWOItemID }),
            success: function (data) {
                if (data != "") {
                    $(trw).find("#PRTABLE006COLUMN04").val(data.Data);
                    $(trw).find("#PRTABLE006COLUMN05").val(data.Data1);
                    $(trw).find("#PRTABLE006COLUMN09").val(data.Data4);
                    $(trw).find("#PRTABLE006COLUMN04").prop("readonly", true);
                    $(trw).find("#PRTABLE006COLUMN05").prop("readonly", true);
                    $(trw).find("#PRTABLE006COLUMN09").prop("readonly", true);
                    $(trw).find("#UPCSearch").val("");
                }
                else {
                    Alert.render('There Is No Data For Selected Item'); $("#alertok").focus();
                    $(trw).find("#PUTABLE002COLUMN04").val('');
                    $(trw).find("#PUTABLE002COLUMN05").val('');
                    $(trw).find("#PUTABLE002COLUMN09").val('');
                }
            }
        });
    }
    else if (typeof RCWOItemID != "undefined") {
        $.ajax({
            url: "/FormBuilding/GetItemDetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ ItemID: RCWOItemID }),
            success: function (data) {
                if (data != "") {
                    $(trw).find("#PRTABLE008COLUMN04").val(data.Data1);
                    $(trw).find("#PRTABLE008COLUMN08").val(data.Data4);
                    $(trw).find("#PRTABLE008COLUMN04").prop("readonly", true);
                    $(trw).find("#PRTABLE008COLUMN08").prop("readonly", true);
                    $(trw).find("#UPCSearch").val("");
                }
                else {
                    Alert.render('There Is No Data For Selected Item'); $("#alertok").focus();

                    $(trw).find("#PRTABLE008COLUMN04").val('');
                    $(trw).find("#PRTABLE008COLUMN08").val('');
                }
            }
        });
    }
    else if (typeof IAdjuItem != "undefined") {
        var ou = $("#FITABLE014COLUMN10").val();
        if (ou == '' || ou == null) {
            Alert.render('Please Select Operating Unit Also'); $("#alertok").focus();
            $(trw).find("#FITABLE015COLUMN03").val('');
            $("#alertok").on("click", function () {
                $("#dialogbox").dialog("close");
                $("#FITABLE014COLUMN10").focus();
                return false;
            })
        } if (ou != '' && ou != null) {
            $.ajax({
                url: "/FormBuilding/GetIAItemDetails?FormName=" + FormName + "",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({ ItemID: IAdjuItem, OPUnit: ou }),
                success: function (data) {
                    if (data != "") {
                        $(trw).find("#FITABLE015COLUMN04").val(data.Data);
                        $(trw).find("#FITABLE015COLUMN05").val(data.Data1);
                        $(trw).find("#FITABLE015COLUMN06").val(data.Data2);
                        $(trw).find("#FITABLE015COLUMN17").val(data.Data2);
                        $(trw).find("#FITABLE015COLUMN10").val(data.Data4);
                        $(trw).find("#FITABLE015COLUMN07").val(data.Data5);
                        $(trw).find("#FITABLE015COLUMN09").val("");
                        $(trw).find("#FITABLE015COLUMN04").prop("readonly", true);
                        $(trw).find("#FITABLE015COLUMN05").prop("readonly", true);
                        $(trw).find("#FITABLE015COLUMN06").attr("readonly", true);
                        $(trw).find("#UPCSearch").val(""); IACalculations();
                    }
                    else {
                        Alert.render('There Is No Data For Selected Item'); $("#alertok").focus();
                        $(trw).find("#FITABLE015COLUMN04").val('');
                        $(trw).find("#FITABLE015COLUMN05").val('');
                        $(trw).find("#FITABLE015COLUMN06").val('');
                        $(trw).find("#FITABLE015COLUMN10").val('');
                        $(trw).find("#FITABLE015COLUMN07").val('');
                    }
                }
            });
        }
    }
    else if (typeof iSOItemID != "undefined") {
        $.ajax({
            url: "/FormBuilding/GetItemDetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ ItemID: iSOItemID }),
            success: function (data) {
                if (data != "") {
                    $(trw).find("#SATABLE010COLUMN03").val(data.Data);
                    $(trw).find("#SATABLE010COLUMN07").val(0);
                    $(trw).find("#SATABLE010COLUMN08").val(0);
                    $(trw).find("#SATABLE010COLUMN06").val(data.Data1);
                    $(trw).find("#SATABLE010COLUMN11").val(data.Data2);
                    $(trw).find("#SATABLE010COLUMN21").val(data.Data5);
                    $(trw).find("#SATABLE010COLUMN10").val('');
                    $(trw).find("#SATABLE010COLUMN22").val(data.Data7);
                    $(trw).find("#SATABLE010COLUMN06").val(data.Data8);
                    $(trw).find("#SATABLE010COLUMN13").val(data.Data6);
                    $(trw).find("#SATABLE010COLUMN09").val(0);
                    if ($(trw).find("#SATABLE010COLUMN20").val() == '') {
                        $(trw).find("#SATABLE010COLUMN14").val(0);
                    }
                    $(trw).find("#SATABLE010COLUMN12").val(0);
                    if (Formid != 1277) { $(trw).find("#SATABLE010COLUMN13").val(data.Data4); }
                    //EMPHCS685 Rajasekhar Patakota 18/7/2015 UPC Search in Sales Order Edit and in new row
                    $(trw).find("#UPCSearch").val("");
                }
                else {
                    Alert.render('There Is No Data For Selected Item'); $("#alertok").focus();
                    $(trw).find("#SATABLE010COLUMN03").val('');
                    $(trw).find("#SATABLE010COLUMN07").val('');
                    $(trw).find("#SATABLE010COLUMN08").val('');
                    $(trw).find("#SATABLE010COLUMN06").val('');
                    $(trw).find("#SATABLE010COLUMN09").val('');
                    $(trw).find("#SATABLE010COLUMN10").val('');
                    $(trw).find("#SATABLE010COLUMN14").val('');
                    $(trw).find("#SATABLE010COLUMN12").val('');
                    $(trw).find("#SATABLE010COLUMN13").val('');
                    $(trw).find("#SATABLE010COLUMN21").val('');
                    $(trw).find("#SATABLE010COLUMN22").val('');
                }
            }
        });
    }
    else if (typeof OPItemID != "undefined") {
        $.ajax({
            url: "/FormBuilding/GetItemDetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ ItemID: OPItemID }),
            success: function (data) {
                if (data != "") {
                    $(trw).find("#SATABLE014COLUMN04").val(data.Data);
                    $(trw).find("#SATABLE014COLUMN05").val(data.Data1);
                    //$(trw).find("#SATABLE014COLUMN06").val(data.Data2);
                    $(trw).find("#SATABLE014COLUMN12").val(data.Data7);
                    $(trw).find("#SATABLE014COLUMN07").val(data.Data4);
                    $(trw).find("#SATABLE014COLUMN04").prop("readonly", true);
                    $(trw).find("#SATABLE014COLUMN05").prop("readonly", true);
                    //$(trw).find("#SATABLE014COLUMN06").prop("readonly", true);
                    $(trw).find("#SATABLE014COLUMN07").attr("readonly", true);
                    $(trw).find("#SATABLE014COLUMN08").attr("readonly", true);
                    $(trw).find("#SATABLE014COLUMN09").attr("readonly", true);
                    $(trw).find("#UPCSearch").val("");
                }
                else {
                    Alert.render('There Is No Data For Selected Item'); $("#alertok").focus();
                    $(trw).find("#SATABLE014COLUMN04").val('');
                    $(trw).find("#SATABLE014COLUMN05").val('');
                    //$(trw).find("#SATABLE014COLUMN06").val('');
                    $(trw).find("#SATABLE014COLUMN12").val('');
                    if (Formid == 1283) {
                        $(trw).find("#SATABLE006COLUMN09").val('');
                    }
                }
            }
        });
    }



    else if (typeof SQItemID != "undefined") {
        $.ajax({
            url: "/FormBuilding/GetItemDetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ ItemID: SQItemID }),
            success: function (data) {
                if (data != "") {
                    $(trw).find("#SATABLE016COLUMN04").val(data.Data);
                    $(trw).find("#SATABLE016COLUMN05").val(data.Data1);
                    $(trw).find("#SATABLE016COLUMN13").val(data.Data7);
                    //$(trw).find("#SATABLE016COLUMN06").val(data.Data2);
                    $(trw).find("#SATABLE016COLUMN07").val(data.Data5);
                    if (Formid == 1283) { $(trw).find("#SATABLE016COLUMN09").val(data.Data4); }
                    $(trw).find("#SATABLE016COLUMN04").prop("readonly", true);
                    $(trw).find("#SATABLE016COLUMN05").prop("readonly", true);
                    $(trw).find("#SATABLE016COLUMN07").prop("readonly", true);
                    $(trw).find("#SATABLE016COLUMN08").prop("readonly", true);
                    //$(trw).find("#SATABLE016COLUMN09").prop("readonly", true);
                    //$(trw).find("#SATABLE016COLUMN06").prop("readonly", true);
                    $(trw).find("#SATABLE016COLUMN12").attr("readonly", true);
                    $(trw).find("#SATABLE016COLUMN13").attr("readonly", true);
                    $(trw).find("#UPCSearch").val("");
                }
                else {
                    Alert.render('There Is No Data For Selected Item'); $("#alertok").focus();
                    $(trw).find("#SATABLE016COLUMN04").val('');
                    $(trw).find("#SATABLE016COLUMN05").val('');
                    $(trw).find("#SATABLE016COLUMN06").val('');
                    $(trw).find("#SATABLE016COLUMN13").val('');
                    if (Formid == 1283) {
                        $(trw).find("#SATABLE016COLUMN09").val('');
                    }
                }
            }
        });
    }
    else if (typeof ItemID != "undefined") {
        debugger
        $.ajax({
            url: "/Common/GetItemDetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ ItemID: ItemID }),
            success: function (data) {
                if (data != "") {
                    $(trw).find("#PUTABLE002COLUMN04").val(data.Data);
                    $(trw).find("#PUTABLE002COLUMN05").val(data.Data1);
                    $(trw).find("#PUTABLE002COLUMN06").val(data.Data2);
                    $(trw).find("#PUTABLE002COLUMN09").val(data.Data4);
                    $(trw).find("#PUTABLE002COLUMN05").val(data.Data9);
                   //EMPHCS1784	Job Order for IN Process 15/7/2016
		    if(data.Data11!="")
                    $(trw).find("#PUTABLE002COLUMN25").val(data.Data11);
		    $(trw).find("#PUTABLE002COLUMN26").val(data.Data7);
		    $(trw).find("#PUTABLE002COLUMN27").val(data.Data7);
                    $(trw).find("#PUTABLE002COLUMN07").val('');

                    $(trw).find("#PUTABLE002COLUMN04").prop("readonly", true);
                    $(trw).find("#PUTABLE002COLUMN05").prop("readonly", true);
                    $(trw).find("#PUTABLE002COLUMN06").prop("readonly", true);
                    $(trw).find("#UPCSearch").val("");
                }
                else {
                    Alert.render('There Is No Data For Selected Item'); $("#alertok").focus();
                    $(trw).find("#PUTABLE002COLUMN04").val('');
                    $(trw).find("#PUTABLE002COLUMN05").val('');
                    $(trw).find("#PUTABLE002COLUMN06").val('');
                    $(trw).find("#PUTABLE002COLUMN07").val('');
                    $(trw).find("#PUTABLE002COLUMN09").val('');
                    $(trw).find("#PUTABLE002COLUMN26").val('');
                }
            }
        });
    }
    else if (typeof GRNItemID != "undefined") {
        debugger
        $.ajax({
            url: "/Common/GetItemDetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ ItemID: GRNItemID }),
            success: function (data) {
                debugger
                if (data != "") {
                    $(trw).find("#PUTABLE023COLUMN04").val(data.Data7);
                  
                }
               
            }
        });
    }
    else if (typeof bItemID != "undefined") {
        debugger
        $.ajax({
            url: "/FormBuilding/GetItemDetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ ItemID: bItemID }),
            success: function (data) {
                if (data != "") {
                    debugger

                    $(trw).find("#PUTABLE006COLUMN05").val(data.Data1);
                    $(trw).find("#PUTABLE006COLUMN07").val(0);
                    $(trw).find("#PUTABLE006COLUMN08").val(0);
                    $(trw).find("#PUTABLE006COLUMN10").val(0);
                    $(trw).find("#PUTABLE006COLUMN19").val(data.Data7);
                    $(trw).find("#PUTABLE006COLUMN09").val('');
                    $(trw).find("#PUTABLE006COLUMN11").val(data.Data4);
                    $(trw).find("#PUTABLE006COLUMN05").val(data.Data9);
                    $(trw).find("#PUTABLE006COLUMN18").val(data.Data11);
                    $(trw).find("#PUTABLE006COLUMN12").val(0);
                    //EMPHCS685 Rajasekhar Patakota 18/7/2015 
                    $(trw).find("#UPCSearch").val("");
                }
                else {
                    Alert.render('There Is No Data For Selected Item'); $("#alertok").focus();
                    $(trw).find("#PUTABLE006COLUMN08").val('');
                    $(trw).find("#PUTABLE006COLUMN09").val('');
                    $(trw).find("#PUTABLE006COLUMN10").val('');
                    $(trw).find("#PUTABLE006COLUMN19").val('');
                    $(trw).find("#PUTABLE006COLUMN11").val('');
                    $(trw).find("#PUTABLE006COLUMN12").val('');
                    $(trw).find("#PUTABLE006COLUMN07").val('');
                    $(trw).find("#PUTABLE006COLUMN05").val('');
                    $(trw).find("#PUTABLE006COLUMN18").val('');
                    $(trw).find("#PUTABLE006COLUMN06").val('');
                }
            }
        });
    }

    else if (typeof JoItemID != "undefined") {
        debugger;
        $.ajax({
            url: "/JobOrderReceipt/JOGetItemDetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ ItemID: JoItemID }),
            success: function (data) {
                if (data != "") {
                    debugger

                    $(trw).find("#PUTABLE004COLUMN03").val(data.Data);
                    $(trw).find("#UPCSearch").val(data.Data1);
                    $(trw).find("#MATABLE007COLUMN04").val(data.Data2);
                    $(trw).find("#MATABLE007COLUMN42").val(data.Data3);
                    $(trw).find("#PUTABLE004COLUMN43").val(data.Data4);
                    $(trw).find("#PUTABLE004COLUMN44").val(data.Data5);
                    $(trw).find("#PUTABLE004COLUMN45").val(data.Data6);
                    
                    //EMPHCS685 Rajasekhar Patakota 18/7/2015 
                    $(trw).find("#UPCSearch").val("");
                }
                else {
                    Alert.render('There Is No Data For Selected Item'); $("#alertok").focus();
                    $(trw).find("#PUTABLE004COLUMN03").val('');
                    $(trw).find("#UPCSearch").val('');
                    $(trw).find("#MATABLE007COLUMN04").val('');
                    $(trw).find("#MATABLE007COLUMN42").val('');
                    $(trw).find("#PUTABLE004COLUMN43").val('');
                    $(trw).find("#PUTABLE004COLUMN44").val('');
                    $(trw).find("#PUTABLE004COLUMN45").val('');
                   
                }
            }
        });
    }
}

//so Line Quantity Calculation
function SOLineQuantityTaxCalculation(tr) {
    var qty = 0; var price = 0; var discount = 0;
    debugger
    qty = $(tr).find("[id='SATABLE006COLUMN07']").val();
    price = $(tr).find("[id='SATABLE006COLUMN09']").val();
    discount = $(tr).find("[id='SATABLE006COLUMN10']").val();
    //EMPHCS1703	job order print and fabric meterage same as qty Raj.jr
    $(tr).find("[id='SATABLE006COLUMNB03']").val(parseFloat(qty).toFixed(2));
    if (qty == "" || typeof (qty) == "undefined") { qty = "0"; }
    if (price == "" || typeof (price) == "undefined") { price = "0"; }
    if (discount == "" || typeof (discount) == "undefined") { discount = "0"; }
    if (isNaN(parseFloat(qty)) || parseFloat(qty) < 0 || !(qty.match('^([0-9]+)(\.[0-9]+)?$'))) {
        $(tr).find("#SATABLE006COLUMN07").val(0);
        $(tr).find("#SATABLE006COLUMN25").val(0);
        $(tr).find("#SATABLE006COLUMN11").val(0);
        $(tr).find("#SATABLE006COLUMN10").val(0);
        //EMPHCS684 Rajasekhar Patakota 18/7/2015 Quantity Calculation Changes
        $(tr).find("[id='aSATABLE006COLUMN25']").val(0);
        Alert.render('Please Enter More Than Zero Value for Quantity'); $("#alertok").focus();
        $("#alertok").on("click", function () {
            $("#dialogbox").dialog("close"); $(tr).find("#SATABLE006COLUMN07").focus();
            SOTaxLevelCalculations(); SOTAXCalculations();
        })

        //CalculationsP();
        return false;
    }
    else if (isNaN(parseFloat(price)) || parseFloat(price) < 0 || !(price.match('^([0-9]+)(\.[0-9]+)?$'))) {
        $(tr).find("#SATABLE006COLUMN09").val(0);

        Alert.render('Please Enter More Than Zero Value for Price'); $("#alertok").focus();
        $("#alertok").on("click", function () {
            $("#dialogbox").dialog("close"); $(tr).find("#SATABLE006COLUMN09").focus();
            SOTaxLevelCalculations(); SOTAXCalculations();
        })

        //CalculationsP();
        return false;
    }
    else if (isNaN(parseFloat(discount)) || parseFloat(discount) < 0 || !(discount.match('^([0-9]+)(\.[0-9]+)?$'))) {
        $(tr).find("#SATABLE006COLUMN10").val(0);
        Alert.render('Please Enter More Than Zero Value for Discount'); $("#alertok").focus();
        $("#alertok").on("click", function () {
            $("#dialogbox").dialog("close"); $(tr).find("#SATABLE006COLUMN10").focus();
            SOTaxLevelCalculations(); SOTAXCalculations();
        })

        //CalculationsP();
        return false;
    } else {
        $(tr).find("#SATABLE006COLUMN25").val((parseFloat(qty) * parseFloat(price)).toFixed(2));
        var linetaxtype = $(tr).find("#SATABLE006COLUMN26").val();
        var TaxCal = $(tr).find("#SATABLE006COLUMN25").val();
        $.ajax({
            url: "/Common/GetTaxAmount",
            type: "POST",
            //EMPHCS729 Rajasekhar Patakota 21/7/2015 Tax Amount Calculation In Transaction Forms
            data: JSON.stringify({ TaxType: linetaxtype, TaxCal: TaxCal, FormName: $("#FormName").val() }),
            dataType: "json", cache: true,
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                if (result.Tax != '') {
                    tax = result.Tax; tax = (parseFloat(tax) * parseFloat(0.01));
                    var tt1 = result.TaxCal
                    tax = (parseFloat(tt1) * parseFloat(tax)).toFixed(2);
                } else { tax = 0; }
                //EMPHCS985 Implement logs for SO and invoice done by rajasekhar 18/8/2015
                logValue += " Line Tax Amount = Total (" + tt1 + ")  * Tax (" + tax + ") \r\n";
                var hiddentax = $(tr).find("[id='aSATABLE006COLUMN25']").val();
                if (typeof (hiddentax) != "undefined")
                    $(tr).find("[id='aSATABLE006COLUMN25']").val(tax);
                else
                    $(tr).find("[id='SATABLE006COLUMN25']").after($('<input>').attr({ type: 'hidden', id: 'aSATABLE006COLUMN25' }).val(tax));
                //$(tr).find("[id='aSATABLE006COLUMN25']").val(tax);
                SOTaxLevelCalculations(); SOTAXCalculations();
            }
        })
        //CalculationsP();
    }
};

function SOTaxLevelCalculations() {
    var tbl = $("#grdData")[0]; var qty = 0; var price = 0; var discount = 0; var tqty = 0; var tprice = 0; var tdiscount = 0; var ttal = 0; var tax = 0; var ttax = 0; var amount = 0;
    var Ttax = 0;
    var TaxCal = 0; var rowcount = 0;
    var shipping = 0;
    //EMPHCS794   Packing and Shipping Charges Calculation in Sales Order and Invoice  and capturing values in  delivery & Package ---by Raj.jr 30/7/2015
    var PackingCharges = $("#SATABLE005COLUMN41").val();
    var ShippingCharges = $("#SATABLE005COLUMN42").val();
    if (typeof (PackingCharges) == "undefined" || PackingCharges == "")
        PackingCharges = 0;
    if (typeof (ShippingCharges) == "undefined" || ShippingCharges == "")
        ShippingCharges = 0;
    for (var irow = 1; irow < tbl.rows.length  ; irow++) {
        rowcount = rowcount + 1;
        var trw = tbl.rows[irow];
        if ($(trw).find("#checkRow").is(':checked')) {
            qty = $(trw).find("[id='SATABLE006COLUMN07']").val();
            price = $(trw).find("[id='SATABLE006COLUMN09']").val();
            discount = $(trw).find("[id='SATABLE006COLUMN10']").val();
            tax = $(trw).find("[id='aSATABLE006COLUMN25']").val();
            if (qty == "") { qty = 0; }
            if (price == "") { price = 0; }
            if (discount == "" || typeof (discount) == "undefined") { discount = "0"; }
            if (tdiscount == "" || typeof (tdiscount) == "undefined") { tdiscount = "0"; }
            if (tax == "" || typeof (tax) == "undefined") { tax = 0; }
            ttax = parseFloat(ttax) + parseFloat(tax);
            tqty = parseFloat(tqty) + parseFloat(qty);
            tprice = parseFloat(tprice) + parseFloat(price);
            tdiscount = parseFloat(tdiscount) + parseFloat(discount);
            amount = parseFloat(qty) * parseFloat(price);
            ttal = parseFloat(ttal) + (amount);
            //EMPHCS684 Rajasekhar Patakota 18/7/2015 Quantity Calculation Changes
            if (qty > 0 && price > 0) {
                $(trw).find("#SATABLE006COLUMN11").val(((parseFloat(amount) + parseFloat(tax)) ).toFixed(2));
                $(trw).find("#SATABLE006COLUMN25").val((amount).toFixed(2));
                //EMPHCS985 Implement logs for SO and invoice done by rajasekhar 18/8/2015
                logValue += " Line Amount = Total (" + amount + ")  \r\n";
                logValue += " Line Total Amount = Amount (" + amount + ") + Tax Amount (" + tax + ")  \r\n";
            }
            debugger
            if (rowcount == tbl.rows.length - 1) {
                var headertax = $("#aSATABLE005COLUMN32").val();
                if (headertax == "" || headertax == null || typeof (headertax) == "undefined" || isNaN(headertax)) { headertax = 0; }
                ttax = parseFloat(ttax) + parseFloat(headertax);
                $("#SATABLE005COLUMN32").val(ttax.toFixed(2));
                //EMPHCS794   Packing and Shipping Charges Calculation in Sales Order and Invoice  and capturing values in  delivery & Package ---by Raj.jr 30/7/2015
                $("#SATABLE005COLUMN12").val((parseFloat(ttal) + parseFloat(PackingCharges) + parseFloat(ShippingCharges)).toFixed(2));
                //$("#SATABLE005COLUMN12").val(parseFloat(ttal).toFixed(2));
                //$("#SATABLE005COLUMN14").val(parseFloat(tdiscount).toFixed(2));
                $("#SATABLE005COLUMN15").val(((parseFloat(ttal) - parseFloat(tdiscount)) + parseFloat(ttax) + parseFloat(PackingCharges) + parseFloat(ShippingCharges)).toFixed(2));
                //EMPHCS985 Implement logs for SO and invoice done by rajasekhar 18/8/2015
                //header total amount
                logValue += " Header Total Amount  = Sub Total (" + parseFloat(ttal).toFixed(2) + ") Tax (" + parseFloat(ttax).toFixed(2) + ")+ PackingCharges (" + parseFloat(PackingCharges).toFixed(2) + ")+ ShippingCharges (" + parseFloat(ShippingCharges).toFixed(2) + ") \r\n";
            }
            else {
                $("#SATABLE005COLUMN32").val("");
                $("#SATABLE005COLUMN12").val("");
                $("#SATABLE005COLUMN14").val("");
                $("#SATABLE005COLUMN15").val("");
            }
        } else {
            if (rowcount == tbl.rows.length - 1) {
                debugger
                var headertax = $("#aSATABLE005COLUMN32").val();
                if (headertax == "" || headertax == null || typeof (headertax) == "undefined" || isNaN(headertax)) { headertax = 0; }
                ttax = parseFloat(ttax) + parseFloat(headertax);
                $("#SATABLE005COLUMN32").val(ttax.toFixed(2));
                //EMPHCS794   Packing and Shipping Charges Calculation in Sales Order and Invoice  and capturing values in  delivery & Package ---by Raj.jr 30/7/2015
                $("#SATABLE005COLUMN12").val((parseFloat(ttal) + parseFloat(PackingCharges) + parseFloat(ShippingCharges)).toFixed(2));
                $("#SATABLE005COLUMN14").val(parseFloat(tdiscount).toFixed(2));
                $("#SATABLE005COLUMN15").val(((parseFloat(ttal) ) + parseFloat(ttax) + parseFloat(PackingCharges) + parseFloat(ShippingCharges)).toFixed(2));
                //EMPHCS985 Implement logs for SO and invoice done by rajasekhar 18/8/2015
                //header total amount
                logValue += " Header Total Amount  = Sub Total (" + parseFloat(ttal).toFixed(2) + ") + Tax (" + parseFloat(ttax).toFixed(2) + ")+ PackingCharges (" + parseFloat(PackingCharges).toFixed(2) + ")+ ShippingCharges (" + parseFloat(ShippingCharges).toFixed(2) + ") \r\n";
            }
        }
    }
    //EMPHCS901,EMPHCS902 LOGS CREATION IN TAX CALCULATION FOR PO AND SO done by rajasekhar 18/8/2015
    $.ajax({
        url: "/FormBuilding/CreateLog",
        type: "POST",
        data: JSON.stringify({ LogValue: logValue, fname: "Job Order Tax" }),
        dataType: "json", cache: true,
        contentType: "application/json; charset=utf-8",
        success: function (result) { }
    });
}

// SO TAX CALUCLATION

function SOTAXCalculations() {
    //EMPHCS794   Packing and Shipping Charges Calculation in Sales Order and Invoice  and capturing values in  delivery & Package ---by Raj.jr 30/7/2015
    var PackingCharges = $("#SATABLE005COLUMN41").val();
    var ShippingCharges = $("#SATABLE005COLUMN42").val();
    if (typeof (PackingCharges) == "undefined" || PackingCharges == "")
        PackingCharges = 0;
    if (typeof (ShippingCharges) == "undefined" || ShippingCharges == "")
        ShippingCharges = 0;
    var taxamount = 0;
    var subtotal = $("#SATABLE005COLUMN12").val();
    var discount = $("#SATABLE005COLUMN14").val();
    var total = $("#SATABLE005COLUMN15").val();
    if (subtotal == "") { subtotal = 0; }
    if (discount == "" || typeof (discount) == "undefined") { discount = "0"; }
    if (total == "") { total = 0; } var linetax = 0;
    TaxTypeH = $("#SATABLE005COLUMN31").val();

    $.ajax({
        url: "/JobOrder/GetTaxAmount",
        type: "POST",
        //EMPHCS729 Rajasekhar Patakota 21/7/2015 Tax Amount Calculation In Transaction Forms
        data: JSON.stringify({ TaxType: TaxTypeH, TaxCal: null, FormName: $("#FormName").val() }),
        dataType: "json", cache: true,
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            debugger;
            if (result.Tax != '') {
                tax = result.Tax; tax = (parseFloat(tax) * parseFloat(0.01));
                tax = ((parseFloat($("#SATABLE005COLUMN12").val()) - parseFloat(PackingCharges) - parseFloat(ShippingCharges)) * parseFloat(tax)).toFixed(2);
                //EMPHCS985 Implement logs for SO and invoice done by rajasekhar 18/8/2015
                logValue += " Header Tax Amount = Subtotal (" + $("#SATABLE005COLUMN12").val() + ")  - PackingCharges (" + PackingCharges + ") - ShippingCharges (" + ShippingCharges + ") * Tax (" + tax + ") \r\n";
            } else { tax = 0; } $("#aSATABLE005COLUMN32").val(tax);
            var tbl = $("#grdData")[0];
            for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                var trw = tbl.rows[irow];
                if ($(trw).find("#checkRow").is(':checked')) {
                    linetax = $(trw).find("#aSATABLE006COLUMN25").val();
                    //EMPHCS985 Implement logs for SO and invoice done by rajasekhar 18/8/2015
                    logValue += " Line Tax Amount Item " + irow + "= (" + linetax + ") \r\n";
                    if (linetax == "") linetax = 0;
                    taxamount = (parseFloat(taxamount) + parseFloat(linetax));
                }
            }
            debugger
            var z = $("#FormName").val();
            //if (z != "Job Order" ) {
                $("#SATABLE005COLUMN32").val((parseFloat(taxamount) + parseFloat(tax)).toFixed(2));
            //EMPHCS797 Shipping and Packing Charges not Carry forward to Invoice Screen done by srinivas 31/7/2015
                $("#SATABLE005COLUMN15").val(((parseFloat(subtotal) ) + (parseFloat(taxamount) + parseFloat(tax))).toFixed(2));
            //EMPHCS985 Implement logs for SO and invoice done by rajasekhar 18/8/2015
                logValue += " Total Tax Amount = Heder Tax (" + tax + ") + Line Tax (" + taxamount + ") \r\n";
                logValue += " Total Amount = Sub Total (" + subtotal + ")  + Heder Tax (" + tax + ") + Line Tax (" + taxamount + ") \r\n";
            //}//CalculationsP();
        }

    });
    //EMPHCS985 Implement logs for SO and invoice done by rajasekhar 18/8/2015
    $.ajax({
        url: "/FormBuilding/CreateLog",
        type: "POST",
        //EMPHCS729 Rajasekhar Patakota 21/7/2015 Tax Amount Calculation In Transaction Forms
        data: JSON.stringify({ LogValue: logValue, fname: "Purchase Order Tax" }),
        dataType: "json", cache: true,
        contentType: "application/json; charset=utf-8",
        success: function (result) { }
    });
}


//Form Save action
function SaveForm(FormName, TableData, FormId) {
    var flag = false; var text;
    $('.ddl').each(function () {
        if (flag == true) {
            return false;
        }
        else {
            var textbox = $(this);
            text = $(this).val(); var id = $(this).attr('id'); var aname = $(this).attr('name');
            var mandatory = $(this).attr('itemid');
            var tabname = $(this).attr('title');
            if (text == "") {
                var Amnt = "";
                if (FormId == 1288) {
                    Amnt = $("#SATABLE011COLUMN13").val();
                }
                if ((Amnt == 0 || Amnt == "" || typeof (Amnt) == "undefined") && ((FormId == 1288 && id == "SATABLE011COLUMN08"))) {
                }
                else if (mandatory == 'Y') {
                    $('#tabs').tabs('select', tabname);
                    Alert.render('Please Select Item  for ' + aname + ' '); $("#alertok").focus();
                    $("#alertok").focus();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); textbox.focus();
                    })
                    //textbox.next().html("*");
                    flag = true;
                    return false;
                }
            } else {
               // textbox.next().empty();
            }
        }
    })
    $(".txtintgridclass  ").each(function (index) {
        if (flag == true) {
            return false;
        }
        else {
            var textbox = $(this);
            text = $(this).val(); var id = $(this).attr('id'); var aname = $(this).attr('name');
            var mandatory = $(this).attr('pattern'); var tabname = $(this).attr('title');
            var trwC = $(this).closest("tr");
            if (text == "") {
                if (mandatory == 'Y') {
                    if ($(trwC).find("#checkRow").is(':checked')) {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Values  for ' + aname + ''); $("#alertok").focus();
                        //textbox.next().html("*");
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
            }
            else {
                //textbox.next().empty();
                var value = $(this).val();
                var id = $(this).attr('id');
                var name = $(this).attr('itemid');
                if (name == 'int' || name == 'bigint') {
                    if (value.match(/[0-9 -()+]+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Integer Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'decimal') {
                    if (value.match(/^-{0,1}\d*\.{0,1}\d+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Decimal Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'Quantity') {
                    if (value.match(/^-{0,1}\d*\.{0,1}\d+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter   Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Email') {
                    if (value.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/)) { }
                    else
                    {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }

                if (aname == 'Phone') {
                    if (value.match(/^\d{10}$/)) { }
                    else
                    {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + ' No '); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }

                if (aname == 'Zip') {
                    if (value.match(/^\d{6}$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + 'No '); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'money') {
                    if (value.match(/^\d+(?:\.\d{0,2})$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter Money Type  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'price') {
                    if (value.match(/^\d+(?:\.\d{0,2})$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter price Type  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
            }
        }
    });
    $(".gridddl").each(function (index) {
        if (flag == true) {
            return false;
        }
        else {
            var textbox = $(this);
            text = $(this).val(); var id = $(this).attr('id'); var aname = $(this).attr('name');
            var mandatory = $(this).attr('itemid'); var tabname = $(this).attr('title');
            var trwC = $(this).closest("tr");
            if (text == "") {
                if (mandatory == 'Y') {

                    if ($(trwC).find("#checkRow").is(':checked')) {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please Select Item  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        //textbox.next().html("*");
                        flag = true;
                        return false;
                    }
                }
            } else {
              //  textbox.next().empty();
            }
        }
    });
    $(".txtclass").each(function (index) {
        if (flag == true) {
            return false;
        }
        else {
            var textbox = $(this);
            text = $(this).val(); var id = $(this).attr('id'); var aname = $(this).attr('name');
            var mandatory = $(this).attr('pattern'); var tabname = $(this).attr('title');
            if (text == "") {
                if (mandatory == 'Y') {
                    $('#tabs').tabs('select', tabname);
                    Alert.render('Please enter values for ' + aname + ''); $("#alertok").focus();
                    textbox.next().html("*"); $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); textbox.focus();
                    })
                    flag = true;
                    return false;
                }
            }
            else {
                //textbox.next().empty();
                var value = $(this).val();
                var id = $(this).attr('id');
                var name = $(this).attr('itemid'); var aname = $(this).attr('name');
                var tabname = $(this).attr('title');
                if (name == 'int' || name == 'bigint') {
                    if (value.match(/[0-9 -()+]+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Integer Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'decimal') {
                    if (value.match(/^-{0,1}\d*\.{0,1}\d+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Decimal Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Email') {
                    if (value.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/)) { }

                    else
                    {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Phone') {
                    if (value.match(/^\d{10}$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + 'No '); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Zip') {
                    if (value.match(/^\d{6}$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + 'No '); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'money') {
                    if (value.match(/^\d+(?:\.\d{0,2})$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Money Type  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
            }
        }
    });
    $(".txttabClass").each(function (index) {
        if (flag == true) {
            return false;
        }
        else {
            var textbox = $(this);
            text = $(this).val();
            var id = $(this).attr('id'); var aname = $(this).attr('name');
            var mandatory = $(this).attr('pattern'); var tabname = $(this).attr('title');
            if (text == "") {
                if (mandatory == 'Y') {
                    $('#tabs').tabs('select', tabname);
                    Alert.render('Please enter  Values  for ' + aname + ''); $("#alertok").focus();
                   // textbox.next().html("*");
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); textbox.focus();
                    })
                    flag = true;
                    return false;
                }
            }
            else {
              //  textbox.next().empty();
                var value = $(this).val();
                var id = $(this).attr('id'); var aname = $(this).attr('name');
                var name = $(this).attr('itemid'); var tabname = $(this).attr('title');
                if (name == 'int' || name == 'bigint') {
                    if (value.match(/[0-9 -()+]+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Integer Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'decimal') {
                    if (value.match(/^-{0,1}\d*\.{0,1}\d+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Decimal Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Email') {
                    if (value.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/)) { }
                    else
                    {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Phone') {
                    if (value.match(/^\d{10}$/)) { }
                    else
                    {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + 'No '); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Zip') {
                    if (value.match(/^\d{6}$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + 'No '); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'money') {
                    if (value.match(/^\d+(?:\.\d{0,2})$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter Money Type for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
            }
        }
    });
    $(".txtgridclass").each(function (index) {
        if (flag == true) {
            return false;
        }
        else {
            var textbox = $(this);
            text = $(this).val(); var id = $(this).attr('id'); var aname = $(this).attr('name');
            var mandatory = $(this).attr('pattern'); var tabname = $(this).attr('title');
            var trwC = $(this).closest("tr");
            if (text == "") {
                if (mandatory == 'Y') {
                    if ($(trwC).find("#checkRow").is(':checked')) {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Values  for ' + aname + ''); $("#alertok").focus();
                       // textbox.next().html("*");
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
            }
            else {
               // textbox.next().empty();
                var value = $(this).val();
                var id = $(this).attr('id');
                var name = $(this).attr('itemid');
                if (name == 'int' || name == 'bigint') {
                    if (value.match(/[0-9 -()+]+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Integer Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'decimal') {
                    if (value.match(/^-{0,1}\d*\.{0,1}\d+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Decimal Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'Quantity') {
                    if (value.match(/^-{0,1}\d*\.{0,1}\d+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter   Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Email') {
                    if (value.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/)) { }
                    else
                    {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Phone') {
                    if (value.match(/^\d{10}$/)) { }
                    else
                    {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + ' No '); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Zip') {
                    if (value.match(/^\d{6}$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + 'No '); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'money') {
                    if (value.match(/^\d+(?:\.\d{0,2})$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter Money Type  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'price') {
                    if (value.match(/^\d+(?:\.\d{0,2})$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter price Type  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
            }
        }
    });

    if ($("#grdData").length > 0) {
        var tbl = $("#grdData")[0];
        var xmldata = "<ROOT>"; var Gridlength = tbl.rows.length;
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow]; var ItemQty = 0; var ItemPrice = 0;
            var jarray = TableData;
            if ($(trw).find("#checkRow").is(':checked')) {
                for (var cll = 0; cll < jarray.length; cll++) {
                    var node1 = jarray[cll];
                    var node2 = node1.replace(' ', '_');
                    var node = node2.replace(' ', '_');
                    var itval = $(trw).find("[id='" + node1 + "']").val();
		    //EMPHCS1837 rajasekhar reddy patakota 30/11/2016 Project wise tracking in Opportunity and job order screens
                    if (typeof itval == "undefined") itval = "";
                    itval = itval.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;')
                    .replace(/'/g, '&apos;');
                    //EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                    if (node1 == "SATABLE006COLUMN34") {
                        if (itval == "" || itval == null)
                        { itval = 0; }
                    }
                    if (typeof itval == "undefined")
                    { itval = 0; }
                    if (cll == 0) {
                        //purchase
                        if (FormId == 1273)
                        { ItemQty = $(trw).find("[id='PUTABLE006COLUMN09']").val(); }
                        else if (FormId == 1272 || FormId == 1286)
                        {
                            ItemQty = $(trw).find("[id='PUTABLE004COLUMN06']").val();
                            ItemPrice = $(trw).find("[id='PUTABLE004COLUMN10']").val();
                        }
                        else if (FormId == 1274)
                        { ItemQty = $(trw).find("[id='PUTABLE015COLUMN06']").val(); }
                        else if (FormId == 1251 || FormId == 1374 || FormId == 1355) {
                            ItemQty = $(trw).find("[id='PUTABLE002COLUMN07']").val();
                            ItemPrice = $(trw).find("[id='PUTABLE002COLUMN09']").val();
                        }

                            //salesSATABLE005COLUMN33
                        else if (FormId == 1278 || FormId == 1288)
                        {
                            var Edit = 0;
                            var payment = $(trw).find("#SATABLE012COLUMN06").val();
                            var adv = $(trw).find("#SATABLE012COLUMN09").val();
                            var prepay = $(trw).find("#aSATABLE012COLUMN06").val();
                            var pre = $(trw).find("#aSATABLE012COLUMN06").val();
                            var Dueamt = $(trw).find("#aSATABLE012COLUMN05").val();
                            var padv = $(trw).find("#SATABLE012COLUMN09").val();
                            if (payment == '' || typeof (payment) == "undefined") { payment = 0; } if (adv == '') { adv = 0; }
                            if (prepay == '' || typeof (prepay) == "undefined") { prepay = 0; }
                            if (Dueamt == '' || typeof (Dueamt) == "undefined") { Dueamt = 0; }
                            if (Edit == 1 || Edit == "1") {
                                Dueamt = parseFloat(Dueamt) + parseFloat(prepay);
                                prepay = adv = 0;
                            }
                            if (payment > 0 && !(isNaN(parseFloat(payment).toFixed(2))) && payment.match('^([0-9]+)?(\.[0-9]+)?$')) {
                                payment = parseFloat(payment) + parseFloat(adv);
                                if (parseFloat(payment) > (parseFloat(Dueamt) + parseFloat(prepay))) {
                                    Alert.render('Payment must not exceed available DueAmount.'); $("#alertok").focus();
                                    $("#alertok").on("click", function () {
                                        $("#dialogbox").dialog("close");
                                        Dueamt = parseFloat(Dueamt) - parseFloat(adv);
                                        $(trw).find("#SATABLE012COLUMN06").val((parseFloat(Dueamt) + parseFloat(prepay)).toFixed(2));
                                        $(trw).find("#SATABLE012COLUMN06").focus();
                                        PaymentCalculation();
                                    })
                                    //PaymentCalculation();
                                    return false;
                                }
                            }
                            else {
                                ItemQty = $(trw).find("[id='SATABLE012COLUMN06']").val()
                                var crd = $(trw).find("[id='SATABLE012COLUMN14']").val();
                                var adv = $(trw).find("[id='SATABLE012COLUMN09']").val();
                                var disc = $(trw).find("[id='SATABLE012COLUMN13']").val();
                                ItemPrice = $(trw).find("[id='SATABLE012COLUMN05']").val();
                                if (ItemQty == '') ItemQty = 0;
                                if (crd == '') crd = 0;
                                if (adv == '') adv = 0;
                                if (disc == '') disc = 0;
                                if ((ItemQty == 0) && (parseFloat(crd) + parseFloat(adv) + parseFloat(disc)) == 0) { break; }
                            }
                        }
                        else if (FormId == 1275 || FormId == 1283 || FormId == 1353) {
                            ItemQty = $(trw).find("[id='SATABLE006COLUMN07']").val();
                            ItemPrice = $(trw).find("[id='SATABLE006COLUMN09']").val();
                        }
                        else if (FormId == 1276 || FormId == 1284 || FormId == 1354) {
                            ItemQty = $(trw).find("[id='SATABLE008COLUMN09']").val();
                            ItemPrice = $(trw).find("[id='SATABLE008COLUMN12']").val();
                            if (FormId == 1276 || FormId == 1284) {
                                if (parseFloat($(trw).find("[id='SATABLE008COLUMN09']").val()) > parseFloat($(trw).find("[id='SATABLE008COLUMN10']").val())) {
				//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                                    $.ajax({
                                        url: "/JobOrderIssue/IssueAvlCheckQty",
                                        type: "POST",
                                        dataType: "json", cache: true,
                                        async: false,
                                        contentType: "application/json; charset=utf-8",
                                        success: function (data) {
                                            if (data.AcOwner != "True" && data.AcOwner != "1") {
                                                Alert.render('Quantity should less than available quantity....'); $("#alertok").focus();
                                                $("#alertok").blur("click", function () {
                                                    $("#dialogbox").dialog("close"); $(trw).find("[id='SATABLE008COLUMN09']").focus();
                                                    $(trw).find("[id='SATABLE008COLUMN09']").val('');
                                                    $("#divLoading").hide();
                                                    flag = true;
                                                    return false;
                                                })
                                                $("#divLoading").hide();
                                                flag = true;
                                                return false;
                                            }
                                            else {
                                                $(trw).find("[id='SATABLE008COLUMN13']").val((parseFloat(ItemQty) * parseFloat(ItemPrice)).toFixed(2))
                                                flag = false;
                                            }
                                        }
                                    });
                                }
                                if (flag == true) { irow = tbl.rows.length; break; }
                            }
                        }
                        else if (FormId == 1277 || FormId == 1287)
                        { ItemQty = $(trw).find("[id='SATABLE010COLUMN10']").val(); }
                        else if (FormId == 1381)
                        { ItemQty = $(trw).find("[id='PRTABLE008COLUMN07']").val(); }
                        else if (FormId == 1359 && $(trw).find("[id='FITABLE018COLUMN09']").val() == "" && $(trw).find("[id='FITABLE018COLUMN10']").val() == "")

                        { ItemQty = 0; }
                        else
                        { ItemQty = 1; }
                        if (ItemQty <= 0) {
                            if (FormId != 1288 && FormId != 1286) {
                                break;
                            }
                        }
                        if (FormId == 1251 || FormId == 1374 || FormId == 1275 || FormId == 1283 || FormId == 1355 || FormId == 1353 || FormId == 1354)
                        { if (ItemPrice <= 0) { break; } }
                    }
                    if (cll == 0) {
                        xmldata = xmldata + "<Column><" + node + ">" + itval + "</" + node + ">";
                    }
                  //EMPHCS1587 :Adding Advance Tab to Jobber Payment by gnaneshwar on 4/3/2016
                    else if (cll == jarray.length - 1) {
                        if (node == "SATABLE012COLUMN06") {
                            var itval1 = $(trw).find("[id='advSATABLE012COLUMN15']").val();
                            xmldata = xmldata + "<" + node + ">" + itval + "</" + node + "><SATABLE012COLUMN15>" + itval1 + "</SATABLE012COLUMN15></Column>";
                        }
                        //EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
			//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                        else if (FormId == 1587 || FormId == 1286) {
                            var itval1 = $(trw).find("[id='LineID']").val();
			    //EMPHCS1837 rajasekhar reddy patakota 30/11/2016 Project wise tracking in Opportunity and job order screens
                            if (typeof (itval1) == "undefined") itval1 = "";
                            xmldata = xmldata + "<" + node + ">" + itval + "</" + node + "><PUTABLE004COLUMN26>" + itval1 + "</PUTABLE004COLUMN26></Column>";
                        }
                         //EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
                        else if (FormId == 1588) {
                            var itval1 = $(trw).find("[id='LineID']").val();
                            xmldata = xmldata + "<" + node + ">" + itval + "</" + node + "><SATABLE008COLUMN26>" + itval1 + "</SATABLE008COLUMN26></Column>";
                        }
                        else {
                            xmldata = xmldata + "<" + node + ">" + itval + "</" + node + "></Column>";
                        }
                        break;
                    }
                    else
                        xmldata = xmldata + "<" + node + ">" + itval + "</" + node + ">";
                }
            }
            else { Gridlength = Gridlength - 1; }
        }
        xmldata = xmldata + "</ROOT>";
        if (Gridlength == 1) {
            if (xmldata == "<ROOT></ROOT>") {
                Alert.render('Please select atleast one row from grid view....'); $("#alertok").focus();
                $("#divLoading").hide();
                return false
            }
        }
        var xml = $.parseXML(xmldata);
        if (FormId == 1273) {
            var x = Number($(xml).find("PUTABLE006COLUMN09").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PUTABLE006COLUMN09").focus();
                Alert.render('There is no Qty to generate Bill'); $("#alertok").focus();
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1381) {
            var x = Number($(xml).find("PRTABLE008COLUMN07").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PRTABLE008COLUMN06").focus();
                Alert.render('There is no Qty On Hand to consume resource'); $("#alertok").focus();
                $("#divLoading").hide();
                return false
            }
        }

        else if (FormId == 1272) {
            var x = Number($(xml).find("PUTABLE004COLUMN08").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PUTABLE004COLUMN08").focus();
                Alert.render('There is no Qty to Receive the Item'); $("#alertok").focus();
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1354) {
            var x = Number($(xml).find("SATABLE008COLUMN09").text());

            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#SATABLE008COLUMN09").focus();
                Alert.render('There is no Qty to Receive the Item'); $("#alertok").focus();
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1358) {
            var x = Number($(xml).find("FITABLE017COLUMN05").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#FITABLE017COLUMN05").focus();
                Alert.render('There is no Amount to Pay'); $("#alertok").focus();
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1330) {
            var x = Number($(xml).find("SATABLE006COLUMN07").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#SATABLE006COLUMN07").focus();
                Alert.render('Please give quantity greater than zero....'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $("#SATABLE006COLUMN07").focus();
                })
                $("#divLoading").hide();
                return false
            }
            var y = Number($(xml).find("SATABLE006COLUMN09").text());
            if (y == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#SATABLE006COLUMN09").focus();
                Alert.render('Please give price greater than zero....'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); textbox.focus();
                })
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1329) {
            var x = Number($(xml).find("PUTABLE004COLUMN08").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PUTABLE004COLUMN08").focus();
                Alert.render('There is no Qty to Receive the Item'); $("#alertok").focus();
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1274) {
            var x = Number($(xml).find("PUTABLE015COLUMN06").text());
            var y = Number($(xml).find("PUTABLE015COLUMN04").text());
            var z = Number($(xml).find("PUTABLE015COLUMN14").text());
            if (y != z) {
                if (x == 0) {
                    $('#tabs').tabs('select', "#Item");
                    $("#PUTABLE015COLUMN06").focus();
                    Alert.render('There Is No Payment or no amount For Selected PONO....'); $("#alertok").focus();
                    $("#divLoading").hide();
                    return false
                }
            }
        }
        else if (FormId == 1286) {
            var y = Number($(xml).find("PUTABLE004COLUMN06").text());
            
            //if (y == 0) {
            //    $('#tabs').tabs('select', "#Item");
            //    $("#PUTABLE004COLUMN08").focus();
            //    Alert.render('Please give Expected quantity greater than zero....'); $("#alertok").focus();
            //    $("#alertok").on("click", function () {
            //        $("#dialogbox").dialog("close"); $("#PUTABLE004COLUMN06").focus();
            //    })
            //    $("#divLoading").hide();
            //    return false
            //}
            var z = Number($(xml).find("PUTABLE004COLUMN08").text());
            if (z == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PUTABLE004COLUMN08").focus();
                Alert.render('Please give Actual quantity greater than zero....'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $("#PUTABLE004COLUMN08").focus();
                })
                $("#divLoading").hide();
                return false
            }
            var x = Number($(xml).find("PUTABLE004COLUMN10").text());
            if (x == 0) {
                //$('#tabs').tabs('select', "#Item");
                //$("#PUTABLE004COLUMN10").focus();
                //Alert.render('Please give price greater than zero....'); $("#alertok").focus();
                //$("#alertok").on("click", function () {
                //    $("#dialogbox").dialog("close"); $("#PUTABLE004COLUMN10").focus();
                //})
                //$("#divLoading").hide();
                //return false
            }
           
        }
        else if (FormId == 1251 || FormId == 1355) {
            var x = Number($(xml).find("PUTABLE002COLUMN07").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PUTABLE002COLUMN07").focus();
                Alert.render('Please give quantity greater than zero....'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $("#PUTABLE002COLUMN07").focus();
                })
                $("#divLoading").hide();
                return false
            }
            var y = Number($(xml).find("PUTABLE002COLUMN09").text());
            if (y == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PUTABLE002COLUMN09").focus();
                Alert.render('Please give price greater than zero....'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $("#PUTABLE002COLUMN09").focus();
                })
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1328) {
            var x = Number($(xml).find("PUTABLE002COLUMN07").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PUTABLE002COLUMN07").focus();
                Alert.render('Please give quantity greater than zero....'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $("#PUTABLE002COLUMN07").focus();
                })
                $("#divLoading").hide();
                return false
            }
            var y = Number($(xml).find("PUTABLE002COLUMN09").text());
            if (y == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PUTABLE002COLUMN09").focus();
                Alert.render('Please give price greater than zero....'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $("#PUTABLE002COLUMN09").focus();
                })
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1278 || FormId == 1288) {
            for (var i = 0; i < $(xml).find("Column").length; i++) {
                var x = Number($(xml).find("SATABLE012COLUMN06").eq(i).text());
                var y = Number($(xml).find("SATABLE012COLUMN14").eq(i).text());
                var z = Number($(xml).find("SATABLE012COLUMN05").eq(i).text());
                var d = Number($(xml).find("SATABLE012COLUMN13").eq(i).text());
                var W = Number($(xml).find("SATABLE012COLUMN09").eq(i).text());
                if (x == '' || typeof (x) == "undefined") { x = 0 }
                if (y == '' || typeof (y) == "undefined") { y = 0 }
                if (z == '' || typeof (z) == "undefined") { z = 0 }
                if (W == '' || typeof (W) == "undefined") { W = 0 }
                if (d == '' || typeof (d) == "undefined") { d = 0 }
                if ((x == 0) && (parseFloat(y) + parseFloat(W) + parseFloat(d)) == 0) {
                    $('#tabs').tabs('select', "#Item");
                    $("#SATABLE012COLUMN06").focus();
                    Alert.render('There Is No Payment or no amount For Selected SONO....'); $("#alertok").focus();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); $("#SATABLE012COLUMN06").focus();
                    })
                    $("#divLoading").hide();
                    return false
                }
                else if (((x == 0) && (z == 0) && (W == 0) && (d == 0))) {
                    $('#tabs').tabs('select', "#Item");
                    $("#SATABLE012COLUMN06").focus();
                    Alert.render('There Is No Payment or no amount For Selected SONO....'); $("#alertok").focus();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); $("#SATABLE012COLUMN06").focus();
                    })
                    $("#divLoading").hide();
                    return false
                }
            }
            //var x = Number($(xml).find("SATABLE012COLUMN06").text());
            //if (x == 0) {
            //    $('#tabs').tabs('select', "#Item");
            //    $("#SATABLE012COLUMN06").focus();
            //    Alert.render('There Is No Payment or no amount For Selected SONO....'); $("#alertok").focus();
            //    $("#alertok").on("click", function () {
            //        $("#dialogbox").dialog("close"); $("#SATABLE012COLUMN06").focus();
            //    })
            //    $("#divLoading").hide();
            //    return false
            //}
        }
        else if (FormId == 1275 || FormId == 1283 || FormId == 1353) {
            var x = Number($(xml).find("SATABLE006COLUMN07").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#SATABLE006COLUMN07").focus();
                Alert.render('Please give quantity greater than zero....'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $("#SATABLE006COLUMN07").focus();
                })
                $("#divLoading").hide();
                return false
            }
            var y = Number($(xml).find("SATABLE006COLUMN09").text());
            if (y == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#SATABLE006COLUMN09").focus();
                Alert.render('Please give price greater than zero....'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); textbox.focus();
                })
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1276 || FormId == 1284 || FormId == 1354) {
            //if (parseFloat($("#SATABLE008COLUMN09").val()) > parseFloat($("#SATABLE008COLUMN10").val()))
            //    {
            //        Alert.render('Quantity should less than available quantity....');$("#alertok").focus();
            //        $("#alertok").blur("click",function(){
            //            $("#dialogbox").dialog("close"); $("#SATABLE008COLUMN09").focus();
            //            $("#SATABLE008COLUMN09").val('');
            //        })
            //        $("#divLoading").hide();
            //        return false
            //    }
            var x = Number($(xml).find("SATABLE008COLUMN09").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#SATABLE008COLUMN09").focus();
                Alert.render('There is no Qty to Issue the Item'); $("#alertok").focus();
                $("#alertok").blur("click", function () {
                    $("#dialogbox").dialog("close"); $("#SATABLE008COLUMN09").focus();
                })
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1277 || FormId == 1287) {
            var x = Number($(xml).find("SATABLE010COLUMN10").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#SATABLE010COLUMN10").focus();
                if (FormId == 1287) {
                    Alert.render('There is no Qty to Genarate Bill'); $("#alertok").focus();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); $("#SATABLE010COLUMN10").focus();
                    })
                    $("#divLoading").hide();
                }
                else {
                    Alert.render('There is no Qty to Genarate Invoice'); $("#alertok").focus();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); $("#SATABLE010COLUMN10").focus();
                    })
                    $("#divLoading").hide();
                }
                return false
            }
        }
        //browser compatability issue
        //event.preventDefault(); 
        if (flag == false) {
            if ($("#PUTABLE003COLUMN20").val() == '') {
                $("#divLoading").hide();
                var msg = 'Do You Want To Commit ?';
                var div = $("<div>" + msg + "</div>");
                div.dialog({
                    title: "Confirm",
                    buttons: [
                        {
                            text: "Yes", id: "btnok",
                            click: function () {
                                $("#PUTABLE003COLUMN20").prop("checked", true);
                                document.getElementById("PUTABLE003COLUMN20").value = true;
                                div.dialog("close"); $("#divLoading").show();
                                if (flag == false) {
                                    var dataparam = { list: xmldata };
                                    $.ajax({
                                        url: "/Common/SaveNewRow?FormName=" + FormName + "",
                                        type: "POST",
                                        data: JSON.stringify(dataparam),
                                        dataType: "html",
                                        cache: true,
                                        contentType: "application/json; charset=utf-8",
                                        success: function (resultG) {
                                            document.forms[0].submit();
                                        }
                                    });
                                }
                                else {
                                    $("#divLoading").hide();
                                }
                            }
                        },
                             {
                                 text: "No",
                                 click: function () {
                                     div.dialog("close");
                                     if (flag == false) {
                                         $("#divLoading").show();
                                         var dataparam = { list: xmldata };
                                         $.ajax({
                                             url: "/Common/SaveNewRow?FormName=" + FormName + "",
                                             type: "POST",
                                             data: JSON.stringify(dataparam),
                                             dataType: "html",
                                             cache: true,
                                             contentType: "application/json; charset=utf-8",
                                             success: function (resultG) {
                                                 document.forms[0].submit();
                                             }
                                         });
                                     }
                                     else {
                                         $("#divLoading").hide();
                                     }
                                 }
                             }
                    ]
                });
                //EMPHCS947 rajasekhar reddy patakota 14/8/2015 Job Order Receipt - alert message after save is not properly visible
                $(".ui-dialog").css("border", "3px solid");
                $(".ui-dialog").css("padding", "0");
                $(".ui-dialog").find(".ui-dialog-titlebar").css("background", "#666");
                $(".ui-dialog").find(".ui-dialog-titlebar").css("padding", "10px 1.6px");
                $(".ui-dialog").find(".ui-dialog-title").css("color", "white");
                $(".ui-dialog").find(".ui-dialog-title").css("text-align", "center");
                $(".ui-dialog").find(".ui-widget").css("background", "#cceded");
                $(".ui-dialog").find(".ui-widget-content").css("background", "#cceded");
                $(".ui-dialog").find(".ui-widget-content").css("color", "red");
                $(".ui-dialog").find(".ui-widget-content").css("padding", "4px");
                $(".ui-dialog").find(".ui-widget-content").css("text-align","center");
                $(".ui-dialog").find(".ui-dialog-buttonpane").css("background", "#9DD1EA");
                $(".ui-dialog").find(".ui-dialog-buttonpane").css("margin-top", "0em");
                $(".ui-dialog").find(".ui-button-text-only").css("background", "white");
                return false;
            }
        }
        if (flag == false) {
            debugger;
            var dataparam = { list: xmldata };
            $("#divLoading").show();
            $.ajax({
                url: "/Common/SaveNewRow?FormName=" + FormName + "",
                type: "POST",
                data: JSON.stringify(dataparam),
                dataType: "html",
                cache: true,
                contentType: "application/json; charset=utf-8",
                success: function (resultG) {
                    document.forms[0].submit();
                }
            });
        }
        else {
            $("#divLoading").hide();
        }
    }
    else
        if (flag == false) {
            $("#divLoading").show();
            $.ajax({
                url: "/FormBuilding/SaveAction?FormName=" + FormName + "",
                type: "POST",
                dataType: "html",
                cache: true,
                contentType: "application/json; charset=utf-8",
                success: function (resultG) {
                    document.forms[0].submit();
                }
            });
        }
        else {
            $("#divLoading").hide();
        }
};

//Edit Transaction
function EditTransaction(ide, NewCols, FormName, FormId) {
    if (ide != "null") {
        $.ajax({
            url: "/JobberPayments/EditEligibility",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ TransactionID: ide, FormId: FormId }),
            success: function (data) {
                if (FormId == 1288) {
                    saveformEdit(FormName, NewCols, FormId);
                }
                else if (data.Data1 == "" || data.Data1 == null) {
                    saveformEdit(FormName, NewCols, FormId);
                }
                else {
                    Alert.render("Chaild Records Exists! Edit can't be permited."); $("#alertok").focus();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close");
                    });
                }
            }
        });
    }
};
//EMPHCS1587 :Adding Advance Tab to Jobber Payment by gnaneshwar on 4/3/2016
//Delete Transaction
function DeleteTransaction(ide,FormName, FormId) {debugger
    if (ide != "null") {
        $.ajax({
            url: "/JobberPayments/DeletionEligibility",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ TransactionID: ide, FormId: FormId }),
            success: function (data) { debugger
                if (data.Data1 == "" || data.Data1 == null) {
                    if (FormId=="1286")
                        window.location.href = "/JoborderReceipt/Delete?FormName=" + FormName + "";
                    //EMPHCS1784	Job Order for IN Process 15/7/2016
		    else if (FormId == "1284" || FormId == "1587"|| FormId=="1588")
                        window.location.href = "/JobOrderIssue/Delete?FormName=" + FormName + "";
                    else if (FormId == "1288")
                        window.location.href = "/JobberPayments/Delete?FormName=" + FormName + "";
                        //EMPHCS1784	Job Order for IN Process 15/7/2016
                        //EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
                    else if (FormId == "1283" || FormId == "1586" || FormId == "1605")
                        window.location.href = "/JobOrder/Delete?FormName=" + FormName + "";
                    else if (FormId == "1287")
                        window.location.href = "/BilltoJobber/Delete?FormName=" + FormName + "";
                }
                else {
                    Alert.render("Chaild Records Exists! Transaction can't be deleted."); $("#alertok").focus();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close");
                    });
                }
            }
        });
    }
};
//Form EDIT Save action
function saveformEdit(FormName, TableData, FormId) {
    debugger
    var flag = false; var text;
    $('.ddl').each(function () {
        if (flag == true) {
            return false;
        }
        else {
            var textbox = $(this);
            text = $(this).val(); var id = $(this).attr('id'); var aname = $(this).attr('name');
            var mandatory = $(this).attr('itemid');
            var tabname = $(this).attr('title');
            if (text == "") {
                var Amnt = "";
                if (FormId == 1288) {
                    Amnt = $("#SATABLE011COLUMN13").val();
                }
                if ((Amnt == 0 || Amnt == "" || typeof (Amnt) == "undefined") && ((FormId == 1288 && id == "SATABLE011COLUMN08"))) {
                }
                else if (mandatory == 'Y') {
                    $('#tabs').tabs('select', tabname);
                    Alert.render('Please Select Item  for ' + aname + ' '); $("#alertok").focus();
                    $("#alertok").focus();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); textbox.focus();
                    })
                   // textbox.next().html("*");
                    flag = true;
                    return false;
                }
            } else {
               // textbox.next().empty();
            }
        }
    })
    $(".txtintgridclass  ").each(function (index) {
        if (flag == true) {
            return false;
        }
        else {
            var textbox = $(this);
            text = $(this).val(); var id = $(this).attr('id'); var aname = $(this).attr('name');
            var mandatory = $(this).attr('pattern'); var tabname = $(this).attr('title');
            var trwC = $(this).closest("tr");
            if (text == "") {
                if (mandatory == 'Y') {
                    if ($(trwC).find("#checkRow").is(':checked')) {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Values  for ' + aname + ''); $("#alertok").focus();
                        //textbox.next().html("*");
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
            }
            else {
                //textbox.next().empty();
                var value = $(this).val();
                var id = $(this).attr('id');
                var name = $(this).attr('itemid');
                if (name == 'int' || name == 'bigint') {
                    if (value.match(/[0-9 -()+]+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Integer Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'decimal') {
                    if (value.match(/^-{0,1}\d*\.{0,1}\d+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Decimal Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'Quantity') {
                    if (value.match(/^-{0,1}\d*\.{0,1}\d+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter   Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Email') {
                    if (value.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/)) { }
                    else
                    {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Phone') {
                    if (value.match(/^\d{10}$/)) { }
                    else
                    {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + ' No '); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Zip') {
                    if (value.match(/^\d{6}$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + 'No '); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'money') {
                    if (value.match(/^\d+(?:\.\d{0,2})$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter Money Type  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'price') {
                    if (value.match(/^\d+(?:\.\d{0,2})$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter price Type  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
            }
        }
    });
    $(".gridddl").each(function (index) {
        if (flag == true) {
            return false;
        }
        else {
            var textbox = $(this);
            text = $(this).val(); var id = $(this).attr('id'); var aname = $(this).attr('name');
            var mandatory = $(this).attr('itemid'); var tabname = $(this).attr('title');
            var trwC = $(this).closest("tr");
            if (text == "") {
                if (mandatory == 'Y') {

                    if ($(trwC).find("#checkRow").is(':checked')) {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please Select Item  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                       // textbox.next().html("*");
                        flag = true;
                        return false;
                    }
                }
            } else {
               // textbox.next().empty();
            }
        }
    });
    $(".txtclass").each(function (index) {
        if (flag == true) {
            return false;
        }
        else {
            var textbox = $(this);
            text = $(this).val(); var id = $(this).attr('id'); var aname = $(this).attr('name');
            var mandatory = $(this).attr('pattern'); var tabname = $(this).attr('title');
            if (text == "") {
                if (mandatory == 'Y') {
                    $('#tabs').tabs('select', tabname);
                    Alert.render('Please enter values for ' + aname + ''); $("#alertok").focus();
                   // textbox.next().html("*");
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); textbox.focus();
                    })
                    flag = true;
                    return false;
                }
            }
            else {
               // textbox.next().empty();
                var value = $(this).val();
                var id = $(this).attr('id');
                var name = $(this).attr('itemid'); var aname = $(this).attr('name');
                var tabname = $(this).attr('title');
                if (name == 'int' || name == 'bigint') {
                    if (value.match(/[0-9 -()+]+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Integer Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'decimal') {
                    if (value.match(/^-{0,1}\d*\.{0,1}\d+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Decimal Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Email') {
                    if (value.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/)) { }

                    else
                    {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                // Commented by Sudheer on 07/19/15 to bypass 10 digits phone validation.
                /*    if (aname == 'Phone') {
                    if (value.match(/^\d{10}$/)) { }
                    else
                    {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + ' No '); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }*/
                if (aname == 'Zip') {
                    if (value.match(/^\d{6}$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + 'No '); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'money') {
                    if (value.match(/^\d+(?:\.\d{0,2})$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Money Type  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
            }
        }
    });
    $(".txttabClass").each(function (index) {
        if (flag == true) {
            return false;
        }
        else {
            var textbox = $(this);
            text = $(this).val();
            var id = $(this).attr('id'); var aname = $(this).attr('name');
            var mandatory = $(this).attr('pattern'); var tabname = $(this).attr('title');
            if (text == "") {
                if (mandatory == 'Y') {
                    $('#tabs').tabs('select', tabname);
                    Alert.render('Please enter  Values  for ' + aname + ''); $("#alertok").focus();
                  //  textbox.next().html("*");
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); textbox.focus();
                    })
                    flag = true;
                    return false;
                }
            }
            else {
                textbox.next().empty();
                var value = $(this).val();
                var id = $(this).attr('id'); var aname = $(this).attr('name');
                var name = $(this).attr('itemid'); var tabname = $(this).attr('title');
                if (name == 'int' || name == 'bigint') {
                    if (value.match(/[0-9 -()+]+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Integer Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'decimal') {
                    if (value.match(/^-{0,1}\d*\.{0,1}\d+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Decimal Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Email') {
                    if (value.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/)) { }
                    else
                    {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Phone') {
                    if (value.match(/^\d{10}$/)) { }
                    else
                    {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + 'No '); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Zip') {
                    if (value.match(/^\d{6}$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + 'No '); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'money') {
                    if (value.match(/^\d+(?:\.\d{0,2})$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter Money Type for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
            }
        }
    });
    $(".txtgridclass").each(function (index) {
        if (flag == true) {
            return false;
        }
        else {
            var textbox = $(this);
            text = $(this).val(); var id = $(this).attr('id'); var aname = $(this).attr('name');
            var mandatory = $(this).attr('pattern'); var tabname = $(this).attr('title');
            var trwC = $(this).closest("tr");
            if (text == "") {
                if (mandatory == 'Y') {
                    if ($(trwC).find("#checkRow").is(':checked')) {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Values  for ' + aname + ''); $("#alertok").focus();
                        //textbox.next().html("*");
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
            }
            else {
               // textbox.next().empty();
                var value = $(this).val();
                var id = $(this).attr('id');
                var name = $(this).attr('itemid');
                if (name == 'int' || name == 'bigint') {
                    if (value.match(/[0-9 -()+]+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Integer Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'decimal') {
                    if (value.match(/^-{0,1}\d*\.{0,1}\d+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  Decimal Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'Quantity') {
                    if (value.match(/^-{0,1}\d*\.{0,1}\d+$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter   Values  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Email') {
                    if (value.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/)) { }
                    else
                    {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Phone') {
                    if (value.match(/^\d{10}$/)) { }
                    else
                    {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + ' No '); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                if (aname == 'Zip') {
                    if (value.match(/^\d{6}$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter  valid ' + aname + 'No '); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'money') {
                    if (value.match(/^\d+(?:\.\d{0,2})$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter Money Type  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
                else if (name == 'price') {
                    if (value.match(/^\d+(?:\.\d{0,2})$/)) {
                    }
                    else {
                        $('#tabs').tabs('select', tabname);
                        Alert.render('Please enter price Type  for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        flag = true;
                        return false;
                    }
                }
            }
        }
    });
    if ($("#grdData").length > 0) {
        debugger
        var tbl = $("#grdData")[0];
        var xmldata = "<ROOT>"; var Gridlength = tbl.rows.length;
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow]; var ItemQty = 0; var ItemPrice = 0;
            var jarray = TableData;
            if ($(trw).find("#checkRow").is(':checked')) {
                for (var cll = 0; cll < jarray.length; cll++) {
                    var node1 = jarray[cll];
                    var node2 = node1.replace(' ', '_');
                    var node = node2.replace(' ', '_');
                    var itval = $(trw).find("[id='" + node1 + "']").val();
                    if (typeof itval == "undefined")
                    { itval = 0; }
		    //EMPHCS1837 rajasekhar reddy patakota 30/11/2016 Project wise tracking in Opportunity and job order screens
                    itval = itval.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;')
                    .replace(/'/g, '&apos;');
                    if (cll == 0) {
                        //purchase
                        if (FormId == 1273)
                        { ItemQty = $(trw).find("[id='PUTABLE006COLUMN09']").val(); }
                        else if (FormId == 1272)
                        { ItemQty = $(trw).find("[id='PUTABLE004COLUMN08']").val(); }
                        else if (FormId == 1286)
                        {
                            //ItemQty = $(trw).find("[id='PUTABLE004COLUMN08']").val();
                            ItemQty = $(trw).find("[id='PUTABLE004COLUMN06']").val();
                            ItemPrice = $(trw).find("[id='PUTABLE004COLUMN10']").val();
                        }
                        else if (FormId == 1274)
                        { ItemQty = $(trw).find("[id='PUTABLE015COLUMN06']").val(); }
                        else if (FormId == 1251 || FormId == 1374) {
                            ItemQty = $(trw).find("[id='PUTABLE002COLUMN07']").val();
                            ItemPrice = $(trw).find("[id='PUTABLE002COLUMN09']").val();
                        }
                            //sales
                        else if (FormId == 1278 || FormId == 1288)
                        {
                            var Edit = 1;
                            var payment = $(trw).find("#SATABLE012COLUMN06").val();
                            var adv = $(trw).find("#SATABLE012COLUMN09").val();
                            var prepay = $(trw).find("#aSATABLE012COLUMN06").val();
                            var pre = $(trw).find("#aSATABLE012COLUMN06").val();
                            var Dueamt = $(trw).find("#aSATABLE012COLUMN05").val();
                            var padv = $(trw).find("#SATABLE012COLUMN09").val();
                            if (payment == '' || typeof (payment) == "undefined") { payment = 0; } if (adv == '') { adv = 0; }
                            if (prepay == '' || typeof (prepay) == "undefined") { prepay = 0; }
                            if (Dueamt == '' || typeof (Dueamt) == "undefined") { Dueamt = 0; }
                            if (Edit == 1 || Edit == "1") {
                                Dueamt = parseFloat(Dueamt) + parseFloat(prepay);
                                prepay = adv = 0;
                            }
                            if (payment > 0 && !(isNaN(parseFloat(payment).toFixed(2))) && payment.match('^([0-9]+)?(\.[0-9]+)?$')) {
                                payment = parseFloat(payment) + parseFloat(adv);
                                if (parseFloat(payment) > (parseFloat(Dueamt) + parseFloat(prepay))) {
                                    Alert.render('Payment must not exceed available DueAmount.'); $("#alertok").focus();
                                    $("#alertok").on("click", function () {
                                        $("#dialogbox").dialog("close");
                                        Dueamt = parseFloat(Dueamt) - parseFloat(adv);
                                        $(trw).find("#SATABLE012COLUMN06").val((parseFloat(Dueamt) + parseFloat(prepay)).toFixed(2));
                                        $(trw).find("#SATABLE012COLUMN06").focus();
                                        PaymentCalculation();
                                    })
                                    //PaymentCalculation();
                                    return false;
                                }
                            }
                            else {
                                ItemQty = $(trw).find("[id='SATABLE012COLUMN06']").val()
                                var crd = $(trw).find("[id='SATABLE012COLUMN14']").val();
                                var adv = $(trw).find("[id='SATABLE012COLUMN09']").val();
                                var disc = $(trw).find("[id='SATABLE012COLUMN13']").val();
                                ItemPrice = $(trw).find("[id='SATABLE012COLUMN05']").val();
                                if (ItemQty == '') ItemQty = 0;
                                if (crd == '') crd = 0;
                                if (adv == '') adv = 0;
                                if (disc == '') disc = 0;
                                if ((ItemQty == 0) && (parseFloat(crd) + parseFloat(adv) + parseFloat(disc)) == 0) { break; }
                            }
                        }
                        else if (FormId == 1275 || FormId == 1283) {
                            ItemQty = $(trw).find("[id='SATABLE006COLUMN07']").val();
                            ItemPrice = $(trw).find("[id='SATABLE006COLUMN09']").val();
                        }
                        else if (FormId == 1276 || FormId == 1284) {
                            ItemQty = $(trw).find("[id='SATABLE008COLUMN09']").val();
                            if (FormId == 1276 || FormId == 1284) {
                                if (parseFloat($(trw).find("[id='SATABLE008COLUMN09']").val()) > parseFloat($(trw).find("[id='SATABLE008COLUMN10']").val())) {
                                    Alert.render('Quantity should less than available quantity....'); $("#alertok").focus();
                                    $("#alertok").blur("click", function () {
                                        $("#dialogbox").dialog("close"); $(trw).find("[id='SATABLE008COLUMN09']").focus();
                                        $(trw).find("[id='SATABLE008COLUMN09']").val('');
                                    })
                                    $("#divLoading").hide();
                                    return false
                                }
                            }
                        }
                        else if (FormId == 1277 || FormId == 1287)
                        { ItemQty = $(trw).find("[id='SATABLE010COLUMN10']").val(); }
                        else if (FormId == 1381)
                        { ItemQty = $(trw).find("[id='PRTABLE008COLUMN07']").val(); }
                        else if (FormId == 1359 && $(trw).find("[id='FITABLE018COLUMN09']").val() == "" && $(trw).find("[id='FITABLE018COLUMN10']").val() == "")
                        { ItemQty = 0; }
                        else
                        { ItemQty = 1; }
                        if (ItemQty <= 0) {
                            if (FormId == 1278 || FormId == 1274 || FormId == 1286) {
                            }
                            else {
                                break;
                            }
                        }
                        if (FormId == 1251 || FormId == 1374 || FormId == 1275 || FormId == 1283)
                        { if (ItemPrice <= 0) { break; } }
                    }
                    if (cll == 0) {
                        xmldata = xmldata + "<Column><" + node + ">" + itval + "</" + node + ">";
                    }
                    else if (cll == jarray.length - 1) {
                        //EMPHCS942 rajasekhar reddy patakota 14/8/2015 After creating multiple line and after editing / saving , Invoice is getting shown with only one line (for same item with different uom)
                        //xmldata = xmldata + "<"+node+">" + itval + "</"+node+"></Column>";
			//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                        if (FormId == 1587 || FormId == 1286) {
                            var itval1 = $(trw).find("[id='LineID']").val();
			    //EMPHCS1837 rajasekhar reddy patakota 30/11/2016 Project wise tracking in Opportunity and job order screens
                            if (typeof (itval1) == "undefined") itval1 = "";
                            xmldata = xmldata + "<" + node + ">" + itval + "</" + node + "><PUTABLE004COLUMN26>" + itval1 + "</PUTABLE004COLUMN26><checkRow>" + $(trw).find("[id='checkRow']").val() + "</checkRow></Column>";
                        }
                        else {
                            xmldata = xmldata + "<" + node + ">" + itval + "</" + node + "><checkRow>" + $(trw).find("[id='checkRow']").val() + "</checkRow></Column>";
                        }
                        break;
                    }
                    else
                        xmldata = xmldata + "<" + node + ">" + itval + "</" + node + ">";
                }
            }
            //else{Gridlength=Gridlength-1;}
        }
        xmldata = xmldata + "</ROOT>";
        //EMPHCS679 Rajasekhar patakota on 17/7/2015.While Updating Item Without Price it Should allow
        if (FormId != 1261) {
            if (Gridlength == 1) {
                if (xmldata == "<ROOT></ROOT>") {
                    Alert.render('Please select atleast one row from grid view....'); $("#alertok").focus();
                    $("#divLoading").hide();
                    return false
                }
            }
        }
        var xml = $.parseXML(xmldata);
        if (FormId == 1273) {
            var x = Number($(xml).find("PUTABLE006COLUMN09").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PUTABLE006COLUMN09").focus();
                Alert.render('There is no Qty to generate Bill'); $("#alertok").focus();
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1381) {
            var x = Number($(xml).find("PRTABLE008COLUMN07").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PRTABLE008COLUMN06").focus();
                Alert.render('There is no Qty On Hand to consume resource'); $("#alertok").focus();
                $("#divLoading").hide();
                return false
            }
        }

        else if (FormId == 1272) {
            var x = Number($(xml).find("PUTABLE004COLUMN08").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PUTABLE004COLUMN08").focus();
                Alert.render('There is no Qty to Receive the Item'); $("#alertok").focus();
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1354) {
            var x = Number($(xml).find("SATABLE008COLUMN09").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#SATABLE008COLUMN09").focus();
                Alert.render('There is no Qty to Receive the Item'); $("#alertok").focus();
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1358) {
            var x = Number($(xml).find("FITABLE017COLUMN05").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#FITABLE017COLUMN05").focus();
                Alert.render('There is no Amount to Pay'); $("#alertok").focus();
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1330) {
            var x = Number($(xml).find("SATABLE006COLUMN07").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#SATABLE006COLUMN07").focus();
                Alert.render('Please give quantity greater than zero....'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $("#SATABLE006COLUMN07").focus();
                })
                $("#divLoading").hide();
                return false
            }
            var y = Number($(xml).find("SATABLE006COLUMN09").text());
            if (y == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#SATABLE006COLUMN09").focus();
                Alert.render('Please give price greater than zero....'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); textbox.focus();
                })
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1329) {
            var x = Number($(xml).find("PUTABLE004COLUMN08").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PUTABLE004COLUMN08").focus();
                Alert.render('There is no Qty to Receive the Item'); $("#alertok").focus();
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1274) {
            var x = Number($(xml).find("PUTABLE015COLUMN06").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PUTABLE015COLUMN06").focus();
                Alert.render('There Is No Payment or no amount For Selected PONO....'); $("#alertok").focus();
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1286) {
            var x = Number($(xml).find("PUTABLE004COLUMN10").text());
            if (x == 0) {
                //$('#tabs').tabs('select', "#Item");
                //$("#PUTABLE004COLUMN10").focus();
                //Alert.render('Please give price greater than zero....'); $("#alertok").focus();
                //$("#alertok").on("click", function () {
                //    $("#dialogbox").dialog("close"); $("#PUTABLE004COLUMN10").focus();
                //})
                //$("#divLoading").hide();
                //return false
            }
            var y = Number($(xml).find("PUTABLE004COLUMN08").text());
            if (y == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PUTABLE004COLUMN08").focus();
                Alert.render('Please give quantity greater than zero....'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $("#PUTABLE004COLUMN08").focus();
                })
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1251) {
            var x = Number($(xml).find("PUTABLE002COLUMN07").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PUTABLE002COLUMN07").focus();
                Alert.render('Please give quantity greater than zero....'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $("#PUTABLE002COLUMN07").focus();
                })
                $("#divLoading").hide();
                return false
            }
            var y = Number($(xml).find("PUTABLE002COLUMN09").text());
            if (y == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PUTABLE002COLUMN09").focus();
                Alert.render('Please give price greater than zero....'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $("#PUTABLE002COLUMN09").focus();
                })
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1328) {
            var x = Number($(xml).find("PUTABLE002COLUMN07").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PUTABLE002COLUMN07").focus();
                Alert.render('Please give quantity greater than zero....'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $("#PUTABLE002COLUMN07").focus();
                })
                $("#divLoading").hide();
                return false
            }
            var y = Number($(xml).find("PUTABLE002COLUMN09").text());
            if (y == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#PUTABLE002COLUMN09").focus();
                Alert.render('Please give price greater than zero....'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $("#PUTABLE002COLUMN09").focus();
                })
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1278 || FormId == 1288) {
            for (var i = 0; i < $(xml).find("Column").length; i++) {
                var x = Number($(xml).find("SATABLE012COLUMN06").eq(i).text());
                var y = Number($(xml).find("SATABLE012COLUMN14").eq(i).text());
                var z = Number($(xml).find("SATABLE012COLUMN05").eq(i).text());
                var d = Number($(xml).find("SATABLE012COLUMN13").eq(i).text());
                var W = Number($(xml).find("SATABLE012COLUMN09").eq(i).text());
                if (x == '' || typeof (x) == "undefined") { x = 0 }
                if (y == '' || typeof (y) == "undefined") { y = 0 }
                if (z == '' || typeof (z) == "undefined") { z = 0 }
                if (W == '' || typeof (W) == "undefined") { W = 0 }
                if (d == '' || typeof (d) == "undefined") { d = 0 }
                if ((x == 0) && (parseFloat(y) + parseFloat(W) + parseFloat(d)) == 0) {
                    $('#tabs').tabs('select', "#Item");
                    $("#SATABLE012COLUMN06").focus();
                    Alert.render('There Is No Payment or no amount For Selected SONO....'); $("#alertok").focus();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); $("#SATABLE012COLUMN06").focus();
                    })
                    $("#divLoading").hide();
                    return false
                }
                else if (((x == 0) && (z == 0) && (W == 0) && (d == 0))) {
                    $('#tabs').tabs('select', "#Item");
                    $("#SATABLE012COLUMN06").focus();
                    Alert.render('There Is No Payment or no amount For Selected SONO....'); $("#alertok").focus();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); $("#SATABLE012COLUMN06").focus();
                    })
                    $("#divLoading").hide();
                    return false
                }
            }
            //var x = Number($(xml).find("SATABLE012COLUMN06").text());
            //if (x == 0) {
            //    $('#tabs').tabs('select', "#Item");
            //    $("#SATABLE012COLUMN06").focus();
            //    Alert.render('There Is No Payment or no amount For Selected SONO....'); $("#alertok").focus();
            //    $("#alertok").on("click", function () {
            //        $("#dialogbox").dialog("close"); $("#SATABLE012COLUMN06").focus();
            //    })
            //    $("#divLoading").hide();
            //    return false
            //}
        }
        else if (FormId == 1275 || FormId == 1283) {
            var x = Number($(xml).find("SATABLE006COLUMN07").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#SATABLE006COLUMN07").focus();
                Alert.render('Please give quantity greater than zero....'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $("#SATABLE006COLUMN07").focus();
                })
                $("#divLoading").hide();
                return false
            }
            var y = Number($(xml).find("SATABLE006COLUMN09").text());
            if (y == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#SATABLE006COLUMN09").focus();
                Alert.render('Please give price greater than zero....'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); textbox.focus();
                })
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1276 || FormId == 1284) {
            //if (parseFloat($("#SATABLE008COLUMN09").val()) > parseFloat($("#SATABLE008COLUMN10").val())) {
            //    Alert.render('Quantity should less than available quantity....'); $("#alertok").focus();
            //    $("#alertok").blur("click", function () {
            //        $("#dialogbox").dialog("close"); $("#SATABLE008COLUMN09").focus();
            //        $("#SATABLE008COLUMN09").val('');
            //    })
            //    $("#divLoading").hide();
            //    return false
            //}
            var x = Number($(xml).find("SATABLE008COLUMN09").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#SATABLE008COLUMN09").focus();
                Alert.render('There is no Qty to Issue the Item'); $("#alertok").focus();
                $("#alertok").blur("click", function () {
                    $("#dialogbox").dialog("close"); $("#SATABLE008COLUMN09").focus();
                })
                $("#divLoading").hide();
                return false
            }
        }
        else if (FormId == 1277 || FormId == 1287) {
            if ($("#SATABLE010COLUMN04").val() == "") {
                if (parseInt($("#SATABLE010COLUMN10").val()) > parseInt($("#SATABLE010COLUMN11").val())) {
                    Alert.render('Quantity Must not greater than Avl Qty'); $("#alertok").focus();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); $("#SATABLE010COLUMN10").focus();
                    })
                    $("#divLoading").hide();
                    return false
                }
            }
            var x = Number($(xml).find("SATABLE010COLUMN10").text());
            if (x == 0) {
                $('#tabs').tabs('select', "#Item");
                $("#SATABLE010COLUMN10").focus();
                if (FormId == 1287) {
                    Alert.render('There is no Qty to Genarate Bill'); $("#alertok").focus();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); $("#SATABLE010COLUMN10").focus();
                    })
                    $("#divLoading").hide();
                }
                else {
                    Alert.render('There is no Qty to Genarate Invoice'); $("#alertok").focus();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); $("#SATABLE010COLUMN10").focus();
                    })
                    $("#divLoading").hide();
                }
                return false
            }
        }
        //browser compatability issue
        //event.preventDefault(); 
        if (flag == false) {
            if ($("#PUTABLE003COLUMN20").val() == '') {
                $("#divLoading").hide();
                var msg = 'Do You Want To Commit ?';
                var div = $("<div>" + msg + "</div>");
                div.dialog({
                    title: "Confirm",
                    buttons: [
                        {
                            text: "Yes", id: "btnok",
                            click: function () {
                                $("#PUTABLE003COLUMN20").prop("checked", true);
                                document.getElementById("PUTABLE003COLUMN20").value = true;
                                div.dialog("close"); $("#divLoading").show();
                                if (flag == false) {
                                    var dataparam = { list: xmldata };
                                    $.ajax({
                                        url: "/JobOrder/SaveNewRow?FormName=" + FormName + "",
                                        type: "POST",
                                        data: JSON.stringify(dataparam),
                                        dataType: "html",
                                        cache: true,
                                        contentType: "application/json; charset=utf-8",
                                        success: function (resultG) {
                                            document.forms[0].submit();
                                        }
                                    });
                                }
                                else {
                                    $("#divLoading").hide();
                                }
                            }
                        },
                             {
                                 text: "No",
                                 click: function () {
                                     div.dialog("close");
                                     if (flag == false) {
                                         $("#divLoading").show();
                                         var dataparam = { list: xmldata };
                                         $.ajax({
                                             url: "/JobOrder/SaveNewRow?FormName=" + FormName + "",
                                             type: "POST",
                                             data: JSON.stringify(dataparam),
                                             dataType: "html",
                                             cache: true,
                                             contentType: "application/json; charset=utf-8",
                                             success: function (resultG) {
                                                 document.forms[0].submit();
                                             }
                                         });
                                     }
                                     else {
                                         $("#divLoading").hide();
                                     }
                                 }
                             }
                    ]
                });
                return false;
            }
        }
        if (flag == false) {
            //to display loader giff in edit save done by srinivas
            $("#divLoading").show();
            var dataparam = { list: xmldata };
            $.ajax({
                url: "/JobOrder/SaveNewRow?FormName=" + FormName + "",
                type: "POST",
                data: JSON.stringify(dataparam),
                dataType: "html",
                cache: true,
                contentType: "application/json; charset=utf-8",
                success: function (resultG) {
                    document.forms[0].submit();
                }
            });
        }
        else {
            $("#divLoading").hide();
        }
    }
    else
        if (flag == false) {
            //to display loader giff in edit save done by srinivas
            $("#divLoading").show();
            $.ajax({
                url: "/JobOrder/SaveNewRow?FormName=" + FormName + "",
                type: "POST",
                dataType: "html",
                cache: true,
                contentType: "application/json; charset=utf-8",
                success: function (resultG) {
                    document.forms[0].submit();
                }
            });
        }
        else {
            $("#divLoading").hide();
        }
};
//new row click 
function newrowclickevent(FormID, FormName) {
    debugger;
    var tbl = $('#grdData')[0];
    var irow = tbl.rows.length - 1
    var tr = tbl.rows[irow];
    var flag = false; var text;
    if ($(tr).find("#checkRow").is(':checked')) {
        $(".gridddl").each(function (index) {
            if (flag == true) {
                return false;
            }
            else {
                var textbox = $(this);
                text = $(this).val(); var id = $(this).attr('id'); var aname = $(this).attr('name');
                var mandatory = $(this).attr('itemid');
                if (text == "") {
                    if (mandatory == 'Y') {
                        Alert.render('Please  Select Item for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                       // textbox.next().html("*");
                        flag = true;
                        return false;
                    }
                } else {
                   // textbox.next().empty();
                }
            }
        });
        $(".txtgridclass").each(function (index) {
            if (flag == true) {
                return false;
            }
            else {
                var textbox = $(this);
                text = $(this).val(); var id = $(this).attr('id'); var aname = $(this).attr('name');
                var mandatory = $(this).attr('pattern');
                if (text == "") {
                    if (mandatory == 'Y') {
                        Alert.render('Please enter Values for ' + aname + ''); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close"); textbox.focus();
                        })
                        //textbox.next().html("*");
                        flag = true;
                        return false;
                    }
                }
                else {
                    textbox.next().remove();
                    var value = $(this).val();
                    var id = $(this).attr('id'); var aname = $(this).attr('name');
                    var name = $(this).attr('itemid');
                    if (name == 'int' || name == 'bigint') {
                        if (value.match(/[0-9 -()+]+$/)) {
                        }
                        else {
                            Alert.render('Please enter Integer Values for ' + aname + ''); $("#alertok").focus();
                            $("#alertok").on("click", function () {
                                $("#dialogbox").dialog("close"); textbox.focus();
                            })
                            flag = true;
                            return false;
                        }
                    }
                    else if (name == 'decimal') {
                        if (value.match(/^-{0,1}\d*\.{0,1}\d+$/)) {
                        }
                        else {
                            Alert.render('Please enter Decimal Values for ' + aname + ''); $("#alertok").focus();
                            $("#alertok").on("click", function () {
                                $("#dialogbox").dialog("close"); textbox.focus();
                            })
                            flag = true;
                            return false;
                        }
                    } else if (name == 'money') {
                        if (value.match(/^\d+(?:\.\d{0,2})$/)) {
                        }
                        else {
                            Alert.render('Please enter Money Type for ' + aname + ''); $("#alertok").focus();
                            $("#alertok").on("click", function () {
                                $("#dialogbox").dialog("close"); textbox.focus();
                            })
                            flag = true;
                            return false;
                        }
                    }
                }
            }
        });
        $(".txtintgridclass  ").each(function (index) {
            if (flag == true) {
                return false;
            }
            else {
                var textbox = $(this);
                text = $(this).val(); var id = $(this).attr('id'); var aname = $(this).attr('name');
                var mandatory = $(this).attr('pattern'); var tabname = $(this).attr('title');
                var trwC = $(this).closest("tr");
                if (text == "") {
                    if (mandatory == 'Y') {
                        if ($(trwC).find("#checkRow").is(':checked')) {
                            $('#tabs').tabs('select', tabname);
                            Alert.render('Please enter  Values  for ' + aname + ''); $("#alertok").focus();
                            //textbox.next().html("*");
                            $("#alertok").on("click", function () {
                                $("#dialogbox").dialog("close"); textbox.focus();
                            })
                            flag = true;
                            return false;
                        }
                    }
                }
                else {
                   // textbox.next().empty();
                    var value = $(this).val();
                    var id = $(this).attr('id');
                    var name = $(this).attr('itemid');
                    if (name == 'int' || name == 'bigint') {
                        if (value.match(/[0-9 -()+]+$/)) {
                        }
                        else {
                            $('#tabs').tabs('select', tabname);
                            Alert.render('Please enter  Integer Values  for ' + aname + ''); $("#alertok").focus();
                            $("#alertok").on("click", function () {
                                $("#dialogbox").dialog("close"); textbox.focus();
                            })
                            flag = true;
                            return false;
                        }
                    }
                    else if (name == 'decimal') {
                        if (value.match(/^-{0,1}\d*\.{0,1}\d+$/)) {
                        }
                        else {
                            $('#tabs').tabs('select', tabname);
                            Alert.render('Please enter  Decimal Values  for ' + aname + ''); $("#alertok").focus();
                            $("#alertok").on("click", function () {
                                $("#dialogbox").dialog("close"); textbox.focus();
                            })
                            flag = true;
                            return false;
                        }
                    }
                    else if (name == 'Quantity') {
                        if (value.match(/^-{0,1}\d*\.{0,1}\d+$/)) {
                        }
                        else {
                            $('#tabs').tabs('select', tabname);
                            Alert.render('Please enter   Values  for ' + aname + ''); $("#alertok").focus();
                            $("#alertok").on("click", function () {
                                $("#dialogbox").dialog("close"); textbox.focus();
                            })
                            flag = true;
                            return false;
                        }
                    }
                    if (aname == 'Email') {
                        if (value.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/)) { }
                        else
                        {
                            $('#tabs').tabs('select', tabname);
                            Alert.render('Please enter  valid ' + aname + ''); $("#alertok").focus();
                            $("#alertok").on("click", function () {
                                $("#dialogbox").dialog("close"); textbox.focus();
                            })
                            flag = true;
                            return false;
                        }
                    }
                    if (aname == 'Phone') {
                        if (value.match(/^\d{10}$/)) { }
                        else
                        {
                            $('#tabs').tabs('select', tabname);
                            Alert.render('Please enter  valid ' + aname + ' No '); $("#alertok").focus();
                            $("#alertok").on("click", function () {
                                $("#dialogbox").dialog("close"); textbox.focus();
                            })
                            flag = true;
                            return false;
                        }
                    }
                    if (aname == 'Zip') {
                        if (value.match(/^\d{6}$/)) {
                        }
                        else {
                            $('#tabs').tabs('select', tabname);
                            Alert.render('Please enter  valid ' + aname + 'No '); $("#alertok").focus();
                            $("#alertok").on("click", function () {
                                $("#dialogbox").dialog("close"); textbox.focus();
                            })
                            flag = true;
                            return false;
                        }
                    }
                    else if (name == 'money') {
                        if (value.match(/^\d+(?:\.\d{0,2})$/)) {
                        }
                        else {
                            $('#tabs').tabs('select', tabname);
                            Alert.render('Please enter Money Type  for ' + aname + ''); $("#alertok").focus();
                            $("#alertok").on("click", function () {
                                $("#dialogbox").dialog("close"); textbox.focus();
                            })
                            flag = true;
                            return false;
                        }
                    }
                    else if (name == 'price') {
                        if (value.match(/^\d+(?:\.\d{0,2})$/)) {
                        }
                        else {
                            $('#tabs').tabs('select', tabname);
                            Alert.render('Please enter price Type  for ' + aname + ''); $("#alertok").focus();
                            $("#alertok").on("click", function () {
                                $("#dialogbox").dialog("close"); textbox.focus();
                            })
                            flag = true;
                            return false;
                        }
                    }
                }
            }
        });
    }
    var Edit = 0; var micode = 0;
    var eirow = tbl.rows.length; var rq = 0; var bq = 0;
    for (var erow = 1; erow < eirow; erow++) {
        var etr = tbl.rows[erow];
        var erowval = 0;
        if (FormID == 1251) { erowval = $(etr).find("[id='PUTABLE002COLUMN03']").val(); }
        else if (FormID == 1273) { erowval = $(etr).find("[id='PUTABLE006COLUMN04']").val(); }
        else if (FormID == 1275 || FormID == 1283) { erowval = $(etr).find("[id='SATABLE006COLUMN03']").val(); }
        else if (FormID == 1277 || FormID == 1287) { erowval = $(etr).find("[id='SATABLE010COLUMN05']").val(); }
        else if (FormID == 1357) { erowval = $(etr).find("[id='FITABLE015COLUMN03']").val(); }
        if (FormID != 1285) {
            if (erow == 1) {
                Edit = erowval;
            }
            else {
                Edit += "," + erowval;
            }
        }
    }
    micode = tr.cells[1].children[0].value;
    var payeetype = $('#FITABLE020COLUMN11').val();
    if (FormID == 1293)
        payeetype = '4113';
    if (flag == false) {
        $.ajax({
            url: "/Common/NewRow?FormName=" + FormName + "",
            type: "POST",
            data: JSON.stringify({ Edit: Edit, micode: micode, Payee: payeetype }),
            dataType: "json",
            cache: true,
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                $('#grdData').append(result.Data);
                if (FormID == 1285) {
                    var tbl = $('#grdData')[0];
                    for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                        var trw = tbl.rows[irow];
                        $(trw).find("#MATABLE007COLUMN05").val('ITTY008');
                    }
                }
                var t = $('#grdData')[0];
                var irow = t.rows.length - 1
                var trw = t.rows[irow];
                if (FormID == 1605) {
                 
                        $i(trw).find("#PUTABLE023COLUMN03").select2();

                    }
                    else if (FormID == 1586) {
                        $i(trw).find("#PUTABLE002COLUMN03").select2();
                    }
                    else if (FormID == 1283) {

                        $i(trw).find("#SATABLE006COLUMN03").select2();
                    }
                    else if (FormID == 1286) {

                        $i(trw).find("#PUTABLE004COLUMN03").select2();
                    }
                    dateupdate();
                    EnableDisable(FormID);
                    $i(trw).find("#UPCSearch").focus();
                }
            });
    
    }
};
//EMPHCS949	Job Order Payment - allowing more amount then total amount,after save no record in grid BY RAJ.Jr 14/8/2015
//jobber Payment Calculations
//function SOPaymentCalculations(tr) {
//    var discount = 0;
//    var totalAmount = $(tr).find("#SATABLE012COLUMN04").val();
//    var Advance=$(tr).find("#SATABLE012COLUMN09").val();
//    var payment = $(tr).find("#SATABLE012COLUMN06").val();
//    var Dueamt = $(tr).find("#aSATABLE012COLUMN05").val();
//    if (payment == '') { payment = 0; } if (totalAmount == '') { totalAmount = 0; }
//    if (Dueamt == '') { Dueamt = 0; } if (discount == '') { discount = 0; }
//    if (Advance == '') { Advance = 0; }
//    if (payment > 0 && !(isNaN(parseFloat(payment).toFixed(2))) && payment.match('^([0-9]+)?(\.[0-9]+)?$')) {
//        payment = parseFloat(payment) + parseFloat(Advance) ;
//        if (parseFloat(payment) > parseFloat(Dueamt)) {
//            Alert.render('Payment must not exceed available DueAmount.'); $("#alertok").focus();
//            $("#alertok").on("click", function () {
//                $("#dialogbox").dialog("close");
//                Dueamt = parseFloat(Dueamt) - parseFloat(Advance);
//                $(tr).find("#SATABLE012COLUMN06").val(Dueamt.toFixed(2));
//                $(tr).find("#SATABLE012COLUMN06").focus();
//            })
//            return false;
//        }
//        else {
//        }
//    }
//    else {
//        Alert.render('Please Enter Value Greater Than Zero Only'); $("#alertok").focus();
//        $("#alertok").on("click", function () {
//            $("#dialogbox").dialog("close");
//            $(tr).find("#SATABLE012COLUMN06").val(''); $(tr).find("#SATABLE012COLUMN06").focus();
//        })
//        return false;
//    }
//}
function SOPaymentCalculations(tr, Edit, FromID) {
    debugger
    var payment = $(tr).find("#SATABLE012COLUMN06").val();
    var adv = $(tr).find("#SATABLE012COLUMN09").val();
    var prepay = $(tr).find("#aSATABLE012COLUMN06").val();
    var pre = $(tr).find("#aSATABLE012COLUMN06").val();
    var Dueamt = $(tr).find("#aSATABLE012COLUMN05").val();
    var padv = $(tr).find("#SATABLE012COLUMN09").val();
    if (payment == '' || typeof (payment) == "undefined") { payment = 0; }  if (adv == '') { adv = 0; }
    if (prepay == '' || typeof (prepay) == "undefined") { prepay = 0; }
    if (Dueamt == '' || typeof (Dueamt) == "undefined") { Dueamt = 0; } 
    if (Edit == 1 || Edit == "1") {
        Dueamt = parseFloat(Dueamt) + parseFloat(prepay) ;
        prepay = adv = 0;
    }
    if (payment > 0 && !(isNaN(parseFloat(payment).toFixed(2))) && payment.match('^([0-9]+)?(\.[0-9]+)?$')) {
        //payment = parseFloat(Dueamt) - (parseFloat(credit) + parseFloat(adv));
        payment = parseFloat(payment) + parseFloat(adv);
        if (parseFloat(payment) > (parseFloat(Dueamt) + parseFloat(prepay))) {
            Alert.render('Payment must not exceed available DueAmount.'); $("#alertok").focus();
            $("#alertok").on("click", function () {
                $("#dialogbox").dialog("close");
                Dueamt = parseFloat(Dueamt)  - parseFloat(adv);
                $(tr).find("#SATABLE012COLUMN06").val((parseFloat(Dueamt) + parseFloat(prepay)).toFixed(2));
                $(tr).find("#SATABLE012COLUMN06").focus();
                PaymentCalculation();
            })
            //PaymentCalculation();
            return false;
        }
        else {
            if (Edit == 1 || Edit == "1") {
                var billID = $(tr).find("#SATABLE012COLUMN03").val();
                var prepay1; var total; var count;
                $.ajax({
                    url: "/JobberPayments/GetPrePay?FormName=" + FormName + "",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify({ billID: billID, FromID: FromID }),
                    success: function (data) {debugger
                        if (data != "") {
                            debugger
                            prepay1 = data.Data1;
                            count = data.Data2;
                            var tot = $(tr).find("#SATABLE012COLUMN04").val();
                            var pay = $(tr).find("#SATABLE012COLUMN06").val();
                            if (pay == '' || typeof (pay) == "undefined") { pay = 0; }
                            if (padv == '' || typeof (padv) == "undefined") { padv = 0; }
                            if (prepay1 == '' || typeof (prepay1) == "undefined") { prepay1 = 0; }
                            if (count > 1) {
                                total = (parseFloat(prepay1) - parseFloat(pre) + parseFloat(pay)).toFixed(2)
                                if (total > parseFloat(tot).toFixed(2)) {
                                    Alert.render('Payment must not exceed available DueAmount.'); $("#alertok").focus();
                                    $("#alertok").on("click", function () {
                                        $("#dialogbox").dialog("close");
                                    $(tr).find("#SATABLE012COLUMN06").val(parseFloat(pre).toFixed(2));
                                    $("#SATABLE011COLUMN13").val(parseFloat(pre).toFixed(2));
                                })
                                 return false;
                                }
                                else {
                                    if (parseFloat(tot).toFixed(2) == total)
                                        $(tr).find("#SATABLE012COLUMN05").val((parseFloat(tot)).toFixed(2));
                                    else
                                        $(tr).find("#SATABLE012COLUMN05").val((parseFloat(tot) - (parseFloat(total))).toFixed(2));
                                }
                            }
                            else {
                                total = (parseFloat(padv) + parseFloat(pay)).toFixed(2)
                                if (parseFloat(tot).toFixed(2) == total)
                                    $(tr).find("#SATABLE012COLUMN05").val((parseFloat(tot)).toFixed(2));
                                else
                                    $(tr).find("#SATABLE012COLUMN05").val((parseFloat(tot) - (parseFloat(padv) + parseFloat(pay))).toFixed(2));
                            }
                        }
                    }
                })
            }
        }
    }
    else {
        Alert.render('Please Enter Value Greater Than Zero Only'); $("#alertok").focus();
        $("#alertok").on("click", function () {
            $("#dialogbox").dialog("close");
            $(tr).find("#SATABLE012COLUMN06").val(''); $(tr).find("#SATABLE012COLUMN06").focus();
            $("#SATABLE011COLUMN13").val('');
            PaymentCalculation();
        })
        //PaymentCalculation();
        if (Edit == 1 || Edit == "1") {
            var billID = $(tr).find("#SATABLE012COLUMN03").val();
            var prepay1; var total; var count;
            $.ajax({
                url: "/JobberPayments/GetPrePay?FormName=" + FormName + "",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({ billID: billID, FromID: FromID }),
                success: function (data) {debugger
                    if (data != "") {
                        debugger
                        prepay1 = data.Data1
                        count = data.Data2;
                        var tot = $(tr).find("#SATABLE012COLUMN04").val();
                        var pay = $(tr).find("#SATABLE012COLUMN06").val();
                        if (pay == '' || typeof (pay) == "undefined") { pay = 0; }
                        if (padv == '' || typeof (padv) == "undefined") { padv = 0; }
                        if (prepay1 == '' || typeof (prepay1) == "undefined") { prepay1 = 0; }
                        if (count > 1) {
                            total = (parseFloat(prepay1) - parseFloat(pre) - parseFloat(pay)).toFixed(2)
                            if (parseFloat(tot).toFixed(2) == total)
                                $(tr).find("#SATABLE012COLUMN05").val((parseFloat(tot)).toFixed(2));
                            else
                                $(tr).find("#SATABLE012COLUMN05").val((parseFloat(tot) - (parseFloat(total))).toFixed(2));
                        }
                        else {
                            total = ( parseFloat(padv) + parseFloat(pay)).toFixed(2)
                            if (parseFloat(tot).toFixed(2) == total)
                                $(tr).find("#SATABLE012COLUMN05").val((parseFloat(tot)).toFixed(2));
                            else
                                $(tr).find("#SATABLE012COLUMN05").val((parseFloat(tot) - ( parseFloat(padv) + parseFloat(pay))).toFixed(2));
                        }
                    }
                }
            })
        }
        return false;
    }
    //EMPHCS1391	Single Header For Vendor and Customer Payment Srinivas 26/11/2015
    PaymentCalculation();
}
function PaymentCalculation() {debugger
    var tbl = $("#grdData")[0]; var amt = 0; var tqty = 0;
    for (var irow = 1; irow < tbl.rows.length  ; irow++) {
        var trw = tbl.rows[irow];
        if ($(trw).find("#checkRow").is(':checked')) {
            amt = $(trw).find("[id='SATABLE012COLUMN06']").val();
            if (amt == "" || isNaN(parseFloat(amt))) { amt = 0; }
            tqty += parseFloat(amt);
        }
    }
    var balance = $("#SATABLE011COLUMN13").val();
    $("#SATABLE011COLUMN13").val((tqty).toFixed(2));
    }
//cancel row click
function cancelrowclickevent(FormID) {
    var tbl = $("#grdData")[0];
    if (tbl.rows.length > 2) {
        $('#grdData > tbody:last tr:last').remove();
        Alert.render('one row deleted......'); $("#alertok").focus();
        //EMPHCS684 Rajasekhar Patakota 18/7/2015 Quantity Calculation Changes
        var tr = tbl.rows[tbl.rows.length - 1];
        if (FormID == "1251" || FormID == "1355") {
            var price = $i(tr).find("[id*=PUTABLE002COLUMN07]").val();
            POLineQuantityCalculation(tr, price);
        }
        else if (FormID == "1273") {
            POBillTaxCalculations(tr);
        }
        else if (FormID == "1275"||FormID == "1283") {
            SOLineQuantityTaxCalculation(tr);
        }
        else if (FormID == "1277") {
            SOInvoiceTaxCalculations(tr);
        }
        else if (FormID == "1353") {
            SOEditLineTax(1353);
            SOLineQuantityTaxCalculation(tr);
        }
        else if (FormID == "1355") {
            SOEditLineTax(1355);
        }
        else if (FormID == "1328") {
            POLineQuantityCalculation(tr);
        }
        else if (FormID == "1330") {
            SOLineQuantityTaxCalculation(tr);
        }
    }
    else {
        Alert.render('Cancel Row permited, when more than one row exists in Grid......'); $("#alertok").focus();
        return false;
    }
}

function GRNtoJobOrderIssueIN(eve, FormName, FormId) {
    var JOID = eve;
    if (eve != "null") {
        $.ajax({
            url: "/JobOrderIssue/JORddlSelectGrn?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ JOID: JOID }),
            success: function (data) {
                if (data != "") {

                    ddlSelectII(data.pid, FormName, FormId);
                }
            }
        });
    }
};
//Job Order Issue

//Job Order Item Issue job Order ddl Selection
function ddlSelectII(eve, FormName, FormId) {
    debugger
    var SalesOrderID = eve;
    if (eve != "null") {
        if (FormId == 1588) {
            $.ajax({
                url: "/JobOrderIssue/getHeaderJOII?FormName=" + FormName + "",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({ SalesOrderID: SalesOrderID }),
                success: function (data) {
                    if (data != "") {
                        $("#SATABLE007COLUMN06").val(data.pid);
                        $("#SATABLE007COLUMN05").val(data.Data14);
                        $("#select2-SATABLE007COLUMN05-container").text(data.cName);
                        $("#SATABLE007COLUMN07").val(data.Data);
                        $("#SATABLE007COLUMN09").val(data.Data1);
                        $("#SATABLE007COLUMN12").val(data.Data2);
                        //$("#SATABLE007COLUMN13").val(data.Data3);
                        $("#SATABLE007COLUMN14").val(data.DataS);
                        $("#SATABLE007COLUMN15").val(data.Data4);
                        $("#SATABLE007COLUMN16").val(data.Data5);
                        $("#SATABLE007COLUMN17").val(data.Data6);
                        $("#SATABLE007COLUMN18").val(data.Data7);
                        $("#SATABLE007COLUMN20").val(data.Data8);
                        $("#SATABLE007COLUMN21").val(data.Data15);
                        $("#SATABLE007COLUMN22").val(data.Data16);
                        $('#grdData').remove();
                        $('#itemgrid').html(data.grid);
                        $('#grdData').append(data.remain);
                        //EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
                        var s = 1;
                        var tbl = $("#grdData")[0];
                        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                            var trw = tbl.rows[irow];
                            $(trw).find("#SATABLE008COLUMN27").val(s);
                            $(trw).find("#SATABLE008COLUMN27").prop("readonly",true);
                            s = s + 1;
                        }
                        dateupdate(); EnableDisable(FormId);
                    }
                }
            });
        }
        else {
            $.ajax({
                url: "/JobOrderIssue/getHeaderII?FormName=" + FormName + "",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify({ SalesOrderID: SalesOrderID }),
                success: function (data) {
                    if (data != "") {
                        $("#SATABLE007COLUMN06").val(data.pid);
                        $("#SATABLE007COLUMN05").val(data.Data14);
                        $("#select2-SATABLE007COLUMN05-container").text(data.cName);
                        $("#SATABLE007COLUMN07").val(data.Data);
                        $("#SATABLE007COLUMN09").val(data.Data1);
                        $("#SATABLE007COLUMN12").val(data.Data2);
                        //$("#SATABLE007COLUMN13").val(data.Data3);
                        $("#SATABLE007COLUMN14").val(data.DataS);
                        $("#SATABLE007COLUMN15").val(data.Data4);
                        $("#SATABLE007COLUMN16").val(data.Data5);
                        $("#SATABLE007COLUMN17").val(data.Data6);
                        $("#SATABLE007COLUMN18").val(data.Data7);
                        $("#SATABLE007COLUMN20").val(data.Data8);
                        $("#SATABLE007COLUMN21").val(data.Data15);
                        $("#SATABLE007COLUMN22").val(data.Data16);
                        $('#grdData').remove();
                        $('#itemgrid').html(data.grid);
                        $('#grdData').append(data.remain);
                        dateupdate(); EnableDisable(FormId);
                    }
                }
            });
        }
    }
};

//Sales Order Item Issue Line level Qunatity
function ItemIssueQuantity(tr) {
    debugger;
    var aqty = $(tr).find("#aSATABLE008COLUMN09").val();
    var qty = $(tr).find("#SATABLE008COLUMN09").val();
    var rqty = $(tr).find("#SATABLE008COLUMN11").val();
    var avlqty = $(tr).find("#SATABLE008COLUMN10").val();
    var price = $(tr).find("#SATABLE008COLUMN12").val();
    if (qty == '') { qty = 0; }
    if (price == '') { price = 0; }
    //EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
    var Backorder="";
    $.ajax({
        url: "/JobOrderIssue/IssueAvlCheckQty",
        type: "POST",
        dataType: "json", cache: true,
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if (data.AcOwner == "True" || data.AcOwner == "1") { Backorder = "True"; }
        }
    });
    //EMPHCS760 Rajasekhar reddy patakota 23/7/2015 Item issue remaining Qty calculation
    if (parseFloat(qty) > 0 && !(isNaN(parseFloat(qty).toFixed(2)))) {
        if ((parseFloat(qty) > (parseFloat(aqty))) || (parseFloat(qty) > (parseFloat(avlqty)))) {
            var edit = window.location.href.slice(window.location.href).split("?");
            var editval = edit[0].split("/").indexOf("Edit", 0);
            if (parseFloat(editval) > 0) { aqty = parseFloat(aqty) + parseFloat(rqty); }
            if (parseFloat(qty) > (parseFloat(aqty))) {
	    //EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                if ((parseFloat(qty) > (parseFloat(avlqty))) && (parseFloat(avlqty) < (parseFloat(aqty))) && Backorder != "True") {
                    Alert.render('Qty must not exceed Available quantity'); $("#alertok").focus();
                    $(tr).find("#SATABLE008COLUMN09").val(avlqty);
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); $(tr).find("#SATABLE008COLUMN09").focus();
                        $(tr).find("#SATABLE008COLUMN13").val(parseFloat(price) * parseFloat(avlqty));
                        return false;
                    })
                    return false;
                }
                else {
                    Alert.render('Qty must not exceed Ordered quantity'); $("#alertok").focus();
                    $(tr).find("#SATABLE008COLUMN09").val(aqty);
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close"); $(tr).find("#SATABLE008COLUMN09").focus();
                        $(tr).find("#SATABLE008COLUMN13").val(parseFloat(price) * parseFloat(aqty));
                    })
                }
            }
                //EMPHCS760 Rajasekhar reddy patakota 23/7/2015 Item issue remaining Qty calculation
            else if ((parseFloat(qty) > (parseFloat(avlqty))) && (parseFloat(avlqty) < (parseFloat(aqty))) && Backorder != "True") {
                Alert.render('Qty must not exceed Available quantity'); $("#alertok").focus();
                $(tr).find("#SATABLE008COLUMN09").val(avlqty);
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $(tr).find("#SATABLE008COLUMN09").focus();
                    $(tr).find("#SATABLE008COLUMN13").val(parseFloat(price) * parseFloat(avlqty));
                    return false;
                })
                return false;
            }

                //if (parseFloat(qty) > (parseFloat(aqty))) {
                //    Alert.render('Qty must not exceed available quantity'); $("#alertok").focus();
                //    $("#alertok").on("click", function () {
                //        $("#dialogbox").dialog("close");
                //        $(tr).find("#SATABLE008COLUMN09").val(aqty);
                //        $(tr).find("#SATABLE008COLUMN13").val(parseFloat(price) * parseFloat(aqty));
                //        $(tr).find("#SATABLE008COLUMN09").focus();
                //    })
                //}
                //else if (parseFloat(qty) > (parseFloat(avlqty))) {
                //    Alert.render('Qty must not exceed available quantity'); $("#alertok").focus();
                //    $("#alertok").on("click", function () {
                //        $("#dialogbox").dialog("close");
                //        $(tr).find("#SATABLE008COLUMN09").val(aqty);
                //        $(tr).find("#SATABLE008COLUMN13").val(parseFloat(price) * parseFloat(aqty));
                //        $(tr).find("#SATABLE008COLUMN09").focus();
                //    })
                //}
            else {
                $(tr).find("#SATABLE008COLUMN13").val(parseFloat(price).toFixed(2) * parseFloat(qty).toFixed(2));
            }
        }
        else {
            $(tr).find("#SATABLE008COLUMN13").val(parseFloat(price).toFixed(2) * parseFloat(qty).toFixed(2));
        }
    }
    else {
        Alert.render('Please Enter Value Greater Than Zero Only'); $("#alertok").focus();
        $(tr).find("#SATABLE008COLUMN09").val(''); $(tr).find("#SATABLE008COLUMN13").val(0);
        $("#alertok").on("click", function () {
            $("#dialogbox").dialog("close"); $(tr).find("#SATABLE008COLUMN09").focus();
        })
    }
}

function JOINItemIssueQuantity(tr) {
    debugger;
    var aqty = $(tr).find("#aSATABLE008COLUMN09").val();
    var qty = $(tr).find("#SATABLE008COLUMN09").val();
    var rqty = $(tr).find("#SATABLE008COLUMN11").val();
    //var avlqty = $(tr).find("#SATABLE008COLUMN10").val();
    var price = $(tr).find("#SATABLE008COLUMN12").val();
    if (qty == '') { qty = 0; }
    if (price == '') { price = 0; }
    debugger
    //EMPHCS760 Rajasekhar reddy patakota 23/7/2015 Item issue remaining Qty calculation
    if (parseFloat(qty) > 0 && !(isNaN(parseFloat(qty).toFixed(2)))) {
        if ((parseFloat(qty) > (parseFloat(aqty))) ) {
            var edit = window.location.href.slice(window.location.href).split("?");
            var editval = edit[0].split("/").indexOf("Edit", 0);
            if (parseFloat(editval) > 0) { aqty = parseFloat(aqty) + parseFloat(rqty); }
            //if (parseFloat(qty) > (parseFloat(aqty))) {
            //    Alert.render('Qty must not exceed Available quantity'); $("#alertok").focus();
            //    $(tr).find("#SATABLE008COLUMN09").val(aqty);
            //    $("#alertok").on("click", function () {
            //        $("#dialogbox").dialog("close"); $(tr).find("#SATABLE008COLUMN09").focus();
            //        $(tr).find("#SATABLE008COLUMN13").val(parseFloat(price) * parseFloat(aqty));
            //    })}
            //}
            //else {
                $(tr).find("#SATABLE008COLUMN13").val(parseFloat(price).toFixed(2) * parseFloat(qty).toFixed(2));
            //}
        }
    //    else {
    //        $(tr).find("#SATABLE008COLUMN13").val(parseFloat(price).toFixed(2) * parseFloat(qty).toFixed(2));
    //    }
    }
    else {
        Alert.render('Please Enter Value Greater Than Zero Only'); $("#alertok").focus();
        $(tr).find("#SATABLE008COLUMN09").val(''); $(tr).find("#SATABLE008COLUMN13").val(0);
        $("#alertok").on("click", function () {
            $("#dialogbox").dialog("close"); $(tr).find("#SATABLE008COLUMN09").focus();
        })
    }
}

//Details Filling in Grid
//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
function DetailsFillGrid(FormId, jarray, jarray1, jarray2, jarray3, jarray4, jarray5, jarray6, jarray7) {
    var tbl = $("#grdData")[0];
    for (var irow = 1; irow < tbl.rows.length  ; irow++) {
        var trw = tbl.rows[irow];
        for (var cll = irow - 1; cll < jarray.length; cll++) {
            debugger
            var node1 = jarray[cll];
            var node2 = jarray1[cll];
            var node3 = jarray2[cll];
            var node4 = jarray3[cll];
            var node5 = jarray4[cll];
            var node6 = jarray5[cll];
            var node7 = jarray6[cll];
            var node8 = jarray7[cll];
            $(trw).find("#checkRow").val(node7);
            if (FormId == 1283) {
                $(trw).find("#SATABLE006COLUMN03").val(node1);
                $(trw).find("#SATABLE006COLUMN27").val(node2);
                $(trw).find("#SATABLE006COLUMN26").val(node3);
            }
            else if (FormId == 1284) {
                $(trw).find("#SATABLE008COLUMN04").val(node1);
                $(trw).find("#SATABLE008COLUMN19").val(node2);
            }
            //EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
            else if (FormId == 1605) {
                $(trw).find("#PUTABLE023COLUMN03").val(node1);
                $(trw).find("#PUTABLE023COLUMN04").val(node2);
            }
            else if (FormId == 1588) {
                $(trw).find("#SATABLE008COLUMN04").val(node1);
                $(trw).find("#SATABLE008COLUMN19").val(node2);
            }
            else if (FormId == 1286) {
                $(trw).find("#PUTABLE004COLUMN03").val(node1);
                $(trw).find("#PUTABLE004COLUMN17").val(node2);
                $(trw).find("#MATABLE007COLUMN42").val(node3);
                $(trw).find("#MATABLE007COLUMN43").val(node4);
                $(trw).find("#MATABLE007COLUMN44").val(node5);
                $(trw).find("#MATABLE007COLUMN10").val(node6);
		//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                $(trw).find("#checkRow").after($('<input>').attr({ type: 'hidden', id: 'LineID' }).val(node8));
            }
                //EMPHCS957	Bill to Jobber - view mode not showing receipt and item details BY  RAJ.Jr 14/8/2015
            else if (FormId == 1287) {
                $(trw).find("#SATABLE010COLUMN04").val(node1);
                $(trw).find("#SATABLE010COLUMN05").val(node2);
                $(trw).find("#SATABLE010COLUMN21").val(node3);
               //EMPHCS1572 - working on multi units sizes in job order by GNANESHWAR ON 18/2/2016
	        $(trw).find("#SATABLE010COLUMN07").val(node4);
            }
            else if (FormId == 1288) {
                $(trw).find("#SATABLE012COLUMN03").val(node1);
            }
	    //EMPHCS1784	Job Order for IN Process 15/7/2016
            else if (FormId == 1586) {debugger
                $(trw).find("#PUTABLE002COLUMN03").val(node1);
                $(trw).find("#PUTABLE002COLUMN27").val(node2);
                $(trw).find("#PUTABLE002COLUMN25").val(node3);
            }
            else if (FormId == 1587) {
                debugger
                $(trw).find("#PUTABLE004COLUMN03").val(node1);
                $(trw).find("#PUTABLE004COLUMN17").val(node2);
            }
            break;
        }
    }
}
//EMPHCS1698 Displaying control values in lable in all VIEW click by GNANESHWAR ON 16/4/2016
function DetailsFillGridD(FormId, jarray, jarray1, jarray2, jarray3, jarray4, jarray5, jarray6) {
    debugger
    var tbl = $("#grdData")[0];
    for (var irow = 1; irow < tbl.rows.length  ; irow++) {
        var trw = tbl.rows[irow];
        for (var cll = irow - 1; cll < jarray.length; cll++) {
            debugger
            var node1 = jarray[cll];
            var node2 = jarray1[cll];
            var node3 = jarray2[cll];
            var node4 = jarray3[cll];
            var node5 = jarray4[cll];
            var node6 = jarray5[cll];
            var node7 = jarray6[cll];
            $(trw).find("#checkRow").val(node7);
            if (FormId == 1283) {
                $(trw).find("#SATABLE006COLUMN03").text(node1);
                $(trw).find("#SATABLE006COLUMN27").text(node2);
                $(trw).find("#SATABLE006COLUMN26").text(node3);
            }
            else if (FormId == 1284) {
                $(trw).find("#SATABLE008COLUMN04").text(node1);
                $(trw).find("#SATABLE008COLUMN19").text(node2);
            }
            //EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
            else if (FormId == 1605) {
                $(trw).find("#PUTABLE023COLUMN03").text(node1);
                $(trw).find("#PUTABLE023COLUMN04").text(node2);
            }
            else if (FormId == 1286) {
                $(trw).find("#PUTABLE004COLUMN03").text(node1);
                $(trw).find("#PUTABLE004COLUMN17").text(node2);
                $(trw).find("#MATABLE007COLUMN42").text(node3);
                $(trw).find("#MATABLE007COLUMN43").text(node4);
                $(trw).find("#MATABLE007COLUMN44").text(node5);
                $(trw).find("#MATABLE007COLUMN10").text(node6);
            }
                //EMPHCS957	Bill to Jobber - view mode not showing receipt and item details BY  RAJ.Jr 14/8/2015
            else if (FormId == 1287) {
                $(trw).find("#SATABLE010COLUMN04").text(node1);
                $(trw).find("#SATABLE010COLUMN05").text(node2);
                $(trw).find("#SATABLE010COLUMN21").text(node3);
               //EMPHCS1572 - working on multi units sizes in job order by GNANESHWAR ON 18/2/2016
                $(trw).find("#SATABLE010COLUMN07").text(node4);
            }
            else if (FormId == 1288) {
                $(trw).find("#SATABLE012COLUMN03").text(node1);
            }
	    //EMPHCS1784	Job Order for IN Process 15/7/2016
            else if (FormId == 1586) {
                $(trw).find("#PUTABLE002COLUMN03").text(node1);
                $(trw).find("#PUTABLE002COLUMN27").text(node2);
                $(trw).find("#PUTABLE002COLUMN25").text(node3);
            }
            else if (FormId == 1587) {
                $(trw).find("#PUTABLE004COLUMN03").text(node1);
                $(trw).find("#PUTABLE004COLUMN17").text(node2);
            }
                //GJ
            else if (FormId == 1588) {
                $(trw).find("#SATABLE008COLUMN04").text(node1);
                $(trw).find("#SATABLE008COLUMN19").text(node2);
            }
            break;
        }
    }
}

//Edit form load actions
function EditLoadEnableDisable(sessionformid) {

    if (sessionformid == 1276) {
        $("#SATABLE007COLUMN20").val(1002);
        //$("#SATABLE007COLUMN07").prop("readonly",true);
        //$("#SATABLE007COLUMN14").prop("readonly",true);  
        //$("#SATABLE007COLUMN12").prop("readonly",true);                        
        $("#SATABLE007COLUMN09").prop("readonly", true);
        $("#SATABLE007COLUMN13").prop("readonly", true);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#SATABLE008COLUMN04").prop("disabled", true);
            $(trw).find("#SATABLE008COLUMN05").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN06").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN07").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN08").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN11").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN10").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN12").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN13").prop("readonly", true);
        }
    }
    else if (sessionformid == 1588) {
        debugger
        $("#SATABLE007COLUMN20").val(1000);
        //$("#SATABLE007COLUMN13").prop("readonly", true);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#SATABLE008COLUMN07").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN10").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN11").prop("readonly", true);
            //EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
            $(trw).find("#SATABLE008COLUMN27").prop("readonly", true);
        }
    }
   //EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
    else if (sessionformid == 1586) {
        debugger
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#PUTABLE002COLUMN36").prop("readonly", true);
            if ($(trw).find("#PUTABLE002COLUMN25").val() == "")
                $(trw).find("#PUTABLE002COLUMN25").val(1000);
        }
    }
    else if (sessionformid == 1380) {
        //$("#PRTABLE005COLUMN09").prop("disabled", true);
        $("#PRTABLE005COLUMN06").val(1005);
        $("#PRTABLE005COLUMN17").val(1000);
    }
    else if (sessionformid == 1322) {
        $("#MATABLE013COLUMN07lbl").show();
        $("#MATABLE013COLUMN17lbl").show();
        $("#MATABLE013COLUMN18lbl").show();
        $("#MATABLE013COLUMN07lbla").show();
        $("#MATABLE013COLUMN17lbla").show();
        $("#MATABLE013COLUMN18lbla").show();
        $("#MATABLE013COLUMN07").show();
        $("#MATABLE013COLUMN17").show();
        $("#MATABLE013COLUMN18").show();
    }
    else if (sessionformid == 1381) {
        $("#PRTABLE007COLUMN07").val(1005);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#PRTABLE008COLUMN04").prop("readonly", true);
            $(trw).find("#PRTABLE008COLUMN05").prop("readonly", true);
            $(trw).find("#PRTABLE008COLUMN07").prop("readonly", true);
            $(trw).find("#PRTABLE008COLUMN08").prop("readonly", true);
        }
    }
    else if (sessionformid == 1358) {
        $("#FITABLE017COLUMN03").prop("readonly", true);
        $("#FITABLE017COLUMN04").prop("readonly", true);
    }
    else if (sessionformid == 1293) {
        debugger
        $("#FITABLE012COLUMN16").prop("disabled", true);
        $("#FITABLE012COLUMN26").prop("disabled", true);
        $("#FITABLE012COLUMN08").prop("disabled", true);
        $("#FITABLE012COLUMN20").prop("disabled", true);
        //Expense Form Chaneging to old model done by SRINIVAS 7/22/2015
        $("#FITABLE012COLUMN21").prop("readonly", true);
        //$("#FITABLE012COLUMN22").prop("disabled", true);
        //$("#FITABLE012COLUMN23").prop("readonly", true);
        //$("#FITABLE012COLUMN24").prop("readonly", true);
        $("#FITABLE012COLUMN06").prop("readonly", true);
        $("#ExpenseReceive").prop("disabled", true);
        $("#ExpensePay").prop("disabled", true);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#FITABLE021COLUMN03").prop("disabled", true);
        }
        var advamt = $("#FITABLE012COLUMN06").val();
        var balamt = $("#FITABLE012COLUMN24").val();
        if (advamt == '' || advamt == null || typeof (advamt) == "undefined") { advamt = 0; }
        if (balamt == '' || balamt == null || typeof (balamt) == "undefined") { balamt = 0; }
        if (parseFloat(advamt) > parseFloat(balamt))
            $("#ExpenseReceive").prop("checked", true);
        else if (parseFloat(advamt) < parseFloat(balamt))
            $("#ExpensePay").prop("checked", true);
    }
    else if (sessionformid == 1363) {
        $("#FITABLE020COLUMN07").prop("readonly", true);
        $("#FITABLE020COLUMN10").prop("readonly", true);
        $("#FITABLE020COLUMN11").find('option[value=22306]').remove();
        $("#FITABLE020COLUMN11").find('option[value=22335]').remove();
        //$("#FITABLE020COLUMN11").val(22305);
        //$("#FITABLE020COLUMN17").val(1000);
        $("#FITABLE020COLUMN19").val("");
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#FITABLE022COLUMN06").prop("readonly", true);
        }
    }
    else if (sessionformid == 1386) {
        $("#FITABLE023COLUMN10").prop("readonly", true);
        $("#FITABLE023COLUMN12").val(1000);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#FITABLE024COLUMN06").prop("readonly", true);
        }
    }
    else if (sessionformid == 1279) {

        $("#MATABLE009COLUMN06").prop("checked", true);
    }
    else if (sessionformid == 1280) {
        $("#MATABLE011COLUMN06").prop("checked", true);
    }
    else if (sessionformid == 1260) {
        $("#MATABLE010COLUMN09").attr('readonly', 'true');
    }
    else if (sessionformid == 1285) {

        $("#MATABLE007COLUMN05").val('ITTY008');
    }
    else if (sessionformid == 1252 || sessionformid == 1265) {
        //$('#SATABLE003COLUMN11').find('option').remove();
    }

    else if (sessionformid == 1283) {
            $("#SATABLE005COLUMN07").prop("checked", true); $("#SATABLE005COLUMN16").val("Approved");
            $("#SATABLE005COLUMN29").val(1000);
            $("#PUTABLE001COLUMN12").prop("readonly", true);
            $("#PUTABLE001COLUMN14").prop("readonly", true);
            $("#PUTABLE001COLUMN15").prop("readonly", true);
            $("#SATABLE005COLUMN12").prop("readonly", true);
            $("#SATABLE005COLUMN32").prop("readonly", true);
            $("#SATABLE005COLUMN15").prop("readonly", true);
            var tax = 0;
            var totaltax = 0;
            var tbl = $("#grdData")[0];
            for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                var trw = tbl.rows[irow];
                $(trw).find("#PUTABLE002COLUMN04").prop("readonly", true);
                $(trw).find("#PUTABLE002COLUMN05").prop("readonly", true);
                $(trw).find("#PUTABLE002COLUMN06").prop("readonly", true);
                $(trw).find("#PUTABLE002COLUMN11").prop("readonly", true);
                $(trw).find("#PUTABLE002COLUMN12").prop("readonly", true);
                $(trw).find("#PUTABLE002COLUMN13").prop("readonly", true);
                tax = ((parseFloat($(trw).find("#SATABLE006COLUMN11").val()) - parseFloat($(trw).find("#SATABLE006COLUMN25").val()))).toFixed(2);
                totaltax = (parseFloat(totaltax) + (parseFloat($(trw).find("#SATABLE006COLUMN11").val()) - parseFloat($(trw).find("#SATABLE006COLUMN25").val()))).toFixed(2);
                $(trw).find("#aSATABLE006COLUMN25").val(tax);
            }
            tax = (parseFloat($("#SATABLE005COLUMN32").val()) - parseFloat(totaltax)).toFixed(2);
            $("#SATABLE005COLUMN32").after($('<input>').attr({ type: 'hidden', id: 'aSATABLE005COLUMN32' }).val(tax));
    }
    else if (sessionformid == 1354) {

        $("#SATABLE007COLUMN20").val(1003);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#SATABLE008COLUMN11").prop("readonly", true);
        }
    }
    else if (sessionformid == 1355) {

        $("#PUTABLE001COLUMN29").val(1003);
        $("#PUTABLE001COLUMN31").val(1000);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#PUTABLE008COLUMN11").prop("readonly", true);
        }
    }
    else if (sessionformid == 1284) {

        $("#SATABLE007COLUMN20").val(1000);
        $("#SATABLE007COLUMN07").prop("readonly", true);
        $("#SATABLE007COLUMN09").prop("readonly", true);
        $("#SATABLE007COLUMN12").prop("readonly", true);
        //$("#SATABLE007COLUMN13").prop("readonly", true);
        $("#SATABLE007COLUMN14").prop("readonly", true);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#SATABLE008COLUMN04").prop("disabled", true);
            $(trw).find("#SATABLE008COLUMN05").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN06").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN07").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN08").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN11").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN10").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN12").prop("readonly", true);
            $(trw).find("#SATABLE008COLUMN13").prop("readonly", true);
        }
    }

    else if (sessionformid == 1286 || sessionformid == 1329) {
        if (sessionformid == 1329) {
            $("#PUTABLE003COLUMN17").val(1003);
        } else {
            $("#PUTABLE003COLUMN17").val(1000);
        }
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#PUTABLE004COLUMN09").prop("readonly", true);
            $(trw).find("#PUTABLE004COLUMN06").prop("readonly", true);
        }
    }
    else if (sessionformid == 1288) {

        $("#SATABLE011COLUMN18").val(1000);
        //$("#SATABLE011COLUMN08").val(4000);
     //EMPHCS1587 :Adding Advance Tab to Jobber Payment by gnaneshwar on 4/3/2016
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];

            $(trw).find("#SATABLE012COLUMN03").prop("disabled", true);
            $(trw).find("#SATABLE012COLUMN04").prop("readonly", true);
            $(trw).find("#SATABLE012COLUMN05").prop("readonly", true);
            $(trw).find("#SATABLE012COLUMN09").prop("readonly", true);
        }
    }
    else if (sessionformid == 1287) {
        $("#SATABLE009COLUMN19").val(1000);
        $("#SATABLE009COLUMN23").val(1000);
        $("#SATABLE009COLUMN07").prop("readonly", true);
        $("#SATABLE009COLUMN09").prop("readonly", true);
        $("#SATABLE009COLUMN11").prop("readonly", true);
        $("#SATABLE009COLUMN12").prop("readonly", true);
        $("#SATABLE009COLUMN13").prop("readonly", true);
        $("#SATABLE009COLUMN22").prop("readonly", true);
        $("#SATABLE009COLUMN24").prop("readonly", true);
        $("#SATABLE009COLUMN20").prop("readonly", true);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#SATABLE010COLUMN04").prop("disabled", true);
            $(trw).find("#SATABLE010COLUMN06").prop("readonly", true);
            $(trw).find("#SATABLE010COLUMN08").prop("readonly", true);
            $(trw).find("#SATABLE010COLUMN09").prop("readonly", true);
            $(trw).find("#SATABLE010COLUMN11").prop("readonly", true);
            $(trw).find("#SATABLE010COLUMN14").prop("readonly", true);
            $(trw).find("#SATABLE010COLUMN12").prop("readonly", true);
        }
    }
    else if (sessionformid == 1277) {
        $("#SATABLE009COLUMN19").val(1002);

        if ($("#SATABLE009COLUMN23").val() == "") {
            $("#SATABLE009COLUMN23").val(1000);
        }
        $("#SATABLE009COLUMN07").prop("readonly", true);
        $("#SATABLE009COLUMN11").prop("readonly", true);
        $("#SATABLE009COLUMN13").prop("readonly", true);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#SATABLE010COLUMN04").prop("disabled", true);
            $(trw).find("#SATABLE010COLUMN03").prop("readonly", true);
            $(trw).find("#SATABLE010COLUMN06").prop("readonly", true);
            $(trw).find("#SATABLE010COLUMN08").prop("readonly", true);
            $(trw).find("#SATABLE010COLUMN09").prop("readonly", true);
            $(trw).find("#SATABLE010COLUMN11").prop("readonly", true);
            $(trw).find("#SATABLE010COLUMN14").prop("readonly", true);
            $(trw).find("#SATABLE010COLUMN12").prop("readonly", true);
        }
    }
    else if (sessionformid == 1278) {
        $("#SATABLE011COLUMN18").val(1002);
        $("#SATABLE011COLUMN08").val(4000);
        $("#SATABLE011COLUMN07").prop("readonly", true);
        $("#SATABLE011COLUMN08").prop("readonly", true);
        $("#SATABLE011COLUMN09").prop("readonly", true);
        $("#SATABLE011COLUMN11").prop("readonly", true);
        $("#SATABLE011COLUMN12").prop("readonly", true);
        $("#SATABLE011COLUMN13").prop("readonly", true);
        $("#SATABLE012COLUMN09").prop("readonly", true);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#SATABLE012COLUMN04").prop("readonly", true);
            $(trw).find("#SATABLE012COLUMN05").prop("readonly", true);
        }
    }
    else if (sessionformid == 1272) {
        $("#PUTABLE003COLUMN17").val(1001);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#PUTABLE004COLUMN03").prop("disabled", true);
            $(trw).find("#PUTABLE004COLUMN04").prop("readonly", true);
            $(trw).find("#PUTABLE004COLUMN05").prop("readonly", true);
            //$(trw).find("#PUTABLE004COLUMN06").prop("readonly", true);
            $(trw).find("#PUTABLE004COLUMN07").prop("readonly", true);
            $(trw).find("#PUTABLE004COLUMN11").prop("readonly", true);
            //$(trw).find("#PUTABLE004COLUMN10").prop("readonly", true);
            $(trw).find("#PUTABLE004COLUMN09").prop("readonly", true);
        }
    }
    else if (sessionformid == 1273) {
        //Bill Screen
        $("#PUTABLE005COLUMN20").val(1001);
        if ($("#PUTABLE005COLUMN23").val() == "") {
            $("#PUTABLE005COLUMN23").val(1000);
        }
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#PUTABLE006COLUMN03").prop("disabled", true);
            $(trw).find("#PUTABLE006COLUMN07").prop("readonly", true);
            $(trw).find("#PUTABLE006COLUMN08").prop("readonly", true);
        }
    }
    else if (sessionformid == 1274) {
        //PO Payment Screen
        $("#PUTABLE014COLUMN17").val(1001);
        $("#PUTABLE014COLUMN08").val(4000);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#PUTABLE015COLUMN04").prop("readonly", true);
            $(trw).find("#PUTABLE015COLUMN05").prop("readonly", true);
            $(trw).find("#PUTABLE015COLUMN14").prop("readonly", true);
        }
    }
    else if (sessionformid == 1251 || sessionformid == 1374) {
        //$("#PUTABLE001COLUMN07").prop( "checked", true );
        //$("#PUTABLE001COLUMN16").val( "Approved");
        if ($("#PUTABLE001COLUMN31").val() == "") {
            $("#PUTABLE001COLUMN31").val(1000);
        }
        $("#PUTABLE001COLUMN29").val(1001);
        $("#PUTABLE001COLUMN12").prop("readonly", true);
        $("#PUTABLE001COLUMN14").prop("readonly", true);
        $("#PUTABLE001COLUMN15").prop("readonly", true);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#PUTABLE002COLUMN04").prop("readonly", true);
            $(trw).find("#PUTABLE002COLUMN05").prop("readonly", true);
            $(trw).find("#PUTABLE002COLUMN06").prop("readonly", true);
            $(trw).find("#PUTABLE002COLUMN11").prop("readonly", true);
            $(trw).find("#PUTABLE002COLUMN12").prop("readonly", true);
            $(trw).find("#PUTABLE002COLUMN13").prop("readonly", true);
        }
    }
    else if (sessionformid == 1375) {
        $("#SATABLE013COLUMN10").prop("checked", true);
        $("#SATABLE013COLUMN06").val("Approved");
        $("#SATABLE013COLUMN16").prop("readonly", true);
        $("#SATABLE013COLUMN17").prop("readonly", true);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#SATABLE014COLUMN04").prop("readonly", true);
            $(trw).find("#SATABLE014COLUMN05").prop("readonly", true);
            $(trw).find("#SATABLE014COLUMN07").prop("readonly", true);
            $(trw).find("#SATABLE014COLUMN08").prop("readonly", true);
            $(trw).find("#SATABLE014COLUMN09").prop("readonly", true);
        }
    } else if (sessionformid == 1376) {
        $("#SATABLE015COLUMN09").prop("checked", true);
        $("#SATABLE015COLUMN06").val("Approved");
        $("#SATABLE015COLUMN10").val(1002);
        if ($("#SATABLE015COLUMN16").val() == "") {
            $("#SATABLE015COLUMN16").val(1000);
        }
        $("#SATABLE015COLUMN19").prop("readonly", true);
        $("#SATABLE015COLUMN20").prop("readonly", true);
        $("#SATABLE015COLUMN21").prop("readonly", true);
        $("#SATABLE015COLUMN22").prop("readonly", true);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#SATABLE016COLUMN04").prop("readonly", true);
            $(trw).find("#SATABLE016COLUMN05").prop("readonly", true);
            $(trw).find("#SATABLE016COLUMN07").prop("readonly", true);
            $(trw).find("#SATABLE016COLUMN08").prop("readonly", true);
            $(trw).find("#SATABLE016COLUMN10").prop("readonly", true);
        }
    }
    else if (sessionformid == 1328) {
        $("#PUTABLE001COLUMN07").prop("checked", true);
        $("#PUTABLE001COLUMN16").val("Approved");
        if ($("#PUTABLE001COLUMN31").val() == "") {
            $("#PUTABLE001COLUMN31").val(1000);
        }
        $("#PUTABLE001COLUMN29").val(1003);
        $("#PUTABLE001COLUMN12").prop("readonly", true);
        $("#PUTABLE001COLUMN14").prop("readonly", true);
        $("#PUTABLE001COLUMN15").prop("readonly", true);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#PUTABLE002COLUMN04").prop("readonly", true);
            $(trw).find("#PUTABLE002COLUMN05").prop("readonly", true);
            $(trw).find("#PUTABLE002COLUMN06").prop("readonly", true);
            $(trw).find("#PUTABLE002COLUMN11").prop("readonly", true);
            $(trw).find("#PUTABLE002COLUMN12").prop("readonly", true);
            $(trw).find("#PUTABLE002COLUMN13").prop("readonly", true);
            $(trw).find("#PUTABLE002COLUMN08").prop("readonly", true);
            $(trw).find("#PUTABLE002COLUMN24").prop("readonly", true);
            //$(trw).find("#PUTABLE002COLUMN24").val(parseFloat($(trw).find("#PUTABLE002COLUMN07").val())*$parseFloat((trw).find("#PUTABLE002COLUMN09").val()));
        }
    }
        //EMPHCS761 - GNANESHWAR 16/7/2015 Read only Propert appying to fields in all form with out user entaring data
    else if (sessionformid == 1353) {
        debugger;

        $('#SATABLE003COLUMN10').find('option').remove();
        $("#SATABLE003COLUMN11").attr("disabled", "disabled");
        $("#SATABLE005COLUMN07").prop("checked", true);
        $("#SATABLE005COLUMN16").val("Approved");
        if ($("#SATABLE005COLUMN31").val() == "") {
            $("#SATABLE005COLUMN31").val(1000);
        }
        $("#SATABLE005COLUMN29").val(1003);
        $("#SATABLE005COLUMN12").prop("readonly", true);
        $("#SATABLE005COLUMN14").prop("readonly", true);
        $("#SATABLE005COLUMN15").prop("readonly", true);
        $("#SATABLE005COLUMN32").prop("readonly", true);
        $("#SATABLE005COLUMN09").prop("readonly", true);
        $("#SATABLE005COLUMN11").prop("readonly", true);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#SATABLE006COLUMN04").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN05").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN06").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN11").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN12").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN13").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN09").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN25").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN11").prop("readonly", true);
        }
    }
    else if (sessionformid == 1275) {
        //SO Screen
        // $("#SATABLE005COLUMN07").prop( "checked", true );$("#SATABLE005COLUMN16").val( "Approved");
        $("#SATABLE005COLUMN29").val(1002);
        if ($("#SATABLE005COLUMN31").val() == "") {
            $("#SATABLE005COLUMN31").val(1000);
        }
        $("#SATABLE005COLUMN12").prop("readonly", true);
        $("#SATABLE005COLUMN14").prop("readonly", true);
        $("#SATABLE005COLUMN15").prop("readonly", true);
        $("#SATABLE005COLUMN32").prop("readonly", true);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#SATABLE006COLUMN04").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN05").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN06").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN11").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN12").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN13").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN25").prop("readonly", true);
        }
    }
    else if (sessionformid == 1330) {
        //SO Screen
        $("#SATABLE005COLUMN07").prop("checked", true); $("#SATABLE005COLUMN16").val("Approved");
        $("#SATABLE005COLUMN29").val(1003);
        if ($("#SATABLE005COLUMN31").val() == "") {
            $("#SATABLE005COLUMN31").val(1000);
        }
        $("#SATABLE005COLUMN12").prop("readonly", true);
        $("#SATABLE005COLUMN14").prop("readonly", true);
        $("#SATABLE005COLUMN15").prop("readonly", true);
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#SATABLE006COLUMN04").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN05").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN06").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN11").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN12").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN13").prop("readonly", true);
            $(trw).find("#SATABLE006COLUMN07").prop("readonly", true);
        }
    }
    //EMPHCS1784	Job Order for IN Process 15/7/2016
    else if (sessionformid == 1587) {
        var tbl = $("#grdData")[0];
        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
            var trw = tbl.rows[irow];
            $(trw).find("#PUTABLE004COLUMN03").prop("disabled", true);
            $(trw).find("#PUTABLE004COLUMN06").prop("readonly", true);
            $(trw).find("#PUTABLE004COLUMN09").prop("readonly", true);
            //$(trw).find("#PUTABLE004COLUMN10").prop("readonly", true);
            $(trw).find("#PUTABLE004COLUMN11").prop("readonly", true);
            //EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
            $(trw).find("#PUTABLE004COLUMN27").prop("readonly", true);
        }
    }
    else {
    }
};

//Job%20Order%20Receipt

//JOReceipt Details
function ddlSelectJOR(eve, FormName) {
    var JOID = eve;
    if (eve != "null") {
        $.ajax({
            url: "/JobOrderReceipt/getJOReceiptDetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ JOID: JOID }),
            success: function (data) {
                if (data != "") {debugger
                    //IR Header Fields
                    $("#PUTABLE003COLUMN05").val(data.Data14);
                    $("#select2-PUTABLE003COLUMN05-container").text(data.cName);
                    $("#PUTABLE003COLUMN06").val(data.pid);
                    $("#PUTABLE003COLUMN08").val(data.Data);
                    $("#PUTABLE003COLUMN10").val(data.Data1);
                    $("#PUTABLE003COLUMN11").val(data.Data2);
                    $("#PUTABLE003COLUMN12").val(data.Data3);
                    $("#PUTABLE003COLUMN13").val(data.Data4);
                    $("#PUTABLE003COLUMN14").val(data.Data5);
                    $("#PUTABLE003COLUMN15").val(data.Data6);
                    $("#PUTABLE003COLUMN16").val(data.Data7);
                    $("#PUTABLE003COLUMN17").val(data.Data8);
                    $("#PUTABLE003COLUMN18").val(data.Data9);
                    $("#PUTABLE003COLUMN19").val(data.Data10);
		    //EMPHCS1837 rajasekhar reddy patakota 30/11/2016 Project wise tracking in Opportunity and job order screens
                    $("#PUTABLE003COLUMN21").val(data.Data11);
                }
            }
        });
    }
};


//JOB Order Invoice Ref JobOrder ddl Selection
function ddlSelectJOPM(eve, str, FormName) {
    debugger;
    var SalesOrderID = eve;
    if (eve != "null") {
        $.ajax({
            url: "/JobberPayments/GetJOPaymentDetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ SalesOrderID: SalesOrderID, Str: str }),
            success: function (data) {
                if (data != "") {
                    $("#SATABLE011COLUMN06").val(data.pid);
                    $("#SATABLE011COLUMN07").val(data.Data);
                    $("#SATABLE011COLUMN05").val(data.Data1);
                    $("#select2-SATABLE011COLUMN05-container").text(data.cName);
                    $("#SATABLE011COLUMN09").val(data.Data2);
                    $("#SATABLE011COLUMN10").val(data.Data3);
                    $("#SATABLE011COLUMN12").val(data.Data5);
                    $("#SATABLE011COLUMN11").val(data.Data4);
                    $("#SATABLE011COLUMN13").val(data.Data10);
                    $("#SATABLE011COLUMN14").val(data.Data6);
                    $("#SATABLE011COLUMN15").val(data.Data7);
                    $("#SATABLE011COLUMN16").val(data.Data8);
                    $("#SATABLE011COLUMN17").val(data.Data9);
                    $("#SATABLE011COLUMN18").val(data.Data11);
                    $("#SATABLE011COLUMN20").val(data.Data12);
                    $('#grdData').remove();
                    $('#grdfData').remove();
                    $('#itemgrid').html(data.grid);
                    $('.gridLevelFB').html(data.grid1);
                    $('#grdData').append(data.remain);
                    $('#grdfData').append(data.grid1);
                    dateupdate();
                    if (str == '0') {
                        var tbl = $("#grdData")[0];
                        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                            var trw = tbl.rows[irow];
                            JOPaymentDefaultAmountcal(trw);
                            $(trw).find("#SATABLE012COLUMN03").prop("disabled", true);
                            $(trw).find("#SATABLE012COLUMN04").prop("readonly", true);
                            $(trw).find("#SATABLE012COLUMN05").prop("readonly", true);
                            $(trw).find("#SATABLE012COLUMN09").prop("readonly", true);
                        }
                    }
                    else {
                        var tbl = $("#grdData")[0];
                        for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                            var trw = tbl.rows[irow];
                            $(trw).find("#SATABLE012COLUMN04").prop("readonly", true);
                            $(trw).find("#SATABLE012COLUMN05").prop("readonly", true);
                            $(trw).find("#SATABLE012COLUMN09").prop("readonly", true);
                        }
                        EnableDisable();
                    }
                }
            }
        });
    }
};

function JOPaymentDefaultAmountcal(trw) {
    var payment = $(trw).find("#SATABLE012COLUMN06").val();
    var Dueamt = $(trw).find("#SATABLE012COLUMN05").val();
    var Advamt = $(trw).find("#SATABLE012COLUMN09").val();
    if (payment == '' || typeof (payment) == "undefined") { payment = 0; }
    if (Dueamt == '' || typeof (Dueamt) == "undefined") { Dueamt = 0; }
    if (Advamt == '' || typeof (Advamt) == "undefined") { Advamt = 0; }
    payment = (parseFloat(Dueamt) - parseFloat(Advamt)).toFixed(2);
    if (payment <= 0 || payment == "0" || payment == "0.00") payment = "";
    $(trw).find("#SATABLE012COLUMN06").val(payment);
    $("#SATABLE011COLUMN13").val(payment);
}
//jo Bill Services Header Level Calulation
function BillServicesCalculations() {
debugger
    var tbl = $("#grdData")[0]; var qty = 0; var price = 0; var tqty = 0; var tprice = 0; var tdiscount = 0; var ttal = 0; var tax = 0; var advance = 0;
    for (var irow = 1; irow < tbl.rows.length  ; irow++) {
        var trw = tbl.rows[irow];
        qty = $(trw).find("[id='SATABLE010COLUMN10']").val(); price = $(trw).find("[id='SATABLE010COLUMN13']").val();
        if (qty == "") { qty = 0; } if (price == "") { price = 0; }
        $(trw).find("[id='SATABLE010COLUMN14']").val(parseInt(qty) * parseInt(price));
        tqty = parseInt(tqty) + parseInt(qty);
        tprice = parseInt(tprice) + parseInt(price);
        ttal = parseInt(ttal) + (parseInt(qty) * parseInt(price));
    }
    tax = $("#SATABLE009COLUMN24").val(); advance = $("#SATABLE009COLUMN25").val();
    if (tax == "") { tax = 0; } if (advance == "" || typeof advance == "undefined") { advance = 0; }
    if (tqty > 0 && tprice > 0) {
        var TaxType = $("#SATABLE009COLUMN23").val();
        $.ajax({
            url: "/Common/GetTaxAmount",
            type: "POST",
            //EMPHCS729 Rajasekhar Patakota 21/7/2015 Tax Amount Calculation In Transaction Forms
            data: JSON.stringify({ TaxType: TaxType,  FormName: $("#FormName").val() }),
            dataType: "json", cache: true,
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                if (result.Tax != '') {
                    tax = result.Tax; tax = (tax * (0.01));
                    tax = parseInt((((ttal) - parseInt(tdiscount)) * tax).toFixed(2));
                } else { tax = 0; }
                $("#SATABLE009COLUMN24").val(tax);
                $("#SATABLE009COLUMN22").val(ttal);
                $("#SATABLE009COLUMN20").val((((ttal)) + (parseFloat(tax))) - parseInt(advance));
            }
        });
    } else {
        $("#SATABLE009COLUMN24").val(""); $("#SATABLE009COLUMN25").val("");
        $("#SATABLE009COLUMN22").val("");
        $("#SATABLE009COLUMN20").val("");
    }
}

function InvoiceLinePriceCalculation(tr, Price) {
debugger
    if ((isNaN((Price))) && !(Price.match('^(0|[1-9][0-9]*)$'))) {
        $(tr).find("#SATABLE010COLUMN14").val(0);
        $(tr).find("#SATABLE010COLUMN13").val(0);
        Alert.render('Please Enter More Than Zero Value for Price'); $("#alertok").focus();
        $("#alertok").on("click", function () {
            $("#dialogbox").dialog("close"); $(tr).find("#SATABLE010COLUMN13").focus();
        })
        return false;
    }
}

function SortByStatus(items, FormName) {
    var dat = { items: items };
    $.ajax({
        url: "/FormBuilding/SortByStatus?FormName=" + FormName + "",
        type: "POST",
        data: JSON.stringify(dat),
        dataType: "html",
        cache: true,
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            window.location.reload();
        }
    });
    dynamicStyle($.session.get("dynamicStyle")); $("#FddlDynamicQuickSort").val($.session.get("dynamicSort"));
}

//SOInvoice Calculations
function SOInvoiceTaxCalculations(tr) {
    debugger
    var qty = $(tr).find("#SATABLE010COLUMN10").val();
    //EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Remaining Quantity Calculation
    var SOID = $("#SATABLE009COLUMN06").val();
    var aqty = $(tr).find("#aSATABLE010COLUMN10").val();
    //EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Remaining Quantity Calculation
    if (SOID == "" || SOID == null) { aqty = qty; }
    var avqty = $(tr).find("#SATABLE010COLUMN11").val();
    var rqty = $(tr).find("#SATABLE010COLUMN12").val();
    var taxType = $(tr).find("#SATABLE010COLUMN21").val();
    var price = $(tr).find("#SATABLE010COLUMN13").val();
    var discount = $(tr).find("[id='SATABLE010COLUMN19']").val();

    if (qty == '' || typeof (qty) == "undefined") { qty = "0"; }
    if (price == '' || typeof (price) == "undefined") { price = "0"; }
    if (discount == '' || typeof (discount) == "undefined") { discount = "0"; }
    //EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Remaining Quantity Calculation
    if (avqty == '' || typeof (avqty) == "undefined") { avqty = "0"; }
    if (rqty == '' || typeof (rqty) == "undefined") { rqty = "0"; }
    //EMPHCS684 Rajasekhar Patakota 18/7/2015 Quantity Calculation Changes
    if ((isNaN(parseFloat(qty)) || parseFloat(qty) < 0 || !(qty.match('^([0-9]+)(\.[0-9]+)?$')))) {
        Alert.render('Please Enter Value Greater Than Zero Only!'); $("#alertok").focus();
        $(tr).find("#SATABLE010COLUMN10").val(''); $(tr).find("#SATABLE010COLUMN14").val(0); $(tr).find("#SATABLE010COLUMN23").val(0);
        $("#alertok").on("click", function () {
            $("#dialogbox").dialog("close"); $(tr).find("#SATABLE010COLUMN10").focus();
            //EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Remaining Quantity Calculation
            //SOInvoiceTaxHeaderLevelCalculations(); SOInvoiceTaxLineLevelCalculations();
        })
    }
        //EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Remaining Quantity Calculation
    else if ((parseFloat(qty) > (parseFloat(aqty))) || (parseFloat(qty) > (parseFloat(avqty)))) {
        var edit = window.location.href.slice(window.location.href).split("?");
        var editval = edit[0].split("/").indexOf("Edit", 0);
        if (parseFloat(editval) > 0) { aqty = parseFloat(aqty) + parseFloat(rqty); }
        if (parseFloat(qty) > (parseFloat(aqty))) {
            Alert.render('Qty must not exceed Issued quantity'); $("#alertok").focus();
            $(tr).find("#SATABLE010COLUMN10").val(aqty);
            $("#alertok").on("click", function () {
                $("#dialogbox").dialog("close"); $(tr).find("#SATABLE010COLUMN10").focus();
                //SOInvoiceTaxHeaderLevelCalculations(); SOInvoiceTaxLineLevelCalculations();
            })
        }
        if (SOID == "" || SOID == null) {
            if ((parseFloat(qty) > (parseFloat(avqty))) && (parseFloat(avqty) < (parseFloat(aqty)))) {
                Alert.render('Qty must not exceed Available quantity'); $("#alertok").focus();
                $(tr).find("#SATABLE010COLUMN10").val(avqty);
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $(tr).find("#SATABLE010COLUMN10").focus();
                    //SOInvoiceTaxHeaderLevelCalculations(); SOInvoiceTaxLineLevelCalculations();
                })
            }
        }
    }
    else if (isNaN(parseFloat(price)) || parseFloat(price) < 0 || !(price.match('^([0-9]+)(\.[0-9]+)?$'))) {
        Alert.render('Please Enter Value Greater Than Zero Only!'); $("#alertok").focus();
        $(tr).find("#SATABLE010COLUMN13").val('');
        $("#alertok").on("click", function () {
            $("#dialogbox").dialog("close"); $(tr).find("#SATABLE010COLUMN13").focus();
            //EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Remaining Quantity Calculation
            //SOInvoiceTaxHeaderLevelCalculations(); SOInvoiceTaxLineLevelCalculations();
        })

    }
    else if (isNaN(parseFloat(discount)) || parseFloat(discount) < 0 || !(discount.match('^([0-9]+)(\.[0-9]+)?$'))) {
        Alert.render('Please Enter Value Greater Than Zero Only!'); $("#alertok").focus();
        $(tr).find("#SATABLE010COLUMN19").val('');
        $("#alertok").on("click", function () {
            $("#dialogbox").dialog("close"); $(tr).find("#SATABLE010COLUMN19").focus();
            //EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Remaining Quantity Calculation
            //SOInvoiceTaxHeaderLevelCalculations(); SOInvoiceTaxLineLevelCalculations();
        })

    }
    //EMPHCS731 Rajasekhar reddy patakota 22/7/2015 Remaining Quantity Calculation
    //else {
    var qty = 0; var price = 0;
    var discount = 0;
    qty = $(tr).find("[id='SATABLE010COLUMN10']").val();
    price = $(tr).find("[id='SATABLE010COLUMN13']").val();
    discount = $(tr).find("[id='SATABLE010COLUMN19']").val();
    if (qty == "") { qty = 0; }
    if (price == "") { price = 0; }
    if (discount == "" || typeof (discount) == "undefined") { discount = "0"; }
    $(tr).find("#SATABLE010COLUMN14").val((parseFloat(qty) * parseFloat(price)).toFixed(2));
    var linetaxtype = $(tr).find("#SATABLE010COLUMN21").val();
    var TaxCal = $(tr).find("#SATABLE010COLUMN14").val();
    $.ajax({
        url: "/Common/GetTaxAmount",
        type: "POST",
        //EMPHCS729 Rajasekhar Patakota 21/7/2015 Tax Amount Calculation In Transaction Forms
        data: JSON.stringify({ TaxType: linetaxtype, TaxCal: TaxCal, FormName: $("#FormName").val() }),
        dataType: "json", cache: true,
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            if (result.Tax != '') {
                tax = result.Tax; tax = (parseFloat(tax) * parseFloat(0.01));
                var tt1 = result.TaxCal
                //EMPHCS878		Sales order tax calculation is not considering discount amount. Tax calculation should one ( total amount - discount) done by srinivas 8/8/2015
                tax = (parseFloat(parseFloat(tt1) - parseFloat(discount)) * parseFloat(tax)).toFixed(2);
            } else { tax = 0; }
            var po = $("#SATABLE009COLUMN06").val();
            if (po != '' || po != null) { $(tr).find("[id='SATABLE010COLUMN23']").val(tax); } else {
                $(tr).find("[id='SATABLE010COLUMN23']").val(tax); $(tr).find("[id='aSATABLE010COLUMN14']").val(tax);
            }
	    //EMPHCS1169 rajasekhar reddy patakota 28/09/2015 Job order module end to end process deploying to prod
            SOInvoiceTaxLineLevelCalculations();
            //SOInvoiceTaxLineLevelCalculations();
        }
    })
    //CalculationsP();
    //}
};
// SO Invoice TAX CALUCLATION

function SOInvoiceTaxLineLevelCalculations() {
    debugger
    var taxamount = 0; var TaxCal = 0;
    var subtotal = $("#SATABLE009COLUMN22").val();
    var discount = $("#SATABLE009COLUMN25").val();
    var total = $("#SATABLE009COLUMN20").val();
   
    if (subtotal == "" || typeof (subtotal) == "undefined") { subtotal = "0"; }
    if (discount == "" || typeof (discount) == "undefined") { discount = "0"; }
    if (total == "") { total = 0; } var linetax = 0;
    TaxTypeH = $("#SATABLE009COLUMN23").val();
    //EMPHCS794   Packing and Shipping Charges Calculation in Sales Order and Invoice  and capturing values in  delivery & Package ---by Raj.jr 30/7/2015
    var PackingCharges = $("#SATABLE009COLUMN35").val();
    var ShippingCharges = $("#SATABLE009COLUMN36").val();
    if (typeof (PackingCharges) == "undefined" || PackingCharges == "")
        PackingCharges = 0;
    if (typeof (ShippingCharges) == "undefined" || ShippingCharges == "")
        ShippingCharges = 0;

    $.ajax({
        url: "/Common/GetTaxAmount",
        type: "POST",
        //EMPHCS729 Rajasekhar Patakota 21/7/2015 Tax Amount Calculation In Transaction Forms
        data: JSON.stringify({ TaxType: TaxTypeH,  FormName: $("#FormName").val() }),
        dataType: "json", cache: true,
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            if (result.Tax != '') {
                tax = result.Tax; tax = (parseFloat(tax) * parseFloat(0.01));
                //EMPHCS878		Sales order tax calculation is not considering discount amount. Tax calculation should one ( total amount - discount) done by srinivas 8/8/2015
		//EMPHCS1169 rajasekhar reddy patakota 28/09/2015 Job order module end to end process deploying to prod
                var tott=$("#SATABLE009COLUMN22").val();
                if (typeof (tott) == "undefined" || tott == "")
                    tott = 0;
                tax = (parseFloat(((parseFloat(tott) - parseFloat(discount)) - (parseFloat(PackingCharges) + parseFloat(ShippingCharges)))) * parseFloat(tax)).toFixed(2);
            } else { tax = 0; } $("#aSATABLE009COLUMN24").val(tax);
            var tbl = $("#grdData")[0];
            for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                var trw = tbl.rows[irow];
                if ($(trw).find("#checkRow").is(':checked')) {
                    var po = $("#SATABLE009COLUMN06").val();
                    if (po != '' || po != null) { linetax = $(trw).find("[id='SATABLE010COLUMN23']").val(); } else {
                        linetax = $(trw).find("#aSATABLE010COLUMN14").val();
                    }

                    if (linetax == "") linetax = 0;
                    taxamount = (parseFloat(taxamount) + parseFloat(linetax));
                }

            }
            debugger
            $("#SATABLE009COLUMN24").val((parseFloat(taxamount) + parseFloat(tax)).toFixed(2));
            //EMPHCS797 Shipping and Packing Charges not Carry forward to Invoice Screen done by srinivas 31/7/2015
            $("#SATABLE009COLUMN20").val(((parseFloat(subtotal) - parseFloat(discount)) + (parseFloat(taxamount) + parseFloat(tax))).toFixed(2));
            //$("#PUTABLE005COLUMN14").val(((parseFloat(subtotal)) + (parseFloat(taxamount) + parseFloat(tax))).toFixed(2));
            //CalculationsP();
        }

    });
}
function SOInvoiceLineTax(tr) {
    var qty = 0; var price = 0; var discount = 0;
    qty = $(tr).find("[id='SATABLE010COLUMN10']").val();
    price = $(tr).find("[id='SATABLE010COLUMN13']").val();
    discount = $(tr).find("[id='SATABLE010COLUMN19']").val();
    if (qty == "") { qty = 0; }
    if (price == "") { price = 0; }
    if (discount == "") { discount = 0; }
    $(tr).find("#SATABLE010COLUMN14").val((parseFloat(qty) * parseFloat(price)).toFixed(2));
    var linetaxtype = $(tr).find("#SATABLE010COLUMN21").val();
    var TaxCal = $(tr).find("#SATABLE010COLUMN14").val();
    $.ajax({
        url: "/Common/GetTaxAmount",
        type: "POST",
        //EMPHCS729 Rajasekhar Patakota 21/7/2015 Tax Amount Calculation In Transaction Forms
        data: JSON.stringify({ TaxType: linetaxtype, TaxCal: TaxCal, FormName: $("#FormName").val() }),
        dataType: "json", cache: true,
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            if (result.Tax != '') {
                tax = result.Tax; tax = (parseFloat(tax) * parseFloat(0.01));
                var tt1 = result.TaxCal
                //EMPHCS878		Sales order tax calculation is not considering discount amount. Tax calculation should one ( total amount - discount) done by srinivas 8/8/2015
                tax = (parseFloat(parseFloat(tt1) - parseFloat(discount)) * parseFloat(tax)).toFixed(2);
            } else { tax = 0; }
            var po = $("#SATABLE009COLUMN06").val();
            if (po != '' || po != null) { $(tr).find("[id='SATABLE010COLUMN23']").val(tax); } else {
                $(tr).find("[id='SATABLE010COLUMN23']").val(tax); $(tr).find("[id='aSATABLE010COLUMN14']").val(tax);
            }
	    //EMPHCS1169 rajasekhar reddy patakota 28/09/2015 Job order module end to end process deploying to prod
            SOInvoiceTaxLineLevelCalculations();
        }
    })
}
//SO Invoice Header Calculations
function SOInvoiceTaxHeaderLevelCalculations() {
    debugger
    var tbl = $("#grdData")[0]; var qty = 0; var price = 0; var discount = 0; var tqty = 0; var tprice = 0; var tdiscount = 0; var ttal = 0; var tax = 0; var ttax = 0; var amount = 0;
    var Ttax = 0;
    var TaxCal = 0; var rowcount = 0;
    //EMPHCS794   Packing and Shipping Charges Calculation in Sales Order and Invoice  and capturing values in  delivery & Package ---by Raj.jr 30/7/2015
    var PackingCharges = $("#SATABLE009COLUMN35").val();
    var ShippingCharges = $("#SATABLE009COLUMN36").val();
    if (typeof (PackingCharges) == "undefined" || PackingCharges == "")
        PackingCharges = 0;
    if (typeof (ShippingCharges) == "undefined" || ShippingCharges == "")
        ShippingCharges = 0;
    for (var irow = 1; irow < tbl.rows.length  ; irow++) {
        rowcount = rowcount + 1;
        var trw = tbl.rows[irow]; debugger
        if ($(trw).find("#checkRow").is(':checked')) {
            qty = $(trw).find("[id='SATABLE010COLUMN10']").val();
            price = $(trw).find("[id='SATABLE010COLUMN13']").val();
            discount = $(trw).find("[id='SATABLE010COLUMN19']").val();
            var po = $("#SATABLE009COLUMN06").val();
            if (po != '' || po != null)
            { tax = $(trw).find("[id='SATABLE010COLUMN23']").val(); }
            else
            { tax = $(trw).find("[id='aSATABLE010COLUMN14']").val(); }
           
            if (qty == "" || typeof (qty) == "undefined") { qty = "0"; }
            if (price == "" || typeof (price) == "undefined") { price = "0"; }
            if (discount == "" || typeof (discount) == "undefined") { discount = "0"; }
            if (tax == "" || typeof (tax) == "undefined") { tax = "0"; }
    
            ttax = parseFloat(ttax) + parseFloat(tax);
            tqty = parseFloat(tqty) + parseFloat(qty);
            tprice = parseFloat(tprice) + parseFloat(price);
            tdiscount = parseFloat(tdiscount) + parseFloat(discount);
            amount = parseFloat(qty) * parseFloat(price);
            ttal = parseFloat(ttal) + (amount);
            if (qty >= 0 && price >= 0) {
                //$(trw).find("#PUTABLE002COLUMN11").val(((amount) - discount).toFixed(2));
                $(trw).find("#SATABLE010COLUMN14").val((amount).toFixed(2));
            }
            if (rowcount == tbl.rows.length - 1) {
                var headertax = $("#aSATABLE009COLUMN24").val();
                if (headertax == "" || headertax == null || typeof (headertax) == "undefined" || isNaN(headertax)) { headertax = 0; }
                ttax = parseFloat(ttax) + parseFloat(headertax);
                $("#SATABLE009COLUMN24").val(ttax.toFixed(2));
                //EMPHCS794   Packing and Shipping Charges Calculation in Sales Order and Invoice  and capturing values in  delivery & Package ---by Raj.jr 30/7/2015
                $("#SATABLE009COLUMN22").val((parseFloat(ttal) + parseFloat(PackingCharges) + parseFloat(ShippingCharges)).toFixed(2));
                $("#SATABLE009COLUMN25").val(parseFloat(tdiscount).toFixed(2));
                $("#SATABLE009COLUMN20").val(((parseFloat(ttal) - parseFloat(tdiscount)) + parseFloat(ttax) + parseFloat(PackingCharges) + parseFloat(ShippingCharges)).toFixed(2));
                //$("#PUTABLE005COLUMN14").val(((parseFloat(ttal)) + parseFloat(ttax)).toFixed(2));
            }
            else {
                $("#SATABLE009COLUMN24").val("");
                $("#SATABLE009COLUMN22").val("");
                $("#SATABLE009COLUMN25").val("");
                $("#SATABLE009COLUMN20").val("");
            }

        }
        else {
            if (rowcount == tbl.rows.length - 1) {
                var headertax = $("#aSATABLE009COLUMN24").val();
                if (headertax == "" || headertax == null || typeof (headertax) == "undefined" || isNaN(headertax)) { headertax = 0; }
                ttax = parseFloat(ttax) + parseFloat(headertax);
                $("#SATABLE009COLUMN24").val(ttax.toFixed(2));
                //EMPHCS794   Packing and Shipping Charges Calculation in Sales Order and Invoice  and capturing values in  delivery & Package ---by Raj.jr 30/7/2015
                $("#SATABLE009COLUMN22").val((parseFloat(ttal) + parseFloat(PackingCharges) + parseFloat(ShippingCharges)).toFixed(2));
                $("#SATABLE009COLUMN25").val(parseFloat(tdiscount).toFixed(2));
                $("#SATABLE009COLUMN20").val(((parseFloat(ttal) - parseFloat(tdiscount)) + parseFloat(ttax) + parseFloat(PackingCharges) + parseFloat(ShippingCharges)).toFixed(2));
                //$("#PUTABLE005COLUMN14").val(((parseFloat(ttal)) + parseFloat(ttax)).toFixed(2));
            }
        }
    }
}





//UPCSearch Details
function UPCSearchDetails(tr, formid, FormName) {
    debugger
    var iSOItemID = $(tr).find("#UPCSearch").val();
    if (formid == 1286) {
        $.ajax({
            url: "/Common/getJODetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ ItemID: iSOItemID }),
            success: function (data) {
                debugger
                $(tr).find("#MATABLE007COLUMN42").val(data.color);
                $(tr).find("#MATABLE007COLUMN04").val(data.Itemdesc);
                $(tr).find("#MATABLE007COLUMN43").val(data.style);
                $(tr).find("#MATABLE007COLUMN44").val(data.size);
                $(tr).find("#MATABLE007COLUMN10").val(data.brand);

                $(tr).find("#PUTABLE004COLUMN17").val(data.UOM);
                $(tr).find("#PUTABLE004COLUMN23").val(data.distP);
                $(tr).find("#PUTABLE004COLUMN10").val(data.price);
                $(tr).find("#PUTABLE004COLUMN03").val(data.item);
                $(tr).find("#select2-PUTABLE004COLUMN03-container").text(data.iName);

                //$(tr).find("#PUTABLE002COLUMN25").val(data.Tax);
                //$(tr).find("#PUTABLE002COLUMN26").val(data.UOM);
                //$(tr).find("#PUTABLE006COLUMN05").val(data.billitemdesc);



                $(tr).find("#PUTABLE004COLUMN03 option:contains(" + data.item + ")").attr('selected', 'selected');
                ddlChange1(tr, "", "")
            }
        });
    }
    else if (formid == 1357) {
        $.ajax({
            url: "/FormBuilding/GetIAItemDetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ ItemID: iSOItemID }),
            success: function (data) {
                $(tr).find("#FITABLE015COLUMN03").val(data.Data3);
                $(tr).find("#FITABLE015COLUMN04").val(data.Data);
                $(tr).find("#FITABLE015COLUMN05").val(data.Data1);
                $(tr).find("#FITABLE015COLUMN06").val(data.Data2);
                $(tr).find("#FITABLE015COLUMN10").val(data.Data4);
                $(tr).find("#FITABLE015COLUMN07").val(data.Data5);
                $(tr).find("#FITABLE015COLUMN09").val("");
                $(tr).find("#FITABLE015COLUMN04").prop("readonly", true);
                $(tr).find("#FITABLE015COLUMN05").prop("readonly", true);
                $(tr).find("#FITABLE015COLUMN06").attr("readonly", true);
                IACalculations();
            }
        });
    }
    else if (formid == 1375) {
        $.ajax({
            url: "/FormBuilding/GetItemDetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ ItemID: iSOItemID }),
            success: function (data) {
                if (data != "") {
                    $(tr).find("#SATABLE014COLUMN04").val(data.Data);
                    $(tr).find("#SATABLE014COLUMN05").val(data.Data1);
                    $(tr).find("#SATABLE014COLUMN03").val(data.Data3);
                    $(tr).find("#SATABLE014COLUMN07").val(data.Data4);
                    $(tr).find("#SATABLE014COLUMN04").prop("readonly", true);
                    $(tr).find("#SATABLE014COLUMN05").prop("readonly", true);
                    //$(rw).find("#SATABLE014COLUMN06").prop("readonly", true);
                    $(tr).find("#SATABLE014COLUMN07").attr("readonly", true);
                    $(tr).find("#SATABLE014COLUMN08").attr("readonly", true);
                    $(tr).find("#SATABLE014COLUMN09").attr("readonly", true);
                    $(tr).find("#UPCSearch").val("");
                }
                else {
                    //EMPHCS685 Rajasekhar Patakota 18/7/2015 
                    Alert.render('UPC Code Does Not Exists!'); $("#alertok").focus();
                    $(tr).find("#SATABLE014COLUMN04").val('');
                    $(tr).find("#SATABLE014COLUMN05").val('');
                    //$(trw).find("#SATABLE014COLUMN06").val('');
                    if (Formid == 1283) {
                        $(tr).find("#SATABLE006COLUMN09").val('');
                    }
                }
            }
        });

    }
    else {
        $.ajax({
            url: "/Common/getJODetails?FormName" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ ItemID: iSOItemID }),
            success: function (data) {
                if (data != "") {
                    debugger
                    if (formid == 1251 || formid == 1328) {
                        $(tr).find("#PUTABLE002COLUMN04").val(data.Data);
                        $(tr).find("#PUTABLE002COLUMN05").val(data.Data9);
                        $(tr).find("#PUTABLE002COLUMN06").val(data.Data7);
                        $(tr).find("#PUTABLE002COLUMN03").val(data.Data3);
                        $(tr).find("#PUTABLE002COLUMN25").val(data.Data11);
                        $(tr).find("#PUTABLE002COLUMN09").val(data.Data4);
                        $(tr).find("#PUTABLE002COLUMN26").val(data.Data12);
                        //EMPHCS685 Rajasekhar Patakota 18/7/2015 
                        $(tr).find("#PUTABLE002COLUMN07").val('');

                    }
                    else if (formid == 1273) {
                        $(tr).find("#PUTABLE006COLUMN04").val(data.Data3);
                        $(tr).find("#PUTABLE006COLUMN05").val(data.Data1);
                        $(tr).find("#PUTABLE006COLUMN07").val(0);
                        $(tr).find("#PUTABLE006COLUMN08").val(0);
                        $(tr).find("#PUTABLE006COLUMN09").val(0);
                        $(tr).find("#PUTABLE006COLUMN10").val(0);
                        $(tr).find("#PUTABLE006COLUMN11").val(data.Data4);
                        $(tr).find("#PUTABLE006COLUMN05").val(data.Data9);
                        $(tr).find("#PUTABLE006COLUMN18").val(data.Data11);
                        $(tr).find("#PUTABLE006COLUMN12").val(0);
                        $(tr).find("#PUTABLE006COLUMN19").val(data.Data12);
                        //EMPHCS688 Rajasekhar Patakota 18/7/2015 UPC Search in Bill Edit and add new row feild
                        $(tr).find("#PUTABLE006COLUMN09").val('');
                    }
                    else if (formid == 1275) {
                        $(tr).find("#SATABLE006COLUMN04").val(data.Data);
                        $(tr).find("#SATABLE006COLUMN05").val(data.Data8);
                        $(tr).find("#SATABLE006COLUMN06").val(data.Data2);
                        $(tr).find("#SATABLE006COLUMN27").val(data.Data7);
                        $(tr).find("#SATABLE006COLUMN07").val('');
                        $(tr).find("#SATABLE006COLUMN03").val(data.Data3);
                        $(tr).find("#SATABLE006COLUMN26").val(data.Data5);
                        $(tr).find("#SATABLE006COLUMN09").val(data.Data6);

                    }
                    else if (formid == 1277) {
                        debugger
                        $(tr).find("#SATABLE010COLUMN03").val(data.Data);
                        //$(tr).find("#SATABLE010COLUMN06").val(data.Data1);
                        $(tr).find("#SATABLE010COLUMN05").val(data.Data3);
                        $(tr).find("#SATABLE010COLUMN21").val(data.Data5);
                        $(tr).find("#SATABLE010COLUMN13").val(data.Data6);
                        $(tr).find("#SATABLE010COLUMN22").val(data.Data7);

                        $(tr).find("#SATABLE010COLUMN07").val(0);
                        $(tr).find("#SATABLE010COLUMN08").val(0);
                        $(tr).find("#SATABLE010COLUMN21").val(data.Data5);
                        $(tr).find("#SATABLE010COLUMN09").val(0);
                        $(tr).find("#SATABLE010COLUMN10").val('');
                        $(tr).find("#SATABLE010COLUMN11").val(data.Data2);
                        $(tr).find("#SATABLE010COLUMN12").val(0);


                    }
                    else if (formid == 1283) {
                        $(tr).find("#SATABLE006COLUMN03").val(data.item);
                        $(tr).find("#select2-SATABLE006COLUMN03-container").text(data.iName);
                        $(tr).find("#SATABLE006COLUMN04").val(data.UPC);
                        $(tr).find("#SATABLE006COLUMN27").val(data.UOM);
                        $(tr).find("#SATABLE006COLUMN05").val(data.idesc);
                        $(tr).find("#SATABLE006COLUMN09").val(data.price);
                        $(tr).find("#SATABLE006COLUMN26").val(data.tax);
                    }
                    else if (formid == 1380) {
                        $(tr).find("#PRTABLE006COLUMN04").val(data.Data);
                        $(tr).find("#PRTABLE006COLUMN05").val(data.Data1);
                        $(tr).find("#PRTABLE006COLUMN03").val(data.Data3);
                        $(tr).find("#PRTABLE006COLUMN09").val(data.Data4);
                    }
                    if (data.Data3 == "") {
                        //EMPHCS688 Rajasekhar Patakota 18/7/2015 UPC Search in Bill Edit and add new row feild	
                        Alert.render('UPC Code Does Not Exists!'); $("#alertok").focus();
                    }
                }
            }
        });
    }
};










//InvoiceQunatity Calculation
function InvoiceQunatityCalculation(tr, FormId) {
    debugger
    var soid = $("#SATABLE009COLUMN06").val();
    var aqty = $(tr).find("#aSATABLE010COLUMN10").val();
    var qty = $(tr).find("#SATABLE010COLUMN10").val();
    var oqty = $(tr).find("#SATABLE010COLUMN08").val();
    if (typeof aqty == "undefined") { aqty = oqty; }
    var avlqty = $(tr).find("#SATABLE010COLUMN11").val();
    var price = $(tr).find("#SATABLE010COLUMN13").val();
    if (qty == '') { qty = 0; }
    if (price == '') { price = 0; }
    if (aqty == '') { aqty = 0; }
    if (FormId == 1287) {
        if (parseFloat(qty) > 0.00 && !(isNaN(parseFloat(qty).toFixed(2))) && qty.match('^([0-9]+)(\.[0-9]+)?$')) {
            if (parseFloat(qty) > (parseFloat(aqty))) {
                $(tr).find("#SATABLE010COLUMN10").val(aqty);
                $(tr).find("#SATABLE010COLUMN14").val(parseFloat(price) * parseFloat(aqty));
                Alert.render('Qty must not exceed Order Quantity'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close");
                    $(tr).find("#SATABLE010COLUMN10").focus();
                })
                return false;
            } BillServicesCalculations();
        }
    } else {
        debugger

        if (parseFloat(qty) > 0 && !(isNaN(parseFloat(qty).toFixed(2)))) {
            if (soid != '') {
                if (parseFloat(qty) > (parseFloat(aqty))) {
                    $(tr).find("#SATABLE010COLUMN10").val(aqty);
                    $(tr).find("#SATABLE010COLUMN14").val(parseFloat(price) * parseFloat(aqty));
                    Alert.render('Qty must not exceed available quantity'); $("#alertok").focus();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close");
                        $(tr).find("#SATABLE010COLUMN10").focus();
                    })
                }
                else if (parseFloat(qty) > (parseFloat(avlqty))) {
                    if ($(tr).find("#SATABLE010COLUMN04").val() == "") {
                        $(tr).find("#SATABLE010COLUMN10").val(aqty);
                        $(tr).find("#SATABLE010COLUMN14").val(parseFloat(price) * parseFloat(aqty));
                        Alert.render('Qty must not exceed available quantity'); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close");
                            $(tr).find("#SATABLE010COLUMN10").focus();
                        })
                    }
                    else {
                        $(tr).find("#SATABLE010COLUMN14").val(parseFloat(price) * parseFloat(qty));
                        //EMPHCS684 Rajasekhar Patakota 18/7/2015 Quantity Calculation Changes
                        SOInvoiceTaxCalculations(tr);
                    }
                }
                else {
                    $(tr).find("#SATABLE010COLUMN14").val(parseFloat(price) * parseFloat(qty));
                    //EMPHCS684 Rajasekhar Patakota 18/7/2015 Quantity Calculation Changes
                    SOInvoiceTaxCalculations(tr);
                }
            }
            else {
                if (parseFloat(qty) > (parseFloat(avlqty))) {
                    $(tr).find("#SATABLE010COLUMN10").val(aqty);
                    $(tr).find("#SATABLE010COLUMN10").val(avlqty); qty = avlqty;
                    Alert.render('Qty must not exceed available quantity'); $("#alertok").focus();
                    $("#alertok").on("click", function () {
                        $("#dialogbox").dialog("close");
                        $(tr).find("#SATABLE010COLUMN10").focus();
                    })
                }
                $(tr).find("#SATABLE010COLUMN14").val(parseFloat(price) * parseFloat(qty));
                debugger
                SOInvoiceTaxCalculations(tr);
            }
        }
        else {
            Alert.render('Please Enter Value Greater Than Zero Only'); $("#alertok").focus();
            $(tr).find("#SATABLE010COLUMN10").val(''); $(tr).find("#SATABLE010COLUMN14").val(0);
            $("#alertok").on("click", function () {
                $("#dialogbox").dialog("close"); $(tr).find("#SATABLE010COLUMN10").focus();
            })
        }
        //InvoiceCalculations();
    }
}

//ItemReceiptLineQuantity Calculations
function ItemReceiptLineQuantity(tr,FormID) {
    var qty = $(tr).find("#PUTABLE004COLUMN08").val();
    var price = $(tr).find("#PUTABLE004COLUMN10").val();
    var Item = $(tr).find("#PUTABLE004COLUMN03").val();
    var uom = $(tr).find("#PUTABLE004COLUMN17").val();
    var JOID = $("#PUTABLE003COLUMN06").val();
    var JOIssue = $("#PUTABLE003COLUMN10").val();
    //EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
    var ProdID = $("#PUTABLE003COLUMN23").val();
    var count = 0;
    var ACtualQty = 0;
    if (qty == '') { qty = 0; }
    if (price == '') { price = 0; }
    var aqty = $(tr).find("#aPUTABLE004COLUMN08").val();
    aqty = $(tr).find("#PUTABLE004COLUMN06").val();
    //EMPHCS806 rajasekhar reddy patakota 27/7/2015 Remaining Quantity calculation
    var rqty = $(tr).find("#PUTABLE004COLUMN09").val();
    var edit = window.location.href.slice(window.location.href).split("?");
    var editval = edit[0].split("/").indexOf("Edit", 0);
    if (parseFloat(editval) > 0) { aqty = parseFloat(aqty) + parseFloat(rqty); }
    if (FormID == 1286) {
        $.ajax({
            url: "/JobOrderReceipt/RemainingQTY?FormName" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ FormID: FormID, JOID: JOID, JOISSue: JOIssue, Item: Item, uom: uom, ProdID: ProdID }),
            success: function (data) {
                debugger
                if (data != "") {
                    debugger
                    count = data.Data2;
                    ACtualQty = data.Data1;
                    if (count >= 1) {
                        var PQty = (parseFloat(aqty) - parseFloat(ACtualQty)).toFixed(2);
                        if (qty > PQty) {
                            Alert.render('Actual Qty Should not Greater Than Expected Quantity Only'); $("#alertok").focus();
                            $("#alertok").on("click", function () {
                                $("#dialogbox").dialog("close");
                                $(tr).find("#PUTABLE004COLUMN08").val((parseFloat(aqty) - parseFloat(ACtualQty)).toFixed(2));
                                $(tr).find("#PUTABLE004COLUMN09").val((parseFloat(aqty) - (parseFloat(PQty) + parseFloat(ACtualQty))).toFixed(2));
                                $(tr).find("#PUTABLE004COLUMN11").val((parseFloat(price) * parseFloat(PQty)).toFixed(2));
                                $(tr).find("#PUTABLE004COLUMN08").focus();
                            })
                            return false;
                        }
                    }
                    else if (aqty == '') {
                        aqty = 0;
                        Alert.render('Expected Qty Should Be Greater Than Actual Quantity Only'); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close");
                            $(tr).find("#PUTABLE004COLUMN08").val('');
                            $(tr).find("#PUTABLE004COLUMN09").val('');
                            $(tr).find("#PUTABLE004COLUMN11").val('');
                            $(tr).find("#PUTABLE004COLUMN06").focus();
                        })
                        return false;
                    }
                    else if (parseFloat(qty) > parseFloat(aqty)) {
                        Alert.render('Actual Quantity Should Be Less Than Expected Qty Only'); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close");
                            $(tr).find("#PUTABLE004COLUMN08").val(aqty);
                            $(tr).find("#PUTABLE004COLUMN09").val((parseFloat(aqty) - parseFloat(aqty)).toFixed(2));
                            $(tr).find("#PUTABLE004COLUMN11").val((parseFloat(price) * parseFloat(aqty)).toFixed(2));
                            $(tr).find("#PUTABLE004COLUMN08").focus();
                        })
                        return false;
                    }
                    else {
                        $(tr).find("#PUTABLE004COLUMN09").val((parseFloat(aqty) - parseFloat(qty)).toFixed(2));
                        $(tr).find("#PUTABLE004COLUMN11").val((parseFloat(price) * parseFloat(qty)).toFixed(2)); return false;
                    }
                }
            }
        });
        if (qty > 0 && !(isNaN(parseFloat(qty))) && qty.match('^([0-9]+)(\.[0-9]+)?$')) {
            if (parseFloat(qty) > (parseFloat(aqty))) {
                //EMPHCS806 rajasekhar reddy patakota 27/7/2015 Remaining Quantity calculation
                Alert.render('Qty must not exceed ordered quantity'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close");
                    $(tr).find("#PUTABLE004COLUMN08").val(aqty);
                    $(tr).find("#PUTABLE004COLUMN11").val((parseFloat(price) * parseFloat(aqty)).toFixed(2));
                    $(tr).find("#PUTABLE004COLUMN08").focus();
                })
            }
            else {
                $(tr).find("#PUTABLE004COLUMN11").val((parseFloat(price) * parseFloat(qty)).toFixed(2));
            }
        }
        else {
            Alert.render('Please Enter Value Greater Than Zero Only'); $("#alertok").focus();
            $(tr).find("#PUTABLE004COLUMN08").val(''); $(tr).find("#PUTABLE004COLUMN11").val(0);
            $("#alertok").on("click", function () {
                $("#dialogbox").dialog("close"); $(tr).find("#PUTABLE004COLUMN08").focus();
            })
        }
    }
}


function ItemReceiptLineQuantityNew(tr, FormID) {
    debugger
    var qty = $(tr).find("#PUTABLE004COLUMN08").val();
    var price = $(tr).find("#PUTABLE004COLUMN10").val();
    var Item = $(tr).find("#PUTABLE004COLUMN03").val();
    var uom = $(tr).find("#PUTABLE004COLUMN17").val();
    var JOID = $("#PUTABLE003COLUMN06").val();
    var JOIssue = $("#PUTABLE003COLUMN10").val();
    var count = 0;
    var ACtualQty = 0;
    if (qty == "" || typeof (qty) == "undefined") { qty = "0"; }
    if (price == "" || typeof (price) == "undefined") { price = "0"; }
   
    var aqty = $(tr).find("#aPUTABLE004COLUMN08").val();
    if (aqty == "" || typeof (aqty) == "undefined") { aqty = "0"; }
    var Rqty = $(tr).find("#aPUTABLE004COLUMN09").val();
    if (Rqty == "" || typeof (Rqty) == "undefined") { Rqty = "0"; }
    var Eqty = $(tr).find("#PUTABLE004COLUMN06").val();
    if (Eqty == "" || typeof (Eqty) == "undefined") { Eqty = "0"; }
    //EMPHCS806 rajasekhar reddy patakota 27/7/2015 Remaining Quantity calculation
    var rqty = $(tr).find("#PUTABLE004COLUMN09").val();
    var edit = window.location.href.slice(window.location.href).split("?");
    var editval = edit[0].split("/").indexOf("Edit", 0);
    if (parseFloat(editval) > 0) { aqty = parseFloat(aqty) + parseFloat(rqty); }
    if (FormID == 1286) {
        $.ajax({
            url: "/JobOrderReceipt/RemainingQTY?FormName" + FormName + "",
            type: "POST",async:false,
            contentType: "application/json",
            data: JSON.stringify({ FormID: FormID, JOID: JOID, JOISSue: JOIssue, Item: Item, uom: uom }),
            success: function (data) {
                if (data != "") {
                    count = data.Data2;
                    ACtualQty = data.Data1;
                    if (count >= 1) {
                        //var PQty = (parseFloat(aqty) - parseFloat(ACtualQty)).toFixed(2);
                        var PQty = $(tr).find("#PUTABLE004COLUMN06").val();
                        if (PQty == "" || typeof (PQty) == "undefined") { PQty = "0"; }
                        PQty = parseFloat(PQty);
                        if (qty > PQty && PQty!="0") {

                            $("#divLoading").hide();
                            var msg = 'Actual Quantity Greater Than Expected Qty.Do You Want To Continue ? ';
                            var div = $("<div>" + msg + "</div>");
                            div.dialog({
                                title: "Confirm",
                                buttons: [
                                    {
                                        text: "Yes", id: "btnok",
                                        click: function () {
                                            div.dialog("close");
                                            $("#dialogbox").dialog("close");
                                            $(tr).find("#PUTABLE004COLUMN11").val((parseFloat(price) * parseFloat(qty)).toFixed(2));
                                            $(tr).find("#PUTABLE004COLUMN09").val((parseFloat(PQty) - (parseFloat(qty))).toFixed(2));
                                        }
                                    },
                                         {
                                             text: "No",
                                             click: function () {

                                                 div.dialog("close"); debugger
                                                 $("#dialogbox").dialog("close");
                                                 $(tr).find("#PUTABLE004COLUMN08").val((parseFloat(Eqty) - (parseFloat(Rqty))).toFixed(2));
                                                 $(tr).find("#PUTABLE004COLUMN09").val((parseFloat(Rqty)).toFixed(2))
                                                 $(tr).find("#PUTABLE004COLUMN11").val((parseFloat(price) * ((parseFloat(Eqty) - (parseFloat(Rqty))).toFixed(2))).toFixed(2));
                                                 $(tr).find("#PUTABLE004COLUMN08").focus();
                                                 $("#totalQty").text(parseFloat(parseFloat($("#totalQty").text()) - parseFloat(qty) + parseFloat(Eqty)).toFixed(2));
                                             }
                                         }
                                ]
                            });
                            $(".ui-dialog").css("border", "3px solid");
                            $(".ui-dialog").css("padding", "0");
                            $(".ui-dialog").find(".ui-dialog-titlebar").css("background", "#666");
                            $(".ui-dialog").find(".ui-dialog-titlebar").css("padding", "10px 1.6px");
                            $(".ui-dialog").find(".ui-dialog-title").css("color", "white");
                            $(".ui-dialog").find(".ui-dialog-title").css("text-align", "center");
                            $(".ui-dialog").find(".ui-widget").css("background", "#cceded");
                            $(".ui-dialog").find(".ui-widget-content").css("background", "#cceded");
                            $(".ui-dialog").find(".ui-widget-content").css("color", "red");
                            $(".ui-dialog").find(".ui-widget-content").css("padding", "4px");
                            $(".ui-dialog").find(".ui-widget-content").css("text-align", "center");
                            $(".ui-dialog").find(".ui-dialog-buttonpane").css("background", "#9DD1EA");
                            $(".ui-dialog").find(".ui-dialog-buttonpane").css("margin-top", "0em");
                            $(".ui-dialog").find(".ui-button-text-only").css("background", "white");
                            return false;


                        }

                        else {
                            if (PQty != "0") {
                                $(tr).find("#PUTABLE004COLUMN09").val((parseFloat(PQty) - parseFloat(qty)).toFixed(2));
                            } else {
                                $(tr).find("#PUTABLE004COLUMN09").val(0);
                            } $(tr).find("#PUTABLE004COLUMN11").val((parseFloat(price) * parseFloat(qty)).toFixed(2)); return false;
                        }
                        return false;
                    }
                    else if (aqty == '') {
                        aqty = 0;
                        Alert.render('Expected Qty Should Be Greater Than Actual Quantity Only'); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close");
                            $(tr).find("#PUTABLE004COLUMN08").val('');
                            $(tr).find("#PUTABLE004COLUMN09").val('');
                            $(tr).find("#PUTABLE004COLUMN11").val('');
                            $(tr).find("#PUTABLE004COLUMN06").focus();
                        })
                        return false;
                    }
                    else if (parseFloat(qty) > parseFloat(Eqty) && Eqty != "0") {

                        $("#divLoading").hide();
                        var msg = 'Actual Quantity Greater Than Expected Qty.Do You Want To Continue ? ';
                        var div = $("<div>" + msg + "</div>");
                        div.dialog({
                            title: "Confirm",
                            buttons: [
                                {
                                    text: "Yes", id: "btnok",
                                    click: function () {
                                        div.dialog("close"); debugger
                                        $("#dialogbox").dialog("close");
                                        $(tr).find("#PUTABLE004COLUMN11").val((parseFloat(price) * parseFloat(qty)).toFixed(2));
                                        $(tr).find("#PUTABLE004COLUMN09").val((parseFloat(Eqty) - (parseFloat(qty))).toFixed(2));
                                    }
                                },
                                     {
                                         text: "No",
                                         click: function () {

                                             div.dialog("close"); debugger
                                             $("#dialogbox").dialog("close");
                                             $(tr).find("#PUTABLE004COLUMN08").val(Eqty);
                                             $(tr).find("#PUTABLE004COLUMN09").val((parseFloat(Eqty) - parseFloat(Eqty)).toFixed(2));
                                             $(tr).find("#PUTABLE004COLUMN11").val((parseFloat(price) * parseFloat(Eqty)).toFixed(2));
                                             $(tr).find("#PUTABLE004COLUMN08").focus();
                                         }
                                     }
                            ]
                        });
                        $(".ui-dialog").css("border", "3px solid");
                        $(".ui-dialog").css("padding", "0");
                        $(".ui-dialog").find(".ui-dialog-titlebar").css("background", "#666");
                        $(".ui-dialog").find(".ui-dialog-titlebar").css("padding", "10px 1.6px");
                        $(".ui-dialog").find(".ui-dialog-title").css("color", "white");
                        $(".ui-dialog").find(".ui-dialog-title").css("text-align", "center");
                        $(".ui-dialog").find(".ui-widget").css("background", "#cceded");
                        $(".ui-dialog").find(".ui-widget-content").css("background", "#cceded");
                        $(".ui-dialog").find(".ui-widget-content").css("color", "red");
                        $(".ui-dialog").find(".ui-widget-content").css("padding", "4px");
                        $(".ui-dialog").find(".ui-widget-content").css("text-align", "center");
                        $(".ui-dialog").find(".ui-dialog-buttonpane").css("background", "#9DD1EA");
                        $(".ui-dialog").find(".ui-dialog-buttonpane").css("margin-top", "0em");
                        $(".ui-dialog").find(".ui-button-text-only").css("background", "white");

                        return false;
                    }
                    else {
                        if (Eqty != "0") {
                            $(tr).find("#PUTABLE004COLUMN09").val((parseFloat(Eqty) - parseFloat(qty)).toFixed(2));
                        } else {
                            $(tr).find("#PUTABLE004COLUMN09").val(0);
                        }
                        $(tr).find("#PUTABLE004COLUMN11").val((parseFloat(price) * parseFloat(qty)).toFixed(2)); return false;
                    }
                }
            }
        });

    }
}
//JO Receipt Amount Calculation
function JOAmountCalculation(tr) {
    var qty = $(tr).find("[id='PUTABLE004COLUMN08']").val(); var price = $(tr).find("[id='PUTABLE004COLUMN10']").val();
    if (qty == "") { qty = 0; } else if (price == "") { price = 0; }
    if (qty <= 0) {
        $(tr).find("[id='PUTABLE004COLUMN11']").val(0);
        Alert.render('Quantity Should Be Greater Than Zero'); $("#alertok").focus();
        $("#alertok").on("click", function () {
            $("#dialogbox").dialog("close"); $(tr).find("[id='PUTABLE004COLUMN08']").focus();
        })
        return false;
    }
    else if (price <= 0) {
        $(tr).find("[id='PUTABLE004COLUMN11']").val(0);
        Alert.render('Price Should Be Greater Than Zero'); $("#alertok").focus();
        $("#alertok").on("click", function () {
            $("#dialogbox").dialog("close"); $(tr).find("[id='PUTABLE004COLUMN10']").focus();
        })
        return false;
    }
    else {
        var amt = qty * (price);
        $(tr).find("[id='PUTABLE004COLUMN11']").val(amt);
    }
}
//JOOrderQtyCalculation
function JOOrderQtyCalculation(tr, FormID) {
    if (FormID == 1286) {
        var aqty = $(tr).find("#PUTABLE004COLUMN06").val();
        var qty = $(tr).find("#PUTABLE004COLUMN08").val();
        var price = $(tr).find("#PUTABLE004COLUMN10").val();
        if (price == '') { price = 0; }
        if (aqty == '') { aqty = 0; } if (qty == '') { qty = 0; }
        if (aqty > 0 && !(isNaN(parseFloat(aqty))) && aqty.match('^([0-9]+)(\.[0-9]+)?$')) {
            if (parseFloat(qty) <= (parseFloat(aqty))) {
                $(tr).find("#PUTABLE004COLUMN09").val(parseFloat(aqty) - parseFloat(qty));
                $(tr).find("#PUTABLE004COLUMN11").val(parseFloat(price) * parseFloat(qty));
            }
            else {
                $(tr).find("#PUTABLE004COLUMN08").val('');
                $(tr).find("#PUTABLE004COLUMN09").val('');
                $(tr).find("#PUTABLE004COLUMN11").val('');
                $(tr).find("#PUTABLE004COLUMN06").focus();
                return false;
            }
        }
        else {
            Alert.render('Expected Qty Should Be Greater Than Zero Only'); $("#alertok").focus();
            $("#alertok").on("click", function () {
                $("#dialogbox").dialog("close");
                $(tr).find("#PUTABLE004COLUMN08").val('');
                $(tr).find("#PUTABLE004COLUMN09").val('');
                $(tr).find("#PUTABLE004COLUMN11").val('');
                $(tr).find("#PUTABLE004COLUMN06").val('');
                $(tr).find("#PUTABLE004COLUMN06").focus();
            })
            return false;
        }
    }
}
//Jo Receipt CheckBox Selection
function JoReceiptCheckBox(checkval) {
    if (checkval == true) {
        document.getElementById("PUTABLE003COLUMN20").value = true;
    }
    else {
        document.getElementById("PUTABLE003COLUMN20").value = '';
    }
};
//JOBill Details
function ddlSelectJOB(eve, FormName) {
    var JOID = eve;
    if (eve != "null") {
        $.ajax({
            url: "/BilltoJobber/getJOBillDetails?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ JOID: JOID }),
            success: function (data) {
                if (data != "") {
                    //JO BILL Header Fields
                    $("#SATABLE009COLUMN23").val(1000);
                    $("#SATABLE009COLUMN06").val(data.sid);
                    $("#SATABLE009COLUMN07").val(data.Data);
                    $("#SATABLE009COLUMN19").val(data.Data1);
                    $("#SATABLE009COLUMN05").val(data.Data2);
                    $("#select2-SATABLE009COLUMN05-container").text(data.cName);
                    $("#SATABLE009COLUMN11").val(data.Data3);
                    $("#SATABLE009COLUMN12").val(data.Data4);
                    $("#SATABLE009COLUMN14").val(data.Data5);
                    $("#SATABLE009COLUMN15").val(data.Data6);
                    $("#SATABLE009COLUMN17").val(data.Data7);
                    $("#SATABLE009COLUMN16").val(data.Data8);
                    $("#SATABLE009COLUMN20").val(parseFloat(data.Data9).toFixed(2));
                    $("#SATABLE009COLUMN22").val(parseFloat(data.Data9).toFixed(2));
                    $("#SATABLE009COLUMN09").val(data.Data10);
                    $("#SATABLE009COLUMN13").val(data.Data11);
                    $("#SATABLE009COLUMN21").val(data.Data12);
                    $("#SATABLE009COLUMN32").val(data.Data13);
                    $("#SATABLE009COLUMN33").val(data.Data14);
                    $("#SATABLE009COLUMN34").val(data.Data15);
                    $("#SATABLE009COLUMN35").val(data.Data16);
                    $("#SATABLE009COLUMN36").val(data.Data17);
                    if (data.grid != "undefined") {
                        $('#grdData').remove();
                        $('#itemgrid').html(data.grid);
                        $('#grdData').append(data.remain);
                    }
                    //EnableDisable();
                    EditLoadEnableDisable(1287);
                }
            }
        });
    }
};
//EMPHCS1784	Job Order for IN Process 15/7/2016
function JO_IN_LineQuantityCalculation(tr) {debugger
    var qty = 0; var price = 0; var discount = 0; var Amtin = 0; var disType = 0;
    qty = $(tr).find("[id='PUTABLE002COLUMN07']").val();
    price = $(tr).find("[id='PUTABLE002COLUMN09']").val();
    if (qty == "" || typeof (qty)=="undefined") { qty = "0"; }
    if (price == "" || typeof (price) == "undefined") { price = "0"; }
    if (isNaN(parseFloat(qty)) || parseFloat(qty) < 0 || !(qty.match('^([0-9]+)(\.[0-9]+)?$'))) {
        $(tr).find("#PUTABLE002COLUMN07").val(0);
        $(tr).find("#PUTABLE002COLUMN11").val(0);
        $(tr).find("#PUTABLE002COLUMN10").val(0);
        $(tr).find("#PUTABLE001COLUMN12").val(0);
        $(tr).find("#PUTABLE001COLUMN14").val(0);
        $(tr).find("#PUTABLE001COLUMN15").val(0);
        $(tr).find("#PUTABLE002COLUMN24").val(0);
        $(tr).find("[id='aPUTABLE002COLUMN24']").val(0);
        Alert.render('Please Enter More Than Zero Value for Quantity'); $("#alertok").focus();
        $("#alertok").on("click", function () {
            $("#dialogbox").dialog("close"); $(tr).find("#PUTABLE002COLUMN07").focus();
            TaxLevelCalculations(); POTAXCalculations();
        })
        return false;
    }
    else if (isNaN(parseFloat(price)) || parseFloat(price) < 0 || !(price.match('^([0-9]+)(\.[0-9]+)?$'))) {
        $(tr).find("#PUTABLE002COLUMN09").val(0);

        Alert.render('Please Enter More Than Zero Value for Price'); $("#alertok").focus();
        $("#alertok").on("click", function () {
            $("#dialogbox").dialog("close"); $(tr).find("#PUTABLE002COLUMN09").focus();
            TaxLevelCalculations(); POTAXCalculations();
        })
        return false;
    }else {
        $(tr).find("#PUTABLE002COLUMN24").val((parseFloat(qty) * parseFloat(price)).toFixed(2));
        var linetaxtype = $(tr).find("#PUTABLE002COLUMN25").val();
        var prty = $("#PUTABLE001COLUMN50").val();
        if (prty == "22335") { prty = 22335; }
        else { prty = 0; }
        var TaxCal = (parseFloat(qty) * parseFloat(price)).toFixed(2);
        $.ajax({
            url: "/JobOrder/GetTaxAmount",
            type: "POST",
            data: JSON.stringify({ TaxType: linetaxtype, TaxCal: TaxCal, FormName: $("#FormName").val() }),
            dataType: "json", cache: true,
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                debugger
                if (result.Tax != '') {
                    tax = result.Tax; var acttax = result.Tax; tax = (parseFloat(tax) * parseFloat(0.01));
                    var tt1 = result.TaxCal
                    logValue += " Line Tax Amount = Total (" + tt1 + ") - Discount (" + discount + ") * Tax (" + tax + ") \r\n";
                    if (disType == "22556") { discount = (parseFloat(tt1) * parseFloat(parseFloat(discount) * (0.01))).toFixed(2); }
                    if (Amtin == "22713") { tax = (parseFloat(tt1) * (parseFloat(acttax) / (100 + parseFloat(acttax)))).toFixed(2); $(tr).find("#PUTABLE002COLUMN24").val((parseFloat(tt1) - parseFloat(tax)).toFixed(2)); }
                    else { tax = (parseFloat(parseFloat(tt1)) * parseFloat(tax)).toFixed(2);
                        $(tr).find("#PUTABLE002COLUMN24").val((parseFloat(tt1)).toFixed(2));  }
                } else { tax = 0; }
                $(tr).find("[id='aPUTABLE002COLUMN24']").remove();
                $(tr).find("[id='PUTABLE002COLUMN24']").after($('<input>').attr({ type: 'hidden', id: 'aPUTABLE002COLUMN24' }).val(tax));
                PODiscountCalculations(tr);
                TaxLevelCalculations(); POTAXCalculations();
            }
        })
    }
};
function TaxLevelCalculations() {
    debugger
    var tbl = $("#grdData")[0]; var qty = 0; var price = 0; var discount = 0; var tqty = 0; var tprice = 0; var tdiscount = 0; var ttal = 0; var tax = 0; var ttax = 0; var amount = 0; var amount1 = 0; var ttal1 = 0; 
    var Ttax = 0; var disType = 0;
    var TaxCal = 0; var rowcount = 0;
    var PackingCharges = $("#PUTABLE001COLUMN40").val();
    var ShippingCharges = $("#PUTABLE001COLUMN41").val();
    var Amtin = $("#PUTABLE001COLUMN49").val();
    if (typeof (PackingCharges) == "undefined" || PackingCharges == "")
        PackingCharges = 0;
    if (typeof (ShippingCharges) == "undefined" || ShippingCharges == "")
        ShippingCharges = 0;
    $("#PUTABLE001COLUMN32").val("");
    for (var irow = 1; irow < tbl.rows.length  ; irow++) {
        rowcount = rowcount + 1;
        var trw = tbl.rows[irow];
        if ($(trw).find("#checkRow").is(':checked')) {
            qty = $(trw).find("[id='PUTABLE002COLUMN07']").val();
            price = $(trw).find("[id='PUTABLE002COLUMN09']").val();
            var price1 = $(trw).find("[id='PUTABLE002COLUMN32']").val();
            discount = $(trw).find("[id='PUTABLE002COLUMN10']").val();
            if (typeof (discount) == "undefined")
                discount = $(trw).find("[id='Discount']").val();
            disType = $(trw).find("[id='PUTABLE002COLUMN33']").val();
            tax = $(trw).find("[id='aPUTABLE002COLUMN24']").val();
            if (qty == "") { qty = 0; }
            if (price == "") { price = 0; }
            if (price1 == "" || typeof (price1) == "undefined") { price1 = price; } else price = price1;
            amount = $(trw).find("[id='PUTABLE002COLUMN24']").val();
            if (amount == "" || typeof (amount) == "undefined") { amount = 0; }
            if (discount == "" || typeof (discount) == "undefined") { discount = 0; }
            if (disType == "" || typeof (disType) == "undefined") { disType = 0; }
            if (tax == "" || typeof (tax) == "undefined") { tax = 0; }
            ttax = parseFloat(ttax) + parseFloat(tax);
            tqty = parseFloat(tqty) + parseFloat(qty);
            tprice = parseFloat(tprice) + parseFloat(price);
            if (disType == "22556") { discount = ((parseFloat(amount)) * parseFloat(parseFloat(discount) * (0.01))).toFixed(2); tdiscount = parseFloat(tdiscount) + parseFloat(discount); }
            else tdiscount = parseFloat(tdiscount) + parseFloat(discount);
            ttal = parseFloat(ttal) + parseFloat(amount);
            if (qty > 0 && price > 0) {
                logValue += " Line Amount = Total (" + amount + ")  \r\n";
                logValue += " Line Total Amount = Amount (" + amount + ") + Tax Amount (" + tax + ")  \r\n";
                if (Amtin == "22713") { $(trw).find("#PUTABLE002COLUMN11").val(((parseFloat(amount) + parseFloat(tax))).toFixed(2)); $(trw).find("#PUTABLE002COLUMN24").val((parseFloat(amount)).toFixed(2)); }
                else
                    $(trw).find("#PUTABLE002COLUMN11").val(((parseFloat(amount) + parseFloat(tax))).toFixed(2));
            }
            else {
                $(trw).find("#PUTABLE002COLUMN11").val(0); $(trw).find("#PUTABLE002COLUMN24").val(0);  }
            if (rowcount == tbl.rows.length - 1) {
                var headertax = $("#aPUTABLE001COLUMN32").val();
                var TaxTypeH = $("#PUTABLE001COLUMN31").val();
                if (TaxTypeH == "" || TaxTypeH == null || typeof (TaxTypeH) == "undefined" || isNaN(TaxTypeH)) TaxTypeH = 0;
                if (headertax == "" || headertax == null || typeof (headertax) == "undefined" || isNaN(headertax)) { headertax = 0; }
                ttax = parseFloat(ttax) + parseFloat(headertax);
                $("#PUTABLE001COLUMN12").val(parseFloat(ttal).toFixed(2));
                $("#PUTABLE001COLUMN12").val((parseFloat(ttal) + parseFloat(PackingCharges) + parseFloat(ShippingCharges)).toFixed(2));
                $("#PUTABLE001COLUMN14").val(parseFloat(tdiscount).toFixed(2));
                if (TaxTypeH == 0) {
                    $("#PUTABLE001COLUMN32").val(ttax.toFixed(2));
                    if (Amtin == "22713") $("#PUTABLE001COLUMN15").val(((parseFloat(ttal) + parseFloat(ttax))).toFixed(2));
                    else $("#PUTABLE001COLUMN15").val(((parseFloat(ttal) + parseFloat(ttax))).toFixed(2));
                    logValue += " Header Total Amount  = Sub Total (" + parseFloat(ttal).toFixed(2) + ") - Discount (" + parseFloat(tdiscount).toFixed(2) + ") + Tax (" + parseFloat(ttax).toFixed(2) + ") \r\n";
                }
            }
            else {
                $("#PUTABLE001COLUMN32").val("");
                $("#PUTABLE001COLUMN12").val("");
                $("#PUTABLE001COLUMN15").val("");
                $("#PUTABLE001COLUMN14").val("");
            }
        } else { 
            if (rowcount == tbl.rows.length - 1) {
                var headertax = $("#aPUTABLE001COLUMN32").val();
                if (headertax == "" || headertax == null || typeof (headertax) == "undefined" || isNaN(headertax)) { headertax = 0; }
                ttax = parseFloat(ttax) + parseFloat(headertax);
                $("#PUTABLE001COLUMN32").val(ttax.toFixed(2));
                $("#PUTABLE001COLUMN12").val((parseFloat(ttal) + parseFloat(PackingCharges) + parseFloat(ShippingCharges)).toFixed(2));
                $("#PUTABLE001COLUMN14").val(parseFloat(tdiscount).toFixed(2));
                if (Amtin == "22713") $("#PUTABLE001COLUMN15").val(((parseFloat(ttal)) + parseFloat(ttax) + parseFloat(PackingCharges) + parseFloat(ShippingCharges)).toFixed(2));
                else $("#PUTABLE001COLUMN15").val(((parseFloat(ttal)) + parseFloat(ttax) + parseFloat(PackingCharges) + parseFloat(ShippingCharges)).toFixed(2));
                logValue += " Header Total Amount  = Sub Total (" + parseFloat(ttal).toFixed(2) + ") - Discount (" + parseFloat(tdiscount).toFixed(2) + ") + Tax (" + parseFloat(ttax).toFixed(2) + ")+ PackingCharges (" + parseFloat(PackingCharges).toFixed(2) + ")+ ShippingCharges (" + parseFloat(ShippingCharges).toFixed(2) + ") \r\n";

            }
        }
    }
    $.ajax({
        url: "/FormBuilding/CreateLog",
        type: "POST",
        data: JSON.stringify({ LogValue: logValue, fname: "Purchase Order Tax" }),
        dataType: "json", cache: true,
        contentType: "application/json; charset=utf-8",
        success: function (result) { }
    });
}

function POTAXCalculations() {
    debugger
    var Amtin = 0;
    var PackingCharges = $("#PUTABLE001COLUMN40").val();
    var ShippingCharges = $("#PUTABLE001COLUMN41").val();
    if (typeof (PackingCharges) == "undefined" || PackingCharges == "")
        PackingCharges = 0;
    if (typeof (ShippingCharges) == "undefined" || ShippingCharges == "")
        ShippingCharges = 0;
    var taxamount = 0;
    var subtotal = $("#PUTABLE001COLUMN12").val();
    var discount = $("#PUTABLE001COLUMN14").val();
    var total = $("#PUTABLE001COLUMN15").val();
    if (typeof (subtotal) == "undefined") {
        var tbls = $("#grdData")[0]; var lsubtotal = 0;var ltotal = 0;
        for (var irow = 1; irow < tbls.rows.length  ; irow++) {
            var trws = tbls.rows[irow];
            ltotal = $(trws).find("#PUTABLE002COLUMN24").val();
            if (ltotal== "" || typeof (ltotal) == "undefined") {ltotal= 0; }
            lsubtotal = parseFloat(lsubtotal) + parseFloat(ltotal);
        }
        subtotal = lsubtotal;
    }
    else if (subtotal == "") subtotal = 0;
    if (discount == "" || typeof (discount) == "undefined") { discount = 0; }
    if (total == "" || typeof (total) == "undefined") { total = 0; } var linetax = 0;
    TaxTypeH = $("#PUTABLE001COLUMN31").val();
    var prty = $("#PUTABLE001COLUMN50").val();
    if (prty == "22335") { prty = 22335; }
    else { prty = 0; }
    $.ajax({
        url: "/JobOrder/GetTaxAmount",
        type: "POST",
        data: JSON.stringify({ TaxType: TaxTypeH, TaxCal: null, FormName: $("#FormName").val() }),
        dataType: "json", cache: true,
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            debugger
            if (result.Tax != '') {
                tax = result.Tax; tax = (parseFloat(tax) * parseFloat(0.01));
                discount = $("#PUTABLE001COLUMN14").val();
                if (discount == "" || typeof (discount) == "undefined") { discount = 0; }
                 logValue += " Header Tax Amount = Subtotal (" + subtotal + ") - Discount (" + discount + ")- PackingCharges (" + PackingCharges + ") - ShippingCharges (" + ShippingCharges + ") * Tax (" + tax + ") \r\n";
                tax = (parseFloat(parseFloat(subtotal) - parseFloat(discount) - parseFloat(PackingCharges) - parseFloat(ShippingCharges)) * parseFloat(tax)).toFixed(2);
            } else { tax = 0; } $("#aPUTABLE001COLUMN32").val(tax);
            var tbl = $("#grdData")[0];
            for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                var trw = tbl.rows[irow];
                if ($(trw).find("#checkRow").is(':checked')) {
                    linetax = $(trw).find("#aPUTABLE002COLUMN24").val();
                    if (linetax == "" || typeof (linetax) == "undefined") linetax = 0;
                    logValue += " Line Tax Amount Item " + irow + "= (" + linetax + ") \r\n";
                    taxamount = (parseFloat(taxamount) + parseFloat(linetax));
                }
            }
            $("#PUTABLE001COLUMN32").val((parseFloat(taxamount) + parseFloat(tax)).toFixed(2));
            if (Amtin == "22713") $("#PUTABLE001COLUMN15").val(((parseFloat(subtotal) + (parseFloat(taxamount) + parseFloat(tax)) )).toFixed(2));
            else $("#PUTABLE001COLUMN15").val(((parseFloat(subtotal) ) + (parseFloat(taxamount) + parseFloat(tax))).toFixed(2));
            logValue += " Total Tax Amount = Heder Tax (" + tax + ") + Line Tax (" + taxamount + ") \r\n";
            logValue += " Total Amount = Sub Total (" + subtotal + ") - Discount (" + discount + ") + Heder Tax (" + tax + ") + Line Tax (" + taxamount + ") \r\n";
        }

    });
    $.ajax({
        url: "/FormBuilding/CreateLog",
        type: "POST",
        data: JSON.stringify({ LogValue: logValue, fname: "Purchase Order Tax" }),
        dataType: "json", cache: true,
        contentType: "application/json; charset=utf-8",
        success: function (result) { }
    });
}
function LineTax(tr) {debugger
    var qty = 0; var price = 0; var discount = 0;
    qty = $(tr).find("[id='PUTABLE002COLUMN07']").val();
    price = $(tr).find("[id='PUTABLE002COLUMN09']").val();
    var price1 = $(tr).find("[id='PUTABLE002COLUMN32']").val();
    if (typeof (price1) != "undefined" && price1 != "") price = price1; else price1 = price;
    var Amtin = $("#PUTABLE001COLUMN49").val();
    discount = $(tr).find("[id='PUTABLE002COLUMN10']").val();
    if (typeof (discount) == "undefined")
        discount = $(tr).find("[id='Discount']").val();
    if (qty == "") { qty = 0; }
    if (price == "") { price = 0; }
    if (discount == "" || typeof (discount) == "undefined") { discount = "0"; }
    $(tr).find("#PUTABLE002COLUMN24").val((parseFloat(qty) * parseFloat(price)).toFixed(2));
    var linetaxtype = $(tr).find("#PUTABLE002COLUMN25").val();
    var prty = $("#PUTABLE001COLUMN50").val();
    if (prty == "22335") { prty = 22335; }
    else { prty = 0; }
    var TaxCal = $(tr).find("#PUTABLE002COLUMN24").val();
    $.ajax({
        url: "/JobOrder/GetTaxAmount",
        type: "POST",
        data: JSON.stringify({ TaxType: linetaxtype, TaxCal: TaxCal, FormName: $("#FormName").val() }),
        dataType: "json", cache: true,
        contentType: "application/json; charset=utf-8",
        success: function (result) {
            if (result.Tax != '') {
                debugger
                tax = result.Tax; var acttax = result.Tax; tax = (parseFloat(tax) * parseFloat(0.01));
                var tt1 = result.TaxCal;
                discount = $(tr).find("[id='PUTABLE002COLUMN10']").val(); if (discount == "" || typeof (discount) == "undefined") { discount = "0"; }
                logValue += " Line Tax Amount = Total (" + tt1 + ") - Discount (" + discount + ") * Tax (" + tax + ") \r\n";
                if (Amtin == "22713") { tax = (parseFloat(tt1) * (parseFloat(acttax) / (100 + parseFloat(acttax)))).toFixed(2); $(tr).find("#PUTABLE002COLUMN24").val((parseFloat(tt1) - parseFloat(tax)).toFixed(2)); }
                else { tax = (parseFloat(parseFloat(tt1)) * parseFloat(tax)).toFixed(2); $(tr).find("#PUTABLE002COLUMN24").val((parseFloat(tt1)).toFixed(2)); }
            } else { tax = 0; }
            $(tr).find("[id='aPUTABLE002COLUMN24']").val(tax);
            TaxLevelCalculations(); POTAXCalculations();
        }
    })
}
function ddlSelectCustomer(eve, FormName, FormId) {debugger
    var CustomerId = eve;
    if (eve != "null") {
        $.ajax({
            url: "/JobOrder/GetCustomerAddres?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ CustomerId: CustomerId }),
            success: function (data) {
                debugger
                if (data != "") {debugger
                    var items = [];
                    items.push("<option value=''>" + "--Select--" + "</option>");
                    for (var i = 0; i < data.item.length; i++) {
                        items.push("<option value=" + data.item[i].Value + ">" + data.item[i].Text + "</option>");
                    }
                    $("#SATABLE003COLUMN11").html(items.join(' '));
                    $("#SATABLE003COLUMN06").val(data.Data1);
                    $("#SATABLE003COLUMN07").val(data.Data2);
                    $("#SATABLE003COLUMN08").val(data.Data3);
                    $("#SATABLE003COLUMN09").val(data.Data4);
                    $("#SATABLE003COLUMN10").val(data.Data5);
                    $("#SATABLE003COLUMN11").val(data.Data6);
                    $("#SATABLE003COLUMN12").val(data.Data7);
                    $("#SATABLE003COLUMN16").val(data.Data8);

                    if (data.data17 == "True") {
                        $("#SATABLE003COLUMN17").prop("checked", true);
                    }
                    else { $("#SATABLE003COLUMN17").prop("checked", false); }
                    if (data.data18 == "True") {
                        $("#SATABLE003COLUMN18").prop("checked", true);
                    }
                    else { $("#SATABLE003COLUMN18").prop("checked", false); }
                }
            }
        });
    }
};


function JORddlSelectGrn(eve, FormName) {debugger
    var JOID = eve;
    if (eve != "null") {
        $.ajax({
            url: "/JobOrderReceipt/JORddlSelectGrn?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ JOID: JOID }),
            success: function (data) {
                if (data != "") {

                    ddlSelect(data.pid, FormName);
                }
            }
        });
    }
};
function ddlSelect(eve, FormName) {debugger
    var PurchaseOrderID = eve;
    if (eve != "null") {
        $.ajax({
            url: "/JobOrderIssue/getHeaderJORIN?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({ PurchaseOrderID: PurchaseOrderID }),
            success: function (data) {
                debugger
                if (data != "") {
                    $("#PUTABLE003COLUMN05").val(data.Data14);
                    $("#select2-PUTABLE003COLUMN05-container").text(data.cName);
                    $("#PUTABLE003COLUMN06").val(data.pid);
                    $("#PUTABLE003COLUMN08").val(data.Data);
                    $("#PUTABLE003COLUMN10").val(data.Data1);
                    $("#PUTABLE003COLUMN11").val(data.Data2);
                    $("#PUTABLE003COLUMN12").val(data.Data3);
                    $("#PUTABLE003COLUMN13").val(data.Data4);
                    $("#PUTABLE003COLUMN14").val(data.Data5);
                    $("#PUTABLE003COLUMN15").val(data.Data6);
                    $("#PUTABLE003COLUMN16").val(data.Data7);
                    $("#PUTABLE003COLUMN17").val(data.DataRT);
                    $("#PUTABLE003COLUMN18").val(data.Data15);
                    $("#PUTABLE003COLUMN19").val(data.Data16);
                    $("#PUTABLE003COLUMN21").val(data.Data17);
                    $("#PUTABLE003COLUMN22").val(data.Data18);
                    //IR  Line Fields
                    $("#PUTABLE004COLUMN03").val(data.Data8);
                    $("#PUTABLE004COLUMN04").val(data.Data9);
                    $("#PUTABLE004COLUMN06").val(data.Data11);
                    $("#PUTABLE004COLUMN07").val(data.Data12);
                    $("#PUTABLE004COLUMN10").val(data.Data13);
                    $("#PUTABLE004COLUMN17").val(data.Data19);

                    $("#PUTABLE004COLUMN08").val(parseInt(data.Data11) - parseInt(data.Data12));

                    $("#PUTABLE004COLUMN07").prop("readonly", true);
                    $("#PUTABLE004COLUMN09").prop("readonly", true);
                    $('#grdData').remove();
                    $('#itemgrid').html(data.grid);
                    $('#grdData').append(data.remain);
                    $("#PUTABLE003COLUMN06").val(PurchaseOrderID);
                    dateupdate();
                    //EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
                    var s = 1;
                    var tbl = $("#grdData")[0];
                    for (var irow = 1; irow < tbl.rows.length  ; irow++) {
                        var trw = tbl.rows[irow];
                        $(trw).find("#PUTABLE004COLUMN03").prop("disabled", true);
                        $(trw).find("#PUTABLE004COLUMN06").prop("readonly", true);
                        $(trw).find("#PUTABLE004COLUMN07").prop("readonly", true);
                        $(trw).find("#PUTABLE004COLUMN11").prop("readonly", true);
                        //$(trw).find("#PUTABLE004COLUMN17").prop("disabled", true);
                       // $(trw).find("#PUTABLE004COLUMN10").prop("readonly", true);
                        $(trw).find("#PUTABLE004COLUMN25").prop("readonly", true);
                        $(trw).find("#PUTABLE004COLUMN09").prop("readonly", true);
                        //EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
                        $(trw).find("#PUTABLE004COLUMN27").val(s);
                        $(trw).find("#PUTABLE004COLUMN27").prop("readonly", true);
                        s = s + 1;
                    }
                }
            }
        });
    }
};
function JOR_IN_LineQuantity(tr, FormID) {
    var qty = $(tr).find("#PUTABLE004COLUMN08").val();
    var price = $(tr).find("#PUTABLE004COLUMN10").val();
    var price1 = $(tr).find("#PUTABLE004COLUMN25").val();
    if (typeof (price1) != "undefined" && price1 != "") { price = price1; }
    if (qty == '') { qty = 0; }
    if (price == '') { price = 0; }
    var aqty = $(tr).find("#aPUTABLE004COLUMN08").val();
    var rqty = $(tr).find("#PUTABLE004COLUMN09").val();
    var edit = window.location.href.slice(window.location.href).split("?");
    var editval = edit[0].split("/").indexOf("Edit", 0);
    if (parseFloat(editval) > 0) { aqty = parseFloat(aqty) + parseFloat(rqty); }
    if (FormID == 1286||FormID == 1587) {
        aqty = $(tr).find("#PUTABLE004COLUMN06").val();
        if (aqty == '') {
            aqty = 0;
            Alert.render('Expected Qty Should Be Greater Than Actual Quantity Only'); $("#alertok").focus();
            $("#alertok").on("click", function () {
                $("#dialogbox").dialog("close");
                $(tr).find("#PUTABLE004COLUMN08").val('');
                $(tr).find("#PUTABLE004COLUMN09").val('');
                $(tr).find("#PUTABLE004COLUMN11").val('');
                $(tr).find("#PUTABLE004COLUMN06").focus();
            })
            return false;
        }
        else if (parseFloat(qty) > parseFloat(aqty)) {
            Alert.render(' Quantity Should Be Less Than Ordered Qty Only'); $("#alertok").focus();
            $("#alertok").on("click", function () {
                $("#dialogbox").dialog("close");
                if (parseFloat(aqty) == parseFloat(rqty)) {
                    $(tr).find("#PUTABLE004COLUMN08").val(aqty);
                }
                else {
                    $(tr).find("#PUTABLE004COLUMN08").val(rqty);
                }
                //$(tr).find("#PUTABLE004COLUMN09").val((parseFloat(aqty) - parseFloat(aqty)).toFixed(2));
                //$(tr).find("#PUTABLE004COLUMN11").val((parseFloat(price) * parseFloat(aqty)).toFixed(2));
                $(tr).find("#PUTABLE004COLUMN08").focus();
            })
            return false;
        }
        //else if (parseFloat(qty) > parseFloat(rqty)) {
        //    Alert.render(' Quantity Should Be Less Than Remaining Qty Only'); $("#alertok").focus();
        //    $("#alertok").on("click", function () {
        //        $("#dialogbox").dialog("close");
        //        $(tr).find("#PUTABLE004COLUMN08").val(rqty);
        //        $(tr).find("#PUTABLE004COLUMN09").val((parseFloat(aqty) - parseFloat(qty)).toFixed(2));
        //        $(tr).find("#PUTABLE004COLUMN11").val((parseFloat(price) * parseFloat(qty)).toFixed(2));
        //        $(tr).find("#PUTABLE004COLUMN08").focus();
        //    })
        //    return false;
        //}
        else {
            $(tr).find("#PUTABLE004COLUMN09").val((parseFloat(aqty) - parseFloat(qty)).toFixed(2));
            $(tr).find("#PUTABLE004COLUMN11").val((parseFloat(price) * parseFloat(qty)).toFixed(2)); return false;
        }
    }
    if (qty > 0 && !(isNaN(parseFloat(qty))) && qty.match('^([0-9]+)(\.[0-9]+)?$')) {
        //EMPHCS1726	Ovreages allowed in item receipt Raj.Jr
        $.ajax({
            url: "/FormBuilding/salesAvlCheckQty?FormName=" + FormName + "",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({}),
            success: function (data) {
                debugger
                if (data != "") {
                    Owner = data.AcOwner;
                }
                if (Owner != "True") {
                    if (parseFloat(qty) > (parseFloat(aqty))) {
                        Alert.render('Qty must not exceed ordered quantity'); $("#alertok").focus();
                        $("#alertok").on("click", function () {
                            $("#dialogbox").dialog("close");
                            $(tr).find("#PUTABLE004COLUMN08").val(aqty);
                            $(tr).find("#PUTABLE004COLUMN11").val((parseFloat(price) * parseFloat(aqty)).toFixed(2));
                            $(tr).find("#PUTABLE004COLUMN08").focus();
                        })
                    }
                    //else if (parseFloat(qty) > parseFloat(rqty)) {
                    //    Alert.render(' Quantity Should Be Less Than Remaining Qty Only'); $("#alertok").focus();
                    //    $("#alertok").on("click", function () {
                    //        $("#dialogbox").dialog("close");
                    //        $(tr).find("#PUTABLE004COLUMN08").val(rqty);
                    //        $(tr).find("#PUTABLE004COLUMN08").focus();
                    //    })
                    //    return false;
                    //}
                    else {
                        $(tr).find("#PUTABLE004COLUMN11").val((parseFloat(price) * parseFloat(qty)).toFixed(2));
                        //$(tr).find("#PUTABLE004COLUMN09").val((parseFloat(rqty) - parseFloat(qty)).toFixed(2));
                    }
                }
            }
        })
    }
}

function getInvoiceJRCPT(ide) {debugger
    var form = "Invoice";
    var Ref = "InvoiceConvertJR";
    window.location.href = "/FormBuilding/FormBuild?FormName=" + form + "&ide=" + ide + "&Ref=" + Ref + "";
}

function TotalQTYCALC() {debugger
    var tbl = $("#grdData")[0];
    var totalQty = 0;
    $("#totalQty").text('0');
    for (var irow = 1; irow < tbl.rows.length  ; irow++) {
        var trw = tbl.rows[irow]; 
        if ($(trw).find("#PUTABLE004COLUMN08").val() != "" && $(trw).find("#PUTABLE004COLUMN08").val() != null && $(trw).find("#checkRow").is(':checked')) {
            totalQty = totalQty + parseFloat($(trw).find("#PUTABLE004COLUMN08").val());
            $("#totalQtyL").show();
            $("#totalQty").text(parseFloat(totalQty).toFixed(2));
        }
    }
}
function TotalQTYCALCV() {debugger
    var tbl = $("#grdData")[0];
    var totalQty = 0;
    $("#totalQty").text('0');
    for (var irow = 1; irow < tbl.rows.length  ; irow++) {
        var trw = tbl.rows[irow]; 
        if ($(trw).find("#PUTABLE004COLUMN08").text() != "" && $(trw).find("#PUTABLE004COLUMN08").text() != null) {
            totalQty = totalQty + parseFloat($(trw).find("#PUTABLE004COLUMN08").text());
            $("#totalQtyL").show();
            $("#totalQty").text(parseFloat(totalQty).toFixed(2));
        }
    }
}
// Latest open issues by Venkat
function NameCheck(val, val1, FormName) {
    debugger
    $.ajax({
        url: "/FormBuilding/NameCheck?FormName=" + FormName + "",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({ ItemName: val, VenName: val1 }),
        success: function (data) {
            if (data.ItemName != 0) {
                Alert.render('Item Name already existed choose another one'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $("#MATABLE007COLUMN04").focus();
                })
                $("#MATABLE007COLUMN04").val("");
                return false;
            }
            else if (data.VenName != 0) {
                Alert.render('Vendor Name already existed choose another one'); $("#alertok").focus();
                $("#alertok").on("click", function () {
                    $("#dialogbox").dialog("close"); $("#SATABLE001COLUMN05").focus();
                })
                $("#SATABLE001COLUMN05").val("");
                return false;
            }
        }
    });
}
function GetTransactionNo(opunit, FormName, FormId) {debugger
    $.ajax({
        //url: "/FormBuilding/GetTransactionNo?FormName=" + FormName + "",
        url: "/JobOrder/TransactionNoGenerate?FormName=" + FormName + "",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify({ FormName: FormName, FormId: FormId, opunit: opunit }),
        success: function (data) {
            debugger
            if (data != "") {
                $("#" + data.trancol + "").val(data.item);
                if (data.Override == "False") $("#" + data.trancol + "").prop("readonly", true);
                else $("#" + data.trancol + "").prop("readonly", false);
            } else $("#" + data.trancol + "").prop("readonly", false);
        }
    })
}
