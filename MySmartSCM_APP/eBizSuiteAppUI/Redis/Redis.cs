﻿using Newtonsoft.Json;
using ServiceStack.Redis;
using ServiceStack.Redis.Generic;
//using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeBizSuiteAppUI.Redis
{

    public class Redis
    {
        RedisClient redisClient;
        public Redis(string hostname, int port)
        {
            redisClient = new RedisClient(hostname, port);
        }
        public Redis(string hostname)
        {
            redisClient = new RedisClient(hostname);
        }
        public T GetValue<T>(string key, string module, string appid)
        {
            IRedisTypedClient<T> datas = redisClient.As<T>();
            T data = datas.GetValue(appid + "_" + module + "_" + key);
            return data;

        }
        public bool SetValue<T>(string key, string module, string appid, T data)
        {
            IRedisTypedClient<T> datas = redisClient.As<T>();
            datas.SetEntry(appid + "_" + module + "_" + key, data);
            data = datas.GetValue(appid + "_" + module + "_" + key);
            if (data != null)
            {
                return true;
            }
            return false;
        }
        public bool DeleteValue<T>(string key, string module, string appid)
        {
            IRedisTypedClient<T> datas = redisClient.As<T>();
            var keys =datas.GetAllKeys();
            //foreach (var key1 in keys)
            //{
            //    datas.DeleteById(key1);
            //}
            //datas.DeleteById(appid + "_" + module + "_" + key);
            //redisClient.As<T>().DeleteById(appid + "_" + module + "_" + key);
            redisClient.Del(appid + "_" + module + "_" + key);
            T data = datas.GetValue(appid + "_" + module + "_" + key);
            if (data != null)
            {
                return true;
            }
            return false;
        }
    }

}