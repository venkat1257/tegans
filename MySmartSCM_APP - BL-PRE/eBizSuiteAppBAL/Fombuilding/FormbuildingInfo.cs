﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Data;

namespace eBizSuiteAppBAL.Fombuilding
{
    public class FormbuildingInfo
    {
        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        //Order Selection Screen
        //MIS Filters Venkat
        public FormBuildingEntities GetInfo(int frmID, object viewStyle, object sortByStatus, object ViewSort, string OPUnit, string AcOwner, SqlConnection cn, object itemType, string idi, object Cust, object itm, object family, object group, object brand, object state, object city, object dtFrm, object dtTo, object opu, object UPC, string MisCatgory, string MisMilestone, string MisSalesRep, string MisStatus, string MISCompany)
        {
            DateTime start = Convert.ToDateTime("1/1/2010"), end = Convert.ToDateTime("1/1/2100");
            SqlCommand cmd = new SqlCommand("INFO_DATE", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AcOwner", AcOwner);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                start = Convert.ToDateTime(dt.Rows[0]["COLUMN04"]);
                end = Convert.ToDateTime(dt.Rows[0]["COLUMN05"]);
            }
            string str = "select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + System.Web.HttpContext.Current.Session["eid"] + " and isnull(COLUMNA13,0)=0";
            SqlDataAdapter daf = new SqlDataAdapter(str, cn);
            DataTable dtf = new DataTable();
            daf.Fill(dtf); string DateFormat = ""; string JQDateFormat = "";
            if (dtf.Rows.Count > 0)
            {
                DateFormat = dtf.Rows[0]["COLUMN04"].ToString();
                JQDateFormat = dtf.Rows[0]["COLUMN10"].ToString();
            }
            if (JQDateFormat != "")
            {
                System.Web.HttpContext.Current.Session["DateFormat"] = DateFormat;
                System.Web.HttpContext.Current.Session["Format"] = DateFormat;
                System.Web.HttpContext.Current.Session["ReportDate"] = JQDateFormat;
                System.Web.HttpContext.Current.Session["FormatDate"]= DateFormat;
            }
            else
            {
                System.Web.HttpContext.Current.Session["DateFormat"] = "dd/MM/yyyy";
                System.Web.HttpContext.Current.Session["Format"] = "dd/MM/yyyy";
                System.Web.HttpContext.Current.Session["ReportDate"] = "dd/mm/yy";
                System.Web.HttpContext.Current.Session["FormatDate"] = "dd/MM/yyyy";
            }
            string CLOS = "";
            var status = ""; string statuslist = null;
            if (frmID == 1251)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                    var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "09" || a.COLUMN02 == "41" || a.COLUMN02 == "10").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    //CLOS = "AND P.COLUMN16 not IN ('OPEN','Approved','PARTIALLY RECEIVED/NOT BILLED','Waiting For Approval')";
                    CLOS = "AND P.COLUMN16 not IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1272)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                    var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "83").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = "AND P.COLUMN18 not IN (" + statuslist + ")";
                    //CLOS = "AND P.COLUMN18 IN ('OPEN','FULLY RECEIVED/NOT BILLED','PARTIALLY RECEIVED/NOT BILLED')";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1273)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                    var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "12").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = "AND P.COLUMN13 not IN (" + statuslist + ")";
                    //CLOS = "AND P.COLUMN13 IN ('OPEN','PARTIALLY PAID','PARTIALLY BILLED/FULLY RECEIVED')";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1274)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                    //CLOS = "AND P.COLUMN11 IN ('OPEN','PARTIALLY PAID')";
                    CLOS = "";
                }
                else
                {
                    CLOS = "";
                }
            }



            else if (frmID == 1275)
            {
                if (Convert.ToString(sortByStatus) == "")
                {
                    //EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
					var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "30" || a.COLUMN02 == "26" || a.COLUMN02 == "43").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    //CLOS = "AND P.COLUMN16 not IN ('OPEN','Approved','PARTIALLY RECEIVED/NOT BILLED','Waiting For Approval')";
                    CLOS = "AND P.COLUMN16 not IN (" + statuslist + ")";
                    //CLOS = "AND P.COLUMN16 IN ('OPEN','Approved','PARTIALLY FUFILLED/NOT INVOICED')";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1276)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                                        //EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
					var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "81").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = "AND P.COLUMN14 not IN (" + statuslist + ")";
                    //CLOS = "AND P.COLUMN14 IN ('OPEN','Approved','PARTIALLY FUFILLED/NOT INVOICED')";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1277)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                                        //EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
					var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "34").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = "AND P.COLUMN13 not IN (" + statuslist + ")";
                    //CLOS = "AND P.COLUMN13 IN ('OPEN','AMOUNT PARTIALLY RECEIVED')";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1278)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                                        //EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
					//CLOS = "AND P.COLUMN11 IN ('OPEN','AMOUNT PARTIALLY RECEIVED')";
                    CLOS = "";
                }
                else
                {
                    CLOS = "";
                }
            }
			// oppurtinity Filter Changes By Venkat 
            else if (frmID == 1375)
            {
                if ((Convert.ToString(sortByStatus) == "")||(Convert.ToString(sortByStatus) == "OPEN"))
                {

                    var r = dc.CONTABLE025.Where(a => a.COLUMN03 == "OP" &&( a.COLUMN02 == "122" || a.COLUMN02 == "126" || a.COLUMN02 == "125")).ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = " AND so.COLUMN04 IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }
			// JOB ORDER OUT FILTERS BY VENKAT
            else if (frmID == 1283)
            {
                if ((Convert.ToString(sortByStatus) == "") || (Convert.ToString(sortByStatus) == "OPEN"))
                {

                    var r = dc.CONTABLE025.Where(a => a.COLUMN03 == "JOO" && (a.COLUMN02 == "134" || a.COLUMN02 == "135" || a.COLUMN02 == "137" || a.COLUMN02 == "133")).ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = " AND p.COLUMN16 IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1284)
            {
                if ((Convert.ToString(sortByStatus) == ""))
                {

                    var r = dc.CONTABLE025.Where(a => a.COLUMN03 == "JIO" && (a.COLUMN02 == "141" || a.COLUMN02 == "143")).ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = " AND p.COLUMN14 IN (" + statuslist + ")";
                }
                else if ((Convert.ToString(sortByStatus) == "OPEN"))
                {

                    var r = dc.CONTABLE025.Where(a => a.COLUMN03 == "JIO" && (a.COLUMN02 == "141")).ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = " AND p.COLUMN14 IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }
            //MIS Filters Venkat
            else if (frmID == 1404)
            {
                int mis = 0;
                if (mis == 0)
                {
                    CLOS = "";
                }
                if (Convert.ToString(MisCatgory)!= "")
                {
                    CLOS =CLOS + "AND P.COLUMN10 IN (" + MisCatgory + ")";mis = 1;
                }
                if (Convert.ToString(MisSalesRep) != "")
                {
                    CLOS = CLOS + "AND P.COLUMNA08 IN (" + MisSalesRep + ")"; mis = 1;
                }
                if (Convert.ToString(MisMilestone) != "")
                {
                    CLOS = CLOS + "AND P.COLUMN11 IN (" + MisMilestone + ")"; mis = 1;
                }
                if (Convert.ToString(MisStatus) != "")
                {
                    CLOS = CLOS + "AND P.COLUMN12 IN (" + MisStatus + ")"; mis = 1;
                }
                if (Convert.ToString(MISCompany) != "")
                {
                    CLOS = CLOS + "AND P.COLUMN06 IN ('" + MISCompany + "')"; mis = 1;
                }
                
               
            }

            //EMPHCS1536	Status filerts for Sales Returns and Purchase Returns by srinivas 24/1/2016
			else if (frmID == 1328)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
				//EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
                    var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "104" || a.COLUMN02 == "105" || a.COLUMN02 == "106").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = "AND P.COLUMN16 not IN (" + statuslist + ")";
					//EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1329)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                    var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "109").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = "AND P.COLUMN18 not IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1353)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
				//EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
                    var r = dc.CONTABLE025.Where(a => a.COLUMN02 =="98" || a.COLUMN02 == "99" || a.COLUMN02 == "100").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = "AND P.COLUMN16 not IN (" + statuslist + ")";
					//EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1354)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                    var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "112").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = "AND P.COLUMN14 not IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }


            FormBuildingEntities info = new FormBuildingEntities();
            List<CONTABLE006> con6 = new List<CONTABLE006>();
            List<CONTABLE013> con13 = new List<CONTABLE013>();
            eBizSuiteTableEntities db = new eBizSuiteTableEntities();
            List<string> Oper = OPUnit.Split(',').ToList<string>();
			//EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
            var POPUnit = OPUnit;
            if (Oper.ElementAt(Oper.Count - 1) == "null")
                OPUnit = " (p.COLUMNA02 in(" + OPUnit + ") or p.COLUMNA02 is null) ";
            else
                OPUnit = " (p.COLUMNA02 in(" + OPUnit + ") or p.COLUMNA02 is null) ";
            var flds = con6;
            var fldsV = con13;
            var tblID = 0;
            if (viewStyle == "Default" || viewStyle == null || viewStyle == "")
            {
                flds = db.CONTABLE006.Where(q => q.COLUMN03 == frmID).ToList();
                tblID = Convert.ToInt32(flds.Select(q => q.COLUMN04).FirstOrDefault());
                flds = db.CONTABLE006.Where(q => q.COLUMN04 == tblID && q.COLUMN03 == frmID).ToList();

            }
            if (viewStyle != "Default" && viewStyle != null && viewStyle != "")
            {
                var vName = viewStyle;
                fldsV = db.CONTABLE013.Where(q => q.COLUMN05 == frmID & q.COLUMN03 == vName).ToList();
                tblID = Convert.ToInt32(fldsV.Select(q => q.COLUMN04).FirstOrDefault());
            }
            var tblName = db.CONTABLE004.Where(q => q.COLUMN02 == tblID);
            var table = tblName.Select(q => q.COLUMN04).First();

            var pid = idi; var collname = "";
            if (table == "PUTABLE001")
                collname = "COLUMN02";
            else if (table == "PUTABLE003")
                collname = "COLUMN05";
            else if (table == "PUTABLE005")
                collname = "COLUMN05";
            //sales
            else if (table == "SATABLE005")
                collname = "COLUMN02";
            else if (table == "SATABLE007")
                collname = "COLUMN05";
            else if (table == "SATABLE009")
                collname = "COLUMN05";
            if (collname != null && collname != "" && pid != null && pid != "")
            {
                SqlCommand cmddi = new SqlCommand("select   " + collname + "  from  " + table + "  where " + collname + "=" + pid + "  ", cn);
                SqlDataAdapter dadi = new SqlDataAdapter(cmddi);
                DataTable dtdi = new DataTable();
                dadi.Fill(dtdi);
                if (dtdi.Rows.Count > 0)
                {
                    System.Web.HttpContext.Current.Session["POID"] = dtdi.Rows[0][0].ToString();
                }
            }
            System.Web.HttpContext.Current.Session["Table"] = table;
            System.Web.HttpContext.Current.Session["TBLID"] = tblID;
            string strQry = null;
            List<string> actCol = new List<string>();
            List<string> alCol = new List<string>();
            string dateF = System.Web.HttpContext.Current.Session["Format"].ToString();
            if (fldsV.Count == 0)
            {

                //Purchase Order
                if (frmID == 1251)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    //EMPHCS858 rajasekhar reddy patalota 15/8/2015 Purchase order list info - Receipt type internal id is getting displayed. It should display the name of receipt type
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Purchase Order#],FORMAT(p.[COLUMN06 ],'" + dateF + "') AS [Date],s.[COLUMN05 ] AS [Vendor],FORMAT(p.[COLUMN08 ],'" + dateF + "') AS [Ship Date],p.COLUMN11 Reference#,p.COLUMN09 Memo,p.[COLUMN15 ] AS [Total Amount],p.[COLUMN16 ] AS [Status],ma.COLUMN09 [Created By] From PUTABLE001 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 inner join  MATABLE011 m on m.COLUMN02=p.COLUMN29 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column06 between '"+start+"' and '"+end+"' and p.COLUMNA13='False'" + CLOS + " ";
                    //" left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN24 " +
                    //" left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN27 ";
                    alCol.AddRange(new List<string> { "ID", "Purchase Order#", "Date", "Vendor",  "Ship Date", "Reference#","Memo", "Total Amount", "Status","Created By" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN06", "s.COLUMN05", "p.COLUMN08", "p.COLUMN11", "p.COLUMN09", "p.COLUMN15", "p.COLUMN16", "ma.COLUMN09" });
                }
               //EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
			    //Price Level
                else if (frmID == 1582){
					//EMPHCS1857 rajasekhar reddy patakota 03/02/2017 Discount setup in invoice from Price level
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Price Level],m.COLUMN04 [Type],p.[COLUMN06 ] AS Discount,p.[COLUMN11] AS [Fixed Amount],ma.COLUMN09 [Created By] From MATABLE023 p left outer join  MATABLE002 m on m.COLUMN02=p.COLUMN05 and (m.COLUMNA03=p.COLUMNA03 or m.COLUMNA03 is null) left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where isnull(p.COLUMNA13,'False')='False'" + CLOS + "";
                    alCol.AddRange(new List<string> { "ID", "Price Level", "Type", "Discount","Fixed Amount" , "Created By" });
                    actCol.AddRange(new List<string> { " p.COLUMN02", "p.COLUMN04", "m.COLUMN04", "p.COLUMN06", "p.COLUMN11", "ma.COLUMN09" });
                }
                //project
                else if (frmID == 1378)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Project#],P.[COLUMN05 ] AS [Name],s.[COLUMN05] AS [Customer],P.[COLUMN08 ] AS [Estimated Cost],p.[COLUMN09 ] AS [Actual Cost],m.[COLUMN04 ] AS [Status],d.[COLUMN04 ] AS [Deparment],o.[COLUMN04 ] AS [Operating Unit] From PRTABLE001 p left outer join SATABLE002 s on s.COLUMN02=p.COLUMN06 left outer join MATABLE002 m on m.COLUMN02=p.COLUMN12  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN19  left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN18 where p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Project#", "Name", "Customer", "Estimated Cost", "Actual Cost", "Status", "Deparment", "Operating Unit" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN05", "s.COLUMN05", "p.COLUMN08", "p.COLUMN09", "m.COLUMN04", "d.COLUMN04", "o.COLUMN04" });
                }
                //Journal Ledger
                else if (frmID == 1414)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    //EMPHCS792	Journal Entry fields : Journal Entry# . Date , Payee  and Operating unit. Remove rest of the fields or hide by Raj.jr 28/7/2015
					//EMPHCS1247 rajasekhar reddy patakota 01/10/2015 Journal Entry Calculation Changes and also edit and delete
                    //EMPHCS1322	Add Account,Credit,Debit,Memo in Journal Entry info display BY Raj.Jr 05/11/2015
                    //EMPHCS1764	Journal Entry should have Payee Type in Item Level not in Header level By RAJ.Jr
                    strQry = "SELECT p.[COLUMN02] AS ID,(p.[COLUMN04]) AS [Journal Entry#],FORMAT(p.[COLUMN09 ],'" + dateF + "') AS [Date],(case when p.column07='22305' or fj.column14='22305' then (select cOLUMN05 from sATABLe001 where COLUMN02=fj.COLUMN07) when p.column07='22334' or fj.column14='22334' then (select cOLUMN05 from sATABLe001 where COLUMN02=fj.COLUMN07) when p.column07='22335' or fj.column14='22335' then (select cOLUMN05 from sATABLe002 where COLUMN02=fj.COLUMN07)	when p.column07='22306' or fj.column14='22306' then (select COLUMN09 from MATABLE010 where COLUMN02=fj.COLUMN07) when p.column07='22492' or fj.column14='22492' then (select cOLUMN09 from MATABLE010 where COLUMN02=fj.COLUMN07) else NULL end) Party,fj.COLUMN06 as Memo,f.COLUMN04 as Account,fj.COLUMN04 as Debit,fj.COLUMN05 as Credit,ma.[COLUMN09 ] AS [Created By] From FITABLE031 p  left  join  CONTABLE007 o on o.COLUMN02=p.COLUMN10 left outer join FITABLE032 fj on fj.COLUMN12=p.COLUMN01 and isnull(fj.COLUMNA13,0)=0 left outer join FITABLE001 f on f.COLUMN02=fj.COLUMN03 and f.COLUMNA03=p.columna03 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null)  where p.column09 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' ";
                    alCol.AddRange(new List<string> { "ID", "Journal Entry#", "Date", "Party", "Memo", "Account", "Debit", "Credit", "Created By" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN09", "p.COLUMN07", "fj.COLUMN06", "f.COLUMN04", "fj.COLUMN04", "fj.COLUMN05", "ma.COLUMN09" });
                }

                //Stock Return
                else if (frmID == 1379)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Transfer#],FORMAT(p.[COLUMN08 ],'" + dateF + "') AS [Date],'' Reference#,p.COLUMN07 Memo,d.[COLUMN03 ] AS [Source Operating Unit],o.[COLUMN03 ] AS [Destination Operating Unit],'' Status,ma.COLUMN09 [Created By] From PRTABLE003 p  left outer join  CONTABLE007 d on d.COLUMN02=p.COLUMN05   left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN06 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column08 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Transfer#","Date","Reference#","Memo", "Source Operating Unit", "Destination Operating Unit", "Status","Created By" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN08", "p.COLUMN07", "p.COLUMN07", "d.COLUMN03", "o.COLUMN03", "p.COLUMN07","ma.COLUMN09" });
                }
                //Work Order
                else if (frmID == 1380)
                {
                    strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Indent#],e.COLUMN04 [Order Type], s.[COLUMN05 ]  Customer," +
                                "FORMAT(p.[COLUMN07 ],'" + dateF + "')  [Date],  po.[COLUMN05 ]  [Project],o.[COLUMN03] AS [Operating Unit] " +
                                "From PRTABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 left outer join  PRTABLE001 po on po.COLUMN02=p.COLUMN08 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN10  left outer join  PRTABLE002 ms on ms.COLUMN02=p.COLUMN09 left outer join  MATABLE009 e on e.COLUMN02=p.COLUMN06 where p.column07 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Indent#", "Order Type", "Customer", "Date", "Project",  "Operating Unit" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "e.COLUMN04", "s.COLUMN05", "p.COLUMN07", "po.COLUMN05", "o.COLUMN03" });
                }
                //Resourse Consumption
                else if (frmID == 1381)
                {
                    strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Material Dispatch#],w.[COLUMN04 ]  [Indent#],e.COLUMN04 [Order Type], s.[COLUMN05 ]  Customer,FORMAT(p.[COLUMN09 ],'" + dateF + "')  [Date],  po.[COLUMN05 ]  [Project Name]," +
                              " o.[COLUMN03] AS [Operating Unit] From PRTABLE007 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN06 left outer join  PRTABLE001 po on po.COLUMN02=p.COLUMN08 left outer join " +
                              " CONTABLE007 o on o.COLUMN02=p.COLUMN11  left outer join  PRTABLE002 ms on ms.COLUMN02=p.COLUMN09 left outer join  MATABLE009 e on e.COLUMN02=p.COLUMN07   left outer join  PRTABLE005 w on w.COLUMN02=p.COLUMN05 where p.column09 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Material Dispatch#", "Indent#", "Order Type", "Customer", "Date", "Project Name",  "Operating Unit" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "w.COLUMN05", "e.COLUMN04", "s.COLUMN05", "p.COLUMN09", "po.COLUMN05",  "o.COLUMN03" });
                }
                //EMPHCS1793	Resource consumption (Direct) screen by Raj.jr
                else if (frmID == 1602)
                {
                    strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Resourse Consumption#], FORMAT(p.[COLUMN09 ],'" + dateF + "') [Date],s.[COLUMN05 ]  Customer,  po.[COLUMN05 ]  [Project Name],m.COLUMN04 [Sub Project], o.[COLUMN03] AS [Operating Unit] From PRTABLE007 p left join  SATABLE002 s on s.COLUMN02=p.COLUMN06 and s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0 left join  PRTABLE001 po on po.COLUMN02=p.COLUMN08 and po.COLUMNA03=p.COLUMNA03 and isnull(po.COLUMNA13,0)=0 left join CONTABLE007 o on o.COLUMN02=p.COLUMN11 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0 left outer join MATABLE002 m on m.COLUMN02=p.COLUMN22 and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0  where p.column09 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 and p.COLUMN03=1602";
                    alCol.AddRange(new List<string> { "ID", "Resourse Consumption#", "Date", "Customer", "Project Name", "Sub Project", "Operating Unit" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN09", "s.COLUMN05", "po.COLUMN05", "m.COLUMN04", "o.COLUMN03" });
                }
                //EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
                else if (frmID == 1605)
                {
                    strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [GRN#], FORMAT(p.[COLUMN06 ],'" + dateF + "') [Date],s.[COLUMN05 ]  Customer,  o.[COLUMN03] AS [Operating Unit] From PUTABLE022 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN09 where p.column06 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' ";
                    alCol.AddRange(new List<string> { "ID", "GRN#", "Date", "Customer","Operating Unit" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN06", "s.COLUMN05", "o.COLUMN03" });
                }
                //EMPHCS1796 Production Entry Screen and Cost of Production Report Changes by gnaneshwar on 10/8/2016
                else if (frmID == 1606)
                {
                    strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Production Entry#], FORMAT(p.[COLUMN05 ],'" + dateF + "') [Date], o.[COLUMN03] AS [Operating Unit] From PRTABLE009 p  left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN06   where p.column05 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' and p.COLUMN03=1606";
                    alCol.AddRange(new List<string> { "ID", "Production Entry#", "Date", "Operating Unit" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN05", "o.COLUMN03" });
                }
				//EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
                else if (frmID == 1619)
                {
                    strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [SMTP Port],p.[COLUMN05]  [SMTP Server],p.[COLUMN06]  [POP3 Port],p.[COLUMN07]  [POP3 Server],p.[COLUMN09]  [Default] From SETABLE016 p where p.COLUMNA13='False' and p.COLUMN03=1619";
                    alCol.AddRange(new List<string> { "ID", "SMTP Port", "SMTP Server", "POP3 Port", "POP3 Server", "Default" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN05", "p.COLUMN06", "p.COLUMN07", "p.COLUMN09" });
                }
                //Purchase Order Quotation
                else if (frmID == 1374)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Purchase Quote#],P.[COLUMN29 ] AS [Receipt Type],s.[COLUMN05 ] AS [Vendor],P.[COLUMN07 ] AS [Manager Approval],FORMAT(p.[COLUMN06 ],'" + dateF + "') AS [Date],FORMAT(p.[COLUMN08],'" + dateF + "') AS [Ship Date],p.[COLUMN32 ] AS [Tax Amount],p.[COLUMN15 ] AS [Total Amount],p.[COLUMN16 ] AS [Order Status] From PUTABLE001 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 where p.column06 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'";
                    //" left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN24 " +
                    //" left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN27 ";
                    alCol.AddRange(new List<string> { "ID", "Purchase Quote#", "Date", "Receipt Type", "Vendor", "Manager Approval", "Ship Date", "Tax Amount", "Total Amount", "Order Status" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN06", "p.COLUMN29", "s.COLUMN05", "p.COLUMN07", "p.COLUMN08", "p.COLUMN32", "p.COLUMN15", "p.COLUMN16" });
                }
                //Item Reciept
                else if (frmID == 1272)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                                        //EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Item Receipt#],FORMAT(p.[COLUMN09 ],'" + dateF + "') AS Date,s.[COLUMN05 ] AS Vendor,f.[COLUMN04 ] AS [Reference PO#],p.[COLUMN10 ] AS Reference#,p.[COLUMN12 ] AS Memo,sum(cast(iif(p4.COLUMN11='',0,cast(isnull(p4.COLUMN11,0) as decimal(18,2))) as decimal(18,2))) [Total Amount],p.COLUMN18 Status,ma.COLUMN09 [Created By] From PUTABLE003 p left outer join PUTABLE004 p4 on p4.COLUMN12=p.COLUMN01 left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN13 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN16 left outer join  PUTABLE001 f on f.COLUMN02=p.COLUMN06 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null)  where p.column09 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'" + CLOS + "";
                                        //EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
                    alCol.AddRange(new List<string> { "ID", "Item Receipt#", "Date", "Vendor", "Reference PO#", "Reference#", "Memo","Total Amount", "Status", "Created By"});
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN09", "s.COLUMN05", "f.COLUMN04", "p.COLUMN10", "p.COLUMN12","p4.COLUMN11", "p.COLUMN18","ma.COLUMN09" });

                }
                //Bill
                else if (frmID == 1273)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                                        //EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
										//EMPHCS1573 rajasekhar reddy patakota 19/02/2016 Latest Issues raised by satya on 18th fixes
					//EMPHCS1811 rajasekhar reddy patakota 07/09/2016 Line Grid Details filling in sales & Purchase module
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS Bill#,max(FORMAT(p.[COLUMN08 ],'" + dateF + "')) AS Date,s.[COLUMN05 ] AS Vendor,f.[COLUMN04 ] AS [Reference PO#],p.[COLUMN09 ] AS Reference#,p.[COLUMN12 ] AS Memo,p.COLUMN14 [Total Amount],sum(isnull(f1.COLUMN15,0)) [Returns/Debit],(sum(isnull(p15.COLUMN16,0))+sum(isnull(p15.COLUMN14,0))+sum(isnull(p15.COLUMN06,0))) [Amount Paid],(p.COLUMN14-(sum(isnull(p15.COLUMN16,0))+sum(isnull(p15.COLUMN14,0))+sum(isnull(p15.COLUMN06,0)))) [Due Amount],DATEDIFF(day,p.COLUMN11,GETDATE()) as [Days Overdue],p.COLUMN13 Status,ma.COLUMN09 [Created By] From PUTABLE005 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05  left outer join  PUTABLE001 f on f.COLUMN02=p.COLUMN06 left outer join  PUTABLE001 f1 on f1.COLUMN11=p.COLUMN04 and f1.COLUMNA02=p.COLUMNA02 and f1.COLUMNA03=p.COLUMNA03  and f1.COLUMN15>0.00 and f1.COLUMNA13='False' left outer join PUTABLE015 p15 on p15.COLUMN03=p.COLUMN02 and p15.COLUMNA13='False' and p15.COLUMNA03=p.COLUMNA03 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column08 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' and p.COLUMN04!='' " + CLOS + "";
                                        //EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
                    alCol.AddRange(new List<string> { "ID", "Bill#", "Date", "Vendor", "Reference PO#", "Reference#", "Memo","Total Amount", "Returns/Debit", "Amount Paid", "Due Amount", "Days Overdue","Status","Created By" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN08", "s.COLUMN05", "f.COLUMN04", "p.COLUMN09", "p.COLUMN12", "p.COLUMN14", "p15.COLUMN16", "p15.COLUMN06", "p15.COLUMN05", "p.COLUMN11", "p.COLUMN13", "ma.COLUMN09" });

                }
                    //EMPHCS727 payment grid-account column showing numbee...modified by Raj.jr 22/07/2015
                //Bill Payment
                else if (frmID == 1274)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    //EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
                    //EMPHCS1319	Add Date in Payment info and Account displaying number By RAj.jr 05/11/2015
					//EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
                    //EMPHCS1545	Payment & CustomerPayment->In list payment  not calculating credit,advance,discount BY Raj.Jr
						//EMPHCS1573 rajasekhar reddy patakota 19/02/2016 Latest Issues raised by satya on 18th fixes
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS Payment#,FORMAT(p.[COLUMN18 ],'" + dateF + "') as Date,(case when p.COLUMN20=22306 then r.COLUMN09 else s.[COLUMN05 ] end) AS Vendor,       b.[COLUMN04 ] AS [Reference Bill#],p.COLUMN10 Memo,f1.[COLUMN04 ] AS [Bank Account],(case when p.COLUMN20=22306 then fb.COLUMN04 else b.COLUMN14 end) [Total Amount],isnull((isnull(fb.COLUMN06,0)+isnull(fb.COLUMN16,0)+isnull(fb.COLUMN14,0)),0) as [Amount Paid],ma.COLUMN09 [Created By] From PUTABLE014 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 left outer join  MATABLE010 r on r.COLUMN02=p.COLUMN05 and r.COLUMN30='True'  left outer join  PUTABLE015 fb on fb.COLUMN08=p.COLUMN01 and fb.COLUMNA13='False' left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN13 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN16 left outer join  PUTABLE001 f on f.COLUMN02=p.COLUMN06  left outer join  PUTABLE005 b on b.COLUMN02=fb.COLUMN03  left outer join FITABLE001 f1 on f1.COLUMN02=p.COLUMN08 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null)  where p.column18 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' " + CLOS + "";

                    alCol.AddRange(new List<string> { "ID", "Payment#", "Date", "Vendor", "Reference Bill#", "Memo","Bank Account","Total Amount","Amount Paid","Created By" });
                    actCol.AddRange(new List<string> { " p.COLUMN02", "p.COLUMN04", "p.COLUMN18", "s.COLUMN05", "b.COLUMN04","p.COLUMN10", "f1.COLUMN04","b.COLUMN14", "fb.COLUMN06","ma.COLUMN09" });
                }
                //purchase return order
                else if (frmID == 1353)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
					//EMPHCS1536	Status filerts for Sales Returns and Purchase Returns by srinivas 24/1/2016
					//EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Return order#],FORMAT(p.[COLUMN06 ],'" + dateF + "') AS [Date], s.[COLUMN05 ] AS Vendor,p.COLUMN11 [Reference PO#],p.[COLUMN09 ] AS Memo,p.[COLUMN15] AS [Total Amount], p.[COLUMN16 ] AS [Status],ma.COLUMN09 [Created By] From SATABLE005 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.COLUMNA13='False'  " + CLOS + "";
                    //" left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN24 " +
                    //" left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN27 ";

                    alCol.AddRange(new List<string> { "ID", "Return Order#", "Date","Vendor", "Reference PO#","Memo", "Total Amount", "Status" ,"Created By"});
					//EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN06","s.COLUMN05", "p.COLUMN11", "p.COLUMN09", "p.COLUMN15","p.COLUMN16","ma.COLUMN09" });
                }
                //Return Issue
                else if (frmID == 1354)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
				//EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Item Issue#],FORMAT(p.[COLUMN08 ],'" + dateF + "') AS Date,s.[COLUMN05 ] AS Vendor,f.[COLUMN04 ] AS [Reference Return#],p.[COLUMN09 ] AS [Reference#],p.[COLUMN13 ] AS Memo,sum(isnull(s8.COLUMN13,0)) [Total AMount],p.COLUMN14 Status,ma.COLUMN09 [Created By] From SATABLE007 p left outer join SATABLE008 s8 on s8.COLUMN14=p.COLUMN01 left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN15 left outer join  SATABLE005 f on f.COLUMN02=p.COLUMN06 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.COLUMNA13='False' " + CLOS + "";
                    alCol.AddRange(new List<string> { "ID", "Item Issue#", "Date", "Vendor", "Reference Return#", "Reference#", "Memo", "Total Amount","Status" ,"Created By"});
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN08", "s.COLUMN05", "f.COLUMN04", "p.COLUMN09", "p.COLUMN13","s8.COLUMN13","p.COLUMN14", "ma.COLUMN09" });

                }
                //Refund
                else if (frmID == 1355)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Debit Memo#],FORMAT(p.[COLUMN06 ],'" + dateF + "') AS [Date],(case when p.COLUMN50='22335' then s2.COLUMN05 else s.[COLUMN05 ] end) AS Vendor,(select COLUMN11 from SATABLE005 where COLUMN02=(select COLUMN06 from SATABLE007 where COLUMN02=p.COLUMN33)) [Reference PO#],p14.[COLUMN04] AS [Reference Bill#],p.[COLUMN11] AS [Reference#],p.COLUMN09 Memo,p.[COLUMN15] AS [Total Amount],p.[COLUMN16 ] AS [Status],ma.COLUMN09 [Created By] From PUTABLE001 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 left outer join  SATABLE002 s2 on s2.COLUMN02=p.COLUMN05 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN24 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN27 left outer join PUTABLE014 p14 on p14.COLUMN02=p.COLUMN48 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column06 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Debit Memo#", "Date", "Vendor", "Reference PO#", "Reference Bill#", "Reference#", "Memo", "Total Amount","Status","Created By" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN06", "s.COLUMN05", "Column11", "p14.COLUMN04", "p.COLUMN11","p.COLUMN09", "p.COLUMN15", "p.COLUMN16", "ma.COLUMN09" });
                }
                //Scheduled Task
                else if (frmID == 1366)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Activity#],p.[COLUMN05 ] AS [Description],FORMAT(p.[COLUMN06 ],'" + dateF + "') AS [Created Date], FORMAT(p.[COLUMN07 ],'" + dateF + "') AS [Start Date], FORMAT(p.[COLUMN08 ],'" + dateF + "') AS [End Date],m.[COLUMN04 ] AS [Status],q.[COLUMN04 ] AS [Operating Unit] From SETABLE001 p left outer join MATABLE002 m on m.COLUMN02=p.COLUMN09  left outer join  CONTABLE007 q on q.COLUMN02=p.COLUMN11 where p.COLUMNA13='False'";

                    alCol.AddRange(new List<string> { "ID", "Activity#", "Description", "Created Date", "Start Date", "End Date", "Status", "Operating Unit" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN05", "p.[COLUMN06 ]", "p.[COLUMN07 ]", "p.COLUMN08", "m.COLUMN04", "q.[COLUMN04 ]" });
                }
                else if (frmID == 1367)
                {
                    strQry = "SELECT p.[COLUMN02] AS ID , p.COLUMN04 AS [Task#], p.COLUMN05 AS [Description], m.COLUMN09 AS [Assigned By], m1.COLUMN09 AS [Assigned To], pr.COLUMN04 AS [Priority], s.COLUMN04 AS [Status], FORMAT(p.COLUMN09,'" + dateF + "') AS [Expected Start Date], FORMAT(p.COLUMN10,'" + dateF + "') AS [Expected End Date], FORMAT(p.COLUMN11,'" + dateF + "') AS [Actual Start Date], FORMAT(p.COLUMN12,'" + dateF + "') AS [Actual End Date], p.COLUMN13 AS [Duration], p.COLUMN19 AS [Memo], d.COLUMN04 AS [Department], o.COLUMN04 AS [Operating Unit] FROM SETABLE002 p left outer join MATABLE010 m on m.COLUMN02=p.COLUMN06 left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN07 left outer join MATABLE002 pr on pr.COLUMN02=p.COLUMN08 left outer join MATABLE002 s on s.COLUMN02=p.COLUMN14 left outer join MATABLE002 d on d.COLUMN02=p.COLUMN18 left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN15 where p.COLUMN02 != ''";
                    alCol.AddRange(new List<string> { "ID", "Task#", "Description", "Assigned By", "Assigned To", "Priority", "Status", "Expected Start Date", "Expected End Date", "Actual Start Date", "Actual End Date", "Duration", "Memo", "Department", "Operating Unit" });
                    actCol.AddRange(new List<string> { "COLUMN02", "COLUMN04", "COLUMN05", "COLUMN06", "COLUMN07", "COLUMN08", "COLUMN14", "COLUMN09", "COLUMN10", "COLUMN11", "COLUMN12", "COLUMN13", "COLUMN19", "COLUMN18", "COLUMN04" });
                }
                //KPIS CREATION
                else if (frmID == 1372)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS 'Type#',p.[COLUMN05 ] AS 'Name',p.[COLUMN06 ] AS 'SQL Query'," +
                    "l.[COLUMN04 ] AS 'Graph Type', p.[COLUMN08 ] AS 'X-Axis',p.[COLUMN09 ] AS 'Y-Axis',p.[COLUMN11 ] AS 'Purpose', d.[COLUMN04] AS Department, o.[COLUMN03] AS 'Operating Unit'" +
                    " From SETABLE007 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN13  left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN16  left outer join  MATABLE002 l on l.COLUMN02=p.COLUMN07 where p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Type#", "Name", "SQL Query", "Graph Type", "X-Axis", "Y-Axis", "Purpose", "Department", "Operating Unit" });
                    actCol.AddRange(new List<string> { " p.COLUMN02 ", "p.COLUMN04", "p.COLUMN05", "p.COLUMN06", "p.COLUMN07", "l.COLUMN04", "p.COLUMN09", "p.COLUMN11", "d.COLUMN04", "o.COLUMN03" });
                }

                // address Master
                else if (frmID == 1259)
                {
                    strQry = "SELECT p.COLUMN02  AS ID, p.COLUMN04  AS 'Name',p.COLUMN06 AS 'Addressee'," +
                            "p.COLUMN07  as 'Address1',p.COLUMN16  as 'Country',p.COLUMN11  as 'State'," +
                            "p.COLUMN10  as 'City',p.COLUMN13  as 'Phone',p.COLUMN15  as 'Email', p.COLUMN19  as 'Type of Contact'," +
                            "o.COLUMN03 AS 'Operating Unit',c.COLUMN04  AS Department From SATABLE003 p " +
                            " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN21" +
                            " left outer join  MATABLE002 c on c.COLUMN02=p.COLUMN24 where p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Name", "Addressee", "Address1", "Country", "State", "City", "Phone", "Email", "Type of Contact", "Operating Unit", "Department" });
                    actCol.AddRange(new List<string> { " p.COLUMN02", " p.COLUMN04", "p.COLUMN06", "p.COLUMN09", "p.COLUMN10", "p.COLUMN30", "e.COLUMN09", "p.COLUMN13", "p.COLUMN14", "o.COLUMN03", "c.COLUMN04" });
                }
                //sales
                //Opportinuity
                else if (frmID == 1375)
                {
				////EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
                    strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Opportinuity#],FORMAT(p.[COLUMN07],'" + dateF + "')  [Date], s.[COLUMN05 ]  Customer, ms.COLUMN04  [Mile Stone], sr.COLUMN09  [Sales Rep],FORMAT(p.COLUMN08,'" + dateF + "') [Expected Close Date],FORMAT(p.COLUMN30,'" + dateF + "') [Next Followup]," +
"+so.[COLUMN04 ] AS [Status] From SATABLE013 p left outer join MATABLE010 sr On sr.COLUMN02= p.COLUMN15 AND sr.COLUMNA03=p.COLUMNA03 AND ISNULL(sr.COLUMNA13,0)=0 left join MATABLE002 ms on ms.COLUMN02=p.COLUMN23 AND ISNULL(ms.COLUMNA13,0)=0 left outer join MATABLE002 st on st.COLUMN02=p.COLUMN06 AND ISNULL(st.COLUMNA13,0)=0 left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 left join  MATABLE002 so on so.COLUMN02=p.COLUMN06  where p.column07 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' " + CLOS + " ";
                    alCol.AddRange(new List<string> { "ID", "Opportinuity#", "Date", "Customer", "Mile Stone", "Sales Rep", "Expected Close Date", "Next Followup", "Status" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN07", "s.COLUMN05", "ms.COLUMN04", "sr.COLUMN09", "p.COLUMN08", "p.COLUMN30", "so.COLUMN04" });
                }
                //Sales Quotation
                else if (frmID == 1376)
                {
                    strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Quotation#],FORMAT(p.[COLUMN07],'" + dateF + "')  [Date],FORMAT(p.[COLUMN08 ],'" + dateF + "') [Valid upto], s.[COLUMN05 ]  Customer,case  when p.COLUMN09=1 then 'YES' else 'NO' end [Manager Approval]," +
                                "  p.[COLUMN21] AS [Tax Amount],p.[COLUMN22] AS [Total Amount],p.[COLUMN06 ] AS [Order Status] " +
                                "From SATABLE015 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 where p.column07 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' ";
                    alCol.AddRange(new List<string> { "ID", "Quotation#", "Date", "Valid upto", "Customer", "Manager Approval", "Tax Amount", "Total Amount", "Order Status" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN07", "p.COLUMN08", "p.COLUMN05", "p.COLUMN09", "p.COLUMN21", "p.COLUMN22", "p.COLUMN06" });
                }
                //Sales Order
                else if (frmID == 1275)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],FORMAT(p.[COLUMN06 ],'" + dateF + "')  [Date],s.[COLUMN05 ]  Customer,FORMAT(p.[COLUMN08],'" + dateF + "') [Ship Date],p.[COLUMN11] as Reference#,p.[COLUMN09] as Memo,p.[COLUMN15] AS [Total Amount],p.[COLUMN16 ] AS [Status],ma.column09 [Created By] From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 left outer join  MATABLE009 e on e.COLUMN02=p.COLUMN29 left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMNA08 and ma.COLUMNA03=p.COLUMNA03 and (ma.COLUMNA02=p.COLUMNA02 or ma.COLUMNA02 is null) where p.column06 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'" + CLOS + "";
                    alCol.AddRange(new List<string> { "ID", "Order#", "Date", "Customer", "Ship Date", "Reference#", "Memo", "Total Amount", "Status", "Created By" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN06", "s.COLUMN05", "p.COLUMN08", "p.COLUMN11", "p.COLUMN09", "p.COLUMN15", "p.COLUMN16", "ma.COLUMN09" });
                }
                //Order Selection Screen
                else if (frmID == 1523)
                {
                   
                    if (Cust != "" && itm != "" &&(brand!=""||group!=""||family!="")&& (dtFrm != "" && dtTo != "") && city != "")
                    {
                        if (brand == "") brand = "null"; if (group == "") group = "null"; if (family == "") family = "null";
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and SA3.COLUMN10=''" + city + "'' and SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where (COLUMN10=" + brand + " or COLUMN11=" + group + " or COLUMN12=" + family + ")) and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (Cust != "" && itm != "" && (brand != "" || group != "" || family != "") && (dtFrm != "" && dtTo != "") && state != "")
                    {
                        if (brand == "") brand = "null"; if (group == "") group = "null"; if (family == "") family = "null";
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and SA3.COLUMN11=" + state + " and SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where (COLUMN10=" + brand + " or COLUMN11=" + group + " or COLUMN12=" + family + ")) and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (Cust != "" && itm != "" && UPC != "" && (brand != "" || group != "" || family != "") && (dtFrm != "" && dtTo != ""))
                    {
                        if (brand == "") brand = "null"; if (group == "") group = "null"; if (family == "") family = "null";
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                         "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and SA6.COLUMN04=''" + UPC + "'' and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where (COLUMN10=" + brand + " or COLUMN11=" + group + " or COLUMN12=" + family + ")) and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (Cust != "" && itm != "" && UPC != "" && (dtFrm != "" && dtTo != "") && city != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and SA6.COLUMN04=''" + UPC + "'' and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and SA3.COLUMN10=''" + city + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (Cust != "" && itm != "" && UPC != "" && (dtFrm != "" && dtTo != "") && state != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and SA6.COLUMN04=''" + UPC + "'' and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and SA3.COLUMN11=" + state + " and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (Cust != "" && itm != "" && (dtFrm != "" && dtTo != "") && city != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and SA3.COLUMN10=''" + city + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (Cust != "" && itm != ""  && (dtFrm != "" && dtTo != "") && state != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and SA3.COLUMN11=" + state + " and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (Cust != "" && itm != "" && UPC!="" && (dtFrm != "" && dtTo != ""))
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and SA6.COLUMN04=''" + UPC + "'' and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (Cust != "" && itm != "" && UPC != "" && (brand != "" || group != "" || family != ""))
                    {
                        if (brand == "") brand = "null"; if (group == "") group = "null"; if (family == "") family = "null";
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and SA6.COLUMN04=''" + UPC + "'' and SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where (COLUMN10=" + brand + " or COLUMN11=" + group + " or COLUMN12=" + family + ")) and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (itm != "" && (dtFrm != "" && dtTo != "") && UPC != "" && (brand != "" || group != "" || family != ""))
                    {
                        if (brand == "") brand = "null"; if (group == "") group = "null"; if (family == "") family = "null";
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  SA6.COLUMN03=" + itm + " and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and SA6.COLUMN04=''" + UPC + "'' and SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where (COLUMN10=" + brand + " or COLUMN11=" + group + " or COLUMN12=" + family + ")) and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (Cust != "" && itm != "" && (dtFrm != "" && dtTo != ""))
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05='" + Cust + "' and SA6.COLUMN03=" + itm + "  and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (Cust != "" && itm != "" && UPC!="")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and SA6.COLUMN04=''" + UPC + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (Cust != "" && city != "" && (dtFrm != "" && dtTo != ""))
                    {
                        strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + " where p.COLUMN05=" + Cust + " and SA3.COLUMN10=''" + city + "'' and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and p.COLUMN03=1275 " + CLOS + "";
                    }
                    else if (Cust != "" && state != "" && (dtFrm != "" && dtTo != ""))
                    {
                        strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + " where p.COLUMN05=" + Cust + " and SA3.COLUMN11=" + state + " and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and p.COLUMN03=1275 " + CLOS + "";
                    }
                    else if (itm != "" && (dtFrm != "" && dtTo != "") && UPC != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  SA6.COLUMN03=" + itm + " and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and SA6.COLUMN04=''" + UPC + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    
                    else if (Cust != "" && itm != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (Cust != "" && UPC != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN04=''" + UPC + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (Cust != "" && (dtFrm != "" && dtTo != ""))
                    {
                        strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + " where  p.COLUMN05=" + Cust + " and p.COLUMN06 between '" + dtFrm + "' and '" + dtTo + "' and p.COLUMN03=1275 and p.COLUMNA13='False' " + CLOS + "";
                    }
                    else if (itm != "" && (dtFrm != "" && dtTo != ""))
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  SA6.COLUMN03=" + itm + " and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (itm != ""  && UPC != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), "+
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) "+
	"SELECT @cols= ISNULL(@cols + ',','') "+ 
      " + QUOTENAME(COLUMN04) "+
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "("+
                       " SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  SA6.COLUMN03=" + itm + " and SA6.COLUMN04=''" + UPC + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (Cust != "" && city != "")
                    {
                        strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + " where p.COLUMN05='" + Cust + "' and SA3.COLUMN10='" + city + "' and p.COLUMN03=1275 " + CLOS + "";
                    }
                    else if (Cust != "" && state != "")
                    {
                        strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + " where p.COLUMN05='" + Cust + "' and SA3.COLUMN11='" + state + "' and p.COLUMN03=1275 ' " + CLOS + "";
                    }
                    else if (city != "" && (dtFrm != "" && dtTo != ""))
                    {
                        strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                                                " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + "  where SA3.COLUMN10='" + city + "' and p.COLUMN06 between '" + dtFrm + "' and '" + dtTo + "' and p.COLUMN03=1275 ' " + CLOS + "";
                    }
                    else if (state != "" && (dtFrm != "" && dtTo != ""))
                    {
                        strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                                                " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + "  where SA3.COLUMN11='" + state + "' and p.COLUMN06 between '" + dtFrm + "' and '" + dtTo + "' and p.COLUMN03=1275 ' " + CLOS + "";
                    }
                    else if (UPC != "" && (dtFrm != "" && dtTo != ""))
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  SA6.COLUMN04=''" + UPC + "'' and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (itm != "" && (brand != "" || group != "" || family != ""))
                    {
                        if (brand == "") brand = "null"; if (group == "") group = "null"; if (family == "") family = "null";
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  SA6.COLUMN03=" + itm + " and SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where  (COLUMN10=" + brand + " or COLUMN11=" + group + " or COLUMN12=" + family + ")) and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (UPC != "" && (brand != "" || group != "" || family != ""))
                    {
                        if (brand == "") brand = "null"; if (group == "") group = "null"; if (family == "") family = "null";
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  SA6.COLUMN04=''" + UPC + "'' and SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where (COLUMN10=" + brand + " or COLUMN11=" + group + " or COLUMN12=" + family + ")) and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                        else if (Cust != "" && (brand != "" || group != "" || family != ""))
                        {
                            if (brand == "") brand = "null"; if (group == "") group = "null"; if (family == "") family = "null";
                                strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
            " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
             "SELECT @cols= ISNULL(@cols + ',','') " +
               " + QUOTENAME(COLUMN04) " +
            "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
            "set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
                  "(" +
                                "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                                " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where p.COLUMN05=" + Cust + " and SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where (COLUMN10=" + brand + " or COLUMN11=" + group + " or COLUMN12=" + family + ")) and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                        }
                    else if (Cust != "")
                    {
                        
                            strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + " where p.COLUMN05='" + Cust + "' and p.COLUMN03=1275 and p.COLUMNA13='False' ";
                    }
                    //else if (Cust != "")
                    //{
                    //    strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                    //                    " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                    //                    "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + " where p.COLUMN05='" + Cust + "' and p.COLUMN03=1275 and p.COLUMNA13='False' " + CLOS + "";
                    //}
                    else if (itm != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  SA6.COLUMN03=" + itm + " and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (UPC != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  SA6.COLUMN04=''" + UPC + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (group != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where COLUMN11=" + group + ") and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (family != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                                                " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where COLUMN12=" + family + ") and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if (brand != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
   " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                                                " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where COLUMN10=" + brand + ") and p.COLUMN03=1275 and SA6.COLUMN27!=0 " + CLOS + "";
                    }
                    else if ((dtFrm != "" && dtTo != ""))
                    {
                        strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                    "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + "  where p.COLUMN06 between '" + dtFrm + "' and '" + dtTo + "' and p.COLUMN03=1275 and p.COLUMNA13='False' " + CLOS + "";
                    }
                    else if (city != "")
                    {
                        strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                                                " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + "  where SA3.COLUMN10='" + city + "' and p.COLUMN03=1275 and p.COLUMNA13='False' " + CLOS + "";
                    }
                    else if (state != "")
                    {
                        strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                                                " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + "  where SA3.COLUMN11='" + state + "' and p.COLUMN03=1275 and p.COLUMNA13='False' " + CLOS + "";
                    }
                    else 
                    {
                        strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + " where p.COLUMN05='" + Cust + "' and p.COLUMN03=1275 and p.COLUMNA13='False' " + CLOS + "";
                    }
                    if (itm != "" || UPC != "" || brand != "" || group != "" || family != "")
                    {
                        //alCol.AddRange(new List<string> { "ID", "Order#", "Date", "Shipping Date", "Customer", "City", "Order Status", "Grams", "Nos","Kgs","Lts" });
                        //actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN06", "p.COLUMN08", "s.COLUMN05", "SA3.COLUMN10", "p.COLUMN16", "SA6.COLUMN07", "SA6.COLUMN07", "SA6.COLUMN07", "SA6.COLUMN07" });
                    }
                    else
                    {
                        alCol.AddRange(new List<string> { "ID", "Order#", "Date", "Shipping Date", "Customer", "City", "Order Status" });
                        actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN06", "p.COLUMN08", "s.COLUMN05", "SA3.COLUMN10", "p.COLUMN16" });
                    }
                }
                //Item Issue
                else if (frmID == 1276)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                                        //EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Item Issue#],FORMAT(p.[COLUMN08 ],'" + dateF + "') AS Date,s.[COLUMN05 ] AS Customer,f.[COLUMN04 ] AS [ReferenceOrder#],p.[COLUMN09 ] AS Reference#,p.[COLUMN13 ] AS Memo,sum(isnull(i.COLUMN13,0)) [Total Amount],p.COLUMN14 Status,ma.column09 [Created By] From SATABLE007 p left outer join SATABLE008 i on i.COLUMN14=p.COLUMN01 left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN15 left outer join  SATABLE005 f on f.COLUMN02=p.COLUMN06 left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMNA08 and ma.COLUMNA03=p.COLUMNA03 and (ma.COLUMNA02=p.COLUMNA02 or ma.COLUMNA02 is null) where p.column08 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' " + CLOS + "";
                                        //EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
                    alCol.AddRange(new List<string> { "ID", "Item Issue#", "Date", "Customer", "ReferenceOrder#", "Reference#", "Memo","Total Amount" ,"Status", "Created By" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN08", "s.COLUMN05", "f.COLUMN04", "p.COLUMN09", "p.COLUMN13", "i.COLUMN13", "p.COLUMN14", "ma.COLUMN09" });

                }
                //Invoice
                else if (frmID == 1277)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                                        //EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
											//EMPHCS1573 rajasekhar reddy patakota 19/02/2016 Latest Issues raised by satya on 18th fixes
					//EMPHCS1811 rajasekhar reddy patakota 07/09/2016 Line Grid Details filling in sales & Purchase module
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS Invoice#,FORMAT(p.[COLUMN08],'" + dateF + "') AS Date,s.[COLUMN05 ] AS Customer,f.[COLUMN04 ] AS [Reference Order#],p.[COLUMN09 ] AS Reference#,p.[COLUMN12 ] AS Memo,p.COLUMN20 [Total Amount],sum(isnull(f1.COLUMN12,0))+sum(isnull(f1.COLUMN32,0)) as [Returns/Credit],(sum(isnull(s12.COLUMN06,0))+sum(isnull(s12.COLUMN14,0))+sum(isnull(s12.COLUMN13,0))+sum(isnull(s12.COLUMN09,0))) [Amount Received],(p.COLUMN20-(sum(isnull(s12.COLUMN06,0))+sum(isnull(s12.COLUMN14,0))+sum(isnull(s12.COLUMN13,0))+sum(isnull(s12.COLUMN09,0)))) [Due Amount],DATEDIFF(day,p.COLUMN10,GETDATE()) as [Days Overdue],p.COLUMN13 Status,ma.[COLUMN09] AS [Created By] From SATABLE009 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN14 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN18 left outer join  SATABLE005 f on f.COLUMN02=p.COLUMN06 left outer join  SATABLE005 f1 on f1.COLUMN11=p.COLUMN04 and f1.COLUMNa03=p.COLUMNa03 and f1.COLUMNa02=p.COLUMNa02  and f1.COLUMNA13='False' and f1.COLUMN15>0 left outer join SATABLE012 s12 on s12.COLUMN03=p.column02 and s12.COLUMNA13='False' left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMNA08 and ma.COLUMNA03=p.COLUMNA03 and (ma.COLUMNA02=p.COLUMNA02 or ma.COLUMNA02 is null) where p.column08 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' and p.COLUMN04!='' " + CLOS + "";
                    alCol.AddRange(new List<string> { "ID", "Invoice#", "Date", "Customer", "Reference Order#", "Reference#", "Memo", "Total Amount", "Returns/Credit","Amount Received","Due Amount","Days Overdue","Status", "Created By" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN08", "p.COLUMN05", "f.COLUMN04", "p.COLUMN09", "p.COLUMN12", "p.COLUMN20", "s12.COLUMN14","s12.COLUMN06","s12.COLUMN05","p.COLUMN10" ,"p.COLUMN13", "ma.COLUMN09" });

                }
                //Payment
                else if (frmID == 1278)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                 // EMPHCS1421 BY GNANESHWAR ON 15/12/2015 when invoice edited after payment due amount not updating, it is showing previous due
					//EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
                    //EMPHCS1545	Payment & CustomerPayment->In list payment  not calculating credit,advance,discount BY Raj.Jr
						//EMPHCS1573 rajasekhar reddy patakota 19/02/2016 Latest Issues raised by satya on 18th fixes
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS Payment#,FORMAT(p.[COLUMN19 ],'" + dateF + "') Date,s.[COLUMN05 ] AS Customer,f.[COLUMN04 ] AS [Reference Order#],d.[COLUMN04 ] AS [Reference Invoice#],p.COLUMN10 Memo,c.[COLUMN04 ] AS [Bank Account],d.COLUMN20 [Total Amount],(isnull(fb.COLUMN06,0)+isnull(fb.COLUMN14,0)+isnull(fb.COLUMN13,0)+isnull(fb.COLUMN09,0)) [Amount Paid],ma.COLUMN09 [Created By] From   SATABLE011  p  left outer join  SATABLE012  fb on   fb.COLUMN08 =p.COLUMN01 and isnull(fb.COLUMNA13,0)=0     left outer join  SATABLE002  s  on  s.COLUMN02=p.COLUMN05    left outer join  CONTABLE007 o  on  o.COLUMN02=p.COLUMN14    left outer join  SATABLE005  f  on  f.COLUMN02=p.COLUMN06 left outer join SATABLE009 d on d.COLUMN02=fb.COLUMN03 and d.COLUMNA13='False'  left outer join  FITABLE001  c  on  c.COLUMN02=p.COLUMN08 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column19 between '" + start + "' and '" + end + "' and p.COLUMN03 = 1278 and (d.COLUMN03 = 1277 or isnull(cast(d.COLUMN03 as nvarchar(250)),'')='') and p.COLUMNA13='False' " + CLOS + " ";
                    
                    //EMPHCS1320	Add Paid Amount in Customer Payment By Raj.jr 05/11/2015
                    //strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS Payment#,convert(varchar,p.[COLUMN19],106)  Date,s.[COLUMN05 ] AS Customer,d.[COLUMN04 ] AS Invoice#,f.[COLUMN04 ] AS [Sales Order#],c.[COLUMN04 ] AS Account,d.COLUMN20 as [Total Amount]," +
                    //"fb.COLUMN06 as Payment,(select  top 1 ((isnull( max(d1.COLUMN20),0) -( SUM(isnull(fb.COLUMN06,0)) OVER(ORDER BY fb.COLUMN03 desc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) + SUM(isnull(fb.COLUMN09,0)) OVER(ORDER BY fb.COLUMN03 desc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)+ SUM(isnull(fb.COLUMN13,0)) OVER(ORDER BY fb.COLUMN03 desc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)+  SUM(isnull(fb.COLUMN14,0)) OVER(ORDER BY fb.COLUMN03 desc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW))))  as a   from  SATABLE012 fb   left outer join  SATABLE009 d1 on d1.COLUMN02=fb.COLUMN03 where fb.COLUMN03=d.COLUMN02  group by     fb.COLUMN03,fb.COLUMN09,fb.COLUMN06,fb.COLUMN13,fb.COLUMN14 ORDER BY a asc) as [Due Amount],o.[COLUMN03] AS [Operating Unit] From SATABLE011 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN14 left outer join  SATABLE005 f on f.COLUMN02=p.COLUMN06 " +
                    //"left outer join  SATABLE012 fb on fb.COLUMN08=p.COLUMN01 left outer join  SATABLE009 d on d.COLUMN02=fb.COLUMN03 left outer join  FITABLE001 c on c.COLUMN02=p.COLUMN08 where p.COLUMNA13='False'" + CLOS + " ";
                    alCol.AddRange(new List<string> { "ID", "Payment#", "Date", "Customer", "Reference Order#", "Reference Invoice#","Memo","Bank Account","Total Amount","Amount Paid", "Created By" });
                    actCol.AddRange(new List<string> { " p.COLUMN02", "p.COLUMN04", "p.COLUMN19", "s.COLUMN05", "d.COLUMN04", "f.COLUMN04", "p.COLUMN10", "c.COLUMN04","d.COLUMN20", "fb.COLUMN06", "ma.COLUMN09" });
                }
				//EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
                //Expense Payment
                else if (frmID == 1528)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Expense Payment#],FORMAT(p.[COLUMN05 ],'" + dateF + "')  Date,s.[COLUMN06] AS Employee,p.COLUMN10 Reference#,p.COLUMN13 Memo,fb.COLUMN05 as [Total Amount],fb.COLUMN07 [Advance Paid],fb.COLUMN08 [Amount Paid],c.[COLUMN04 ] AS Bank,ma.COLUMN09 [Created By] From FITABLE045 p left outer join  MATABLE010 s on s.COLUMN02=p.COLUMN06  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN16 left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN09 left outer join MATABLE002 m2 on m2.COLUMN02=p.COLUMN15 left outer join FITABLE044 f44 on f44.COLUMN02=p.COLUMN20 left outer join  FITABLE046 fb on fb.COLUMN09=p.COLUMN01  and isnull(fb.columna13,'False')='False' left outer join  FITABLE001 c on c.COLUMN02=p.COLUMN12 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column05 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'" + CLOS + "";
                    alCol.AddRange(new List<string> { "ID", "Expense Payment#", "Date", "Employee", "Reference#", "Memo", "Total Amount","Advance Paid", "Amount Paid","Bank", "Created By" });
                    actCol.AddRange(new List<string> { " p.COLUMN02", "p.COLUMN04", "p.COLUMN05", "s.COLUMN06", "p.COLUMN10", "p.COLUMN13", "fb.COLUMN05", "fb.COLUMN07", "fb.COLUMN08",  "c.COLUMN04", "ma.COLUMN09" });
                }
                //joborder
                //Job Order
                else if (frmID == 1283)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Job Order#],e.[COLUMN04 ] AS [Order Type], s.[COLUMN05 ] AS Jobber,FORMAT(p.[COLUMN06 ],'" + dateF + "') AS [Date],FORMAT(p.[COLUMN08],'" + dateF + "') AS [Due Date],  p.[COLUMN07 ] AS [Manager Approval],p.[COLUMNB04 ] AS [Delivery Point],p.[COLUMN15] AS [Total Amount], p.[COLUMN16 ] AS [Order Status] From SATABLE005 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 " +
                             " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN24 " +
                             " left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN27 left outer join  MATABLE009 e on e.COLUMN02=p.COLUMN29  where p.column06 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'" + CLOS + "";

                    alCol.AddRange(new List<string> { "ID", "Job Order#", "Date", "Order Type", "Jobber", "Manager Approval", "Due Date", "Delivery Point", "Total Amount", "Order Status" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN06", "e.COLUMN04", "s.COLUMN05", "p.COLUMN07", "p.COLUMN08", "p.COLUMNB04", "p.COLUMN15", "p.COLUMN16" });
                }
				//EMPHCS1784	Job Order for IN Process 15/7/2016
                else if (frmID == 1586)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Job Order#],e.[COLUMN04 ] AS [Order Type], s.[COLUMN05 ] AS Jobber,FORMAT(p.[COLUMN06 ],'" + dateF + "') AS [Date],FORMAT(p.[COLUMN08],'" + dateF + "') AS [Due Date], p.[COLUMN15] AS [Total Amount], p.[COLUMN16 ] AS [Order Status] From PUTABLE001 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN24  left outer join  MATABLE009 e on e.COLUMN02=p.COLUMN29  where p.column06 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' and p.COLUMN03=1586";

                    alCol.AddRange(new List<string> { "ID", "Job Order#", "Date", "Order Type", "Jobber",  "Due Date",  "Total Amount", "Order Status" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN06", "e.COLUMN04", "s.COLUMN05",  "p.COLUMN08", "p.COLUMN15", "p.COLUMN16" });
                }
                //Jobber Item Issue
                else if (frmID == 1284)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Item Issue#],s.[COLUMN05 ] AS Jobber,j.[COLUMN04 ] AS [Job Order#],FORMAT(p.[COLUMN08],'" + dateF + "') AS Date,p.[COLUMN09 ] AS [Reference#],p.[COLUMN13 ] AS Memo,p.[COLUMN14] Status,o.[COLUMN03] AS OperatingUnit,d.[COLUMN04 ] AS Department From SATABLE007 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 " +
                             " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN15 " +
                             " left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN16 " +
                             " left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN06 " +
                             " left outer join  SATABLE005 j on j.COLUMN02=p.COLUMN06 where p.column08 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'" + CLOS + "";
                    alCol.AddRange(new List<string> { "ID", "Item Issue#", "Jobber", "Job Order#", "Date", "Reference#", "Memo","Status", "OperatingUnit" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "s.COLUMN05", "f.COLUMN04", "p.COLUMN08", "p.COLUMN09", "p.COLUMN12","p.COLUMN14", "o.COLUMN03" });

                }
				//EMPHCS1784	Job Order for IN Process 15/7/2016
                else if (frmID == 1587)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Job order Receipt#],s.[COLUMN05 ] AS Customer,j.[COLUMN04 ] AS [Job Order#],FORMAT(p.[COLUMN09] ,'" + dateF + "') AS Date,p22.COLUMN04 AS[Reference#],p.[COLUMN12 ] AS Memo,o.[COLUMN03] AS OperatingUnit From PUTABLE003 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN13  left outer join  PUTABLE001 j on j.COLUMN02=p.COLUMN06 left outer join PUTABLE022 p22 on p22.COLUMN02=p.COLUMN10   where p.column09 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' and p.COLUMN03=1587 ";
                    alCol.AddRange(new List<string> { "ID", "Job order Receipt#", "Customer", "Job Order#", "Date", "Reference#", "Memo", "OperatingUnit" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "s.COLUMN05", "f.COLUMN04", "p.COLUMN09", "p.COLUMN10", "p.COLUMN12", "o.COLUMN03" });

                }
                //Job Order Reciept
                else if (frmID == 1286)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS JoReciept#,s.[COLUMN05 ] AS Jobber,j.[COLUMN04 ] AS [Job Order#],FORMAT(p.[COLUMN09 ],'" + dateF + "') AS Date,p.[COLUMN10 ] AS Reference#,p.[COLUMN12 ] AS Memo,o.[COLUMN03] AS OperatingUnit,d.[COLUMN04 ] AS Department From PUTABLE003 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 " +
                             " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN13 " +
                             " left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN16 " +
                             " left outer join  PUTABLE001 f on f.COLUMN02=p.COLUMN06 " +
                             " left outer join  SATABLE005 j on j.COLUMN02=p.COLUMN06 where p.column09 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "JoReciept#", "Jobber", "Job Order#", "Date", "Reference#", "Memo", "OperatingUnit", "Department" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "s.COLUMN05", "f.COLUMN04", "p.COLUMN09", "p.COLUMN10", "p.COLUMN12", "o.COLUMN03", "d.COLUMN04" });

                }
                 //EMPHCS1784	Job Order for IN Process
                else if (frmID == 1588)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Issue#],s.[COLUMN05 ] AS Customer,j.[COLUMN04 ] AS [Job Order#],FORMAT(p.[COLUMN08],'" + dateF + "') AS Date,p22.COLUMN04 AS [Reference#],p.[COLUMN13 ] AS Memo,o.[COLUMN03] AS OperatingUnit,d.[COLUMN04 ] AS Department From SATABLE007 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 " +
                             " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN15 " +
                             " left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN16 " +
                             " left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN06 " +
                             " left outer join  PUTABLE001 j on j.COLUMN02=p.COLUMN06  left outer join PUTABLE022 p22 on p22.COLUMN02=p.COLUMN09  where p.column08 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' AND p.COLUMN03=1588";
                    alCol.AddRange(new List<string> { "ID", "Issue#", "Customer", "Job Order#", "Date", "Reference#", "Memo", "OperatingUnit" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "s.COLUMN05", "f.COLUMN04", "p.COLUMN08", "p.COLUMN09", "p.COLUMN12", "o.COLUMN03" });

                }
                //Job Order Bill
                else if (frmID == 1287)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Bill#],e.[COLUMN04 ] AS [Order Type],s.[COLUMN05 ] AS Jobber,j.[COLUMN04 ] AS [Job Order#],FORMAT(p.[COLUMN08],'" + dateF + "') AS Date,p.[COLUMN21 ] AS [Payment Mode],p.[COLUMN11 ] AS [Payment Terms],FORMAT(p.[COLUMN10 ],'" + dateF + "') AS [Due Date],p.[COLUMN24 ] AS [Tax Amount],p.[COLUMN20 ] AS [Total Amount],p.[COLUMN13 ] AS Status From SATABLE009 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 " +
                             " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN15 " +
                             " left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN18 " +
                             " left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN06 " +
                             " left outer join  SATABLE005 j on j.COLUMN02=p.COLUMN06  left outer join  MATABLE009 e on e.COLUMN02=p.COLUMN19  where p.column08 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Bill#", "Date", "Jobber", "Order Type", "Job Order#", "Payment Mode", "Payment Terms", "Due Date", "Tax Amount", "Total Amount", "Status" });
                    actCol.AddRange(new List<string> { "COLUMN02", "COLUMN04", "COLUMN08", "COLUMN05", "COLUMN19", "COLUMN06", "COLUMN21", "COLUMN11", "COLUMN10", "COLUMN24", "COLUMN20", "COLUMN13" });

                }
                //Customer Payment
                else if (frmID == 1288)
                {
               
//EMPHCS1587 :Adding Advance Tab to Jobber Payment by gnaneshwar on 4/3/2016
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Payment#],FORMAT(p.[COLUMN19],'" + dateF + "') AS [Date],s.[COLUMN05 ] AS Jobber,f.[COLUMN04 ] AS [Job Order],b.[COLUMN04 ] AS Bill#,c.[COLUMN04 ] AS Account,fb.COLUMN04 as [Total Amount],fb.COLUMN05 as [Due Amount],fb.COLUMN06 as Payment,o.[COLUMN03] AS OperatingUnit,d.[COLUMN04 ] AS Department From SATABLE011 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 " +
                             " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN14 " +
                             " left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN16 " +
                             " left outer join  SATABLE005 f on f.COLUMN02=p.COLUMN06 " +
                             " left outer join  SATABLE009 b on b.COLUMN02=p.COLUMN07 " +
                             " left outer join  SATABLE012 fb on fb.COLUMN08=p.COLUMN01  left outer join  FITABLE001 c on c.COLUMN02=p.COLUMN08 where p.column19 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Payment#", "Date", "Jobber", "Bill#", "Job Order", "Account", "Total Amount", "Due Amount", "OperatingUnit" });
                    actCol.AddRange(new List<string> { " p.COLUMN02", "p.COLUMN04", "p.COLUMN19", "s.COLUMN05", "b.COLUMN04", "f.COLUMN04", "p.COLUMN08", "fb.COLUMN04", "fb.COLUMN05", "o.COLUMN03" });
                }
                //Expence Type
                else if (frmID == 1293)
                {
                    //EMPHCS1150  Purpose is not getting displayed from Expense screen BY RAJ.Jr 21/9/2015
                    // EMPHCS1321	Add Expense# in ExpenseTracking info display BY RAJ.Jr 05/11/2015
					//EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.COLUMN17 as Expense#,FORMAT(p.[COLUMN09],'" + dateF + "') AS Date, o.[COLUMN03 ] AS [Operating Unit],pr.[COLUMN05] as Project,  m.[COLUMN06 ] AS [Employee],p.[COLUMN24 ] AS [Expense Amount],(iif((sum(isnull(f46.COLUMN07,0))+sum(isnull(f46.COLUMN08,0)))<=0,p.COLUMN24,isnull(p.COLUMN24,0)-(sum(isnull(f46.COLUMN07,0))+sum(isnull(f46.COLUMN08,0))))) as [Due Amount], p.[COLUMN07 ] AS Memo From FITABLE012 p" +
                                " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN10" +
                                " left outer join  MATABLE010 m on m.COLUMN02=p.COLUMN16" +
                                " left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN13" +
                                " left outer join  PRTABLE001 pr on pr.COLUMN02=p.COLUMN25" +
                                " left outer join FITABLE046 f46 on f46.COLUMN03=p.COLUMN02  and isnull(f46.COLUMNA13,0)=0  " +
                        //EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
                                "   where p.column09 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID","Expense#", "Date", "Operating Unit","Project","Employee",  "Expense Amount","Due Amount","Memo",  });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN17", "p.COLUMN09", "o.COLUMN03", "pr.COLUMN05", "m.COLUMN06", "p.COLUMN24", "f46.COLUMN06", "f.COLUMN05", });
                }
                //EMPHCS741	Amount Column displays empty in Withdrawl Screen done by srinivas 7/22/2015
                //Withdrawn Money
                else if (frmID == 1349)
                {
					//EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN05 ] AS [Withdraw#],FORMAT(p.[COLUMN04],'" + dateF + "') AS [Transaction Date], o.[COLUMN04 ] AS Account,p.[COLUMN11 ] AS [Amount],p.[COLUMN09 ] AS [Description] From FITABLE062 p " +
                             " left outer join  FITABLE001 o on o.COLUMN02=p.COLUMN03  where p.column04 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'";

                    alCol.AddRange(new List<string> { "ID", "Withdraw#", "Transaction Date", "Account", "Amount", "Description" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN05", "p.COLUMN04", "o.COLUMN04", "p.COLUMN10", "p.COLUMN09" });
                }
                //EMPHCS740	Amount Column displays empty in Deposit Screen done by srinivas 7/22/2015
                //Deposit Money
                else if (frmID == 1350)
                {
					//EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN05 ] AS [Deposit#],FORMAT(p.[COLUMN04],'" + dateF + "') AS [Transaction Date], o.[COLUMN04 ] AS Account,p.[COLUMN11 ] AS [Amount],p.[COLUMN09 ] AS [Description] From FITABLE062 p " +
                             " left outer join  FITABLE001 o on o.COLUMN02=p.COLUMN03 where p.column04 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' ";

                    alCol.AddRange(new List<string> { "ID", "Deposit#", "Transaction Date", "Account", "Amount", "Description" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN05", "p.COLUMN04", "o.COLUMN04", "p.COLUMN10", "p.COLUMN09" });
                }
                //EMPHCS743	Adding Shipping Tab Fileds in Sales order and Invoice done by srinivas 7/22/2015
                //Tranfer Money
                else if (frmID == 1351)
                {
					//EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN05 ] AS [Transfer#],FORMAT(p.[COLUMN04],'" + dateF + "') AS [Transaction Date], o.[COLUMN04 ] AS Account,p.[COLUMN11 ] AS [Amount],p.[COLUMN09 ] AS [Description] From FITABLE062 p " +
                             " left outer join  FITABLE001 o on o.COLUMN02=p.COLUMN03 where p.column04 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' ";

                    alCol.AddRange(new List<string> { "ID", "Transfer#", "Transaction Date", "Account", "Amount", "Description" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN05", "p.COLUMN04", "o.COLUMN04", "p.COLUMN10", "p.COLUMN09" });
                }
                //sales return order
                else if (frmID == 1328)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Return Order#],FORMAT(p.[COLUMN06 ],'" + dateF + "') AS [Date],s.[COLUMN05 ] AS Customer,p.COLUMN11 [Reference Order#],p.COLUMN09 Memo,p.[COLUMN15] AS [Total Amount],p.[COLUMN16 ] AS [Status],ma.COLUMN09 [Created By] From PUTABLE001 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN24  left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN27 left outer join  MATABLE009 e on e.COLUMN02=p.COLUMN29 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and Ma.COLUMNA02=p.COLUMNA02 where p.COLUMNA13='False' " + CLOS + "";
                    alCol.AddRange(new List<string> { "ID", "Return Order#", "Date", "Customer","Reference Order#","Memo", "Total Amount", "Status","Created By" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN06", "s.COLUMN05", "p.COLUMN11", "p.COLUMN09",  "p.COLUMN15", "p.COLUMN16","ma.COLUMN09" });
                }
                //Sales Order Return receipt
                else if (frmID == 1329)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
				//EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Item Receipt#],FORMAT(p.[COLUMN09],'" + dateF + "') AS Date,s.[COLUMN05 ] AS Customer,f.[COLUMN04 ] AS [Reference Order#],p.[COLUMN10 ] AS Reference#,p.[COLUMN12 ] AS Memo,sum(isnull(cast(z.COLUMN11 as decimal(18,2)),0)) [Total Amount],p.COLUMN18 Status,ma.COLUMN09 [Created By] From PUTABLE003 p left outer join PUTABLE004 z on z.COLUMN12=p.COLUMN01 left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05   left outer join   CONTABLE007 o on o.COLUMN02=p.COLUMN13   left outer join  PUTABLE001 f on f.COLUMN02=p.COLUMN06 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.COLUMNA13='False' " + CLOS + "";
                    alCol.AddRange(new List<string> { "ID", "Item Receipt#", "Date", "Customer", "Reference Order#", "Reference#", "Memo","Total Amount","Status", "Created By" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN09", "s.COLUMN05", "f.COLUMN04", "p.COLUMN10", "p.COLUMN12","z.COLUMN11","p.COLUMN18", "ma.COLUMN09" });

                }
                //credit memo
                else if (frmID == 1330)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Credit Memo#],FORMAT(p.[COLUMN06 ],'" + dateF + "') AS [Date],(case when p.COLUMN51='22305' then s1.COLUMN05 else s.[COLUMN05 ] end) AS Customer,(select COLUMN04 from PUTABLE001 where COLUMN02=(select COLUMN06 from PUTABLE003 where COLUMN02=p.COLUMN33))  AS [Reference Order#],s9.COLUMN04 [Reference Invoice#],p.COLUMN11 Reference#,p.COLUMN09 Memo,p.[COLUMN15] AS [Total Amount], p.[COLUMN16 ] AS [Status],ma.COLUMN09 [Created By] From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 left outer join  SATABLE001 s1 on s1.COLUMN02=p.COLUMN05 left outer join PUTABLE003 o on o.COLUMN02=p.COLUMN33  left outer join  MATABLE009 e on e.COLUMN02=p.COLUMN29 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) left outer join SATABLE009 s9 on s9.COLUMN02=p.COLUMN49  where p.column06 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' ";
                    alCol.AddRange(new List<string> { "ID", "Credit Memo#", "Date", "Customer","Reference Order#", "Reference Invoice#", "Reference#","Memo","Total Amount", "Status","Created By" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN06",  "s.COLUMN05","COLUMN04", "s9.COLUMN04", "p.COLUMN11", "p.COLUMN09", "p.COLUMN15", "p.COLUMN16","ma.COLUMN09" });
                }
                //Inventory Adjustment
                else if (frmID == 1357)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    //EMPHCS967	Inventory Adjustment duplicate entries BY RAJ.Jr 15/8/2015
                    strQry = "SELECT   p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Adjustment#],FORMAT(p.[COLUMN05 ],'" + dateF + "') AS [Date],p.COLUMN14 Reference#,p.COLUMN15 Memo,p.[COLUMN07 ] AS [Total Value],e.[COLUMN04] AS [Account], '' AS [Status],ma.[COLUMN09 ] AS [Created By] From FITABLE014 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN10 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN12 left outer join  FITABLE001 e on e.COLUMN02=p.COLUMN06 and e.COLUMNA03=p.COLUMNA03 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column05 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' ";

                    alCol.AddRange(new List<string> { "ID", "Adjustment#", "Date", "Reference#", "Memo", "Total Value", "Account", "Status", "Created By" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN05", "p.COLUMN14", "p.COLUMN07", "p.COLUMN09", "e.COLUMN04", "o.COLUMN03", "ma.COLUMN09" });
                }
                //Inventory Adjust
                else if (frmID == 1634)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    //EMPHCS967	Inventory Adjustment duplicate entries BY RAJ.Jr 15/8/2015
                    strQry = "SELECT   p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Adjustment#],FORMAT(p.[COLUMN05 ],'" + dateF + "') AS [Date],p.COLUMN14 Reference#,p.COLUMN15 Memo,e.[COLUMN04] AS [Account], '' AS [Status],ma.[COLUMN09 ] AS [Created By] From FITABLE014 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN10 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN12 left outer join  FITABLE001 e on e.COLUMN02=p.COLUMN06 and e.COLUMNA03=p.COLUMNA03 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column05 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' AND p.COLUMN03=1634 ";

                    alCol.AddRange(new List<string> { "ID", "Adjustment#", "Date", "Reference#", "Memo","Account", "Status", "Created By" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN05", "p.COLUMN14", "p.COLUMN07", "e.COLUMN04", "o.COLUMN03", "ma.COLUMN09" });
                }
                //Outstanding Payments
                else if (frmID == 1358)
                {
                    strQry = "SELECT   p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Payment#],FORMAT(p.[COLUMN05 ],'" + dateF + "') AS [Date],f.[COLUMN04] AS [Type Of Payee], e.[COLUMN04] AS [Account], o.[COLUMN03 ] AS [Operating Unit],d.[COLUMN04 ] AS Department From FITABLE016 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN10 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN13 left outer join  FITABLE001 e on e.COLUMN02=p.COLUMN07   left outer join  MATABLE002 f on f.COLUMN02=p.COLUMN06 where p.column05 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'";

                    alCol.AddRange(new List<string> { "ID", "Payment#", "Date", "Type Of Payee", "Account", "Operating Unit", "Department" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN05", "f.[COLUMN04]", "e.COLUMN04", "o.COLUMN03", "d.COLUMN04" });
                }
                //opening balance
                else if (frmID == 1359)
                {
				//EMPHCS1430 BY GNANESHWAR ON 20/12/2015 opening Balances upload for all types of chart of account
                    strQry = "SELECT p.[COLUMN02 ] AS ID,o.[COLUMN04 ] AS 'Account',p.[COLUMN09 ] AS 'Increased By', p.[COLUMN10] AS 'Decreased By',p.[COLUMN12 ] as 'Memo'" +
                                "From FITABLE018 p left outer join  FITABLE001 o on o.COLUMN02=p.COLUMN08 AND o.COLUMNA03=p.COLUMNA03 where p.COLUMNA13='False'";

                    alCol.AddRange(new List<string> { "ID", "Account", "Increased By", "Decreased By", "Memo" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "o.COLUMN04", "p.COLUMN09", "p.COLUMN10", "p.COLUMN12" });
                }
				// Opening Balance Customer & Vendor by venkat
                else if (frmID == 1615)
                {

                    strQry = "SELECT f.[COLUMN02] AS ID,f.[COLUMN04 ] AS 'Opening Balance Customer',FORMAT(f.[COLUMN05 ],'" + dateF + "') AS 'Date', o.[COLUMN04 ] AS 'Account',p.[COLUMN12 ] as 'Memo',p.[COLUMN09] AS 'Increased By', p.[COLUMN10] AS 'Decreased By',s.[COLUMN05] as 'Customer' ,ma.[COLUMN09 ] AS [Created By] From FITABLE018 p left outer join  FITABLE001 o on o.COLUMN02=p.COLUMN08 AND o.COLUMNA03=p.COLUMNA03 left outer join SATABLE002 s on s.column02 = p.column07 AND s.COLUMNA03=p.COLUMNA03 inner join FITABLE049 f on f.COLUMN01=p.COLUMN21 AND f.COLUMNA03=p.COLUMNA03 and isnull(f.COLUMNA13,'False')='False' left outer join MATABLE010 ma on ma.COLUMN02=p.columnA08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where f.column05 between '" + start + "' and '" + end + "' and f.COLUMN03 = 1615 and isnull(p.COLUMNA13,'False')='False'";

                    alCol.AddRange(new List<string> { "ID", "Opening Balance Customer", "Date", "Account", "Memo", "Increased By", "Decreased By", "Customer", "Created By" });
                    actCol.AddRange(new List<string> { "f.COLUMN02", "f.COLUMN04", "f.COLUMN05", "o.COLUMN04","p.COLUMN12", "p.COLUMN09", "p.COLUMN10", "s.COLUMN05", "ma.COLUMN09" });
                }
                else if (frmID == 1616)
                {
                    strQry = "SELECT f.[COLUMN02 ] AS ID,f.[COLUMN04 ] AS 'Opening Balance Vendor',FORMAT(f.[COLUMN05 ],'" + dateF + "') AS 'Date',o.[COLUMN04 ] AS 'Account',p.[COLUMN09 ] AS 'Increased By', p.[COLUMN10] AS 'Decreased By',p.[COLUMN12 ] as 'Memo',s.[COLUMN05] as 'Vendor', ma.[COLUMN09 ] AS [Created By] From FITABLE018 p left outer join  FITABLE001 o on o.COLUMN02=p.COLUMN08 AND o.COLUMNA03=p.COLUMNA03 left outer join SATABLE001 s on  s.COLUMN02=p.COLUMN07 AND s.COLUMNA03=p.COLUMNA03 inner join FITABLE049 f on f.COLUMN01=p.COLUMN21 AND f.COLUMNA03=p.COLUMNA03 and isnull(f.COLUMNA13,'False')='False' left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where f.column05 between '" + start + "' and '" + end + "' and f.COLUMN03 = 1616 and isnull(p.COLUMNA13,'False')='False'";

                    alCol.AddRange(new List<string> { "ID", "Opening Balance Vendor", "Date", "Account", "Memo", "Increased By", "Decreased By", "Vendor", "Created By" });
                    actCol.AddRange(new List<string> { "f.COLUMN02", "f.COLUMN04", "f.COLUMN05", "o.COLUMN04", "p.COLUMN12", "p.COLUMN09", "p.COLUMN10", "s.COLUMN05", "ma.COLUMN09" });
                }
                //EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
                else if (frmID == 1625)
                {
                    strQry = "SELECT p.COLUMN02 AS 'ID',p.COLUMN08 AS 'Routing',p.COLUMN04 AS 'Name',p.COLUMN05 AS 'Desc',p.COLUMN06 AS 'Order Of Execution', c.COLUMN04 AS 'Operating Unit',p.COLUMN10 AS 'Skip' From MATABLE030 p left outer join  CONTABLE007 c on c.COLUMN02=p.COLUMN07 and c.COLUMNA03=p.COLUMNA03 and (c.COLUMNA02=p.COLUMNA02 or c.COLUMNA02 is null) where p.COLUMN03 = 1625 and p.COLUMNA13='False'";

                    alCol.AddRange(new List<string> { "ID", "Routing", "Name", "Desc", "Order Of Execution", "Operating Unit", "Skip" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN08", "p.COLUMN04", "p.COLUMN05", "p.COLUMN06", "c.COLUMN04", "p.COLUMN10" });
                }
                //legacy outstanding
                else if (frmID == 1362)
                {
                    strQry = "SELECT   p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Bill#],FORMAT(p.[COLUMN05 ],'" + dateF + "') AS [Date],FORMAT(p.[COLUMN10 ],'" + dateF + "') AS [DueDate], " +
                    " p.[COLUMN08] AS [Amount],f.[COLUMN04] AS [Payment Terms],f1.[COLUMN04] AS [Payment Mode], p.[COLUMN09] AS [Reference#], " +
                    " d.[COLUMN04 ] AS Department, o.[COLUMN03 ] AS [Operating Unit], p.[COLUMN11] AS [Memo] From FITABLE019 p  " +
                    " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN12 " +
                    " left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN15  " +
                    " left outer join  MATABLE002 f on f.COLUMN02=p.COLUMN16 " +
                    " left outer join  MATABLE002 f1 on f1.COLUMN02=p.COLUMN17 where p.column05 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'";

                    alCol.AddRange(new List<string> { "ID", "Bill#", "Date", "DueDate", "Amount", "Payment Terms", "Payment Mode", "Reference#", "Department", "Operating Unit", "Memo" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN05", "p.COLUMN10", "p.COLUMN08", "f.COLUMN04", "f1.COLUMN04", "p.COLUMN09", "d.COLUMN04", "o.COLUMN03", "p.COLUMN11" });
                }
             //EMPHCS1723 : Creating Payment Term New form and displaying dropdown Default vales PT by GNANESHWAR ON 2/5/2016
                else if (frmID == 1580)
                {
                    strQry = "SELECT  p.[COLUMN04 ] AS [Terms],p.[COLUMN05 ] AS [Standard], " +
                    " p.[COLUMN06] AS [Net Due],p.[COLUMN07] AS [Discount Amount],p.[COLUMN08] AS [Discount Paid], p.[COLUMN09] AS [Active Flag]  From MATABLE022 p  " +
                    
                    "where p.COLUMNA13='False'";

                    alCol.AddRange(new List<string> { "Terms", "Standard", "Net Due", "Discount Amount", "Discount Paid", "Active Flag" });
                    actCol.AddRange(new List<string> {  "p.COLUMN04", "p.COLUMN05", "p.COLUMN06", "p.COLUMN07", "p.COLUMN08", "p.COLUMN09"});
                }
                //Issue Cheque
                else if (frmID == 1363)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    //EMPHCS1399	issue cheque-info screen changes(add payee,remove Balance) BY RAJ.Jr
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Voucher#],FORMAT(p.[COLUMN05],'" + dateF + "') AS [Date],(case when p.COLUMN11='22305' then s1.COLUMN05 when p.COLUMN11='22306' then m1.COLUMN09 when p.COLUMN11='22334' then s1.COLUMN05  when p.COLUMN11='22335' then s2.COLUMN05  when p.COLUMN11='22492' then m1.COLUMN09  else '' end) as Party,'' Reference#,p.COLUMN08 Memo,p.[COLUMN18 ]AS [Total Amount] ,o.[COLUMN04 ] AS [Bank],f44.[COLUMN20 ] AS [Cheque#], p.[COLUMN16 ] AS [Status],ma.COLUMN09 [Created By] From FITABLE020 p left outer join FITABLE001 o on o.COLUMN02=p.COLUMN06   left outer join  MATABLE002 m on m.COLUMN02=p.COLUMN11  left outer join  CONTABLE007 q on q.COLUMN02=p.COLUMN12 left outer join FITABLE022 fi on p.COLUMN01=fi.COLUMN09 left outer join SATABLE001 S1 on s1.COLUMN02=p.COLUMN07 left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN07 left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN07 left join FITABLE044 f44 on f44.COLUMN02=p.COLUMN09 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null)  where p.column05 between '" + start + "' and '" + end + "' and p.COLUMN03=1363 and p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Voucher#", "Date", "Party", "Reference#", "Memo", "Total Amount", "Bank", "Cheque#", "Status", "Created By" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN05", "p.COLUMN07", "p.COLUMN08", "p.COLUMN08", "p.COLUMN10", "o.[COLUMN04 ]", "f4.COLUMN20", "p.[COLUMN16 ]", "ma.[COLUMN09 ]" });
                }
                //Payment Voucher
                else if (frmID == 1525)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Voucher#],FORMAT(p.[COLUMN05],'" + dateF + "') AS [Date],(case when p.COLUMN11='22305' or p.COLUMN11='22700' then s1.COLUMN05 when p.COLUMN11='22306' then m1.COLUMN09 when p.COLUMN11='22334' then s1.COLUMN05  when p.COLUMN11='22335' then s2.COLUMN05  when p.COLUMN11='22492' then m1.COLUMN09  else '' end) as Party,f1.[COLUMN04 ] AS  Account,fi.COLUMN04 Memo,fi.COLUMN16 [Advance Paid],fi.COLUMN17 [Amount],o.[COLUMN04 ] AS [Bank],ma.COLUMN09 [Created By] From FITABLE020 p left outer join CONTABLE007 q on q.COLUMN02=p.COLUMN12 and p.COLUMNA03=q.COLUMNA03 and isnull(q.columna13,0)=0 left outer join FITABLE022 fi on p.COLUMN01=fi.COLUMN09 and p.COLUMNA03=fi.COLUMNA03 and isnull(fi.columna13,0)=0 left outer join FITABLE001 o on o.COLUMN02=p.COLUMN06  and o.COLUMNA03=p.COLUMNA03 and isnull(o.columna13,0)=0 left outer join FITABLE001 f1 on f1.COLUMN02=fi.COLUMN03 and f1.COLUMNA03=p.COLUMNA03 and isnull(f1.columna13,0)=0 left outer join SATABLE001 S1 on s1.COLUMN02=p.COLUMN07 and s1.COLUMNA03=p.COLUMNA03 and isnull(s1.columna13,0)=0 left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN07 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.columna13,0)=0 left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN07 and m1.COLUMNA03=p.COLUMNA03 and isnull(m1.columna13,0)=0 left outer join FITABLE044 f4 on f4.COLUMN02=fi.COLUMN12 and f4.COLUMNA03=p.COLUMNA03 and isnull(f4.columna13,0)=0 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column05 between '" + start + "' and '" + end + "' and p.COLUMN03=1525 and p.COLUMNA13='False'";

                    alCol.AddRange(new List<string> { "ID", "Voucher#", "Date", "Party", "Account", "Memo", "Advance Paid", "Amount", "Bank",   "Created By" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN05", "p.[COLUMN07 ]", "fi.COLUMN04", "f1.COLUMN04", "fi.COLUMN16", "fi.COLUMN17", "o.COLUMN04",   "ma.COLUMN09" });
                }
                //Receive Payment
                else if (frmID == 1386)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    //EMPHCS1398	Receive payment-info screen chanes(add payee,remove payment method) BY RAJ.Jr 
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Voucher#],FORMAT(p.[COLUMN05],'" + dateF + "') AS [Date],s2.[COLUMN05] as [Party],'' Reference#,p.[COLUMN11 ] AS [Memo],p.[COLUMN19 ] AS [Total Amount],o.[COLUMN04 ] AS [Bank],cq.[COLUMN20 ] AS [Cheque#],p.COLUMN13 Status,ma.COLUMN09 [Created By] From FITABLE023 p left outer join FITABLE001 o on o.COLUMN02=p.COLUMN09   left outer join  MATABLE002 m on m.COLUMN02=p.COLUMN07  left outer join  CONTABLE007 q on q.COLUMN02=p.COLUMN16 left outer join FITABLE024 fi on p.COLUMN01=fi.COLUMN09 left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN08 left outer join FITABLE044 cq on cq.COLUMN02=fi.COLUMN13 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column05 between '" + start + "' and '" + end + "' and p.COLUMN03=1386 and p.COLUMNA13='False'";

                    alCol.AddRange(new List<string> { "ID", "Voucher#", "Date","Party", "Reference#", "Memo", "Total Amount", "Bank","Cheque#","Status","Created By" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN05", "s2.COLUMN05", "p.[COLUMN11 ]", "p.[COLUMN11 ]", "p.COLUMN10", "o.[COLUMN04 ]", "cq.[COLUMN20 ]", "p.[COLUMN13 ]", "ma.COLUMN09" });
                }
                //Receipt Voucher
                else if (frmID == 1526)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = " SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Voucher#],FORMAT(p.[COLUMN05],'" + dateF + "') AS [Date],(case when p.COLUMN20='22305' then s1.COLUMN05 when p.COLUMN20='22306' then m1.COLUMN09 when p.COLUMN20='22334' then s1.COLUMN05  when p.COLUMN20='22335' then s2.COLUMN05  when p.COLUMN20='22492' then m1.COLUMN09  else '' end) as Party,f1.[COLUMN04 ] AS  Account,f.COLUMN04 Memo,f.COLUMN17 [Advance Received],f.COLUMN18 [Amount],o.[COLUMN04 ] AS [Bank],ma.COLUMN09 [Created By] From FITABLE023 p left outer join FITABLE024 f on f.COLUMN09=p.COLUMN01 AND f.COLUMNA03=p.COLUMNA03 AND ISNULL(f.COLUMNA13,0)=0 left outer join FITABLE001 o on o.COLUMN02=f.COLUMN12 AND o.COLUMNA03=p.COLUMNA03 AND ISNULL(o.COLUMNA13,0)=0 left outer join FITABLE001 f1 on f1.COLUMN02=f.COLUMN03 AND f1.COLUMNA03=p.COLUMNA03 AND ISNULL(f1.COLUMNA13,0)=0 left outer join  MATABLE002 m on m.COLUMN02=p.COLUMN07  AND m.COLUMNA03=p.COLUMNA03 AND ISNULL(m.COLUMNA13,0)=0 left outer join  CONTABLE007 q on q.COLUMN02=p.COLUMN16 AND q.COLUMNA03=p.COLUMNA03 AND ISNULL(q.COLUMNA13,0)=0 left outer join SATABLE001 S1 on s1.COLUMN02=p.COLUMN08 AND S1.COLUMNA03=p.COLUMNA03 AND ISNULL(S1.COLUMNA13,0)=0 left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN08 AND s2.COLUMNA03=p.COLUMNA03 AND ISNULL(s2.COLUMNA13,0)=0 left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN08 AND m1.COLUMNA03=p.COLUMNA03 AND ISNULL(m1.COLUMNA13,0)=0 left outer join FITABLE044 f4 on f4.COLUMN02=f.COLUMN13 AND f4.COLUMNA03=p.COLUMNA03 AND ISNULL(f4.COLUMNA13,0)=0 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null)  where p.column05 between '" + start + "' and '" + end + "' and p.COLUMN03=1526 and p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Voucher#", "Date", "Party", "Account", "Memo", "Advance Received", "Amount", "Bank", "Created By" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN05", "p.[COLUMN08 ]", "f1.[COLUMN04 ]", "f.[COLUMN04 ]", "f.[COLUMN17 ]", "f.COLUMN18", "o.[COLUMN04 ]","ma.[COLUMN09]" });
                }
                //write cheque
                else if (frmID == 1527)
                {
                    strQry = "SELECT p.[COLUMN02] AS ID,p.[COLUMN04] AS [Write Cheque#],FORMAT(p.[COLUMN06 ],'" + dateF + "') AS [Issue Date],o.[COLUMN04] AS [Bank],p.COLUMN20 as Cheque#, " +
"(case when p.COLUMN08='22305' then s1.COLUMN05 "+
    " when p.COLUMN08='22306' then m1.COLUMN09 "+
	" when p.COLUMN08='22334' then s1.COLUMN05 "+
	" when p.COLUMN08='22335' then s2.COLUMN05 "+
	" when p.COLUMN08='22492' then m1.COLUMN09  else '' end) as Payee, "+
" p.[COLUMN13] AS [Amount],FORMAT(p.[COLUMN07 ],'" + dateF + "') as [Cheque Date],m.COLUMN04 as Status From FITABLE044 p left outer join FITABLE001 o on o.COLUMN02=p.COLUMN11   left outer join  MATABLE002 m on m.COLUMN02=p.COLUMN15  left outer join  CONTABLE007 q on q.COLUMN02=p.COLUMN16 left outer join SATABLE001 S1 on s1.COLUMN02=p.COLUMN09 left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN09 left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN09 where p.column06 between '" + start + "' and '" + end + "' and p.COLUMN03=1527 and p.COLUMNA13='False' ";

                    alCol.AddRange(new List<string> { "ID", "Write Cheque#", "Issue Date", "Bank","Cheque#", "Payee", "Amount", "Cheque Date","Status" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN06", "o.[COLUMN04 ]","p.COLUMN20", "p.[COLUMN11 ]", "p.COLUMN13", "p.[COLUMN07 ]" ,"m.COLUMN04"});
                }
                //Contact
                else if (frmID == 1388)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Contact#],p.[COLUMN05 ] AS [Name],m.[COLUMN04]  AS [Type],(case when p.COLUMN10 in (22305) then v.[COLUMN05] else c.[COLUMN05] end) Company#, p.[COLUMN12 ] AS [Designation],p.[COLUMN16] AS [Mobile#],d.[COLUMN04 ] AS [Department],q.[COLUMN04 ] AS [Operating Unit] From MATABLE018 p " +
                    "left outer join FITABLE001 o on o.COLUMN02=p.COLUMN09 left outer join  MATABLE002 m on m.COLUMN02=p.COLUMN10 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN19 left outer join  SATABLE001 v on v.COLUMN02=p.COLUMN11 left outer join  SATABLE002 c on c.COLUMN02=p.COLUMN11 left outer join  CONTABLE007 q on q.COLUMN02=p.COLUMN20  where p.COLUMNA13='False' ";

                    alCol.AddRange(new List<string> { "ID", "Contact#", "Name", "Type", "Company#", "Designation", "Mobile#", "Department", "Operating Unit" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN05", "m.[COLUMN04]", "p.[COLUMN10 ]", "p.COLUMN12", "p.COLUMN16", "d.[COLUMN04 ]", "q.[COLUMN04 ]" });
                }

                // vendar Master
                else if (frmID == 1252)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID, p.[COLUMN04 ] AS 'Vendor#',p.[COLUMN05] AS 'Name',sc.[COLUMN04 ] AS Type,c.[COLUMN04 ] AS Category," +
                    "b.[COLUMN04 ] AS 'Sub Category',p.[COLUMN11 ] as Email,p.[COLUMN12 ] as Phone, p.[COLUMN23 ] as 'TIN#',p.[COLUMN24 ] as Status From SATABLE001 p " +
                    " left outer join  MATABLE002 c on c.COLUMN02=p.COLUMN07  left outer join  MATABLE002 sc on sc.COLUMN02=p.COLUMN22 left outer join  MATABLE002 b on sc.COLUMN02=p.COLUMN08 where"+
                    //p.COLUMN24='False' and
                        //EMPHCS682 Rajasekhar patakota on 17/7/2015.Displaying Inactives
                        " p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Vendor#", "Name", "Type", "Category", "Sub Category", "Email", "Phone", "TIN#","Status" });
                    actCol.AddRange(new List<string> { " p.COLUMN02", " p.COLUMN04", "p.COLUMN05", "sc.COLUMN04", "c.COLUMN04", "b.COLUMN04", "p.COLUMN11", "p.COLUMN12", "p.COLUMN23", "p.COLUMN24" });
                }
                // RECEIPT Type
                else if (frmID == 1280)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS 'Name',p.[COLUMN05 ] AS 'Description',p.[COLUMN06 ] AS 'Default Flag',p.COLUMN11 'Overage%', d.[COLUMN04 ] AS Department ," +
                    "o.[COLUMN03] AS OperatingUnit,p.[COLUMN12] AS 'Inactive Flag' From MATABLE011 p left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN07 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN10 where p.COLUMNA13='False' ";
                    alCol.AddRange(new List<string> { "ID", "Name", "Description", "Default Flag", "Overage%", "Department", "OperatingUnit", "Inactive Flag" });
                    actCol.AddRange(new List<string> { " p.COLUMN02 ", "p.COLUMN04", "p.COLUMN05", "p.COLUMN06", "p.COLUMN11", "d.COLUMN04", "o.COLUMN03", "p.COLUMN12" });
                }
                // customer Master
                else if (frmID == 1265)
                {
                    strQry = "SELECT p.COLUMN02  AS ID, p.COLUMN04  AS 'Customer#',p.COLUMN05 AS 'Name', c.COLUMN04 AS 'Category',sc.COLUMN04  AS 'Sub Category', p.COLUMN10  as 'Email', p.COLUMN11  as 'Phone',p.COLUMN21 'TIN#'," +
                    "e.COLUMN06 AS 'Sales Rep',p.COLUMN23 AS 'Commission%',st.COLUMN04  AS 'Type' From SATABLE002 p " +
                    "left outer join  MATABLE002 c on c.COLUMN02=p.COLUMN07   left outer join  MATABLE002 sc on sc.COLUMN02=p.COLUMN08 left outer join  MATABLE010 e on e.COLUMN02=p.COLUMN22 and e.COLUMN30='True'  left outer join  MATABLE002 st on sc.COLUMN02=p.COLUMN24 where"+
                    //p.COLUMN25='False' and 
                        //EMPHCS681 Rajasekhar patakota on 17/7/2015.Displaying Inactives
                        " p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Customer#", "Name", "Category", "Sub Category", "Email", "Phone", "TIN#", "Sales Rep", "Commission%", "Type" });
                    actCol.AddRange(new List<string> { " p.COLUMN02", " p.COLUMN04", "p.COLUMN05", "c.COLUMN04", "sc.COLUMN04", "p.COLUMN10", "p.COLUMN11", "p.COLUMN21", "e.COLUMN06", "p.COLUMN23", "st.COLUMN04" });
                }
                // order Type
                else if (frmID == 1279)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS 'Name',p.[COLUMN05 ] AS 'Description',p.[COLUMN06 ] AS 'Default Flag', d.[COLUMN04 ] AS Department,o.[COLUMN03] AS 'Operating Unit',p.COLUMN11 'Inactive Flag' From MATABLE009 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN07  left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN10  left outer join  CONTABLE009 s on s.COLUMN02=p.COLUMN08  left outer join  MATABLE002 c on c.COLUMN02=p.COLUMN09 where p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Name", "Description", "Default Flag", "Department", "Operating Unit", "Inactive Flag" });
                    actCol.AddRange(new List<string> { " p.COLUMN02 ", "p.COLUMN04", "p.COLUMN05", "p.COLUMN06", "d.COLUMN04", "o.COLUMN03", "p.COLUMN11" });
                }
                //item master
                else if (frmID == 1261)
                {
				//EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
				    //EMPHCS1819 rajasekhar reddy patakota 21/09/2016 Info Grid Customisation for dynamic pages
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Name],P.[COLUMN50] AS [Description],g.[COLUMN04] AS [Group], b.[COLUMN04] AS [Brand],p.[COLUMN45] AS [Distribution Price],iif(isnull(r.column04,0)>0,r.column04, p.[COLUMN17 ]) AS [Purchase Price] From MATABLE007 p " +
                    "left outer join  MATABLE024 r on r.COLUMN07=p.COLUMN02 and r.column06='Purchase' and  (r.COLUMN03 in(" + POPUnit + ") or isnull(r.COLUMN03,0)=0) and p.columna03=r.columna03 and isnull(r.columna13,0)=0 left outer join  MATABLE008 d on d.COLUMN02=p.COLUMN05  left join  MATABLE004 g on g.COLUMN02=p.COLUMN11 and g.COLUMNA03=p.COLUMNA03 and isnull(g.COLUMNA13,0)=0 left outer join  MATABLE005 b on b.COLUMN02=p.COLUMN10 where" +
                        //p.COLUMN47='False' and 
                        //EMPHCS680 Rajasekhar patakota on 17/7/2015.Displaying Inactive Items also
                        " p.COLUMNA13='False'";
                    //EMPHCS1819 rajasekhar reddy patakota 21/09/2016 Info Grid Customisation for dynamic pages	
                    alCol.AddRange(new List<string> { "ID", "Name", "Description", "Group", "Brand", "Distribution Price", "Purchase Price" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN50", "g.COLUMN04", "b.COLUMN04", "p.COLUMN45", "p.COLUMN17" });

                }
                //Item Family
                else if (frmID == 1262)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS 'Name',p.[COLUMN05 ] AS 'Description',p.[COLUMN10] AS 'Overage%', d.[COLUMN04 ] AS Department,o.[COLUMN03] AS 'Operating Unit', p.[COLUMN12] AS 'Inactive Flag'" +
                    " From MATABLE003 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN06  left outer join MATABLE002 d on d.COLUMN02=p.COLUMN09   left outer join  CONTABLE009 s on s.COLUMN02=p.COLUMN07  left outer join  MATABLE002 c on c.COLUMN02=p.COLUMN08 where p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Name", "Description", "Overage%", "Department", "Operating Unit", "Inactive Flag" });
                    actCol.AddRange(new List<string> { " p.COLUMN02 ", "p.COLUMN04", "p.COLUMN05", "p.COLUMN10", "d.COLUMN04", "o.COLUMN03", "p.COLUMN12" });
                }
                // Item Group
                else if (frmID == 1264)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS 'Name',p.[COLUMN05 ] AS 'Description',i.[COLUMN04 ] AS 'Item Family', d.[COLUMN04 ] AS Department, o.[COLUMN03] AS 'Operating Unit', p.[COLUMN11] AS 'Overage%',p.[COLUMN12] AS 'Inactive Flag' " +
                    " From MATABLE004 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN07   left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN10  left outer join  MATABLE003 i on i.COLUMN02=p.COLUMN06  where p.COLUMNA13='False'";

                    alCol.AddRange(new List<string> { "ID", "Name", "Description", "Item Family", "Department", "Operating Unit", "Overage%", "Inactive Flag" });
                    actCol.AddRange(new List<string> { " p.COLUMN02 ", "p.COLUMN04", "p.COLUMN05", "i.COLUMN04", "d.COLUMN04", "o.COLUMN03", "p.COLUMN11", "p.COLUMN12" });
                }
                // Item Brand
                else if (frmID == 1263)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS 'Name',p.[COLUMN05 ] AS 'Description',v.[COLUMN05 ] AS 'Vendor', d.[COLUMN04 ] AS Department, o.[COLUMN03] AS 'Operating Unit', p.[COLUMN11] AS 'Overage%',p.[COLUMN12] AS 'Inactive Flag'" +
                    " From MATABLE005 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN07   left outer join    MATABLE002 d on d.COLUMN02=p.COLUMN10  left outer join  SATABLE003 v on v.COLUMN02=p.COLUMN06 where p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Name", "Description", "Vendor", "Department", "Operating Unit", "Overage%", "Inactive Flag" });
                    actCol.AddRange(new List<string> { " p.COLUMN02 ", "p.COLUMN04", "p.COLUMN05", "v.COLUMN05", "d.COLUMN04", "o.COLUMN03", "p.COLUMN11", "p.COLUMN12" });
                }
                //MATRIX ITEM MASTER
                else if (frmID == 1285)
                {
                    strQry = " SELECT p.[COLUMN02 ] AS ID,p.[COLUMN41] AS [Item#],p.[COLUMN04 ] AS [Name],d1.[COLUMN04] AS [Color],d2.[COLUMN04] AS [Style],d3.[COLUMN04] AS [Size],b.[COLUMN04] AS [Brand],p.[COLUMN06] AS UPC, o.[COLUMN03] AS 'Operating Unit',d.[COLUMN04 ] AS Department From MATABLE007 p" +
                    " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN37   left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN40   left outer join  MATABLE002 d1 on d1.COLUMN02=p.COLUMN42   left outer join  MATABLE002 d2 on d2.COLUMN02=p.COLUMN43   left outer join  MATABLE002 d3 on d3.COLUMN02=p.COLUMN44   left outer join  MATABLE005 b on b.COLUMN02=p.COLUMN10   left outer join  SATABLE005 j on j.COLUMN02=p.COLUMN06 where p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Item#", "Name", "Color", "Style", "Size", "Brand", "upc", "Operating Unit", "Department" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN41", "p.COLUMN04", "d1.COLUMN04", "d2.COLUMN04", "d3.COLUMN04", "b.COLUMN04", "p.COLUMN06", "o.COLUMN03", "d.COLUMN04" });
                }
				//EMPHCS1497	Item alias form Creation in inventory master By Raj.Jr
                else if (frmID == 1530)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS 'UPC#',i.[COLUMN04] AS 'Item',d.COLUMN04 Units,p.COLUMN07 as Quantity,v.[COLUMN05 ] AS 'Vendor', p.[COLUMN09 ] AS Price,d1.COLUMN04 as Type, o.[COLUMN03] AS 'Operating Unit' From MATABLE021 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN11   left outer join    MATABLE002 d on d.COLUMN02=p.COLUMN06  left outer join  SATABLE001 v on v.COLUMN02=p.COLUMN08 left outer join MATABLE007 i on i.COLUMN02=p.COLUMN05 left outer join    MATABLE002 d1 on d1.COLUMN02=p.COLUMN10 where p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "UPC#", "Item", "Units", "Quantity", "Vendor", "Price", "Type", "Operating Unit" });
                    actCol.AddRange(new List<string> { " p.COLUMN02 ", "p.COLUMN04", "i.COLUMN04", "d.COLUMN04", "p.COLUMN07", "v.COLUMN05", "p.COLUMN09", "d1.COLUMN04", "o.COLUMN03" });
                }
                // Account Master
                else if (frmID == 1250)
                {
                    //EMPHCS1494	COA Reports Changes  BY RAJ.Jr 05/01/2016
				//EMPHCS1402 rajasekhar reddy patakota 5/12/2015 MultiUom Condition checking While opening in another tab
                    strQry = "SELECT p.COLUMN02 AS ID,p.COLUMN04 AS 'Account Name',d.COLUMN04 as 'Type',p.[COLUMN05] AS 'Sub Description', sum(isnull(p.COLUMN08,0)) AS 'Opening Balance',	p.[COLUMN07] AS Typ From FITABLE001 p left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN07 where p.COLUMNA13='False' ";
                    alCol.AddRange(new List<string> { "ID", "Account Name","Type", "Sub Description",  "Opening Balance" });
                    actCol.AddRange(new List<string> { " p.COLUMN02", " p.COLUMN04","d.COLUMN04", "p.COLUMN05",  "p.COLUMN08" });
                }
                //ExpenceType Master
                else if (frmID == 1292)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID, p.[COLUMN04 ] AS [Name], p.[COLUMN05 ] AS [Description],p.COLUMN11 Memo,d.[COLUMN04 ] AS Department, o.[COLUMN03 ] AS [Operating Unit] From FITABLE011 p   left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN06   left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN09 where p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Name", "Description", "Memo", "Department", "Operating Unit" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "P.COLUMN05", "P.COLUMN11", "d.COLUMN04", "o.COLUMN03" });
                }
                // Tax Master
                else if (frmID == 1322)
                {
                    //EMPHCS782 rajasekhar reddy patakota 28/7/2015  Tax Master  record display in list after save
                    strQry = "SELECT p.COLUMN02  ID, p.COLUMN04  'Name', p.COLUMN08  'Description',MA02.COLUMN04  'Filling Frequency', case when p.COLUMN06=1 then 'YES' else 'FALSE' end   'Applies to Sales ',case when p.COLUMN13=1 then 'YES' else 'FALSE' end   'Applies to Purchase',case when p.COLUMN14=1 then 'YES' else 'FALSE' end  'Applies to VAS' From MATABLE013  p left join MATABLE002 MA02 on MA02.COLUMN02=p.COLUMN05 where p.COLUMNA13='False'";
                    alCol.AddRange(new List<string> { "ID", "Name", "Description", "Filling Frequency", "Applies to Sales ", "Applies to Purchase", "Applies to VAS" });
                    actCol.AddRange(new List<string> { " p.COLUMN02", " p.COLUMN04", "p.COLUMN08", "p.COLUMN05", "p.COLUMN06", "p.COLUMN13", "p.COLUMN14" });
                }
                // Tax Group Master
                else if (frmID == 1323)
                {

                    strQry = "SELECT p.COLUMN02  ID, p.COLUMN04  'Name', p.COLUMN08  'Description',MA02.COLUMN04  'Filling Frequency',case when p.COLUMN13=1 then 'YES' else 'FALSE' end   'Applies to Sales ',case when p.COLUMN07=1 then 'YES' else 'FALSE' end   'Applies to Purchase',case when p.COLUMN14=1 then 'YES' else 'FALSE' end  'Applies to VAS' From MATABLE014  p LEFT join MATABLE002 MA02 on MA02.COLUMN02=p.COLUMN06 where p.COLUMNA13='False' ";

                    alCol.AddRange(new List<string> { "ID", "Name", "Description", "Filling Frequency", "Applies to Sales ", "Applies to Purchase", "Applies to VAS" });
                    actCol.AddRange(new List<string> { " p.COLUMN02", " p.COLUMN04", "p.COLUMN08", "p.COLUMN06", "p.COLUMN13", "p.COLUMN07", "p.COLUMN14" });
                }
                //Daily Vist Report
                // EMPHSC MSI# By venkat 19/05/2016
                // EMPHSC MIS COMMUTE By venkat
                else if (frmID == 1404)
                {
                    strQry = "DECLARE @OP nvarchar(250) "+
" Set @OP = (Select isnull(COLUMN26,0) from MATABLE010 where COLUMN02 = "+System.Web.HttpContext.Current.Session["eid"]+")"+
                        //EMPHCS1811 rajasekhar reddy patakota 10/09/2016 Line Grid Details filling in sales & Purchase module
                    " Select p.COLUMN02 ID,p.COLUMN15 as Lead#,FORMAT(p.[COLUMN05],'" + dateF + "') as Date,p.COLUMN06 as [Company],p.COLUMN07 as [Contact Person],p.COLUMN16 Phone#,mc.COLUMN04 as Category,mm.COLUMN04 as Milestone,FORMAT(p.[COLUMN27],'" + dateF + "') as [Next Follow Up],ms.COLUMN04 as Status,m1.COLUMN09 as [Created By] From MATABLE019 p left join MATABLE002 mc on mc.COLUMN02=p.COLUMN10 and isnull(mc.COLUMNA13,0)=0 left join MATABLE002 mm on mm.COLUMN02=p.COLUMN11 and isnull(mm.COLUMNA13,0)=0 left join MATABLE002 ms on ms.COLUMN02=p.COLUMN12 and isnull(ms.COLUMNA13,0)=0 LEFT OUTER JOIN MATABLE010 m1 on m1.COLUMN02 = p.COLUMNa08 and (m1.COLUMN30='True' or m1.COLUMN30='1') where p.COLUMNA13='False'" + CLOS + "";
                    alCol.AddRange(new List<string> { "ID", "Lead#", "Date", "Company", "Contact Person", "Phone#", "Category", "Milestone", "Next Follow Up", "Status", "Created By" });
                    actCol.AddRange(new List<string> { " p.COLUMN02", "p.COLUMN15", "p.COLUMN05", "p.COLUMN06", "p.COLUMN07", "p.COLUMN16", "mc.COLUMN04", "mm.COLUMN04", "p.COLUMN27", "ms.COLUMN04", "m1.COLUMN09" });
                }
               // employee Master1 Code commented by ASR and Raj on 07/13/2015
                else if (frmID == 1260)
                {
                    strQry = "SELECT p.COLUMN02  AS ID, p.COLUMN04  AS 'Employee#', p.COLUMN09  as 'Name',p.COLUMN10  as 'Job Title'," + "p.COLUMN30  as 'Sales Rep',p.COLUMN14  as 'Phone',c.COLUMN04  AS Department, o.COLUMN03 AS 'Operating Unit',p.COLUMN32 AS Status From MATABLE010 p left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN26  left outer join  MATABLE002 c on c.COLUMN02=p.COLUMN29 where p.COLUMNA13='False'"; // and p.COLUMN20='False' "; Commented by Sudheer

                    alCol.AddRange(new List<string> { "ID", "Employee#", "Name", "Job Title", "Sales Rep", "Phone", "Department", "Operating Unit", "Status" });
                    actCol.AddRange(new List<string> { " p.COLUMN02", " p.COLUMN04", "p.COLUMN09", "p.COLUMN10", "p.COLUMN30", "p.COLUMN14", "c.COLUMN04", "o.COLUMN03", "p.COLUMN32" });
                }               
                // UOM Master
                else if (frmID == 1416)
                {
                    strQry = "SELECT p.COLUMN02  AS ID, p.COLUMN05  AS 'Name', p.COLUMN06  as 'Quantity',p.COLUMN07  as 'Base UOM', p.COLUMN08  as 'Default' From FITABLE037 p  where p.COLUMNA13='False' ";

                    alCol.AddRange(new List<string> { "ID", "Name", "Quantity", "Base UOM", "Default" });
                    actCol.AddRange(new List<string> { " p.COLUMN02", " p.COLUMN05", " p.COLUMN06", "p.COLUMN07", "p.COLUMN08" });
                }
                //EMPHCS1140 Item Pricing for Multi Operating Unit level by Srinivas 13/09/2015
                else if (frmID == 1500)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,o.[COLUMN03] AS 'Operating Unit',d.[COLUMN04 ] AS Item,d.[COLUMN06] AS [UPC],p.[COLUMN09 ] AS [Purchage Price],p.[COLUMN10] AS [Sales Price],p.[COLUMN11] AS [Last Purchase Price],p.[COLUMN12] AS [Avarage Price]  From FITABLE039 p left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN04   left outer join  MATABLE007 d on d.COLUMN02=p.COLUMN05 where p.COLUMNA13='False' ";
                    alCol.AddRange(new List<string> { "ID", "Operating Unit", "Item", "UPC", "Purchage Price", "Sales Price", "Last Purchase Price", "Avarage Price" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "o.COLUMN03", "d.COLUMN04", "d.COLUMN06", "p.COLUMN09", "p.COLUMN10", "p.COLUMN11", "p.COLUMN12" });
                }
				//EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
                //Location
                else if (frmID == 1501)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS Name,p.[COLUMN05] AS [Description],p.[COLUMN10] AS [Mobile No],o.[COLUMN03] AS 'Operating Unit',p.[COLUMN18] AS [Addressee]  From CONTABLE030 p left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN11 where p.COLUMNA13='False' ";
                    alCol.AddRange(new List<string> { "ID", "Name", "Description", "Mobile No", "Operating Unit", "Addressee" });
					//EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN05", "p.COLUMN10", "o.COLUMN03", "p.COLUMN18" });
                }
                else if (frmID == 1515)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Lot No],FORMAT(p.[COLUMN05 ],'" + dateF + "') AS [Expiry Date],l.[COLUMN04] AS [Location],m.[COLUMN04] AS [Item],o.[COLUMN03] AS 'Operating Unit'  From FITABLE043 p left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN10 left outer join  CONTABLE030 l on l.COLUMN02=p.COLUMN08 left outer join  MATABLE007 m on m.COLUMN02=p.COLUMN09 where p.COLUMNA13='False' ";
                    alCol.AddRange(new List<string> { "ID", "Lot No", "Expiry Date", "Location", "Item", "Operating Unit" });
                    actCol.AddRange(new List<string> { "p.COLUMN02", "p.COLUMN04", "p.COLUMN05", "l.COLUMN04", "m.COLUMN04", "o.COLUMN03" });
                }
                //Misc
                else
                {
                    if (strQry == null)
                    {
                        strQry += "SELECT [COLUMN02 ] AS ID ";
                        actCol.Add("ID");
                        alCol.Add("ID");
                    }
                    if (flds.Count > 0)
                    {
                        for (int i = 0; i < flds.Count; i++)
                        {
                            if (i == flds.Count - 1)
                            {
                                strQry += ", " + flds[i].COLUMN05 + " AS [" + flds[i].COLUMN06 + "] FROM " + table + " p ";
                                actCol.Add(flds[i].COLUMN05);
                                alCol.Add(flds[i].COLUMN06);
                            }
                            else
                            {
                                strQry += ", " + flds[i].COLUMN05 + " AS [" + flds[i].COLUMN06 + "]";
                                actCol.Add(flds[i].COLUMN05);
                                alCol.Add(flds[i].COLUMN06);
                            }
                        } 
						//EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
						strQry = strQry + "where p.COLUMN02 != '' ";
                    }
                    if (fldsV.Count > 0)
                    {
                        for (int i = 1; i < fldsV.Count; i++)
                        {
                            if (i == fldsV.Count - 1)
                            {
                                strQry += ", " + fldsV[i].COLUMN06 + " AS [" + fldsV[i].COLUMN07 + "] FROM " + table + " p ";
                                actCol.Add(fldsV[i].COLUMN06);
                                alCol.Add(fldsV[i].COLUMN07);
                            }
                            else
                            {
                                strQry += ", " + fldsV[i].COLUMN06 + " AS [" + fldsV[i].COLUMN07 + "]";
                                actCol.Add(fldsV[i].COLUMN06);
                                alCol.Add(fldsV[i].COLUMN07);
                            }
                        }
                    }
                }
            }
            else
            {
                if (strQry == null)
                {
				//EMPHCS1592 rajasekhar reddy patakota 08/03/2016 Custom View Changes for Sales Order
                    strQry += "SELECT p.[COLUMN02 ] AS ID ";
                    actCol.Add("ID");
                    alCol.Add("ID");
                }
                if (flds.Count > 0)
                {
                    for (int i = 0; i < flds.Count; i++)
                    {
                        if (i == flds.Count - 1)
                        {
                            strQry += ", " + flds[i].COLUMN05 + " AS [" + flds[i].COLUMN06 + "] FROM " + table + " p ";
                            actCol.Add(flds[i].COLUMN05);
                            alCol.Add(flds[i].COLUMN06);
                        }
                        else
                        {
                            strQry += ", " + flds[i].COLUMN05 + " AS [" + flds[i].COLUMN06 + "]";
                            actCol.Add(flds[i].COLUMN05);
                            alCol.Add(flds[i].COLUMN06);
                        }
                    }
                }
                if (fldsV.Count > 0)
                {
				//EMPHCS1592 rajasekhar reddy patakota 08/03/2016 Custom View Changes for Sales Order
                    var fltbl = ""; var falcol = "";
                    for (int i = 1; i < fldsV.Count; i++)
                    {
					//EMPHCS1012 rajasekhar reddy patakota 25/08/2015 Customisation of Forms
					//EMPHCS1592 rajasekhar reddy patakota 08/03/2016 Custom View Changes for Sales Order
                        var actcol=fldsV[i].COLUMN06;var aliascol=fldsV[i].COLUMN07;var ctblid=fldsV[i].COLUMN04;var tbl ="";var ltbl ="";var alcol ="";
                        var formlist = db.CONTABLE006.Where(q => q.COLUMN04 == ctblid && q.COLUMN03 == frmID && q.COLUMN05 == actcol).FirstOrDefault();
                        SqlCommand cmdp = new SqlCommand("SELECT COLUMN04,COLUMN03 Parent_Internal_ID FROM CONTABLE005 WHERE COLUMN05 LIKE '%Parent_Internal_ID%'  and COLUMN03 =" + ctblid + " ", cn);
                            SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                            DataTable dtp = new DataTable();
                            dap.Fill(dtp);
                            alcol = "p." + fldsV[i].COLUMN06;
                            if (dtp.Rows.Count > 0)
                            {
                                var ltblid=Convert.ToInt32( dtp.Rows[0][1].ToString());
                                ltbl = db.CONTABLE004.Where(a => a.COLUMN02 == ltblid).FirstOrDefault().COLUMN04;
                                alcol = "q." + fldsV[i].COLUMN06;
                                fltbl = ltbl; falcol = dtp.Rows[0][0].ToString();
                            }
                            if (formlist.COLUMN10 == "DropDownList") {                            
                            if (formlist.COLUMN14 == "Control Value") tbl = "MATABLE002";
                            else
                                tbl = db.CONTABLE004.Where(a => a.COLUMN02 == formlist.COLUMN15).FirstOrDefault().COLUMN04;
                            var col = "COLUMN04";
                            if (tbl == "SATABLE001" || tbl == "SATABLE002") col = "COLUMN05";
                            else if (tbl == "MATABLE010") col = "COLUMN09";
                            else if (tbl == "CONTABLE008"||tbl == "CONTABLE007"||tbl == "CONTABLE009") col = "COLUMN03";
                            strQry += ",  (select " + col + " from " + tbl + " where column02=" + alcol + ") AS [" + fldsV[i].COLUMN07 + "]  ";
                            actCol.Add(fldsV[i].COLUMN06);
                            alCol.Add(fldsV[i].COLUMN07);
                            if (i == fldsV.Count - 1)
                            {
                                if (fltbl != "")
                                    strQry += " ,ma.COLUMN09 [Created By] FROM " + table + " p inner join  " + fltbl + " q on p.COLUMN01=q." + falcol + " and q.COLUMNA13=0";
                                else
                                    strQry += " ,ma.COLUMN09 [Created By] FROM " + table + " p ";
                            }
                        }
                        else if (i == fldsV.Count - 1)
                        {
						//EMPHCS1592 rajasekhar reddy patakota 08/03/2016 Custom View Changes for Sales Order
                            var col = alcol;
                            if (formlist.COLUMN10 == "DatePicker")
                                col = "convert(varchar," + alcol + ", 106)";
                            if (fltbl != "")
                                strQry += ", " + col + " AS [" + fldsV[i].COLUMN07 + "] ,ma.COLUMN09 [Created By] FROM " + table + " p inner join  " + fltbl + " q on p.COLUMN01=q." + falcol + " and q.COLUMNA13=0";
                            else
                                strQry += ", " + col + " AS [" + fldsV[i].COLUMN07 + "] ,ma.COLUMN09 [Created By] FROM " + table + " p ";
                            actCol.Add(fldsV[i].COLUMN06);
                            alCol.Add(fldsV[i].COLUMN07);
                        }
                        else
                        {
						//EMPHCS1592 rajasekhar reddy patakota 08/03/2016 Custom View Changes for Sales Order
                            var col = alcol;
                            if (formlist.COLUMN10 == "DatePicker")
                                col = "convert(varchar," + alcol + ", 106)";
                            strQry += ", " + col + " AS [" + fldsV[i].COLUMN07 + "]";
                            actCol.Add(fldsV[i].COLUMN06);
                            alCol.Add(fldsV[i].COLUMN07);
                        }
                    }
                    actCol.Add("COLUMN09");
                    alCol.Add("Created By");
                    strQry += "  left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and Ma.COLUMNA02=p.COLUMNA02 ";
                } 
				//EMPHCS1012 rajasekhar reddy patakota 25/08/2015 Customisation of Forms
                if (CLOS != "") strQry += " where p.column02!='' " + CLOS + "";
                else strQry += " where p.COLUMN02 != '' ";
            } string viewSort = "";
            object gridsort = "";
            object grdcol = "";
            object SortByStatus = "";
            eBizSuiteTableEntities ety = new eBizSuiteTableEntities();
			//EMPHCS1012 rajasekhar reddy patakota 25/08/2015 Customisation of Forms
            string customfrmID ="";
            var fid = db.CONTABLE0010.Where(q => q.COLUMN06 == frmID && q.COLUMN05 == "Custom").OrderBy(a => a.COLUMN02).ToList();
            if (fid.Count>0)
            {
                for (int i = 0; i < fid.Count; i++) { customfrmID += fid[i].COLUMN02.ToString()+ ",";  }
                customfrmID = customfrmID.TrimEnd(',');
            }
            //EMPHCS1011 rajasekhar reddy patakota 25/08/2015 Customisation of Views
            else { customfrmID = frmID.ToString(); }
            if (frmID == 1272)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "IR" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                gridsort = "p.COLUMN17 = 1001 and ";
                //statusstring = sortByStatus.ToString();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                else if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN18 ";
                    SortByStatus = sortByStatus;
                }

            }
            else if (frmID == 1251)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "PO" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                //statusstring = sortByStatus;
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                else if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus.ToString();
                }
				//EMPHCS1012 rajasekhar reddy patakota 25/08/2015 Customisation of Forms
                gridsort = "p.COLUMN29 = 1001 and (p.COLUMN03=1251 or p.COLUMN03 in(" + customfrmID + "))  and ";
            }
            else if (frmID == 1374)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "PQ").Select(a => a.COLUMN04).ToList();
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus;
                }
				//EMPHCS1012 rajasekhar reddy patakota 25/08/2015 Customisation of Forms
                gridsort = "p.COLUMN29 = 1001 and  (p.COLUMN03=1374 or p.COLUMN03 in(" + customfrmID + "))  and ";
            }
            else if (frmID == 1328)
            {
			//EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "SR" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus;
                }
				//EMPHCS1012 rajasekhar reddy patakota 25/08/2015 Customisation of Forms
                gridsort = "p.COLUMN29 = 1003 and  (p.COLUMN03=1328 or p.COLUMN03 in(" + customfrmID + "))  and ";
            }
            else if (frmID == 1329)
            {
			//EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "RR" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus;
                }
				//EMPHCS1012 rajasekhar reddy patakota 25/08/2015 Customisation of Forms
                gridsort = "p.COLUMN17 = 1003 and  (p.COLUMN03=1329 or p.COLUMN03 in(" + customfrmID + "))  and ";
            }
            else if (frmID == 1330)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "CM").Select(a => a.COLUMN04).ToList();
				//EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus;

                }
				//EMPHCS1012 rajasekhar reddy patakota 25/08/2015 Customisation of Forms
                gridsort = "p.COLUMN29 = 1003 and  (p.COLUMN03=1330 or p.COLUMN03 in(" + customfrmID + "))   and ";
            }
            else if (frmID == 1273)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "BL" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                //statusstring = sortByStatus.ToString();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                else if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN13 ";
                    SortByStatus = sortByStatus;
                }
                gridsort = "p.COLUMN20 = 1001 and ";
            }
            else if (frmID == 1274)
            {

                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "PB").Select(a => a.COLUMN04).ToList();
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN11 ";
                    SortByStatus = sortByStatus;
                }
                gridsort = "p.COLUMN17 = 1001 and ";
            }
				// oppurtinity Filter Changes By Venkat 
            else if (frmID == 1375)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "OP" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL"||Convert.ToString(sortByStatus) == "OPEN")
                {
                    sortByStatus = null;
                }
                else if (sortByStatus != null)
                {
                    grdcol = "so.COLUMN04";
                    SortByStatus = sortByStatus;

                }
                gridsort = "";
            } 
            //else if (frmID == 1376)
            //{
            //    info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "SQ").Select(a => a.COLUMN04);
            //    if (Session["SortByStatus"] != null)
            //    {
            //        grdcol = "p.COLUMN16 ";
            //        SortByStatus = Session["SortByStatus"].ToString();
            //        
            //    }

            //    gridsort = "  p.COLUMN29 = 1002 and ";
            //}
            else if (frmID == 1275)
            {
                                    //EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
				info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "SO" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                //statusstring = sortByStatus;
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                else if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus.ToString();
                }

                gridsort = "  p.COLUMN29 = 1002 and ";
            }
            else if (frmID == 1276)
            {
                                    //EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
				info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "IF" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                //statusstring = sortByStatus.ToString();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                else if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN14 ";
                    SortByStatus = sortByStatus;
                }
                gridsort = "  p.COLUMN20 = 1002 and ";
            }
            else if (frmID == 1277)
            {
                                    //EMPHCS774 rajasekhar reddy patakota 24/7/2015 Sales order status update - end to end life cycle
				info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "IV" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                //statusstring = sortByStatus.ToString();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                else if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN13 ";
                    SortByStatus = sortByStatus;

                }
                gridsort = " p.COLUMN19 = 1002 and ";
            }
            else if (frmID == 1278)
            {

                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "PM").Select(a => a.COLUMN04).ToList();
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN11 ";
                    SortByStatus = sortByStatus;
                }
                gridsort = "  p.COLUMN18 = 1002 and ";
            }
            else if (frmID == 1283)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "JOO" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL" || Convert.ToString(sortByStatus) == "OPEN")
                {
                    sortByStatus = null;
                }
                else if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus;
                }

                gridsort = "  p.COLUMN29 = 1000 and ";
            }
            else if (frmID == 1284)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "JIO" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL" || Convert.ToString(sortByStatus) == "OPEN")
                {
                    sortByStatus = null;
                }
                else if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN14 ";
                    SortByStatus = sortByStatus;
                }

                gridsort = "  p.COLUMN20 = 1000 and ";
            }
            else if (frmID == 1286)
            {
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN18 ";
                    SortByStatus = sortByStatus;
                }
                info.statusdata = new List<string> { };
                gridsort = "  p.COLUMN17 = 1000 and ";
            }
			//EMPHCS1784	Job Order for IN Process 15/7/2016
            else if (frmID == 1588)
            {
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN14 ";
                    SortByStatus = sortByStatus;
                }
                info.statusdata = new List<string> { };
                gridsort = "  p.COLUMN20 = 1000 and ";
            }
            else if (frmID == 1287)
            {
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN13 ";
                    SortByStatus = sortByStatus;
                }
                info.statusdata = new List<string> { };
                gridsort = "  p.COLUMN19 = 1000 and ";
            }
            else if (frmID == 1288)
            {
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN11 ";
                    SortByStatus = sortByStatus;
                }
                info.statusdata = new List<string> { };
                gridsort = "  p.COLUMN18 = 1000 and ";
            }
            else if (frmID == 1353)
            {
			//EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "PR" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus;
                }
				//EMPHCS1012 rajasekhar reddy patakota 25/08/2015 Customisation of Forms
                gridsort = "p.COLUMN29 = 1003 and  (p.COLUMN03=1353 or p.COLUMN03 in(" + customfrmID + "))  and ";
            }
            else if (frmID == 1354)
            {
			//EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "RI" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus;
                }
				//EMPHCS1012 rajasekhar reddy patakota 25/08/2015 Customisation of Forms
                gridsort = "p.COLUMN20 = 1003 and   (p.COLUMN03=1354 or p.COLUMN03 in(" + customfrmID + "))   and ";
            }
            else if (frmID == 1355)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "RF").Select(a => a.COLUMN04).ToList();
				//EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus;
                }
				//EMPHCS1012 rajasekhar reddy patakota 25/08/2015 Customisation of Forms
                gridsort = "p.COLUMN29 = 1003 and   (p.COLUMN03=1355 or p.COLUMN03 in(" + customfrmID + "))   and ";
            }
            else if (frmID == 1285)
            {
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN05 ";
                    SortByStatus = sortByStatus;
                }
                info.statusdata = new List<string> { };
                gridsort = "  p.COLUMN05 = 'ITTY008' and ";
            }
            else if (frmID == 1261)
            {
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN05 ";
                    SortByStatus = sortByStatus;
                }
                //gridsort = "  p.COLUMN05 != 'ITTY008' and ";
                info.statusdata = new List<string> { };
                SqlDataAdapter cmdd2 = new SqlDataAdapter("select COLUMN02, COLUMN04 from MATABLE008", cn);
                DataTable dtdata2 = new DataTable();
                cmdd2.Fill(dtdata2);
                for (int dd = 0; dd < dtdata2.Rows.Count; dd++)
                {
                    info.Country.Add(new SelectListItem { Value = dtdata2.Rows[dd]["COLUMN02"].ToString(), Text = dtdata2.Rows[dd]["COLUMN04"].ToString() });
                }
                //ViewData["ItemType"] = new SelectList(info.Country, "Value", "Text");
                //ViewBag.ItemType = new SelectList(info.Country, "Value", "Text");
            }
			//EMPHCS674 Adding Account Type Filter for Chart of Accounts done by srinivas 7/17/2015
            else if (frmID == 1250)
            {
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN07 ";
                    SortByStatus = sortByStatus;
                }
                //gridsort = "  p.COLUMN05 != 'ITTY008' and ";
                info.statusdata = new List<string> { };
                SqlDataAdapter cmdd2 = new SqlDataAdapter("select COLUMN02, COLUMN04 from MATABLE002 where column03=11115 and columna02 is null and columna03 is null", cn);
                DataTable dtdata2 = new DataTable();
                cmdd2.Fill(dtdata2);
                for (int dd = 0; dd < dtdata2.Rows.Count; dd++)
                {
                    info.Country.Add(new SelectListItem { Value = dtdata2.Rows[dd]["COLUMN02"].ToString(), Text = dtdata2.Rows[dd]["COLUMN04"].ToString() });
                }
                //ViewData["ItemType"] = new SelectList(info.Country, "Value", "Text");
                //ViewBag.ItemType = new SelectList(info.Country, "Value", "Text");
            }
            else
            {
                info.statusdata = new List<string> { };
            }


            if (ViewSort != null)
            {
                viewSort = ViewSort.ToString();

            }
            if (sortByStatus != null)
            {
			//EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
                if (frmID == 1582)
                    strQry = strQry + "AND  " + OPUnit + " AND (P.COLUMNA03='" + AcOwner + "' or P.COLUMNA03 is null) AND " + grdcol + "='" + sortByStatus + "' ";
                	// oppurtinity Filter Changes By Venkat 
				else if (frmID == 1375)
                    strQry = strQry + "AND  " + OPUnit + " AND (P.COLUMNA03='" + AcOwner + "' or P.COLUMNA03 is null) AND " + grdcol + "='" + sortByStatus + "' ";
                else
                    strQry = strQry + "AND  " + OPUnit + " AND P.COLUMNA03='" + AcOwner + "' AND " + grdcol + "='" + sortByStatus + "' ";
            }
            else {
                if (itm != "" || UPC != "" || brand != "" || group != "" || family != "")
                 {  //EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
				    if (frmID == 1582)
                        strQry = strQry + " AND " + OPUnit + " AND (P.COLUMNA03='" + AcOwner + "' or P.COLUMNA03 is null)";
                    //MIS Changes
                    if (frmID == 1404)
                        strQry = strQry + "AND p.COLUMN02 != '' AND  (CASE WHEN @OP!='' or @OP is null THEN p.COLUMNA08 ELSE 1 END)=(CASE WHEN @OP!='' or @OP is null THEN " + System.Web.HttpContext.Current.Session["eid"] + " ELSE 1 END) AND " + OPUnit + " AND P.COLUMNA03=" + AcOwner + "";
                    else
                        strQry = strQry + " AND " + OPUnit + " AND P.COLUMNA03=" + AcOwner + "";
				 }
                else
                {//EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
                    if (frmID == 1582)
                        strQry = strQry + "AND p.COLUMN02 != ''   AND (" + OPUnit + " or P.COLUMNA02 is null) AND (P.COLUMNA03='" + AcOwner + "' or P.COLUMNA03 is null)";
                    if (frmID == 1404)
                        strQry = strQry + "AND p.COLUMN02 != '' AND  (CASE WHEN @OP!='' or @OP is null THEN p.COLUMNA08 ELSE 1 END)=(CASE WHEN @OP!='' or @OP is null THEN " + System.Web.HttpContext.Current.Session["eid"] + " ELSE 1 END) AND " + OPUnit + " AND P.COLUMNA03=" + AcOwner + "";
                    else
                        strQry = strQry + "AND p.COLUMN02 != ''   AND " + OPUnit + " AND P.COLUMNA03='" + AcOwner + "'";
                }
            }
			//EMPHCS674 Adding Account Type Filter for Chart of Accounts done by srinivas 7/17/2015
            if (itemType != null )
            {
                if (frmID == 1261)
                {
                    var itemtype = itemType;
                    strQry = strQry + "AND " + gridsort + " p.COLUMN05='" + itemtype + "'";
                }
                else if (frmID == 1250)
                {
                    var itemtype = itemType;
                    strQry = strQry + "AND  p.COLUMN07='" + itemtype + "'";
                }
            }
            else { itemType = null; }
            if (sortByStatus == null)
            {
                if (frmID == 1273)
                {
                    //strQry = strQry + "AND   p.COLUMN13 != 'FULLY PAID' ";
                }
                else if (frmID == 1274)
                {
                    //strQry = strQry + "AND p.COLUMN11 != 'FULLY PAID' ";
                }
                else if (frmID == 1277)
                {
                    //strQry = strQry + "AND p.COLUMN13 != 'AMOUNT FULLY RECEIVED' ";
                }
                else if (frmID == 1278)
                {
                    //strQry = strQry + "AND p.COLUMN11 != 'AMOUNT FULLY RECEIVED' ";
                }
                else if (frmID == 1349)
                {
					//EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
                    strQry = strQry + "AND p.COLUMN06='WITHDRAW'  ";
                }
                else if (frmID == 1350)
                {
                    strQry = strQry + "AND p.COLUMN06='DEPOSIT'  ";
                }
                else if (frmID == 1351)
                {
                    //EMPHCS1198	MONEY TRANSFER Screen not displaying saved transactions BY RAJ.Jr 23/9/2015
                    strQry = strQry + "AND p.COLUMN06='TRANSFER'  ";
                }
            }
            //if (Session["Active"] == "true")
            //{


            //    // strQry = strQry + "WHERE " + grdcol + "= '" + SortByStatus + "'"; 


            //    if (viewSort.Equals("Newly Created"))
            //    {
            //        strQry = strQry + " AND " + gridsort + " p.COLUMNA13=1 OR p.COLUMNA13=0 ORDER BY p.COLUMNA06 desc ";
            //    }
            //    else if (viewSort.Equals("Newly Modified"))
            //    {
            //        strQry = strQry + " AND " + gridsort + " p.COLUMNA13=1 OR p.COLUMNA13=0 ORDER BY p.COLUMNA07 desc ";
            //    }
            //    else
            //    {
            //        strQry = strQry + " AND " + gridsort + " p.COLUMNA13=1 OR p.COLUMNA13=0";
            //    }
            //}
            //else
            //{
            if (viewSort.Equals("Newly Created"))
            {
                if (frmID == 1414)
                {
                    //EMPHCS1247 rajasekhar reddy patakota 01/10/2015 Journal Entry Calculation Changes and also edit and delete
                    //EMPHCS1322	Add Account,Credit,Debit,Memo in Journal Entry info display BY Raj.Jr 05/11/2015
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.[COLUMN02],p.[COLUMN04],p.[COLUMN09] ,p.[COLUMN07],o.[COLUMN04],p.COLUMNA06,f.COLUMN04,fj.COLUMN04,fj.COLUMN05,fj.COLUMN06,fj.COLUMN07,ma.COLUMN09 ORDER BY p.COLUMNA06 desc ";
                }
                //Order Selection Screen
                else if (frmID == 1523)
                {
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,p.COLUMN06,s.COLUMN05,p.COLUMN08,p.COLUMN16,SA3.COLUMN10 ORDER BY p.COLUMN06 desc ";
                }
				//EMPHCS1727 : Grid Info Page to be display according to created date new to old. Error created date updated with modified date by GNANESHWAR ON 6/5/2016
	            else if (frmID == 1293)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.column02,p.column17,p.column09,o.column03,pr.column05,m.column06,p.column24,p.column07,p.COLUMNA06 ORDER BY p.COLUMNA06 desc ";
                //EMPHCS1549	Info screen Headings changes By RAJ.Jr
                else if (frmID == 1273)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,s.COLUMN05,f.COLUMN04,p.COLUMN09,p.COLUMN12,p.COLUMN14,p.COLUMN11,p.COLUMN13,ma.COLUMN09,p.COLUMNA06 ORDER BY p.COLUMNA06 desc ";
                else if (frmID == 1277)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,s.COLUMN05,f.COLUMN04,p.COLUMN08,p.COLUMN09,p.COLUMN12,p.COLUMN13,o.COLUMN03,d.COLUMN04,p.COLUMN20,ma.COLUMN09,p.COLUMN10,p.COLUMNA06 ORDER BY p.COLUMNA06 desc ";
                else if (frmID == 1276)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,p.COLUMN08,s.COLUMN05,f.COLUMN04,p.COLUMN09,p.COLUMN13,p.COLUMN14,ma.COLUMN09,p.COLUMNA06 ORDER BY p.COLUMNA06 desc ";
                else if (frmID == 1329)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,p.COLUMN09,s.COLUMN05,f.COLUMN04,p.COLUMN10,p.COLUMN12,p.COLUMN18,ma.COLUMN09,p.COLUMNA06 ORDER BY p.COLUMNA06 desc ";
                else if (frmID == 1272)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,p.COLUMN09,s.COLUMN05,f.COLUMN04,p.COLUMN10,p.COLUMN12,p.COLUMN18,ma.COLUMN09,p.COLUMNA06 ORDER BY p.COLUMNA06 desc ";
                else if (frmID == 1354)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,p.COLUMN08,s.COLUMN05,f.COLUMN04,p.COLUMN09,p.COLUMN13,p.COLUMN14,ma.COLUMN09,p.COLUMNA06 ORDER BY p.COLUMNA06 desc ";
                else if (frmID == 1349)
                    strQry = strQry + " AND " + gridsort + " isnull(p.COLUMNA13,0)=0  group by p.column02,p.column05,p.column04,o.column04,p.column11,p.column09,p.columna06 ORDER BY convert(datetime, p.COLUMN04, 101) desc, cast(p.COLUMN02 as int) desc";
                else if (frmID == 1350)
                    strQry = strQry + " AND " + gridsort + " isnull(p.COLUMNA13,0)=0  group by p.column02,p.column05,p.column04,o.column04,p.column11,p.column09,p.columna06 ORDER BY convert(datetime, p.COLUMN04, 101) desc, cast(p.COLUMN02 as int) desc";
                else if (frmID == 1351)
                    strQry = strQry + " AND " + gridsort + " isnull(p.COLUMNA13,0)=0  group by p.column02,p.column05,p.column04,o.column04,p.column11,p.column09,p.columna06 ORDER BY convert(datetime, p.COLUMN04, 101) desc, cast(p.COLUMN02 as int) desc";
                else
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 ORDER BY p.COLUMNA06 desc ";
            }
            else if (viewSort.Equals("Newly Modified"))
            {
                if (frmID == 1414)
                {
				//EMPHCS1247 rajasekhar reddy patakota 01/10/2015 Journal Entry Calculation Changes and also edit and delete
                    //EMPHCS1322	Add Account,Credit,Debit,Memo in Journal Entry info display BY Raj.Jr 05/11/2015
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.[COLUMN02],p.[COLUMN04],p.[COLUMN09] ,p.[COLUMN07],o.[COLUMN04],p.COLUMNA07,f.COLUMN04,fj.COLUMN04,fj.COLUMN05,fj.COLUMN06,fj.COLUMN07,ma.COLUMN09 ORDER BY p.COLUMNA07 desc ";
                }
                //Order Selection Screen
                else if (frmID == 1523)
                {
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,p.COLUMN06,s.COLUMN05,p.COLUMN08,p.COLUMN16,SA3.COLUMN10 ORDER BY p.COLUMN06 desc ";
                }
		//EMPHCS1727 : Grid Info Page to be display according to created date new to old. Error created date updated with modified date by GNANESHWAR ON 6/5/2016
                else if (frmID == 1293)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.column02,p.column17,p.column09,o.column03,pr.column05,m.column06,p.column24,p.column07,p.COLUMNA07 ORDER BY p.COLUMNA07 desc ";
                //EMPHCS1549	Info screen Headings changes By RAJ.Jr
                else if (frmID == 1273)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,s.COLUMN05,f.COLUMN04,p.COLUMN09,p.COLUMN12,p.COLUMN14,p.COLUMN11,p.COLUMN13,ma.COLUMN09,p.COLUMNA07 ORDER BY p.COLUMNA07 desc ";
                else if (frmID == 1277)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,s.COLUMN05,f.COLUMN04,p.COLUMN08,p.COLUMN09,p.COLUMN12,p.COLUMN13,o.COLUMN03,d.COLUMN04,p.COLUMN20,ma.COLUMN09,p.COLUMN10,p.COLUMNA07 ORDER BY p.COLUMNA07 desc ";
                else if (frmID == 1276)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,p.COLUMN08,s.COLUMN05,f.COLUMN04,p.COLUMN09,p.COLUMN13,p.COLUMN14,ma.COLUMN09,p.COLUMNA07 ORDER BY p.COLUMNA07 desc ";
                else if (frmID == 1329)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,p.COLUMN09,s.COLUMN05,f.COLUMN04,p.COLUMN10,p.COLUMN12,p.COLUMN18,ma.COLUMN09,p.COLUMNA07 ORDER BY p.COLUMNA07 desc ";
                else if (frmID == 1272)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,p.COLUMN09,s.COLUMN05,f.COLUMN04,p.COLUMN10,p.COLUMN12,p.COLUMN18,ma.COLUMN09,p.COLUMNA07 ORDER BY p.COLUMNA07 desc ";
                else if (frmID == 1354)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,p.COLUMN08,s.COLUMN05,f.COLUMN04,p.COLUMN09,p.COLUMN13,p.COLUMN14,ma.COLUMN09,p.COLUMNA07 ORDER BY p.COLUMNA07 desc ";
                else if (frmID == 1349)
                    strQry = strQry + " AND " + gridsort + " isnull(p.COLUMNA13,0)=0  group by p.column02,p.column05,p.column04,o.column04,p.column11,p.column09,p.columna06 ORDER BY convert(datetime, p.COLUMN04, 101) desc, cast(p.COLUMN02 as int) desc";
                else if (frmID == 1350)
                    strQry = strQry + " AND " + gridsort + " isnull(p.COLUMNA13,0)=0  group by p.column02,p.column05,p.column04,o.column04,p.column11,p.column09,p.columna06 ORDER BY convert(datetime, p.COLUMN04, 101) desc, cast(p.COLUMN02 as int) desc";
                else if (frmID == 1351)
                    strQry = strQry + " AND " + gridsort + " isnull(p.COLUMNA13,0)=0  group by p.column02,p.column05,p.column04,o.column04,p.column11,p.column09,p.columna06 ORDER BY convert(datetime, p.COLUMN04, 101) desc, cast(p.COLUMN02 as int) desc";
                else
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 ORDER BY p.COLUMNA07 desc ";
            }
            else
            {
                if (frmID == 1414)
                {
				//EMPHCS1247 rajasekhar reddy patakota 01/10/2015 Journal Entry Calculation Changes and also edit and delete
                    //EMPHCS1322	Add Account,Credit,Debit,Memo in Journal Entry info display BY Raj.Jr 05/11/2015
                    //EMPHCS1764	Journal Entry should have Payee Type in Item Level not in Header level By RAJ.Jr
					strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.[COLUMN02],p.[COLUMN04],p.[COLUMN09] ,p.[COLUMN07],o.[COLUMN04],p.COLUMNA06,f.COLUMN04,fj.COLUMN04,fj.COLUMN05,fj.COLUMN06,fj.COLUMN07,ma.COLUMN09,fj.COLUMN14 ORDER BY p.COLUMNA06 desc ";
                }
                //Order Selection Screen
                else if (frmID == 1523)
                {

                    if (itm != "" || UPC != ""||brand!=""||group!=""||family!="")
                    {
                        strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,p.COLUMN06,s.COLUMN05,p.COLUMN08,p.COLUMN16,SA3.COLUMN10" +
                        ",SA6.COLUMN27,SA6.COLUMN07,m.COLUMN04 " +
             ") x" +
            " pivot " +
             "(" +
             "   sum(qty)" +
              "  for UOM in(' + @cols + ')" +
             ") p Order by p.ID desc' " +

             "execute(@query)";
                    }
                    else
                        strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,p.COLUMN06,s.COLUMN05,p.COLUMN08,p.COLUMN16,SA3.COLUMN10 ORDER BY p.COLUMN06 desc ";
                }
                //EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
                else if(frmID == 1293)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.column02,p.column17,p.column09,o.column03,pr.column05,m.column06,p.column24,p.column07,p.COLUMNA06 ORDER BY p.COLUMNA06 desc ";
                //EMPHCS1549	Info screen Headings changes By RAJ.Jr
                else if(frmID == 1273)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,s.COLUMN05,f.COLUMN04,p.COLUMN09,p.COLUMN12,p.COLUMN14,p.COLUMN11,p.COLUMN13,ma.COLUMN09,p.COLUMNA06 ORDER BY p.COLUMNA06 desc ";
                else if(frmID == 1277)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,s.COLUMN05,f.COLUMN04,p.COLUMN08,p.COLUMN09,p.COLUMN12,p.COLUMN13,o.COLUMN03,d.COLUMN04,p.COLUMN20,ma.COLUMN09,p.COLUMN10,p.COLUMNA06 ORDER BY p.COLUMNA06 desc ";
                else if(frmID==1276)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,p.COLUMN08,s.COLUMN05,f.COLUMN04,p.COLUMN09,p.COLUMN13,p.COLUMN14,ma.COLUMN09,p.COLUMNA06 ORDER BY p.COLUMNA06 desc ";
                else if(frmID==1329)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,p.COLUMN09,s.COLUMN05,f.COLUMN04,p.COLUMN10,p.COLUMN12,p.COLUMN18,ma.COLUMN09,p.COLUMNA06 ORDER BY p.COLUMNA06 desc ";
                else if (frmID == 1272)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,p.COLUMN09,s.COLUMN05,f.COLUMN04,p.COLUMN10,p.COLUMN12,p.COLUMN18,ma.COLUMN09,p.COLUMNA06 ORDER BY p.COLUMNA06 desc ";
                else if (frmID == 1354)
                    strQry = strQry + " AND " + gridsort + " p.COLUMNA13=0 group by p.COLUMN02,p.COLUMN04,p.COLUMN08,s.COLUMN05,f.COLUMN04,p.COLUMN09,p.COLUMN13,p.COLUMN14,ma.COLUMN09,p.COLUMNA06 ORDER BY p.COLUMNA06 desc ";
                else if (frmID == 1250)
                    strQry = strQry + " AND " + gridsort + " isnull(p.COLUMNA13,0)=0  group by p.column02,p.column04,d.column04,p.column05,p.column07,p.columna06 ORDER BY p.COLUMNA06 desc ";
                else if (frmID == 1349)
                    strQry = strQry + " AND " + gridsort + " isnull(p.COLUMNA13,0)=0  group by p.column02,p.column05,p.column04,o.column04,p.column11,p.column09,p.columna06 ORDER BY convert(datetime, p.COLUMN04, 101) desc, cast(p.COLUMN02 as int) desc";
                else if (frmID == 1350)
                    strQry = strQry + " AND " + gridsort + " isnull(p.COLUMNA13,0)=0  group by p.column02,p.column05,p.column04,o.column04,p.column11,p.column09,p.columna06 ORDER BY convert(datetime, p.COLUMN04, 101) desc, cast(p.COLUMN02 as int) desc";
                else if (frmID == 1351)
                    strQry = strQry + " AND " + gridsort + " isnull(p.COLUMNA13,0)=0  group by p.column02,p.column05,p.column04,o.column04,p.column11,p.column09,p.columna06 ORDER BY convert(datetime, p.COLUMN04, 101) desc, cast(p.COLUMN02 as int) desc";
                else
                    strQry = strQry + " AND " + gridsort + " isnull(p.COLUMNA13,0)=0 ORDER BY p.COLUMNA06 desc ";
            }
            //}
            info.strQry = strQry;
            info.alCol = alCol;
            info.actCol = actCol;
            return info;

        }
        //Order Selection Screen
		//EMPHCS1592 rajasekhar reddy patakota 08/03/2016 Custom View Changes for Sales Order
        public FormBuildingEntities GetExport(int frmID, object viewStyle, object sortByStatus, object itemType, SqlConnection cn, object Cust, object itm, object family, object group, object brand, object state, object city, object dtFrm, object dtTo, object opu, object AcOwner, object UPC)
        {
            string dateF = System.Web.HttpContext.Current.Session["Format"].ToString();
            string CLOS = "";
            var status = ""; string statuslist = null;
            DateTime start = Convert.ToDateTime("1/1/2010"), end = Convert.ToDateTime("1/1/2100");
            SqlCommand cmd = new SqlCommand("INFO_DATE", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@AcOwner", AcOwner);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                start = Convert.ToDateTime(dt.Rows[0]["COLUMN04"]);
                end = Convert.ToDateTime(dt.Rows[0]["COLUMN05"]);
            }
            if (frmID == 1251)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                    var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "09" || a.COLUMN02 == "41" || a.COLUMN02 == "10").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = "AND P.COLUMN16 not IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1272)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                    var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "83").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = "AND P.COLUMN18 not IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1273)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                    var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "12").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = "AND P.COLUMN13 not IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1274)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                    CLOS = "";
                }
                else
                {
                    CLOS = "";
                }
            }



            else if (frmID == 1275)
            {
                if (Convert.ToString(sortByStatus) == "")
                {
                    var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "30" || a.COLUMN02 == "26" || a.COLUMN02 == "43").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = "AND P.COLUMN16 not IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1276)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                    var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "81").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = "AND P.COLUMN14 not IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1277)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                    var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "34").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = "AND P.COLUMN13 not IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }
				// oppurtinity Filter Changes By Venkat 
            else if (frmID == 1375)
            {
                if ((Convert.ToString(sortByStatus) == "") || (Convert.ToString(sortByStatus) == "OPEN"))
                {

                    var r = dc.CONTABLE025.Where(a => a.COLUMN03 == "OP" && (a.COLUMN02 == "122" || a.COLUMN02 == "126" || a.COLUMN02 == "125")).ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = " AND so.COLUMN04 IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1278)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                    CLOS = "";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1328)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                    var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "104" || a.COLUMN02 == "105" || a.COLUMN02 == "106").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = "AND P.COLUMN16 not IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1329)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                    var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "109").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = "AND P.COLUMN18 not IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1353)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                    var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "98" || a.COLUMN02 == "99" || a.COLUMN02 == "100").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = "AND P.COLUMN16 not IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1354)
            {
                if (Convert.ToString(sortByStatus) == (""))
                {
                    var r = dc.CONTABLE025.Where(a => a.COLUMN02 == "112").ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = "AND P.COLUMN14 not IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }
				// JOB ORDER OUT FILTERS BY VENKAT
            else if (frmID == 1283)
            {
                if ((Convert.ToString(sortByStatus) == "") || (Convert.ToString(sortByStatus) == "OPEN"))
                {

                    var r = dc.CONTABLE025.Where(a => a.COLUMN03 == "JOO" && (a.COLUMN02 == "134" || a.COLUMN02 == "135" || a.COLUMN02 == "137" || a.COLUMN02 == "133")).ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = " AND p.COLUMN16 IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }
            else if (frmID == 1284)
            {
                if ((Convert.ToString(sortByStatus) == "OPEN"))
                {

                    var r = dc.CONTABLE025.Where(a => a.COLUMN03 == "JIO" && (a.COLUMN02 == "141")).ToList();
                    for (int i = 0; i < r.Count; i++) { status = r[i].COLUMN04; statuslist += "'" + status + "',"; }
                    statuslist = statuslist.TrimEnd(',');
                    CLOS = " AND p.COLUMN14 IN (" + statuslist + ")";
                }
                else
                {
                    CLOS = "";
                }
            }
            FormBuildingEntities info = new FormBuildingEntities();
            eBizSuiteTableEntities db = new eBizSuiteTableEntities();
            List<CONTABLE013> con13 = new List<CONTABLE013>();
            var flds = db.CONTABLE006.Where(q => q.COLUMN03 == frmID).ToList();
            var tblID = flds.Select(q => q.COLUMN04).FirstOrDefault();
            var tblName = db.CONTABLE004.Where(q => q.COLUMN02 == tblID);
            var table = tblName.Select(q => q.COLUMN04).First();

            string strQry = null;
            string ob = null;
            string alCol = null;
			//EMPHCS1592 rajasekhar reddy patakota 08/03/2016 Custom View Changes for Sales Order
            var fldsV = con13;
            if (viewStyle == "Default" || viewStyle == null || viewStyle == "")
            {
                flds = db.CONTABLE006.Where(q => q.COLUMN03 == frmID).ToList();
                tblID = Convert.ToInt32(flds.Select(q => q.COLUMN04).FirstOrDefault());
                flds = db.CONTABLE006.Where(q => q.COLUMN04 == tblID && q.COLUMN03 == frmID).ToList();

            }
            if (viewStyle != "Default" && viewStyle != null && viewStyle != "")
            {
                var vName = viewStyle;
                fldsV = db.CONTABLE013.Where(q => q.COLUMN05 == frmID & q.COLUMN03 == vName).ToList();
                tblID = Convert.ToInt32(fldsV.Select(q => q.COLUMN04).FirstOrDefault());
            }
            if (fldsV.Count == 0)
            {

                //po
                if (frmID == 1251)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    //EMPHCS858 rajasekhar reddy patalota 15/8/2015 Purchase order list info - Receipt type internal id is getting displayed. It should display the name of receipt type
                    strQry = "SELECT p.[COLUMN04 ] AS [Purchase Order#],FORMAT(p.[COLUMN06 ],'" + dateF + "') AS [Date],s.[COLUMN05 ] AS [Vendor],FORMAT(p.[COLUMN08 ],'" + dateF + "') AS [Ship Date],p.COLUMN11 Reference#,p.COLUMN09 Memo,p.[COLUMN15 ] AS [Total Amount],p.[COLUMN16 ] AS [Status],ma.COLUMN09 [Created By] From PUTABLE001 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 inner join  MATABLE011 m on m.COLUMN02=p.COLUMN29 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column06 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 ";
                    //" left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN24 " +
                    //" left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN27 ";
                    alCol = "Purchase Order#, Date, Vendor, Ship Date, Reference#, Memo, Total Amount, Status, Created By";
                    ob = "p.COLUMN04, p.COLUMN06, s.COLUMN05, p.COLUMN08, p.COLUMN11, p.COLUMN09, p.COLUMN15, p.COLUMN16, ma.COLUMN09";
                }
                //EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
				//Price Level
                else if (frmID == 1582)
                {
					//EMPHCS1857 rajasekhar reddy patakota 03/02/2017 Discount setup in invoice from Price level
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Price Level],m.COLUMN04 [Type],p.[COLUMN06 ] AS Discount,p.[COLUMN11] AS [Fixed Amount],ma.COLUMN09 [Created By] From MATABLE023 p left outer join  MATABLE002 m on m.COLUMN02=p.COLUMN05 and (m.COLUMNA03=p.COLUMNA03 or m.COLUMNA03 is null)  left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where isnull(p.COLUMNA13,0)=0" + CLOS + " ";
                    alCol= "ID,Price Level,Type,Discount,Fixed Amount,Created By";
                    ob = "p.COLUMN02,p.COLUMN04,m.COLUMN04,p.COLUMN06,p.COLUMN11,ma.COLUMN09";
                }
                //Journal Ledger
                else if (frmID == 1414)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    //EMPHCS1322	Add Account,Credit,Debit,Memo in Journal Entry info display By RAJ.Jr 05/11/2015
                   //EMPHCS1764	Journal Entry should have Payee Type in Item Level not in Header level By RAJ.Jr
                    strQry = "SELECT (p.[COLUMN04]) AS [Journal Entry#],FORMAT(p.[COLUMN09 ],'" + dateF + "') AS [Date],(case when p.column07='22305' or fj.column14='22305' then (select cOLUMN05 from sATABLe001 where COLUMN02=fj.COLUMN07) when p.column07='22334' or fj.column14='22334' then (select cOLUMN05 from sATABLe001 where COLUMN02=fj.COLUMN07) when p.column07='22335' or fj.column14='22335' then (select cOLUMN05 from sATABLe002 where COLUMN02=fj.COLUMN07)	when p.column07='22306' or fj.column14='22306' then (select COLUMN09 from MATABLE010 where COLUMN02=fj.COLUMN07) when p.column07='22492' or fj.column14='22492' then (select cOLUMN09 from MATABLE010 where COLUMN02=fj.COLUMN07) else NULL end) Party,fj.COLUMN06 as Memo,f.COLUMN04 as Account,fj.COLUMN04 as Debit,fj.COLUMN05 as Credit,ma.[COLUMN09 ] AS [Created By] From FITABLE031 p  left  join  CONTABLE007 o on o.COLUMN02=p.COLUMN10 left outer join FITABLE032 fj on fj.COLUMN12=p.COLUMN01 and isnull(fj.COLUMNA13,0)=0 left outer join FITABLE001 f on f.COLUMN02=fj.COLUMN03 and f.COLUMNA03=p.columna03 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null)  where p.column09 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' ";
                    alCol = "Journal Entry#, Date, Party, Memo, Account, Debit, Credit, Created By";
                    ob = "p.COLUMN04, p.COLUMN09, p.COLUMN07, fj.COLUMN06, f.COLUMN04, fj.COLUMN04, fj.COLUMN05, ma.COLUMN09";
                }
                //project
                else if (frmID == 1378)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Project#],P.[COLUMN05 ] AS [Name],s.[COLUMN05] AS [Customer],P.[COLUMN08 ] AS [Estimated Cost],p.[COLUMN09 ] AS [Actual Cost],m.[COLUMN04 ] AS [Status],d.[COLUMN04 ] AS [Deparment],o.[COLUMN04 ] AS [Operating Unit] From PRTABLE001 p inner join SATABLE002 s on s.COLUMN02=p.COLUMN06 inner join MATABLE002 m on m.COLUMN02=p.COLUMN12  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN19  left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN18 where isnull(p.COLUMNA13,0)=0 ";
                    alCol = "ID,Project#,Name,Customer,Estimated Cost,Actual Cost,Status,Deparment,Operating Unit";
                    ob = "p.COLUMN02,p.COLUMN04,p.COLUMN05,s.COLUMN05,p.COLUMN08,p.COLUMN09,m.COLUMN04,d.COLUMN04,o.COLUMN04";
                }

                //Journal Ledger
                //else if (frmID == 1414)
                //{
                //    strQry = "SELECT p.[COLUMN02] AS ID,p.[COLUMN04] AS [Entry#],convert(varchar,p.[COLUMN09],106) AS [Date],(case when p.column07='22305' or p.column07='22334' then (select column05 from satable001 where column02=s.column07)	when p.column07='22335' then (select column05 from satable002 where column02=s.column07)	when p.column07='22306' then (select column09 from matable010 where column02=s.column07)	else NULL end) Name,o.[COLUMN04 ] AS [Operating Unit] From FITABLE031 p left outer join FITABLE032 s on s.COLUMN12=p.COLUMN01 left outer join MATABLE002 m on m.COLUMN02=s.COLUMN07  left outer join SATABLE002 c on c.COLUMN02=s.COLUMN07 left outer join SATABLE001 v on v.COLUMN02=s.COLUMN07  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN10  where p.COLUMNA13='False'  ";
                //    alCol = "ID,Entry#,Date,Name,Operating Unit";
                //    ob = "p.COLUMN02,p.COLUMN04,p.COLUMN09,s.COLUMN07,o.COLUMN04";
                //}
                //Daily Visit Report
                //else if (frmID == 1404)
                //{
                //    strQry = "SELECT COLUMN15  AS FieldID,COLUMN04 as FieldName,COLUMN05 as EmployeeName,COLUMN06 as Date,COLUMN07 as Station,COLUMN08 as CustomerName,COLUMN09 as ContactPerson,COLUMN10 as ContactNumber from MATABLE019";
                //    alCol=  "ID, Project#, Name, Customer, Estimated Cost, Actual Cost, Status, Deparment, Operating Unit";
                //    ob = "p.COLUMN02, p.COLUMN04, p.COLUMN05, s.COLUMN05, p.COLUMN08, p.COLUMN09, m.COLUMN04, d.COLUMN04, o.COLUMN04";
                //}

                    //EMPHC MIS VENKAT
                // EMPHSC MIS COMMUTE By venkat
                else if (frmID == 1404)
                {
                    strQry = "DECLARE @OP nvarchar(250) "+
" Set @OP = (Select isnull(COLUMN26,0) from MATABLE010 where COLUMN02 = "+System.Web.HttpContext.Current.Session["eid"]+")"+
                        //EMPHCS1811 rajasekhar reddy patakota 10/09/2016 Line Grid Details filling in sales & Purchase module
                    " Select p.COLUMN02 ID,p.COLUMN15 as Lead#,FORMAT(p.[COLUMN05],'" + dateF + "') as Date,p.COLUMN06 as [Company],p.COLUMN07 as [Contact Person],p.COLUMN16 Phone#,mc.COLUMN04 as Category,mm.COLUMN04 as Milestone,FORMAT(p.[COLUMN27],'" + dateF + "') as [Next Follow Up],ms.COLUMN04 as Status,m1.COLUMN09 as [Created By] From MATABLE019 p left join MATABLE002 mc on mc.COLUMN02=p.COLUMN10 and isnull(mc.COLUMNA13,0)=0 left join MATABLE002 mm on mm.COLUMN02=p.COLUMN11 and isnull(mm.COLUMNA13,0)=0 left join MATABLE002 ms on ms.COLUMN02=p.COLUMN12 and isnull(ms.COLUMNA13,0)=0 LEFT OUTER JOIN MATABLE010 m1 on m1.COLUMN02 = p.COLUMNa08 and (m1.COLUMN30='True' or m1.COLUMN30='1') where p.COLUMNA13='False' ";
                    alCol = "ID,Lead#,Date,Company,Contact Person,Phone#,Category,Milestone,Next Follow Up,Status,Created By";
                    ob = " p.COLUMN02,p.COLUMN15,p.COLUMN05,p.COLUMN06,p.COLUMN07,p.COLUMN16,mc.COLUMN04,mm.COLUMN04,p.COLUMN27,ms.COLUMN04,m1.COLUMN09";
                }

                //Stock Return
                else if (frmID == 1379)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = "SELECT p.[COLUMN04 ] AS [Transfer#],FORMAT(p.[COLUMN08],'" + dateF + "') AS [Date],'' Reference#,p.COLUMN07 Memo,d.[COLUMN03 ] AS [Source Operating Unit],o.[COLUMN03 ] AS [Destination Operating Unit],'' Status,ma.COLUMN09 [Created By] From PRTABLE003 p  left outer join  CONTABLE007 d on d.COLUMN02=p.COLUMN05   left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN06 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column08 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 ";
                    alCol = "Transfer#, Date, Reference#, Memo, Source Operating Unit, Destination Operating Unit, Status, Created By";
                    ob = "p.COLUMN04, p.COLUMN08, p.COLUMN07, p.COLUMN07, d.COLUMN03, o.COLUMN03, p.COLUMN07, ma.COLUMN09";
                }
                //Resource Consumption
                else if (frmID == 1381)
                {
                    strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Resourse Consumption#],w.[COLUMN04 ]  [Work Order#],e.COLUMN04 [Order Type], s.[COLUMN05 ]  Customer,FORMAT(p.[COLUMN09],'" + dateF + "')  [Date],  po.[COLUMN05 ]  [Project Name]," +
                              " ms.[COLUMN04] AS [MileStone],o.[COLUMN03] AS [Operating Unit] From PRTABLE007 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN06 left outer join  PRTABLE001 po on po.COLUMN02=p.COLUMN08 left outer join " +
                              " CONTABLE007 o on o.COLUMN02=p.COLUMN11  left outer join  PRTABLE002 ms on ms.COLUMN02=p.COLUMN09 left outer join  MATABLE009 e on e.COLUMN02=p.COLUMN07   left outer join  PRTABLE005 w on w.COLUMN02=p.COLUMN05 where p.column08 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 ";
                    alCol = "ID,Resourse Consumption#,Work Order#,Order Type,Customer,Date,Project Name,MileStone,Operating Unit";
                    ob = "p.COLUMN02,p.COLUMN04,w.COLUMN05,e.COLUMN04,s.COLUMN05,p.COLUMN09,po.COLUMN05,ms.COLUMN04,o.COLUMN03";
                }
                //EMPHCS1795	GRN Screen to Job Order IN By Raj.Jr
                else if (frmID == 1605)
                {
                    strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [GRN#], FORMAT(p.[COLUMN06 ],'" + dateF + "') [Date],s.[COLUMN05 ]  Customer,  o.[COLUMN03] AS [Operating Unit] From PUTABLE022 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN09 where p.column06 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' ";
                    alCol="ID,GRN#, Date, Customer, Operating Unit";
                    ob= "p.COLUMN02, p.COLUMN04, p.COLUMN06, s.COLUMN05, o.COLUMN03" ;
                }
				//EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
                else if (frmID == 1619)
                {
                    strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [SMTP Port],p.[COLUMN05]  [SMTP Server],p.[COLUMN06]  [POP3 Port],p.[COLUMN07]  [POP3 Server],p.[COLUMN09]  [Default] From SETABLE016 p where p.COLUMNA13='False' and p.COLUMN03=1619";
                    alCol="ID,SMTP Port,SMTP Server,POP3 Port,POP3 Server,Default";
                    ob= "p.COLUMN02,p.COLUMN04,p.COLUMN05,p.COLUMN06,p.COLUMN07,p.COLUMN09";
                }
                //Project Work Order
                else if (frmID == 1380)
                {
                    strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [WorkOrder#],e.COLUMN04 [Order Type], s.[COLUMN05 ]  Customer," +
                                    "FORMAT(p.[COLUMN07 ],'" + dateF + "')  [Date],  po.[COLUMN05 ]  [Project],o.[COLUMN03] AS [Operating Unit] " +
                                    "From PRTABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 left outer join  PRTABLE001 po on po.COLUMN02=p.COLUMN08 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN10  left outer join  PRTABLE002 ms on ms.COLUMN02=p.COLUMN09 left outer join  MATABLE009 e on e.COLUMN02=p.COLUMN06 where p.column07 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 ";
                    alCol = "ID,WorkOrder#,Order Type,Customer,Date,Project,Operating Unit";
                    ob = "p.COLUMN02,p.COLUMN04,e.COLUMN04,s.COLUMN05,p.COLUMN07,po.COLUMN05, o.COLUMN03";
                }
                //Purchase Order Quotation
                else if (frmID == 1374)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Purchase Quote#],P.[COLUMN29 ] AS [Receipt Type],s.[COLUMN05 ] AS [Vendor],P.[COLUMN07 ] AS [Manager Approval],FORMAT(p.[COLUMN06 ],'" + dateF + "') AS [Date],FORMAT(p.[COLUMN08],'" + dateF + "') AS [Ship Date],p.[COLUMN32 ] AS [Tax Amount],p.[COLUMN15 ] AS [Total Amount],p.[COLUMN16 ] AS [Order Status] From PUTABLE001 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 where p.column06 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 ";
                    //" left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN24 " +
                    //" left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN27 ";
                    alCol = "ID,Purchase Quote#,Date,Receipt Type,Vendor,Manager Approval,Ship Date,Tax Amount,Total Amount,Order Status";
                    ob = "p.COLUMN02,p.COLUMN04,p.COLUMN06,p.COLUMN29,s.COLUMN05,p.COLUMN07,p.COLUMN08,p.COLUMN32,p.COLUMN15, p.COLUMN16";
                }
				//EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
                //Opportinuity
                else if (frmID == 1375)
                {
                    ////EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
                    strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Opportinuity#],FORMAT(p.[COLUMN07],'" + dateF + "')  [Date], s.[COLUMN05 ]  Customer, ms.COLUMN04  [Mile Stone], sr.COLUMN09  [Sales Rep],FORMAT(p.COLUMN08,'" + dateF + "') [Expected Close Date],FORMAT(p.COLUMN30,'" + dateF + "') [Next Followup]," +
"+so.[COLUMN04 ] AS [Status] From SATABLE013 p left outer join MATABLE010 sr On sr.COLUMN02= p.COLUMN15 AND sr.COLUMNA03=p.COLUMNA03 AND ISNULL(sr.COLUMNA13,0)=0 left join MATABLE002 ms on ms.COLUMN02=p.COLUMN23 AND ISNULL(ms.COLUMNA13,0)=0 left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 left join  MATABLE002 so on so.COLUMN02=p.COLUMN06 where p.column07  between '" + start + "' and '" + end + "' and p.COLUMNA13='False' " + CLOS + "";
                    //SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Opportinuity#],FORMAT(p.[COLUMN07],'" + dateF + "')  [Date], s.[COLUMN05 ]  Customer, ms.COLUMN04  [Mile Stone], sr.COLUMN09  [Sales Rep],FORMAT(p.COLUMN08,'" + dateF + "') [Expected Close Date],FORMAT(p.COLUMN30,'" + dateF + "') [Next Followup]," +
//"+so.[COLUMN04 ] AS [Status] From SATABLE013 p left outer join MATABLE010 sr On sr.COLUMN02= p.COLUMN15 AND sr.COLUMNA03=p.COLUMNA03 AND ISNULL(sr.COLUMNA13,0)=0 left join MATABLE002 ms on ms.COLUMN02=p.COLUMN23 AND ISNULL(ms.COLUMNA13,0)=0 left outer join MATABLE002 st on st.COLUMN02=p.COLUMN06 AND ISNULL(st.COLUMNA13,0)=0 left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 left join  MATABLE002 so on so.COLUMN02=p.COLUMN06  where p.COLUMNA13='False' ";
                    alCol = "ID,Opportinuity#,Date,Customer,Mile Stone,Sales Rep,Expected Close Date,Next Followup,Status" ;
                    ob = "p.COLUMN02,p.COLUMN04,p.COLUMN07,s.COLUMN05,ms.COLUMN04,sr.COLUMN09,p.COLUMN08,p.COLUMN30,so.COLUMN04";
                }
                //Sales Quotation
                else if (frmID == 1376)
                {
                    strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Quotation#],FORMAT(p.[COLUMN07],'" + dateF + "')  [Date],FORMAT(p.[COLUMN08 ],'" + dateF + "') [Valid upto], s.[COLUMN05 ]  Customer,case  when p.COLUMN09=1 then 'YES' else 'NO' end [Manager Approval]," +
                                "  p.[COLUMN21] AS [Tax Amount],p.[COLUMN22] AS [Total Amount],p.[COLUMN06 ] AS [Order Status] " +
                                "From SATABLE015 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 where p.column07 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' ";
                    alCol =" ID,Quotation#,Date,Valid upto,Customer,Manager Approval,Tax Amount,Total Amount,Order Status ";
                    ob = " p.COLUMN02,p.COLUMN04,p.COLUMN07,p.COLUMN08,p.COLUMN05,p.COLUMN09,p.COLUMN21,p.COLUMN22,p.COLUMN06 ";
                }
                //Purchase Return
                else if (frmID == 1353)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
				//EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
                    strQry = "SELECT p.[COLUMN04 ] AS [Return order#],FORMAT(p.[COLUMN06 ],'" + dateF + "') AS [Date], s.[COLUMN05 ] AS Vendor,p.COLUMN11 [Reference PO#],p.[COLUMN09 ] AS Memo,p.[COLUMN15] AS [Total Amount], p.[COLUMN16 ] AS [Status],ma.COLUMN09 [Created By] From SATABLE005 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where isnull(p.COLUMNA13,0)=0 ";
                    //" left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN24 " +
                    //" left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN27 ";

                    alCol = "Return Order#, Date, Vendor, Reference PO#, Memo, Total Amount, Status, Created By";
					//EMPHCS1730 rajasekhar reddy patakota 10/05/2016 Reference sales and purchase order nos in sales and puchase module
                    ob = "p.COLUMN04, p.COLUMN06, s.COLUMN05, p.COLUMN11, p.COLUMN09, p.COLUMN15, p.COLUMN16, ma.COLUMN09";
                }
                //Item Issue
                else if (frmID == 1354)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = "SELECT p.[COLUMN04 ] AS [Item Issue#],FORMAT(p.[COLUMN08],'" + dateF + "') AS Date,s.[COLUMN05 ] AS Vendor,f.[COLUMN04 ] AS [Reference Return#],p.[COLUMN09 ] AS [Reference#],p.[COLUMN13 ] AS Memo,sum(isnull(s8.COLUMN13,0)) [Total AMount],p.COLUMN14 Status,ma.COLUMN09 [Created By] From SATABLE007 p left outer join SATABLE008 s8 on s8.COLUMN14=p.COLUMN01 left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN15 left outer join  SATABLE005 f on f.COLUMN02=p.COLUMN06 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.COLUMNA13='False' ";
                    alCol = "Item Issue#, Date, Vendor, Reference Return#, Reference#, Memo, Total Amount, Status, Created By";
                    ob = "p.COLUMN04, p.COLUMN08, s.COLUMN05, f.COLUMN04, p.COLUMN09, p.COLUMN13, s8.COLUMN13, p.COLUMN14, ma.COLUMN09";

                }
                //Refund
                else if (frmID == 1355)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = "SELECT p.[COLUMN04 ] AS [Debit Memo#],FORMAT(p.[COLUMN06 ],'" + dateF + "') AS [Date],(case when p.COLUMN50='22335' then s2.COLUMN05 else s.[COLUMN05 ] end) AS Vendor,(select COLUMN11 from SATABLE005 where COLUMN02=(select COLUMN06 from SATABLE007 where COLUMN02=p.COLUMN33)) [Reference PO#],p14.[COLUMN04] AS [Reference Bill#],p.[COLUMN11] AS [Reference#],p.COLUMN09 Memo,p.[COLUMN15] AS [Total Amount],p.[COLUMN16 ] AS [Status],ma.COLUMN09 [Created By] From PUTABLE001 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 left outer join  SATABLE002 s2 on s2.COLUMN02=p.COLUMN05 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN24 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN27 left outer join PUTABLE014 p14 on p14.COLUMN02=p.COLUMN48 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column06 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 ";
                    alCol = "Debit Memo#, Date, Vendor, Reference PO#, Reference Bill#, Reference#, Memo, Total Amount, Status, Created By";
                    ob = "p.COLUMN04, p.COLUMN06, s.COLUMN05, Column11, p14.COLUMN04, p.COLUMN11, p.COLUMN09, p.COLUMN15, p.COLUMN16, ma.COLUMN09";
                }
                //Item Reciept
                else if (frmID == 1272)
                {
                    strQry = "SELECT p.[COLUMN04 ] AS [Item Receipt#],FORMAT(p.[COLUMN09],'" + dateF + "') AS Date,s.[COLUMN05 ] AS Vendor,f.[COLUMN04 ] AS [Reference PO#],p.[COLUMN10 ] AS Reference#,p.[COLUMN12 ] AS Memo,sum(isnull(cast(p4.COLUMN11 as decimal(18,2)),0)) [Total Amount],p.COLUMN18 Status,ma.COLUMN09 [Created By] From PUTABLE003 p left outer join PUTABLE004 p4 on p4.COLUMN12=p.COLUMN01 left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN13 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN16 left outer join  PUTABLE001 f on f.COLUMN02=p.COLUMN06 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column09 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 ";
                    alCol = "Item Receipt#, Date, Vendor, Reference PO#, Reference#, Memo, Total Amount, Status, Created By";
                    ob = "p.COLUMN04, p.COLUMN09, s.COLUMN05, f.COLUMN04, p.COLUMN10, p.COLUMN12, p4.COLUMN11, p.COLUMN18, ma.COLUMN09";

            }
            //bill
            else if (frmID == 1273)
            {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    //EMPHCS1811 rajasekhar reddy patakota 07/09/2016 Line Grid Details filling in sales & Purchase module
                strQry = "SELECT p.[COLUMN04 ] AS Bill#,max(FORMAT(p.[COLUMN08],'" + dateF + "')) AS Date,s.[COLUMN05 ] AS Vendor,f.[COLUMN04 ] AS [Reference PO#],p.[COLUMN09 ] AS Reference#,p.[COLUMN12 ] AS Memo,p.COLUMN14 [Total Amount],sum(isnull(f1.COLUMN15,0)) [Returns/Debit],(sum(isnull(p15.COLUMN16,0))+sum(isnull(p15.COLUMN14,0))+sum(isnull(p15.COLUMN06,0))) [Amount Paid],(p.COLUMN14-(sum(isnull(p15.COLUMN16,0))+sum(isnull(p15.COLUMN14,0))+sum(isnull(p15.COLUMN06,0)))) [Due Amount],DATEDIFF(day,p.COLUMN11,GETDATE()) as [Days Overdue],p.COLUMN13 Status,ma.COLUMN09 [Created By] From PUTABLE005 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05  left outer join  PUTABLE001 f on f.COLUMN02=p.COLUMN06 left outer join  PUTABLE001 f1 on f1.COLUMN11=p.COLUMN04 and  f1.COLUMNA02=p.COLUMNA02 and  f1.COLUMNA03=p.COLUMNA03 and f1.COLUMN15>0.00 and f1.COLUMNA13='False' left outer join PUTABLE015 p15 on p15.COLUMN03=p.COLUMN02 and p15.COLUMNA13='False' and p15.COLUMNA03=p.COLUMNA03 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column08 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='' ";
                alCol="Bill#, Date, Vendor, Reference PO#, Reference#, Memo, Total Amount, Returns/Debit, Amount Paid, Due Amount, Days Overdue, Status, Created By" ;
                ob= "p.COLUMN04, p.COLUMN08, s.COLUMN05, f.COLUMN04, p.COLUMN09, p.COLUMN12, p.COLUMN14, p15.COLUMN16, p15.COLUMN06, p15.COLUMN05, p.COLUMN11, p.COLUMN13, ma.COLUMN09";

                }
            //Bill Payment
            else if (frmID == 1274)
            {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                //EMPHCS1319	Add Date in Payment info and Account displaying number By RAJ.jr 05/11/2015
				//EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
                //EMPHCS1545	Payment & CustomerPayment->In list payment  not calculating credit,advance,discount BY Raj.Jr
                strQry = "SELECT p.[COLUMN04 ] AS Payment#,FORMAT(p.[COLUMN18 ],'" + dateF + "') as Date,(case when p.COLUMN20=22306 then r.COLUMN09 else s.[COLUMN05 ] end) AS Vendor,b.[COLUMN04 ] AS [Reference Bill#],p.COLUMN10 Memo,f1.[COLUMN04 ] AS [Bank Account],(case when p.COLUMN20=22306 then fb.COLUMN04 else b.COLUMN14 end) [Total Amount],isnull((fb.COLUMN06+fb.COLUMN16+fb.COLUMN14),0) as [Amount Paid],p.COLUMN11 Status,ma.COLUMN09 [Created By] From PUTABLE014 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 left outer join  MATABLE010 r on r.COLUMN02=p.COLUMN05 and r.COLUMN30='True'  left outer join  PUTABLE015 fb on fb.COLUMN08=p.COLUMN01 and fb.COLUMNA13='False' left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN13 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN16 left outer join  PUTABLE001 f on f.COLUMN02=p.COLUMN06  left outer join  PUTABLE005 b on b.COLUMN02=fb.COLUMN03  left outer join FITABLE001 f1 on f1.COLUMN02=p.COLUMN08 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null)  where p.column18 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 ";
                alCol="Payment#, Date, Vendor, Reference Bill#, Memo, Bank Account,Total Amount, Amount Paid, Status, Created By";
                ob= "p.COLUMN04, p.COLUMN18, s.COLUMN05, b.COLUMN04, p.COLUMN10, f1.COLUMN04,b.COLUMN14,  fb.COLUMN06, p.COLUMN11, ma.COLUMN09" ;
            }
                //sales
                //Sales Order
                else if (frmID == 1275)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = "SELECT p.[COLUMN04 ]  [Order#],FORMAT(p.[COLUMN06 ],'" + dateF + "')  [Date],s.[COLUMN05 ]  Customer,FORMAT(p.[COLUMN08],'" + dateF + "') [Ship Date],p.[COLUMN11] as Reference#,p.[COLUMN09] as Memo,p.[COLUMN15] AS [Total Amount],p.[COLUMN16 ] AS [Status],ma.column09 [Created By] From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 left outer join  MATABLE009 e on e.COLUMN02=p.COLUMN29 left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMNA08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column06 between '" + start + "' and '" + end + "' and p.COLUMNA13='False' ";
                    alCol = "Order#, Date,Customer,Ship Date,Reference#, Memo, Total Amount, Status,Created By";
                    ob = "p.COLUMN04, p.COLUMN06, s.COLUMN05, p.COLUMN08, p.COLUMN11,p.COLUMN09, p.COLUMN15, p.COLUMN16,ma.COLUMN09";
                }
                //Order Selection Screen
                else if (frmID == 1523)
                {

                    if (Cust != "" && itm != "" && (brand != "" || group != "" || family != "") && (dtFrm != "" && dtTo != "") && city != "")
                    {
                        if (brand == "") brand = "null"; if (group == "") group = "null"; if (family == "") family = "null";
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and SA3.COLUMN10=''" + city + "'' and SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where (COLUMN10=" + brand + " or COLUMN11=" + group + " or COLUMN12=" + family + ")) and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (Cust != "" && itm != "" && (brand != "" || group != "" || family != "") && (dtFrm != "" && dtTo != "") && state != "")
                    {
                        if (brand == "") brand = "null"; if (group == "") group = "null"; if (family == "") family = "null";
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and SA3.COLUMN11=" + state + " and SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where (COLUMN10=" + brand + " or COLUMN11=" + group + " or COLUMN12=" + family + ")) and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (Cust != "" && itm != "" && UPC != "" && (brand != "" || group != "" || family != "") && (dtFrm != "" && dtTo != ""))
                    {
                        if (brand == "") brand = "null"; if (group == "") group = "null"; if (family == "") family = "null";
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                         "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05='" + Cust + "' and SA6.COLUMN03=" + itm + " and SA6.COLUMN04=''" + UPC + "'' and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where (COLUMN10=" + brand + " or COLUMN11=" + group + " or COLUMN12=" + family + ")) and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (Cust != "" && itm != "" && UPC != "" && (dtFrm != "" && dtTo != "") && city != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and SA6.COLUMN04=''" + UPC + "'' and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and SA3.COLUMN10=''" + city + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (Cust != "" && itm != "" && UPC != "" && (dtFrm != "" && dtTo != "") && state != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and SA6.COLUMN04=''" + UPC + "'' and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and SA3.COLUMN11=" + state + " and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (Cust != "" && itm != "" && (dtFrm != "" && dtTo != "") && city != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and SA3.COLUMN10=''" + city + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (Cust != "" && itm != "" && (dtFrm != "" && dtTo != "") && state != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and SA3.COLUMN11=" + state + " and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (Cust != "" && itm != "" && UPC != "" && (dtFrm != "" && dtTo != ""))
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and SA6.COLUMN04=''" + UPC + "'' and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (Cust != "" && itm != "" && UPC != "" && (brand != "" || group != "" || family != ""))
                    {
                        if (brand == "") brand = "null"; if (group == "") group = "null"; if (family == "") family = "null";
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and SA6.COLUMN04=''" + UPC + "'' and SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where (COLUMN10=" + brand + " or COLUMN11=" + group + " or COLUMN12=" + family + ")) and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (itm != "" && (dtFrm != "" && dtTo != "") && UPC != "" && (brand != "" || group != "" || family != ""))
                    {
                        if (brand == "") brand = "null"; if (group == "") group = "null"; if (family == "") family = "null";
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  SA6.COLUMN03=" + itm + " and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and SA6.COLUMN04=''" + UPC + "'' and SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where (COLUMN10=" + brand + " or COLUMN11=" + group + " or COLUMN12=" + family + ")) and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (Cust != "" && itm != "" && (dtFrm != "" && dtTo != ""))
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05='" + Cust + "' and SA6.COLUMN03=" + itm + "  and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (Cust != "" && itm != "" && UPC != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and SA6.COLUMN04=''" + UPC + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (Cust != "" && city != "" && (dtFrm != "" && dtTo != ""))
                    {
                        strQry = "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + " where p.COLUMN05=" + Cust + " and SA3.COLUMN10='" + city + "' and p.COLUMN06 between '" + dtFrm + "' and '" + dtTo + "' and p.COLUMN03=1275 ";
                    }
                    else if (Cust != "" && state != "" && (dtFrm != "" && dtTo != ""))
                    {
                        strQry = "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + " where p.COLUMN05=" + Cust + " and SA3.COLUMN11=" + state + " and p.COLUMN06 between '" + dtFrm + "' and '" + dtTo + "' and p.COLUMN03=1275 ";
                    }
                    else if (itm != "" && (dtFrm != "" && dtTo != "") && UPC != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  SA6.COLUMN03=" + itm + " and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and SA6.COLUMN04=''" + UPC + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (Cust != "" && itm != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN03=" + itm + " and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (Cust != "" && UPC != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT ID,Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + "left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  p.COLUMN05=" + Cust + " and SA6.COLUMN04=''" + UPC + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (Cust != "" && (dtFrm != "" && dtTo != ""))
                    {
                        strQry = "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + " where  p.COLUMN05=" + Cust + " and p.COLUMN06 between '" + dtFrm + "' and '" + dtTo + "' and p.COLUMN03=1275 and p.COLUMNA13='False' ";
                    }
                    else if (itm != "" && (dtFrm != "" && dtTo != ""))
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  SA6.COLUMN03=" + itm + " and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (itm != "" && UPC != "")
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                       " SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  SA6.COLUMN03=" + itm + " and SA6.COLUMN04=''" + UPC + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (Cust != "" && city != "")
                    {
                        strQry = "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + " where p.COLUMN05='" + Cust + "' and SA3.COLUMN10='" + city + "' and p.COLUMN03=1275 ";
                    }
                    else if (Cust != "" && state != "")
                    {
                        strQry = "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + " where p.COLUMN05='" + Cust + "' and SA3.COLUMN11='" + state + "' and p.COLUMN03=1275 ";
                    }
                    else if (city != "" && (dtFrm != "" && dtTo != ""))
                    {
                        strQry = "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                                                " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + "  where SA3.COLUMN10='" + city + "' and p.COLUMN06 between '" + dtFrm + "' and '" + dtTo + "' and p.COLUMN03=1275 ";
                    }
                    else if (state != "" && (dtFrm != "" && dtTo != ""))
                    {
                        strQry = "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                                                " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + "  where SA3.COLUMN11='" + state + "' and p.COLUMN06 between '" + dtFrm + "' and '" + dtTo + "' and p.COLUMN03=1275 ";
                    }
                    else if (UPC != "" && (dtFrm != "" && dtTo != ""))
                    {
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  SA6.COLUMN04=''" + UPC + "'' and p.COLUMN06 between ''" + dtFrm + "'' and ''" + dtTo + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (itm != "" && (brand != "" || group != "" || family != ""))
                    {
                        if (brand == "") brand = "null"; if (group == "") group = "null"; if (family == "") family = "null";
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  SA6.COLUMN03=" + itm + " and SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where  (COLUMN10=" + brand + " or COLUMN11=" + group + " or COLUMN12=" + family + ")) and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (UPC != "" && (brand != "" || group != "" || family != ""))
                    {
                        if (brand == "") brand = "null"; if (group == "") group = "null"; if (family == "") family = "null";
                        strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
    " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
    "SELECT @cols= ISNULL(@cols + ',','') " +
      " + QUOTENAME(COLUMN04) " +
    "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and COLUMNA13=0) AS COLUMN04  " +
    "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
         "(" +
                        "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                        " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  SA6.COLUMN04=''" + UPC + "'' and SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where (COLUMN10=" + brand + " or COLUMN11=" + group + " or COLUMN12=" + family + ")) and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                    else if (Cust != "" && (brand != "" || group != "" || family != ""))
                    {
                        if (brand == "") brand = "null"; if (group == "") group = "null"; if (family == "") family = "null";
                            strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
        " @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
         "SELECT @cols= ISNULL(@cols + ',','') " +
           " + QUOTENAME(COLUMN04) " +
        "FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
        "set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
              "(" +
                            "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                            " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                            "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where p.COLUMN05=" + Cust + " and SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where (COLUMN10=" + brand + " or COLUMN11=" + group + " or COLUMN12=" + family + ")) and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                    }
                else if (Cust != "")
                {
                    strQry = "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                    " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                    "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + " where p.COLUMN05='" + Cust + "' and p.COLUMN03=1275 and p.COLUMNA13='False' ";
                }
                else if (itm != "")
                {
                    strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
" @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
"SELECT @cols= ISNULL(@cols + ',','') " +
  " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
     "(" +
                    "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                    " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  SA6.COLUMN03=" + itm + " and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                }
                else if (UPC != "")
                {
                    strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
" @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
"SELECT @cols= ISNULL(@cols + ',','') " +
  " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
     "(" +
                    "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                    " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where  SA6.COLUMN04=''" + UPC + "'' and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                }
                else if (group != "")
                {
                    strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
" @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
"SELECT @cols= ISNULL(@cols + ',','') " +
  " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
     "(" +
                    "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                    " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where COLUMN11=" + group + ") and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                }
                else if (family != "")
                {
                    strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
" @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
"SELECT @cols= ISNULL(@cols + ',','') " +
  " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
     "(" +
                    "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                                            " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where COLUMN12=" + family + ") and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                }
                else if (brand != "")
                {
                    strQry = "DECLARE @cols AS NVARCHAR(MAX), " +
" @query  AS NVARCHAR(MAX),@UPC AS NVARCHAR(MAX) " +
"SELECT @cols= ISNULL(@cols + ',','') " +
  " + QUOTENAME(COLUMN04) " +
"FROM (SELECT DISTINCT COLUMN02,COLUMN04 FROM MATABLE002 where COLUMNA03=" + AcOwner + " and COLUMN03=11119 and isnull(COLUMNA13,0)=0) AS COLUMN04  " +
"set @query  = 'SELECT Order#,Date,[Shipping Date],Customer,City,[Order Status],' + @cols + '  from " +
     "(" +
                    "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                                            " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status],SA6.COLUMN07 qty,m.COLUMN04 as UOM  " +
                                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + " left outer join SATABLE006 SA6 on SA6.COLUMN19=p.COLUMN01 and (SA6.COLUMNA02 in(" + opu + ") or SA6.COLUMNA02 is null) and SA6.COLUMNA03=" + AcOwner + " and SA6.COLUMNA13=0 left outer join MATABLE002 m on m.COLUMN02=SA6.COLUMN27 where SA6.COLUMN03 in (select COLUMN02 from MATABLE007  where COLUMN10=" + brand + ") and p.COLUMN03=1275 and SA6.COLUMN27!=0 ";
                }
                else if ((dtFrm != "" && dtTo != ""))
                {
                    strQry = "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                    " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + "  where p.COLUMN06 between '" + dtFrm + "' and '" + dtTo + "' and p.COLUMN03=1275 and p.COLUMNA13='False' ";
                }
                else if (city != "")
                {
                    strQry = "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                                            " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + "  where SA3.COLUMN10='" + city + "' and p.COLUMN03=1275 and p.COLUMNA13='False' ";
                }
                else if (state != "")
                {
                    strQry = "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                                            " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                                        "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null) and SA3.COLUMNA03=" + AcOwner + "  where SA3.COLUMN11='" + state + "' and p.COLUMN03=1275 and p.COLUMNA13='False' ";
                }
                else
                {
                    strQry = "SELECT p.[COLUMN04 ]  [Order#],convert(varchar,p.[COLUMN06 ],106)  [Date],convert(varchar,p.[COLUMN08 ],106)  [Shipping Date], s.[COLUMN05 ]  Customer," +
                                    " SA3.[COLUMN10] As [City],p.[COLUMN16 ] AS [Order Status] " +
                                    "From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join SATABLE003 SA3 on SA3.COLUMN20=p.COLUMN01 and (SA3.COLUMNA02 in(" + opu + ") or SA3.COLUMNA02 is null)  and SA3.COLUMNA03=" + AcOwner + " where p.COLUMN05='" + Cust + "' and p.COLUMN03=1275 and p.COLUMNA13='False' ";
                }
                alCol = " Order#, Date, Shipping Date, Customer, City, Order Status" ;
                ob = "p.COLUMN04, p.COLUMN06, p.COLUMN08, s.COLUMN05, SA3.COLUMN10, p.COLUMN16";
            }
            //Item Issue
            else if (frmID == 1276)
            {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                strQry = "SELECT p.[COLUMN04 ] AS [Item Issue#],convert(varchar,p.[COLUMN08 ],106) AS Date,s.[COLUMN05 ] AS Customer,f.[COLUMN04 ] AS [ReferenceOrder#],p.[COLUMN09 ] AS Reference#,p.[COLUMN13 ] AS Memo,sum(isnull(i.COLUMN13,0)) [Total Amount],p.COLUMN14 Status,ma.column09 [Created By] From SATABLE007 p left outer join SATABLE008 i on i.COLUMN14=p.COLUMN01 left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN15 left outer join  SATABLE005 f on f.COLUMN02=p.COLUMN06 left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMNA08 and ma.COLUMNA03=p.COLUMNA03 and ma.COLUMNA02=p.COLUMNA02 where p.column08 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 ";
                alCol = "Item Issue#,Date,Customer,ReferenceOrder#,Reference#,Memo,Total Amount,Status,Created By";
                ob = "p.COLUMN04,p.COLUMN08s.COLUMN05,f.COLUMN04,,p.COLUMN09,p.COLUMN12,i.COLUMN13,p.COLUMN14,ma.COLUMN09";
            }
            //Invoice
            else if (frmID == 1277)
            {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    //EMPHCS1811 rajasekhar reddy patakota 07/09/2016 Line Grid Details filling in sales & Purchase module
                strQry = "SELECT p.[COLUMN04 ] AS Invoice#,FORMAT(p.[COLUMN08],'" + dateF + "') AS Date,s.[COLUMN05 ] AS Customer,f.[COLUMN04 ] AS [Reference Order#],p.[COLUMN09 ] AS Reference#,p.[COLUMN12 ] AS Memo,p.COLUMN20 [Total Amount],sum(isnull(f1.COLUMN12,0))+sum(isnull(f1.COLUMN32,0)) as [Returns/Credit],(sum(isnull(s12.COLUMN06,0))+sum(isnull(s12.COLUMN14,0))+sum(isnull(s12.COLUMN13,0))+sum(isnull(s12.COLUMN09,0))) [Amount Received],(p.COLUMN20-(sum(isnull(s12.COLUMN06,0))+sum(isnull(s12.COLUMN14,0))+sum(isnull(s12.COLUMN13,0))+sum(isnull(s12.COLUMN09,0)))) [Due Amount],DATEDIFF(day,p.COLUMN10,GETDATE()) as [Days Overdue],p.COLUMN13 Status,ma.[COLUMN09] AS [Created By] From SATABLE009 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN14 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN18 left outer join  SATABLE005 f on f.COLUMN02=p.COLUMN06 left outer join  SATABLE005 f1 on f1.COLUMN11=p.COLUMN04 and  f1.COLUMNA02=p.COLUMNA02 and  f1.COLUMNA03=p.COLUMNA03 and f1.COLUMNA13='False' and f1.COLUMN15>0 left outer join SATABLE012 s12 on s12.COLUMN03=p.column02 and s12.COLUMNA13='False' left outer join  MATABLE010 ma on ma.COLUMN02=p.COLUMNA08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null)  where p.column08 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 and p.COLUMN04!='' ";
                alCol = "Invoice#, Date, Customer, Reference Order#, Reference#, Memo, Total Amount, Returns/Credit, Amount Received, Due Amount, Days Overdue, Status, Created By";
                ob = "p.COLUMN04, p.COLUMN08, p.COLUMN05, f.COLUMN04, p.COLUMN09, p.COLUMN12, p.COLUMN20, s12.COLUMN14, s12.COLUMN06, s12.COLUMN05, p.COLUMN10, p.COLUMN13, ma.COLUMN09";

            }
                //Customer Payment
                //EMPHCS1421 BY GNANESHWAR ON 15/12/2015 when invoice edited after payment due amount not updating, it is showing previous due
            else if (frmID == 1278)
            {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
				//EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
                //EMPHCS1545	Payment & CustomerPayment->In list payment  not calculating credit,advance,discount BY Raj.Jr
                strQry = "SELECT p.[COLUMN04 ] AS Payment#,FORMAT(p.[COLUMN19 ],'" + dateF + "')  Date,s.[COLUMN05 ] AS Customer,f.[COLUMN04 ] AS [Reference Order#],d.[COLUMN04 ] AS [Reference Invoice#],p.COLUMN10 Memo,c.[COLUMN04 ] AS [Bank Account],d.COLUMN20 [Total Amount],(isnull(fb.COLUMN06,0)+isnull(fb.COLUMN14,0)+isnull(fb.COLUMN13,0)+isnull(fb.COLUMN09,0)) [Amount Paid],p.COLUMN11 Status,ma.COLUMN09 [Created By] From   SATABLE011  p  left outer join  SATABLE012  fb on   fb.COLUMN08 =p.COLUMN01 and isnull(fb.COLUMNA13,0)=0 left outer join  SATABLE002  s  on  s.COLUMN02=p.COLUMN05    left outer join  CONTABLE007 o  on  o.COLUMN02=p.COLUMN14    left outer join  SATABLE005  f  on  f.COLUMN02=p.COLUMN06 left outer join SATABLE009 d on d.COLUMN02=fb.COLUMN03 and d.COLUMNA13='False'  left outer join  FITABLE001  c  on  c.COLUMN02=p.COLUMN08 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null)  where p.column19 between '" + start + "' and '" + end + "' and p.COLUMN03 = 1278 and (d.COLUMN03 = 1277 or isnull(cast(d.COLUMN03 as nvarchar(250)),'')='') and isnull(p.COLUMNA13,0)=0 ";

                  //EMPHCS1320	Add Paid Amount in Customer Payment By Raj.jr 05/11/2015
                //strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS Payment#,convert(varchar,p.[COLUMN19],106)  Date,s.[COLUMN05 ] AS Customer,d.[COLUMN04 ] AS Invoice#,f.[COLUMN04 ] AS [Sales Order#],c.[COLUMN04 ] AS Account,fb.COLUMN04 as [Total Amount]," +
                //"fb.COLUMN06 as Payment,fb.COLUMN05 as [Due Amount],o.[COLUMN03] AS [Operating Unit] From SATABLE011 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN14 left outer join  SATABLE005 f on f.COLUMN02=p.COLUMN06 " +
                //"left outer join  SATABLE012 fb on fb.COLUMN08=p.COLUMN01 left outer join  SATABLE009 d on d.COLUMN02=fb.COLUMN03 left outer join  FITABLE001 c on c.COLUMN02=p.COLUMN08 ";
                alCol = "Payment#, Date, Customer, Reference Order#, Reference Invoice#, Memo, Bank Account,Total Amount,Amount Paid,Status, Created By" ;
                ob = "p.COLUMN04, p.COLUMN19, s.COLUMN05, d.COLUMN04, f.COLUMN04, p.COLUMN10, c.COLUMN04,d.COLUMN20,fb.COLUMN06,p.COLUMN11, ma.COLUMN09" ;
            }
                //EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
                //Expense Payment
                else if (frmID == 1528)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = "SELECT p.[COLUMN04 ] AS [Expense Payment#],FORMAT(p.[COLUMN05 ],'" + dateF + "')  Date,s.[COLUMN06] AS Employee,p.COLUMN10 Reference#,p.COLUMN13 Memo,fb.COLUMN05 as [Total Amount],fb.COLUMN07 [Advance Paid],fb.COLUMN08 [Amount Paid],c.[COLUMN04 ] AS Bank,ma.COLUMN09 [Created By] From FITABLE045 p left outer join  MATABLE010 s on s.COLUMN02=p.COLUMN06  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN16 left outer join PRTABLE001 pr on pr.COLUMN02=p.COLUMN09 left outer join MATABLE002 m2 on m2.COLUMN02=p.COLUMN15 left outer join FITABLE044 f44 on f44.COLUMN02=p.COLUMN20 left outer join  FITABLE046 fb on fb.COLUMN09=p.COLUMN01  and isnull(fb.columna13,'False')='False' left outer join  FITABLE001 c on c.COLUMN02=p.COLUMN12 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column05 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 ";
                    alCol = "Expense Payment#,Date, Employee, Reference#, Memo, Total Amount, Advance Paid, Amount Paid, Bank, Created By";
                    ob = "p.COLUMN04, p.COLUMN05, s.COLUMN06, p.COLUMN10, p.COLUMN13, fb.COLUMN05, fb.COLUMN07, fb.COLUMN08, c.COLUMN04, ma.COLUMN09";
                }
                //joborder
                else if (frmID == 1283)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Job Order#],e.[COLUMN04 ] AS [Order Type], s.[COLUMN05 ] AS Jobber,FORMAT(p.[COLUMN06 ],'" + dateF + "') AS [Date],FORMAT(p.[COLUMN08 ],'" + dateF + "') AS [Due Date],  p.[COLUMN07 ] AS [Manager Approval],p.[COLUMNB04 ] AS [Delivery Point],p.[COLUMN15] AS [Total Amount], p.[COLUMN16 ] AS [Order Status] From SATABLE005 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 " +
                                     " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN24 " +
                                     " left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN27 left outer join  MATABLE009 e on e.COLUMN02=p.COLUMN29  where p.column06 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 " + CLOS + "";

                    alCol = "ID,Job Order#,Date,Order Type,Jobber,Manager Approval,Due Date,Delivery Point,Total Amount,Order Status";
                    ob = "p.COLUMN02,p.COLUMN04,p.COLUMN06,e.COLUMN04,s.COLUMN05,p.COLUMN07,p.COLUMN08,p.COLUMNB04,p.COLUMN15,p.COLUMN16";
                }
                //Jobber Issue
                else if (frmID == 1284)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Item Issue#],s.[COLUMN05 ] AS Jobber,j.[COLUMN04 ] AS [Job Order#],FORMAT(p.[COLUMN08 ],'" + dateF + "') AS Date,p.[COLUMN09 ] AS [Reference#],p.[COLUMN13 ] AS Memo,p.[COLUMN14] Status,o.[COLUMN03] AS OperatingUnit,d.[COLUMN04 ] AS Department From SATABLE007 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 " +
                             " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN15 " +
                             " left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN16 " +
                             " left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN06 " +
                             " left outer join  SATABLE005 j on j.COLUMN02=p.COLUMN06 where p.column08 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 " + CLOS + "";
                    alCol = "ID,Item Issue#,Jobber,Job Order#,Date,Reference#,Memo,Status,OperatingUnit";
                    ob = "p.COLUMN02,p.COLUMN04,s.COLUMN05,f.COLUMN04,p.COLUMN08,p.COLUMN09,p.COLUMN12,p.COLUMN14,o.COLUMN03";

                }
                //EMPHCS1784	Job Order for IN Process 15/7/2016
                else if (frmID == 1588)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Issue#],s.[COLUMN05 ] AS Customer,j.[COLUMN04 ] AS [Job Order#],FORMAT(p.[COLUMN08 ],'" + dateF + "') AS Date,p.[COLUMN09 ] AS [Reference#],p.[COLUMN13 ] AS Memo,o.[COLUMN03] AS OperatingUnit,d.[COLUMN04 ] AS Department From SATABLE007 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 " +
                             " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN15 " +
                             " left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN16 " +
                             " left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN06 " +
                             " left outer join  PUTABLE001 j on j.COLUMN02=p.COLUMN06 where p.column08 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 AND p.COLUMN03=1588";
                    alCol = "ID,Issue#,Customer,Job Order#,Date,Reference#,Memo,OperatingUnit";
                    ob = "p.COLUMN02,p.COLUMN04,s.COLUMN05,f.COLUMN04,p.COLUMN08,p.COLUMN09,p.COLUMN12,o.COLUMN03";

                }
                //Jobber Reciept
                else if (frmID == 1286)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS JoReciept#,s.[COLUMN05 ] AS Jobber,j.[COLUMN04 ] AS [Job Order#],FORMAT(p.[COLUMN09],'" + dateF + "') AS Date,p.[COLUMN10 ] AS Reference#,p.[COLUMN12 ] AS Memo,o.[COLUMN03] AS OperatingUnit,d.[COLUMN04 ] AS Department From PUTABLE003 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 " +
                                     " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN13 " +
                                     " left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN16 " +
                                     " left outer join  PUTABLE001 f on f.COLUMN02=p.COLUMN06 " +
                                     " left outer join  SATABLE005 j on j.COLUMN02=p.COLUMN06 where isnull(p.COLUMNA13,0)=0 ";
                    alCol = "ID,JoReciept#,Jobber,Job Order#,Date,Reference#,Memo,OperatingUnit,Department";
                    ob = "p.COLUMN02,p.COLUMN04,s.COLUMN05,f.COLUMN04,p.COLUMN09,p.COLUMN10,p.COLUMN12,o.COLUMN03,d.COLUMN04";

                }
                //Jobber Bill
                else if (frmID == 1287)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Bill#],e.[COLUMN04 ] AS [Order Type],s.[COLUMN05 ] AS Jobber,j.[COLUMN04 ] AS [Job Order#],FORMAT(p.[COLUMN08 ],'" + dateF + "') AS Date,p.[COLUMN21 ] AS [Payment Mode],p.[COLUMN11 ] AS [Payment Terms],FORMAT(p.[COLUMN10 ],'" + dateF + "') AS [Due Date],p.[COLUMN24 ] AS [Tax Amount],p.[COLUMN20 ] AS [Total Amount],p.[COLUMN13 ] AS Status From SATABLE009 p left outer join  SATABLE001 s on s.COLUMN02=p.COLUMN05 " +
                                     " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN15 " +
                                     " left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN18 " +
                                     " left outer join  SATABLE002 f on f.COLUMN02=p.COLUMN06 " +
                                     " left outer join  SATABLE005 j on j.COLUMN02=p.COLUMN06  left outer join  MATABLE009 e on e.COLUMN02=p.COLUMN19  where p.column08 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 ";
                    alCol = "ID,Bill#,Date,Jobber,Order Type,Job Order#,Payment Mode,Payment Terms,Due Date,Tax Amount,Total Amount,Status";
                    ob = "COLUMN02,COLUMN04,COLUMN08,COLUMN05,COLUMN19,COLUMN06,COLUMN21,COLUMN11,COLUMN10,COLUMN24,COLUMN20,COLUMN13";

                }
                //Jobber Payment
                else if (frmID == 1288)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Payment#],FORMAT(p.[COLUMN19 ],'" + dateF + "') AS [Date],s.[COLUMN05 ] AS Jobber,f.[COLUMN04 ] AS [Job Order],b.[COLUMN04 ] AS Bill#,c.[COLUMN04 ] AS Account,fb.COLUMN04 as [Total Amount],fb.COLUMN05 as [Due Amount],fb.COLUMN06 as Payment,o.[COLUMN03] AS OperatingUnit,d.[COLUMN04 ] AS Department From SATABLE011 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 " +
                                     " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN13 " +
                                     " left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN16 " +
                                     " left outer join  SATABLE005 f on f.COLUMN02=p.COLUMN06 " +
                                     " left outer join  SATABLE009 b on b.COLUMN02=p.COLUMN07 " +
                                     " left outer join  SATABLE012 fb on fb.COLUMN08=p.COLUMN01  left outer join  FITABLE001 c on c.COLUMN02=p.COLUMN08 where p.column19 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 ";
                    alCol = "ID,Payment#,Date,Jobber,Bill#,Job Order,Account,Total Amount,Due Amount,OperatingUnit";
                    ob = " p.COLUMN02,p.COLUMN04,p.COLUMN19,s.COLUMN05,b.COLUMN04,f.COLUMN04,p.COLUMN08,fb.COLUMN04,fb.COLUMN05,o.COLUMN03";
                }
                //Expence Type
                else if (frmID == 1293)
                {
                    //EMPHCS1150  Purpose is not getting displayed from Expense screen BY RAJ.Jr 21/9/2015
                    // EMPHCS1321	Add Expense# in ExpenseTracking info display BY RAJ.Jr 05/11/2015
                    //EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.COLUMN17 as Expense#,FORMAT(p.[COLUMN09 ],'" + dateF + "') AS Date, o.[COLUMN03 ] AS [Operating Unit],pr.[COLUMN05] as Project,  m.[COLUMN06 ] AS [Employee],p.[COLUMN24 ] AS [Expense Amount],(iif((sum(isnull(f46.COLUMN07,0))+sum(isnull(f46.COLUMN08,0)))<=0,p.COLUMN24,isnull(p.COLUMN24,0)-(sum(isnull(f46.COLUMN07,0))+sum(isnull(f46.COLUMN08,0))))) as [Due Amount], p.[COLUMN07 ] AS Memo From FITABLE012 p" +
                                    " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN10" +
                                    " left outer join  MATABLE010 m on m.COLUMN02=p.COLUMN16" +
                                    " left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN13" +
                                    " left outer join  PRTABLE001 pr on pr.COLUMN02=p.COLUMN25" +
                                    " left outer join FITABLE046 f46 on f46.COLUMN03=p.COLUMN02 and isnull(f46.COLUMNA13,0)=0 " +
                        //EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
                                    "   where p.column09 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'";
                    alCol = "ID,Expense#, Date, Operating Unit,Project, Employee,  Expense Amount,Due Amount,Memo";
                    ob = "p.COLUMN02,p.COLUMN17, p.COLUMN09,o.COLUMN03,pr.COLUMN05,m.COLUMN06, p.COLUMN24,f46.COLUMN06,f.COLUMN05";
                }
                //Inventory Adjustment
                else if (frmID == 1357)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = "SELECT  p.[COLUMN04 ] AS [Adjustment#],FORMAT(p.[COLUMN05 ],'" + dateF + "') AS [Date],p.COLUMN14 Reference#,p.COLUMN15 Memo,p.[COLUMN07 ] AS [Total Value],e.[COLUMN04] AS [Account], '' AS [Status],ma.[COLUMN09 ] AS [Created By] From FITABLE014 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN10 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN12 left outer join  FITABLE001 e on e.COLUMN02=p.COLUMN06 and e.COLUMNA03=p.COLUMNA03 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column05 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 ";

                    alCol = "Adjustment#, Date, Reference#, Memo, Total Value, Account, Status, Created By";
                    ob = "p.COLUMN04, p.COLUMN05, p.COLUMN14, p.COLUMN07, p.COLUMN09, e.COLUMN04, o.COLUMN03, ma.COLUMN09";
                }
                //EMPHCS741	Amount Column displays empty in Withdrawl Screen done by srinivas 7/22/2015
                //amount withdrawn
                else if (frmID == 1349)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN05 ] AS [Withdraw#],FORMAT(p.[COLUMN04],'" + dateF + "') AS [Transaction Date], o.[COLUMN04 ] AS Account,p.[COLUMN11 ] AS [Amount],p.[COLUMN09 ] AS [Description] From FITABLE062 p " +
                             " left outer join  FITABLE001 o on o.COLUMN02=p.COLUMN03  where p.column04 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'";

                    alCol = "ID,Withdraw#,Transaction Date,Account,Amount,Description";
                    ob = "p.COLUMN02,p.COLUMN05,p.COLUMN04,o.COLUMN04,p.COLUMN10,p.COLUMN09";
                }
                //EMPHCS740	Amount Column displays empty in Deposit Screen done by srinivas 7/22/2015
                //amount Deposit
                else if (frmID == 1350)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN05 ] AS [Deposit#],FORMAT(p.[COLUMN04],'" + dateF + "') AS [Transaction Date], o.[COLUMN04 ] AS Account,p.[COLUMN11 ] AS [Amount],p.[COLUMN09 ] AS [Description] From FITABLE062 p " +
                             " left outer join  FITABLE001 o on o.COLUMN02=p.COLUMN03  where p.column04 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'";

                    alCol = "ID,Deposit#,Transaction Date,Account,Amount,Description";
                    ob = "p.COLUMN02,p.COLUMN05,p.COLUMN04,o.COLUMN04,p.COLUMN10,p.COLUMN09";
                }
                //EMPHCS743	Adding Shipping Tab Fileds in Sales order and Invoice done by srinivas 7/22/2015
                //amount Transfer
                else if (frmID == 1351)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN05 ] AS [Transfer#],FORMAT(p.[COLUMN04],'" + dateF + "') AS [Transaction Date], o.[COLUMN04 ] AS Account,p.[COLUMN11 ] AS [Amount],p.[COLUMN09 ] AS [Description] From FITABLE062 p " +
                             " left outer join  FITABLE001 o on o.COLUMN02=p.COLUMN03  where p.column04 between '" + start + "' and '" + end + "' and p.COLUMNA13='False'";

                    alCol = "ID, Transfer#,Transaction Date,Account,Amount,Description";
                    ob = "p.COLUMN02,p.COLUMN05,p.COLUMN04,o.COLUMN04,p.COLUMN10,p.COLUMN09";
                }//sales return order
                else if (frmID == 1328)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = "SELECT p.[COLUMN04 ] AS [Return Order#],FORMAT(p.[COLUMN06 ],'" + dateF + "') AS [Date],s.[COLUMN05 ] AS Customer,p.COLUMN11 [Reference Order#],p.COLUMN09 Memo,p.[COLUMN15] AS [Total Amount],p.[COLUMN16 ] AS [Status],ma.COLUMN09 [Created By] From PUTABLE001 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN24  left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN27 left outer join  MATABLE009 e on e.COLUMN02=p.COLUMN29 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and Ma.COLUMNA02=p.COLUMNA02 where isnull(p.COLUMNA13,0)=0 ";
                    alCol = "Return Order#, Date, Customer, Reference Order#, Memo, Total Amount, Status, Created By";
                    ob = "p.COLUMN04, p.COLUMN06, s.COLUMN05, p.COLUMN11, p.COLUMN09, p.COLUMN15, p.COLUMN16, ma.COLUMN09";
                }//receipt
                else if (frmID == 1329)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = "SELECT p.[COLUMN04 ] AS [Item Receipt#],FORMAT(p.[COLUMN09 ],'" + dateF + "') AS Date,s.[COLUMN05 ] AS Customer,f.[COLUMN04 ] AS [Reference Order#],p.[COLUMN10 ] AS Reference#,p.[COLUMN12 ] AS Memo,sum(isnull(cast(z.COLUMN11 as decimal(18,2)),0)) [Total Amount],p.COLUMN18 Status,ma.COLUMN09 [Created By] From PUTABLE003 p left outer join PUTABLE004 z on z.COLUMN12=p.COLUMN01 left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05   left outer join   CONTABLE007 o on o.COLUMN02=p.COLUMN13   left outer join  PUTABLE001 f on f.COLUMN02=p.COLUMN06 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where isnull(p.COLUMNA13,0)=0 ";
                    alCol = "Item Receipt#, Date, Customer, Reference Order#, Reference#, Memo, Total Amount, Status, Created By";
                    ob = " p.COLUMN04, p.COLUMN09, s.COLUMN05, f.COLUMN04, p.COLUMN10, p.COLUMN12, z.COLUMN11, p.COLUMN18, ma.COLUMN09";
                }
                //credit memo

                else if (frmID == 1330)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = "SELECT p.[COLUMN04 ] AS [Credit Memo#],FORMAT(p.[COLUMN06 ],'" + dateF + "') AS [Date],(case when p.COLUMN51='22305' then s1.COLUMN05 else s.[COLUMN05 ] end) AS Customer,(select COLUMN04 from PUTABLE001 where COLUMN02=(select COLUMN06 from PUTABLE003 where COLUMN02=p.COLUMN33))  AS [Reference Order#],s9.COLUMN04 [Reference Invoice#],p.COLUMN11 Reference#,p.COLUMN09 Memo,p.[COLUMN15] AS [Total Amount], p.[COLUMN16 ] AS [Status],ma.COLUMN09 [Created By] From SATABLE005 p left outer join  SATABLE002 s on s.COLUMN02=p.COLUMN05 left outer join  SATABLE001 s1 on s1.COLUMN02=p.COLUMN05 left outer join PUTABLE003 o on o.COLUMN02=p.COLUMN33  left outer join  MATABLE009 e on e.COLUMN02=p.COLUMN29 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) left outer join SATABLE009 s9 on s9.COLUMN02=p.COLUMN49  where p.column06 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 ";
                    alCol = " Credit Memo#, Date, Customer, Reference Order#, Reference Invoice#, Reference#, Memo, Total Amount, Status, Created By";
                    ob = " p.COLUMN04, p.COLUMN06, s.COLUMN05, COLUMN04, s9.COLUMN04, p.COLUMN11, p.COLUMN09, p.COLUMN15, p.COLUMN16, ma.COLUMN09";
                }
                else if (frmID == 1602)
                {
                    strQry = "SELECT p.[COLUMN02 ]  ID,p.[COLUMN04 ]  [Resourse Consumption#], FORMAT(p.[COLUMN09 ],'" + dateF + "') [Date],s.[COLUMN05 ]  Customer,  po.[COLUMN05 ]  [Project Name],m.COLUMN04 [Sub Project], o.[COLUMN03] AS [Operating Unit] From PRTABLE007 p left join  SATABLE002 s on s.COLUMN02=p.COLUMN06 and s.COLUMNA03=p.COLUMNA03 and isnull(s.COLUMNA13,0)=0 left join  PRTABLE001 po on po.COLUMN02=p.COLUMN08 and po.COLUMNA03=p.COLUMNA03 and isnull(po.COLUMNA13,0)=0 left join CONTABLE007 o on o.COLUMN02=p.COLUMN11 and o.COLUMNA03=p.COLUMNA03 and isnull(o.COLUMNA13,0)=0 left outer join MATABLE002 m on m.COLUMN02=p.COLUMN22 and m.COLUMNA03=p.COLUMNA03 and isnull(m.COLUMNA13,0)=0  where p.column06 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 and p.COLUMN03=1602";
                    alCol = "ID,Resourse Consumption#,Date,Customer,Project Name,Sub Project,Operating Unit";
                    ob = "p.COLUMN02,p.COLUMN04,p.COLUMN09,s.COLUMN05,po.COLUMN05,m.COLUMN04,o.COLUMN03";
                }
                //Outstanding Payment
                else if (frmID == 1358)
                {
                    strQry = "SELECT   p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Payment NO],FORMAT(p.[COLUMN05 ],'" + dateF + "') AS [Date],f.[COLUMN04] AS [Type Of Payee], e.[COLUMN04] AS [Account], o.[COLUMN03 ] AS [Operating Unit],d.[COLUMN04 ] AS Department From FITABLE016 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN10 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN13 left outer join  FITABLE001 e on e.COLUMN02=p.COLUMN07   left outer join  MATABLE002 f on f.COLUMN02=p.COLUMN06 where p.column07 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 ";

                    alCol = "ID,Payment NO,Date,Type Of Payee,Account,Operating Unit,Department";
                    ob = "p.COLUMN02,p.COLUMN04,p.COLUMN05,f.[COLUMN04],e.COLUMN04,o.COLUMN03, d.COLUMN04";
                }


                //opening balance
                else if (frmID == 1359)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,o.[COLUMN04 ] AS 'Account',p.[COLUMN09 ] AS 'Increased By', p.[COLUMN10] AS 'Decreased By',p.[COLUMN12 ] as 'Memo'" +
                                "From FITABLE018 p left outer join  FITABLE001 o on o.COLUMN02=p.COLUMN08 where isnull(p.COLUMNA13,0)=0 ";

                    alCol = "ID,Account,Increased By,Decreased By,Memo";
                    ob = "p.COLUMN02,o.COLUMN04,p.COLUMN09,p.COLUMN10,p.COLUMN12";
                }
				// Opening Balance Customer & Vendor by venkat
                else if (frmID == 1615)
                {

                    strQry = "SELECT f.[COLUMN02] AS ID,f.[COLUMN04 ] AS 'Opening Balance Customer',f.[COLUMN05 ] AS 'Date',p.[COLUMN12 ] as 'Memo', o.[COLUMN04 ] AS 'Account',p.[COLUMN09] AS 'Increased By', p.[COLUMN10] AS 'Decreased By',s.[COLUMN05] as 'Customer' ,ma.[COLUMN09 ] AS [Created By] From FITABLE018 p left outer join  FITABLE001 o on o.COLUMN02=p.COLUMN08 AND o.COLUMNA03=p.COLUMNA03 left outer join SATABLE002 s on s.column02 = p.column07 AND s.COLUMNA03=p.COLUMNA03 inner join FITABLE049 f on f.COLUMN01=p.COLUMN21 AND f.COLUMNA03=p.COLUMNA03 and isnull(f.COLUMNA13,'False')='False' left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where f.column05 between '" + start + "' and '" + end + "' and f.COLUMN03 = 1615 and isnull(p.COLUMNA13,'False')='False'";
                    //SELECT f.[COLUMN02] AS ID,f.[COLUMN04 ] AS 'Opening Balance Customer',f.[COLUMN05 ] AS 'Date', o.[COLUMN04 ] AS 'Account',p.[COLUMN09] AS 'Increased By', p.[COLUMN10] AS 'Decreased By',p.[COLUMN12 ] as 'Memo',s.[COLUMN05] as 'Customer'  From FITABLE018 p left outer join  FITABLE001 o on o.COLUMN02=p.COLUMN08 AND o.COLUMNA03=p.COLUMNA03 left outer join SATABLE002 s on s.column02 = p.column07 AND s.COLUMNA03=p.COLUMNA03 left outer join FITABLE049 f on f.COLUMN01=p.COLUMN21 AND f.COLUMNA03=p.COLUMNA03 where p.COLUMN03 = 1615 and p.COLUMNA13='False'";

                    alCol = "ID, Opening Balance Customer,Date,Account, Memo, Increased By, Decreased By, Customer, Created By";
                    ob = "f.COLUMN02,f.COLUMN04,f.COLUMN05,p.COLUMN12, o.COLUMN04, p.COLUMN09, p.COLUMN10,  s.COLUMN05, ma.COLUMN09";
                }
                else if (frmID == 1616)
                {

                    strQry = "SELECT f.[COLUMN02 ] AS ID,f.[COLUMN04 ] AS 'Opening Balance Vendor',f.[COLUMN05 ] AS 'Date', o.[COLUMN04 ] AS 'Account',p.[COLUMN09 ] AS 'Increased By', p.[COLUMN10] AS 'Decreased By',p.[COLUMN12 ] as 'Memo',s.[COLUMN05] as 'Vendor'  From FITABLE018 p left outer join  FITABLE001 o on o.COLUMN02=p.COLUMN08 AND o.COLUMNA03=p.COLUMNA03 left outer join SATABLE001 s on  s.COLUMN02=p.COLUMN07 AND s.COLUMNA03=p.COLUMNA03 left outer join FITABLE050 f on f.COLUMN01=p.COLUMN21 AND f.COLUMNA03=p.COLUMNA03  where f.column05 between '" + start + "' and '" + end + "' and p.COLUMN03 = 1616 and p.COLUMNA13='False'";

                    alCol = "ID,Opening Balance Vendor,Date, Account, Increased By, Decreased By, Memo, Vendor";
                    ob = "f.COLUMN02,f.COLUMN04,f.COLUMN05, o.COLUMN04, p.COLUMN09, p.COLUMN10, p.COLUMN12, s.COLUMN05";
                }
            
                //legacy outstanding
                else if (frmID == 1362)
                {
                    strQry = "SELECT   p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Bill#],FORMAT(p.[COLUMN05 ],'" + dateF + "') AS [Date],FORMAT(p.[COLUMN10 ],'" + dateF + "') AS [DueDate], " +
                    " p.[COLUMN08] AS [Amount],f.[COLUMN04] AS [Payment Terms],f1.[COLUMN04] AS [Payment Mode], p.[COLUMN09] AS [Reference#], " +
                    " d.[COLUMN04 ] AS Department, o.[COLUMN03 ] AS [Operating Unit], p.[COLUMN11] AS [Memo] From FITABLE019 p  " +
                    " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN12 " +
                    " left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN15  " +
                    " left outer join  MATABLE002 f on f.COLUMN02=p.COLUMN16 " +
                    " left outer join  MATABLE002 f1 on f1.COLUMN02=p.COLUMN17 where p.column05 between '" + start + "' and '" + end + "' and isnull(p.COLUMNA13,0)=0 ";

                    alCol = "ID,Bill#,Date,DueDate,Amount,Payment Terms,Payment Mode,Reference#,Department,Operating Unit,Memo";
                    ob = "p.COLUMN02,p.COLUMN04,p.COLUMN05,p.COLUMN10,p.COLUMN08,f.COLUMN04,f1.COLUMN04,p.COLUMN09,d.COLUMN04,o.COLUMN03,p.COLUMN11";
                }
                //Issue Cheque
                else if (frmID == 1363)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    //EMPHCS1399	issue cheque-info screen changes(add payee,remove Balance) BY RAJ.Jr
                    strQry = "SELECT p.[COLUMN04 ] AS [Voucher#],FORMAT(p.[COLUMN05],'" + dateF + "') AS [Date],(case when p.COLUMN11='22305' then s1.COLUMN05 when p.COLUMN11='22306' then m1.COLUMN09 when p.COLUMN11='22334' then s1.COLUMN05  when p.COLUMN11='22335' then s2.COLUMN05  when p.COLUMN11='22492' then m1.COLUMN09  else '' end) as Party,'' Reference#,p.COLUMN08 Memo,p.[COLUMN18 ]AS [Total Amount] ,o.[COLUMN04 ] AS [Bank],f44.[COLUMN20 ] AS [Cheque#], p.[COLUMN16 ] AS [Status],ma.COLUMN09 [Created By] From FITABLE020 p left outer join FITABLE001 o on o.COLUMN02=p.COLUMN06   left outer join  MATABLE002 m on m.COLUMN02=p.COLUMN11  left outer join  CONTABLE007 q on q.COLUMN02=p.COLUMN12 left outer join FITABLE022 fi on p.COLUMN01=fi.COLUMN09 left outer join SATABLE001 S1 on s1.COLUMN02=p.COLUMN07 left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN07 left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN07 left join FITABLE044 f44 on f44.COLUMN02=p.COLUMN09 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null)  where p.column05 between '" + start + "' and '" + end + "' and p.COLUMN03=1363 and isnull(p.COLUMNA13,0)=0  ";
                    alCol = "Voucher#, Date, Party, Reference#, Memo, Total Amount, Bank, Cheque#, Status, Created By";
                    ob = "p.COLUMN04, p.COLUMN05, p.COLUMN07, p.COLUMN08, p.COLUMN08, p.COLUMN10, o.[COLUMN04 ], f4.COLUMN20, p.[COLUMN16 ], ma.[COLUMN09 ]";
                }//Receive Payment
                else if (frmID == 1386)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    //EMPHCS1398	Receive payment-info screen chanes(add payee,remove payment method) BY RAJ.Jr 
                    strQry = "SELECT p.[COLUMN04 ] AS [Voucher#],FORMAT(p.[COLUMN05 ],'" + dateF + "') AS [Date],s2.[COLUMN05] as [Party],'' Reference#,p.[COLUMN11 ] AS [Memo],p.[COLUMN19 ] AS [Total Amount],o.[COLUMN04 ] AS [Bank],cq.[COLUMN20 ] AS [Cheque#],p.COLUMN13 Status,ma.COLUMN09 [Created By] From FITABLE023 p left outer join FITABLE001 o on o.COLUMN02=p.COLUMN09   left outer join  MATABLE002 m on m.COLUMN02=p.COLUMN07  left outer join  CONTABLE007 q on q.COLUMN02=p.COLUMN16 left outer join FITABLE024 fi on p.COLUMN01=fi.COLUMN09 left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN08 left outer join FITABLE044 cq on cq.COLUMN02=fi.COLUMN13 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column05 between '" + start + "' and '" + end + "' and p.COLUMN03=1386 and isnull(p.COLUMNA13,0)=0 ";


                    alCol = "Voucher#, Date, Party, Reference#, Memo, Total Amount, Bank, Cheque#, Status, Created By";
                    ob = "p.COLUMN04, p.COLUMN05, s2.COLUMN05, p.[COLUMN11 ], p.[COLUMN11 ], p.COLUMN10, o.[COLUMN04 ], cq.[COLUMN20 ], p.[COLUMN13 ], ma.COLUMN09";
                }
                else if (frmID == 1525)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = "SELECT p.[COLUMN04 ] AS [Voucher#],FORMAT(p.[COLUMN05 ],'" + dateF + "') AS [Date],(case when p.COLUMN11='22305'or p.COLUMN11='22700' then s1.COLUMN05 when p.COLUMN11='22306' then m1.COLUMN09 when p.COLUMN11='22334' then s1.COLUMN05  when p.COLUMN11='22335' then s2.COLUMN05  when p.COLUMN11='22492' then m1.COLUMN09  else '' end) as Party,'' Reference#,fi.COLUMN04 Memo,fi.COLUMN16 [Advance Paid],p.COLUMN10 [Total Amount],o.[COLUMN04 ] AS [Bank],f4.[COLUMN20 ] AS [Cheque#],p.[COLUMN16 ] AS [Status],ma.COLUMN09 [Created By] From FITABLE020 p  left outer join CONTABLE007 q on q.COLUMN02=p.COLUMN12 and p.COLUMNA03=q.COLUMNA03 and isnull(q.columna13,0)=0 left outer join FITABLE022 fi on p.COLUMN01=fi.COLUMN09 and p.COLUMNA03=fi.COLUMNA03 and isnull(fi.columna13,0)=0 left outer join FITABLE001 o on o.COLUMN02=p.COLUMN06  and o.COLUMNA03=p.COLUMNA03 and isnull(o.columna13,0)=0 left outer join FITABLE001 f1 on f1.COLUMN02=fi.COLUMN03 and f1.COLUMNA03=p.COLUMNA03 and isnull(f1.columna13,0)=0 left outer join SATABLE001 S1 on s1.COLUMN02=p.COLUMN07 and s1.COLUMNA03=p.COLUMNA03 and isnull(s1.columna13,0)=0 left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN07 and s2.COLUMNA03=p.COLUMNA03 and isnull(s2.columna13,0)=0 left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN07 and m1.COLUMNA03=p.COLUMNA03 and isnull(m1.columna13,0)=0 left outer join FITABLE044 f4 on f4.COLUMN02=fi.COLUMN12 and f4.COLUMNA03=p.COLUMNA03 and isnull(f4.columna13,0)=0 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null) where p.column05 between '" + start + "' and '" + end + "' and p.COLUMN03=1525 and isnull(p.COLUMNA13,0)=0";

                    alCol = "Voucher#, Date, Party, Reference#,Memo, Advance Paid, Total Amount, Bank, Cheque#, Status, Created By";
                    ob = "p.COLUMN04, p.COLUMN05, p.[COLUMN07 ], fi.COLUMN04, fi.COLUMN04, fi.COLUMN16, p.COLUMN10, o.COLUMN04, f4.[COLUMN20 ], p.[COLUMN16 ], ma.COLUMN09";

                }
                else if (frmID == 1526)
                {//EMPHCS1549	Info screen Headings changes By RAJ.Jr
                    strQry = "SELECT p.[COLUMN04 ] AS [Voucher#],FORMAT(p.[COLUMN05 ],'" + dateF + "') AS [Date],(case when p.COLUMN20='22305' then s1.COLUMN05 when p.COLUMN20='22306' then m1.COLUMN09 when p.COLUMN20='22334' then s1.COLUMN05  when p.COLUMN20='22335' then s2.COLUMN05  when p.COLUMN20='22492' then m1.COLUMN09  else '' end) as Party,'' Reference#,f.COLUMN04 Memo,f.COLUMN17 [Advance Received],p.COLUMN10 [Total Amount],o.[COLUMN04 ] AS [Bank],f4.[COLUMN20 ] AS [Cheque#],p.[COLUMN13 ] AS [Status],ma.COLUMN09 [Created By] From FITABLE023 p left outer join FITABLE024 f on f.COLUMN09=p.COLUMN01 AND f.COLUMNA03=p.COLUMNA03 AND ISNULL(f.COLUMNA13,0)=0 left outer join FITABLE001 o on o.COLUMN02=f.COLUMN12 AND o.COLUMNA03=p.COLUMNA03 AND ISNULL(o.COLUMNA13,0)=0 left outer join FITABLE001 f1 on f1.COLUMN02=f.COLUMN03 AND f1.COLUMNA03=p.COLUMNA03 AND ISNULL(f1.COLUMNA13,0)=0 left outer join  MATABLE002 m on m.COLUMN02=p.COLUMN07  AND m.COLUMNA03=p.COLUMNA03 AND ISNULL(m.COLUMNA13,0)=0 left outer join  CONTABLE007 q on q.COLUMN02=p.COLUMN16 AND q.COLUMNA03=p.COLUMNA03 AND ISNULL(q.COLUMNA13,0)=0 left outer join SATABLE001 S1 on s1.COLUMN02=p.COLUMN08 AND S1.COLUMNA03=p.COLUMNA03 AND ISNULL(S1.COLUMNA13,0)=0 left outer join SATABLE002 s2 on s2.COLUMN02=p.COLUMN08 AND s2.COLUMNA03=p.COLUMNA03 AND ISNULL(s2.COLUMNA13,0)=0 left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN08 AND m1.COLUMNA03=p.COLUMNA03 AND ISNULL(m1.COLUMNA13,0)=0 left outer join FITABLE044 f4 on f4.COLUMN02=f.COLUMN13 AND f4.COLUMNA03=p.COLUMNA03 AND ISNULL(f4.COLUMNA13,0)=0 left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and (Ma.COLUMNA02=p.COLUMNA02 or Ma.COLUMNA02 is null)  where p.column05 between '" + start + "' and '" + end + "' and p.COLUMN03=1526 and isnull(p.COLUMNA13,0)=0 ";

                    alCol = "Voucher#, Date, Party, Reference#, Memo, Advance Received, Total Amount, Bank, Cheque#, Status, Created By";
                    ob = "p.COLUMN04, p.COLUMN05, p.[COLUMN08 ], f.[COLUMN04 ], f.[COLUMN04 ], f.[COLUMN17 ], p.COLUMN10, o.[COLUMN04 ], f4.COLUMN20, p.COLUMN13, ma.[COLUMN09]";
                }
                //Contact
                else if (frmID == 1388)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Contact#],p.[COLUMN05 ] AS [Name],m.[COLUMN04]  AS [Type],(case when p.COLUMN10 in (22305) then v.[COLUMN05] else c.[COLUMN05] end) Company#, p.[COLUMN12 ] AS [Designation],p.[COLUMN16] AS [Mobile#],d.[COLUMN04 ] AS [Department],q.[COLUMN04 ] AS [Operating Unit] From MATABLE018 p " +
                    "left outer join FITABLE001 o on o.COLUMN02=p.COLUMN09 left outer join  MATABLE002 m on m.COLUMN02=p.COLUMN10 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN19 left outer join  SATABLE001 v on v.COLUMN02=p.COLUMN11 left outer join  SATABLE002 c on c.COLUMN02=p.COLUMN11 left outer join  CONTABLE007 q on q.COLUMN02=p.COLUMN20   where isnull(p.COLUMNA13,0)=0 ";

                    alCol = "ID,Contact#,Name,Type,Company#,Designation,Mobile#,Department,Operating Unit";
                    ob = "p.COLUMN02,p.COLUMN04,p.COLUMN05,m.[COLUMN04],p.[COLUMN10 ],p.COLUMN12,p.COLUMN16,d.[COLUMN04 ], q.[COLUMN04 ]";
                }
                //Scheduled Activity
                else if (frmID == 1366)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Activity NO],p.[COLUMN05 ] AS [Description],FORMAT(p.[COLUMN06 ],'" + dateF + "') AS [Created Date], FORMAT(p.[COLUMN07 ],'" + dateF + "') AS [Start Date], FORMAT(p.[COLUMN08 ],'" + dateF + "') AS [End Date],m.[COLUMN04 ] AS [Status],q.[COLUMN04 ] AS [Operating Unit] From SETABLE001 p left outer join MATABLE002 m on m.COLUMN02=p.COLUMN09  left outer join  CONTABLE007 q on q.COLUMN02=p.COLUMN11 where isnull(p.COLUMNA13,0)=0 ";

                    alCol = "ID,Activity NO,Description,Created Date,Start Date,End Date,Status,Operating Unit";
                    ob = "p.COLUMN02,p.COLUMN04,p.COLUMN05,p.[COLUMN06 ],p.[COLUMN07 ],p.COLUMN08,m.COLUMN04,q.[COLUMN04 ]";
                }
            else if (frmID == 1367)
            {
                strQry = "SELECT p.[COLUMN02] AS ID , p.COLUMN04 AS [Task#], p.COLUMN05 AS [Description], m.COLUMN09 AS [Assigned By], m1.COLUMN09 AS [Assigned To], pr.COLUMN04 AS [Priority], s.COLUMN04 AS [Status], FORMAT(p.COLUMN09,'" + dateF + "') AS [Expected Start Date], FORMAT(p.COLUMN10,'" + dateF + "') AS [Expected End Date], FORMAT(p.COLUMN11,'" + dateF + "') AS [Actual Start Date], FORMAT(p.COLUMN12,'" + dateF + "') AS [Actual End Date], p.COLUMN13 AS [Duration], p.COLUMN19 AS [Memo], d.COLUMN04 AS [Department], o.COLUMN04 AS [Operating Unit] FROM SETABLE002 p left outer join MATABLE010 m on m.COLUMN02=p.COLUMN06 left outer join MATABLE010 m1 on m1.COLUMN02=p.COLUMN07 left outer join MATABLE002 pr on pr.COLUMN02=p.COLUMN08 left outer join MATABLE002 s on s.COLUMN02=p.COLUMN14 left outer join MATABLE002 d on d.COLUMN02=p.COLUMN18 left outer join CONTABLE007 o on o.COLUMN02=p.COLUMN15 where p.COLUMN02 != ''";
                alCol = "ID, Task#, Description, Assigned By, Assigned To, Priority, Status, Expected Start Date, Expected End Date, Actual Start Date, Actual End Date, Duration, Memo, Department, Operating Unit";
                ob = "COLUMN02, COLUMN04, COLUMN05, COLUMN06, COLUMN07, COLUMN08, COLUMN14, COLUMN09, COLUMN10, COLUMN11, COLUMN12, COLUMN13, COLUMN19, COLUMN18, COLUMN04";
            }
                //KPIS CREATION
                else if (frmID == 1372)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS 'Type#',p.[COLUMN05 ] AS 'Name',p.[COLUMN06 ] AS 'SQL Query'," +
                    "l.[COLUMN04 ] AS 'Graph Type', p.[COLUMN08 ] AS 'X-Axis',p.[COLUMN09 ] AS 'Y-Axis',p.[COLUMN11 ] AS 'Purpose', d.[COLUMN04] AS Department, o.[COLUMN03] AS 'Operating Unit'" +
                    " From SETABLE007 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN13  left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN16  left outer join  MATABLE002 l on l.COLUMN02=p.COLUMN07  where isnull(p.COLUMNA13,0)=0 ";
                    alCol = "ID,Type#,Name,SQL Query,Graph Type,X-Axis,Y-Axis,Purpose,Department,Operating Unit";
                    ob = " p.COLUMN02 ,p.COLUMN04,p.COLUMN05,p.COLUMN06,p.COLUMN07,l.COLUMN04,p.COLUMN09,p.COLUMN11,d.COLUMN04,o.COLUMN03";
                }
                //Master Forms
                // vendar Master
                else if (frmID == 1252)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID, p.[COLUMN04 ] AS 'Vendor#',p.[COLUMN05] AS 'Name',sc.[COLUMN04 ] AS Type,c.[COLUMN04 ] AS Category," +
                    "b.[COLUMN04 ] AS 'Sub Category',p.[COLUMN11 ] as Email,p.[COLUMN12 ] as Phone, p.[COLUMN23 ] as 'TIN#',p.[COLUMN24 ] as Status From SATABLE001 p " +
                    " left outer join  MATABLE002 c on c.COLUMN02=p.COLUMN07  left outer join  MATABLE002 sc on sc.COLUMN02=p.COLUMN22 left outer join  MATABLE002 b on sc.COLUMN02=p.COLUMN08 where p.COLUMN24='False' and p.COLUMNA13='False' ";
                    alCol = "ID,Vendor#,Name,Type,Category,Sub Category,Email,Phone,TIN#,Status";
                    ob = " p.COLUMN02, p.COLUMN04,p.COLUMN05,sc.COLUMN04,c.COLUMN04,b.COLUMN04,p.COLUMN11,p.COLUMN12,p.COLUMN23,p.COLUMN24";
                }
                // RECEIPT Type
                else if (frmID == 1280)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS 'Name',p.[COLUMN05 ] AS 'Description',p.[COLUMN06 ] AS 'Default Flag',p.COLUMN11 'Overage%', d.[COLUMN04 ] AS Department ," +
                    "o.[COLUMN03] AS OperatingUnit,p.[COLUMN12] AS 'Inactive Flag' From MATABLE011 p left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN07 left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN10  where p.COLUMNA13='False' ";
                    alCol = "ID,Name,Description,Default Flag,Overage%,Department,OperatingUnit,Inactive Flag";
                    ob = " p.COLUMN02 ,p.COLUMN04,p.COLUMN05,p.COLUMN06,p.COLUMN11,d.COLUMN04,o.COLUMN03,p.COLUMN12";
                }
                // customer Master
                else if (frmID == 1265)
                {
                    strQry = "SELECT p.COLUMN02  AS ID, p.COLUMN04  AS 'Customer#',p.COLUMN05 AS 'Name', c.COLUMN04 AS 'Category',sc.COLUMN04  AS 'Sub Category', p.COLUMN10  as 'Email', p.COLUMN11  as 'Phone',p.COLUMN21 'TIN#'," +
                    "e.COLUMN06 AS 'Sales Rep',p.COLUMN23 AS 'Commission%',st.COLUMN04  AS 'Type' From SATABLE002 p " +
                    //EMPHCS1819 rajasekhar reddy patakota 21/09/2016 Info Grid Customisation for dynamic pages
					"left outer join  MATABLE002 c on c.COLUMN02=p.COLUMN07   left outer join  MATABLE002 sc on sc.COLUMN02=p.COLUMN08 left outer join  MATABLE010 e on e.COLUMN02=p.COLUMN22 and e.COLUMN30='True'  left outer join  MATABLE002 st on sc.COLUMN02=p.COLUMN24 where p.COLUMN25='False' and p.COLUMNA13='False' ";

                    alCol = "ID,Customer#,Name,Category,Sub Category,Email,Phone,TIN#,Sales Rep,Commission%,Type";
                    ob = " p.COLUMN02, p.COLUMN04,p.COLUMN05,c.COLUMN04,sc.COLUMN04,p.COLUMN10,p.COLUMN11,p.COLUMN21,e.COLUMN06,p.COLUMN23,st.COLUMN04";
                }
                // order Type
                else if (frmID == 1279)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS 'Name',p.[COLUMN05 ] AS 'Description',p.[COLUMN06 ] AS 'Default Flag', d.[COLUMN04 ] AS Department,o.[COLUMN03] AS 'Operating Unit',p.COLUMN11 'Inactive Flag' From MATABLE009 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN07  left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN10  left outer join  CONTABLE009 s on s.COLUMN02=p.COLUMN08  left outer join  MATABLE002 c on c.COLUMN02=p.COLUMN09  where p.COLUMNA13='False' ";
                    alCol = "ID,Name,Description,Default Flag,Department,Operating Unit,Inactive Flag";
                    ob = " p.COLUMN02 ,p.COLUMN04,p.COLUMN05,p.COLUMN06,d.COLUMN04,o.COLUMN03,p.COLUMN11";
                }
                //item master
                else if (frmID == 1261)
                {
				//EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
				    //EMPHCS1819 rajasekhar reddy patakota 21/09/2016 Info Grid Customisation for dynamic pages
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS [Name],P.[COLUMN50] AS [Description],g.[COLUMN04] AS [Group], b.[COLUMN04] AS [Brand],p.[COLUMN45] AS [Distribution Price],iif(isnull(r.column04,0)>0,r.column04, p.[COLUMN17 ]) AS [Purchase Price] From MATABLE007 p " +
                    "left outer join  MATABLE024 r on r.COLUMN07=p.COLUMN02 and r.column06='Purchase' and  (r.COLUMN03 in(" + opu + ") or isnull(r.COLUMN03,0)=0) and p.columna03=r.columna03 and isnull(r.columna13,0)=0   left outer join  MATABLE008 d on d.COLUMN02=p.COLUMN05   left join  MATABLE004 g on g.COLUMN02=p.COLUMN11 and g.COLUMNA03=p.COLUMNA03 and isnull(g.COLUMNA13,0)=0 left outer join  MATABLE005 b on b.COLUMN02=p.COLUMN10 where isnull(p.COLUMNA13,0)=0 ";
                    alCol = "ID,Name,Description,Group,Brand,Distribution Price,Purchase Price";
                    ob = "p.COLUMN02,p.COLUMN04,p.COLUMN50,g.COLUMN04,b.COLUMN04,p.COLUMN45, p.COLUMN17";
                }
                //Item Family
                else if (frmID == 1262)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS 'Name',p.[COLUMN05 ] AS 'Description',p.[COLUMN10] AS 'Overage%', d.[COLUMN04 ] AS Department,o.[COLUMN03] AS 'Operating Unit', p.[COLUMN12] AS 'Inactive Flag'" +
                    " From MATABLE003 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN06  left outer join MATABLE002 d on d.COLUMN02=p.COLUMN09   left outer join  CONTABLE009 s on s.COLUMN02=p.COLUMN07  left outer join  MATABLE002 c on c.COLUMN02=p.COLUMN08 where isnull(p.COLUMNA13,0)=0 ";
                    alCol = "ID,Name,Description,Overage%,Department,Operating Unit,Inactive Flag";
                    ob = " p.COLUMN02 ,p.COLUMN04,p.COLUMN05,p.COLUMN10,d.COLUMN04,o.COLUMN03, p.COLUMN12";
                }
                // Item Group
                else if (frmID == 1264)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS 'Name',p.[COLUMN05 ] AS 'Description',i.[COLUMN04 ] AS 'Item Family', d.[COLUMN04 ] AS Department, o.[COLUMN03] AS 'Operating Unit', p.[COLUMN11] AS 'Overage%',p.[COLUMN12] AS 'Inactive Flag' " +
                    " From MATABLE004 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN07   left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN10  left outer join  MATABLE003 i on i.COLUMN02=p.COLUMN06  where isnull(p.COLUMNA13,0)=0 ";
                    alCol = "ID,Name,Description,Item Family,Department,Operating Unit,Overage%,Inactive Flag";
                    ob = " p.COLUMN02 ,p.COLUMN04,p.COLUMN05,i.COLUMN04,d.COLUMN04,o.COLUMN03,p.COLUMN11,p.COLUMN12";
                }
                // Item Brand
                else if (frmID == 1263)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS 'Name',p.[COLUMN05 ] AS 'Description',v.[COLUMN05 ] AS 'Vendor', d.[COLUMN04 ] AS Department, o.[COLUMN03] AS 'Operating Unit', p.[COLUMN11] AS 'Overage%',p.[COLUMN12] AS 'Inactive Flag'" +
                    " From MATABLE005 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN07   left outer join    MATABLE002 d on d.COLUMN02=p.COLUMN10  left outer join  SATABLE003 v on v.COLUMN02=p.COLUMN06 where isnull(p.COLUMNA13,0)=0 ";
                    alCol = "ID,Name,Description,Vendor,Department,Operating Unit,Overage%,Inactive Flag";
                    ob = " p.COLUMN02 ,p.COLUMN04,p.COLUMN05,v.COLUMN05,d.COLUMN04,o.COLUMN03,p.COLUMN11, p.COLUMN12";
                }
                //MATRIX ITEM MASTER
                else if (frmID == 1285)
                {
                    strQry = " SELECT p.[COLUMN02 ] AS ID,p.[COLUMN41] AS [Item#],p.[COLUMN04 ] AS [Name],d1.[COLUMN04] AS [Color],d2.[COLUMN04] AS [Style],d3.[COLUMN04] AS [Size],b.[COLUMN04] AS [Brand],p.[COLUMN06] AS UPC, o.[COLUMN03] AS 'Operating Unit',d.[COLUMN04 ] AS Department From MATABLE007 p" +
                    " left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN37   left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN40   left outer join  MATABLE002 d1 on d1.COLUMN02=p.COLUMN42   left outer join  MATABLE002 d2 on d2.COLUMN02=p.COLUMN43   left outer join  MATABLE002 d3 on d3.COLUMN02=p.COLUMN44   left outer join  MATABLE005 b on b.COLUMN02=p.COLUMN10   left outer join  SATABLE005 j on j.COLUMN02=p.COLUMN06 where isnull(p.COLUMNA13,0)=0 ";
                    alCol = "ID,Item#,Name,Color,Style,Size,Brand,upc,Operating Unit,Department";
                    ob = "p.COLUMN02,p.COLUMN41,p.COLUMN04,d1.COLUMN04,d2.COLUMN04,d3.COLUMN04,b.COLUMN04,p.COLUMN06,o.COLUMN03, d.COLUMN04";
                }
                //EMPHCS1497	Item alias form Creation in inventory master By RAJ.Jr
                else if (frmID == 1530)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,p.[COLUMN04 ] AS 'UPC#',i.[COLUMN04] AS 'Item',d.COLUMN04 Units,p.COLUMN07 as Quantity,v.[COLUMN05 ] AS 'Vendor', p.[COLUMN09 ] AS Price,d1.COLUMN04 as Type, o.[COLUMN03] AS 'Operating Unit' From MATABLE021 p  left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN11   left outer join    MATABLE002 d on d.COLUMN02=p.COLUMN06  left outer join  SATABLE001 v on v.COLUMN02=p.COLUMN08 left outer join MATABLE007 i on i.COLUMN02=p.COLUMN05 left outer join    MATABLE002 d1 on d1.COLUMN02=p.COLUMN10 where p.COLUMNA13='False' ";
                    alCol = "ID, UPC#, Item, Units, Quantity, Vendor, Price, Type, Operating Unit";
                    ob = " p.COLUMN02 , p.COLUMN04, i.COLUMN04, d.COLUMN04, p.COLUMN07, v.COLUMN05, p.COLUMN09, d1.COLUMN04, o.COLUMN03";
                }
                // Account Master
                else if (frmID == 1250)
                {
                    //EMPHCS1494	COA Reports Changes  BY RAJ.Jr 05/01/2016
                    strQry = "SELECT p.COLUMN02 AS ID,p.COLUMN04 AS 'Account Name',d.COLUMN04 as 'Type',p.[COLUMN05] AS 'Sub Description', sum(isnull(p.COLUMN08,0)) AS 'Opening Balance',	  p.[COLUMN07] AS Typ From FITABLE001 p left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN07 where p.COLUMNA13='False' ";
                    alCol = "Account Name,Type,Sub Description,Opening Balance";
                    ob = "  p.COLUMN04,d.COLUMN04,p.COLUMN05,o.COLUMN04";
                }
                //ExpenceType Master
                else if (frmID == 1292)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID, p.[COLUMN04 ] AS [Name], p.[COLUMN05 ] AS [Description],p.COLUMN11 Memo,d.[COLUMN04 ] AS Department, o.[COLUMN03 ] AS [Operating Unit] From FITABLE011 p   left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN06   left outer join  MATABLE002 d on d.COLUMN02=p.COLUMN09 where p.COLUMNA13='False' ";
                    alCol = "ID,Name,Description,Memo,Department,Operating Unit";
                    ob = "p.COLUMN02,p.COLUMN04,P.COLUMN05,P.COLUMN11,d.COLUMN04, o.COLUMN03";
                }
                // Tax Master
                else if (frmID == 1322)
                {
                    //EMPHCS782 rajasekhar reddy patakota 28/7/2015  Tax Master  record display in list after save
                    strQry = "SELECT p.COLUMN02  ID, p.COLUMN04  'Name', p.COLUMN08  'Description',MA02.COLUMN04  'Filling Frequency', case when p.COLUMN06=1 then 'YES' else 'FALSE' end   'Applies to Sales ',case when p.COLUMN13=1 then 'YES' else 'FALSE' end   'Applies to Purchase',case when p.COLUMN14=1 then 'YES' else 'FALSE' end  'Applies to VAS' From MATABLE013  p left join MATABLE002 MA02 on MA02.COLUMN02=p.COLUMN05 where p.COLUMNA13='False' ";
                    alCol = "ID,Name,Description,Filling Frequency,Applies to Sales ,Applies to Purchase,Applies to VAS";
                    ob = " p.COLUMN02, p.COLUMN04,p.COLUMN08,p.COLUMN05,p.COLUMN06,p.COLUMN13,p.COLUMN14";
                }
                // Tax Group Master
                else if (frmID == 1323)
                {
                    strQry = "SELECT p.COLUMN02  ID, p.COLUMN04  'Name', p.COLUMN08  'Description',MA02.COLUMN04  'Filling Frequency',case when p.COLUMN13=1 then 'YES' else 'FALSE' end   'Applies to Sales ',case when p.COLUMN07=1 then 'YES' else 'FALSE' end   'Applies to Purchase',case when p.COLUMN14=1 then 'YES' else 'FALSE' end  'Applies to VAS' From MATABLE014  p inner join MATABLE002 MA02 on MA02.COLUMN02=p.COLUMN06 where p.COLUMNA13='False' ";
                    alCol = "ID,Name,Description,Filling Frequency,Applies to Sales ,Applies to Purchase,Applies to VAS";
                    ob = " p.COLUMN02, p.COLUMN04,p.COLUMN08,p.COLUMN06,p.COLUMN13,p.COLUMN07, p.COLUMN14";
                }
                // employee Master2
                else if (frmID == 1260)
                {
                    strQry = "SELECT p.COLUMN02  AS ID, p.COLUMN04  AS 'Employee#', p.COLUMN09  as 'Name',p.COLUMN10  as 'Job Title'," +
                    "p.COLUMN30  as 'Sales Rep',p.COLUMN14  as 'Phone',c.COLUMN04  AS Department, o.COLUMN03 AS 'Operating Unit',p.COLUMN32 AS Status From MATABLE010 p left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN26  left outer join  MATABLE002 c on c.COLUMN02=p.COLUMN29  where isnull(p.COLUMNA13,0)=0 ";
                    alCol = "ID,Employee#,Name,Job Title,Sales Rep,Phone,Department,Operating Unit,Status";
                    ob = " p.COLUMN02, p.COLUMN04,p.COLUMN09,p.COLUMN10,p.COLUMN30,p.COLUMN14,c.COLUMN04, o.COLUMN03,p.COLUMN32";
                }
                //EMPHCS1140 Item Pricing for Multi Operating Unit level by Srinivas 13/09/2015
                else if (frmID == 1500)
                {
                    strQry = "SELECT p.[COLUMN02 ] AS ID,o.[COLUMN03] AS 'Operating Unit',d.[COLUMN04 ] AS Item,d.[COLUMN06] AS [UPC],p.[COLUMN09 ] AS [Purchage Price],p.[COLUMN10] AS [Sales Price],p.[COLUMN11] AS [Last Purchase Price],p.[COLUMN12] AS [Avarage Price]  From FITABLE039 p left outer join  CONTABLE007 o on o.COLUMN02=p.COLUMN04   left outer join  MATABLE007 d on d.COLUMN02=p.COLUMN05 where p.COLUMNA13='False' ";
                    alCol = "ID,Operating Unit,Item,UPC,Purchage Price,Sales Price,Last Purchase Price,Avarage Price";
                    ob = "p.COLUMN02,o.COLUMN03,d.COLUMN04,d.COLUMN06,p.COLUMN09,p.COLUMN10,p.COLUMN11,p.COLUMN12";
                }
            }
            //Misc
            else
            {
                if (strQry == null)
                {
				//EMPHCS1592 rajasekhar reddy patakota 08/03/2016 Custom View Changes for Sales Order
                    strQry += "SELECT p.[COLUMN02 ] AS ID ";
                    alCol += ("\"ID\"");
                    //actCol += ("\"{0}\"");
                    ob += "ID";
                }
				//EMPHCS1592 rajasekhar reddy patakota 08/03/2016 Custom View Changes for Sales Order
                var fltbl = ""; var falcol = "";
                for (int i = 1; i < fldsV.Count; i++)
                {
                    var actcol=fldsV[i].COLUMN06;var aliascol=fldsV[i].COLUMN07;var ctblid=fldsV[i].COLUMN04;var tbl ="";var ltbl ="";var alcol ="";
                        var formlist = db.CONTABLE006.Where(q => q.COLUMN04 == ctblid && q.COLUMN03 == frmID && q.COLUMN05 == actcol).FirstOrDefault();
                        SqlCommand cmdp = new SqlCommand("SELECT COLUMN04,COLUMN03 Parent_Internal_ID FROM CONTABLE005 WHERE COLUMN05 LIKE '%Parent_Internal_ID%'  and COLUMN03 =" + ctblid + " ", cn);
                            SqlDataAdapter dap = new SqlDataAdapter(cmdp);
                            DataTable dtp = new DataTable();
                            dap.Fill(dtp);
                            alcol = "p." + fldsV[i].COLUMN06;
                            if (dtp.Rows.Count > 0)
                            {
                                var ltblid=Convert.ToInt32( dtp.Rows[0][1].ToString());
                                ltbl = db.CONTABLE004.Where(a => a.COLUMN02 == ltblid).FirstOrDefault().COLUMN04;
                                alcol = "q." + fldsV[i].COLUMN06;
                                fltbl = ltbl; falcol = dtp.Rows[0][0].ToString();
                            }
                            if (formlist.COLUMN10 == "DropDownList") {                            
                            if (formlist.COLUMN14 == "Control Value") tbl = "MATABLE002";
                            else
                                tbl = db.CONTABLE004.Where(a => a.COLUMN02 == formlist.COLUMN15).FirstOrDefault().COLUMN04;
                            var col = "COLUMN04";
                            if (tbl == "SATABLE001" || tbl == "SATABLE002") col = "COLUMN05";
                            else if (tbl == "MATABLE010") col = "COLUMN09";
                            else if (tbl == "CONTABLE008"||tbl == "CONTABLE007"||tbl == "CONTABLE009") col = "COLUMN03";
                            strQry += ",  (select " + col + " from " + tbl + " where column02=" + alcol + ") AS [" + fldsV[i].COLUMN07 + "]  ";
                            alCol += (",\"" + fldsV[i].COLUMN07 + "\"");
                            ob += "," + fldsV[i].COLUMN06 + "";
                            if (i == fldsV.Count - 1)
                            {
                                if (fltbl != "")
                                    strQry += " ,ma.COLUMN09 [Created By] FROM " + table + " p inner join  " + fltbl + " q on p.COLUMN01=q." + falcol + " and q.COLUMNA13=0";
                                else
                                    strQry += " ,ma.COLUMN09 [Created By] FROM " + table + " p ";
                            }
                        }
                        else if (i == fldsV.Count - 1)
                        {
                            var col = alcol;
                            if (formlist.COLUMN10 == "DatePicker")
                                col = "convert(varchar," + alcol + ", 106)";
                            if (fltbl != "")
                                strQry += ", " + col + " AS [" + fldsV[i].COLUMN07 + "] ,ma.COLUMN09 [Created By] FROM " + table + " p inner join  " + fltbl + " q on p.COLUMN01=q." + falcol + " and q.COLUMNA13=0";
                            else
                                strQry += ", " + col + " AS [" + fldsV[i].COLUMN07 + "] ,ma.COLUMN09 [Created By] FROM " + table + " p ";
                            alCol += (",\"" + fldsV[i].COLUMN07 + "\"");
                            ob += "," + fldsV[i].COLUMN06 + "";
                        }
                        else
                        {
                            var col = alcol;
                            if (formlist.COLUMN10 == "DatePicker")
                                col = "convert(varchar," + alcol + ", 106)";
                            strQry += ", " + col + " AS [" + fldsV[i].COLUMN07 + "]";
                             alCol += (",\"" + fldsV[i].COLUMN07 + "\"");
                             ob += "," + fldsV[i].COLUMN06 + "";
                        }
                    }
					//EMPHCS1592 rajasekhar reddy patakota 08/03/2016 Custom View Changes for Sales Order
                 alCol += (",\"Created By\"");
                            ob += ", COLUMN09";
                            strQry += "  left outer join MATABLE010 ma on ma.COLUMN02=p.columna08 and ma.COLUMNA03=p.COLUMNA03 and Ma.COLUMNA02=p.COLUMNA02 where p.COLUMNA13=0 ";
            }
            strQry += CLOS ;
            string viewSort = "";
            object gridsort = "";
            object grdcol = "";
            object SortByStatus = "";
            eBizSuiteTableEntities ety = new eBizSuiteTableEntities();
            string customfrmID = "";
            var fid = db.CONTABLE0010.Where(q => q.COLUMN06 == frmID && q.COLUMN05 == "Custom").OrderBy(a => a.COLUMN02).ToList();
            if (fid.Count > 0)
            {
                for (int i = 0; i < fid.Count; i++) { customfrmID += fid[i].COLUMN02.ToString() + ","; }
                customfrmID = customfrmID.TrimEnd(',');
            }
            else { customfrmID = frmID.ToString(); }
            if (frmID == 1272)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "IR" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                gridsort = " and p.COLUMN17 = 1001";
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                else if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN18 ";
                    SortByStatus = sortByStatus;
                }

            }
            else if (frmID == 1251)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "PO" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                else if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus.ToString();
                }
                gridsort = " and p.COLUMN29 = 1001 and (p.COLUMN03=1251 or p.COLUMN03 in(" + customfrmID + ")) ";
            }
            else if (frmID == 1374)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "PQ").Select(a => a.COLUMN04).ToList();
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus;
                }
                gridsort = " and p.COLUMN29 = 1001 and  (p.COLUMN03=1374 or p.COLUMN03 in(" + customfrmID + ")) ";
            }
            else if (frmID == 1328)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "SR" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus;
                }
                gridsort = " and p.COLUMN29 = 1003 and  (p.COLUMN03=1328 or p.COLUMN03 in(" + customfrmID + ")) ";
            }
            else if (frmID == 1329)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "RR" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus;
                }
                gridsort = " and p.COLUMN17 = 1003 and  (p.COLUMN03=1329 or p.COLUMN03 in(" + customfrmID + ")) ";
            }
            else if (frmID == 1330)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "CM").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus;

                }
                gridsort = " and p.COLUMN29 = 1003 and  (p.COLUMN03=1330 or p.COLUMN03 in(" + customfrmID + ")) ";
            }
            else if (frmID == 1273)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "BL" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                else if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN13 ";
                    SortByStatus = sortByStatus;
                }
                gridsort = " and p.COLUMN20 = 1001 ";
            }
            else if (frmID == 1274)
            {

                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "PB").Select(a => a.COLUMN04).ToList();
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN11 ";
                    SortByStatus = sortByStatus;
                }
                gridsort = " and p.COLUMN17 = 1001 ";
            }
            else if (frmID == 1275)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "SO" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                else if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus.ToString();
                }

                gridsort = "  and  p.COLUMN29 = 1002 ";
            }
            else if (frmID == 1276)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "IF" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                else if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN14 ";
                    SortByStatus = sortByStatus;
                }
                gridsort = "  and p.COLUMN20 = 1002 ";
            }
            else if (frmID == 1277)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "IV" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                else if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN13 ";
                    SortByStatus = sortByStatus;

                }
                gridsort = " and  p.COLUMN19 = 1002 ";
            }
				// oppurtinity Filter Changes By Venkat 
                 else if (frmID == 1375)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "OP" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL"||Convert.ToString(sortByStatus) == "OPEN")
                {
                    sortByStatus = null;
                }
                else if (sortByStatus != null)
                {
                    grdcol = "so.COLUMN04";
                    SortByStatus = sortByStatus;

                }
                gridsort = "";
            } 
            else if (frmID == 1278)
            {

                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "PM").Select(a => a.COLUMN04).ToList();
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN11 ";
                    SortByStatus = sortByStatus;
                }
                gridsort = "  and p.COLUMN18 = 1002 ";
            }
            else if (frmID == 1283)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "JOO" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL" || Convert.ToString(sortByStatus) == "OPEN")
                {
                    sortByStatus = null;
                }
                else if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus;
                }

                gridsort = " and p.COLUMN29 = 1000 ";
            }
            else if (frmID == 1284)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "JIO" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL" || Convert.ToString(sortByStatus) == "OPEN")
                {
                    sortByStatus = null;
                }
                else if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN14 ";
                    SortByStatus = sortByStatus;
                }

                gridsort = " and p.COLUMN20 = 1000 ";
            }
            else if (frmID == 1286)
            {
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN18 ";
                    SortByStatus = sortByStatus;
                }
                info.statusdata = new List<string> { };
                gridsort = " and p.COLUMN17 = 1000 ";
            }
            else if (frmID == 1287)
            {
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN13 ";
                    SortByStatus = sortByStatus;
                }
                info.statusdata = new List<string> { };
                gridsort = " and p.COLUMN19 = 1000 ";
            }
            else if (frmID == 1288)
            {
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN11 ";
                    SortByStatus = sortByStatus;
                }
                info.statusdata = new List<string> { };
                gridsort = " and p.COLUMN18 = 1000 ";
            }
            else if (frmID == 1353)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "PR" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus;
                }
                gridsort = " and p.COLUMN29 = 1003 and  (p.COLUMN03=1353 or p.COLUMN03 in(" + customfrmID + ")) ";
            }
            else if (frmID == 1354)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "RI" || a.COLUMN03 == "ALL").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus;
                }
                gridsort = " and p.COLUMN20 = 1003 and   (p.COLUMN03=1354 or p.COLUMN03 in(" + customfrmID + ")) ";
            }
            else if (frmID == 1355)
            {
                info.statusdata = ety.CONTABLE025.Where(a => a.COLUMN03 == "RF").Select(a => a.COLUMN04).ToList();
                if (Convert.ToString(sortByStatus) == "ALL")
                {
                    sortByStatus = null;
                }
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN16 ";
                    SortByStatus = sortByStatus;
                }
                gridsort = " and p.COLUMN29 = 1003 and   (p.COLUMN03=1355 or p.COLUMN03 in(" + customfrmID + ")) ";
            }
            else if (frmID == 1285)
            {
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN05 ";
                    SortByStatus = sortByStatus;
                }
                info.statusdata = new List<string> { };
                gridsort = " and p.COLUMN05 = 'ITTY008' ";
            }
            else if (frmID == 1261)
            {
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN05 ";
                    SortByStatus = sortByStatus;
                }
                info.statusdata = new List<string> { };
                SqlDataAdapter cmdd2 = new SqlDataAdapter("select COLUMN02, COLUMN04 from MATABLE008", cn);
                DataTable dtdata2 = new DataTable();
                cmdd2.Fill(dtdata2);
                for (int dd = 0; dd < dtdata2.Rows.Count; dd++)
                {
                    info.Country.Add(new SelectListItem { Value = dtdata2.Rows[dd]["COLUMN02"].ToString(), Text = dtdata2.Rows[dd]["COLUMN04"].ToString() });
                }
            }
            else if (frmID == 1250)
            {
                if (sortByStatus != null)
                {
                    grdcol = "p.COLUMN07 ";
                    SortByStatus = sortByStatus;
                }
                info.statusdata = new List<string> { };
                SqlDataAdapter cmdd2 = new SqlDataAdapter("select COLUMN02, COLUMN04 from MATABLE002 where column03=11115 and columna02 is null and columna03 is null", cn);
                DataTable dtdata2 = new DataTable();
                cmdd2.Fill(dtdata2);
                for (int dd = 0; dd < dtdata2.Rows.Count; dd++)
                {
                    info.Country.Add(new SelectListItem { Value = dtdata2.Rows[dd]["COLUMN02"].ToString(), Text = dtdata2.Rows[dd]["COLUMN04"].ToString() });
                }
            }
            else
            {
                info.statusdata = new List<string> { };
            }
            strQry = strQry + gridsort;
            if (sortByStatus != null)
            {//EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
                if (frmID == 1582)
					//EMPHCS1819 rajasekhar reddy patakota 21/09/2016 Info Grid Customisation for dynamic pages
                    strQry = strQry + " AND  (P.COLUMNA02 in(" + opu + ") or P.COLUMNA02 is null) AND (P.COLUMNA03='" + AcOwner + "' or P.COLUMNA03 is null) AND " + grdcol + "='" + sortByStatus + "' ";
               	// oppurtinity Filter Changes By Venkat 
			    else if (frmID == 1375)
                    strQry = strQry + " AND  (P.COLUMNA02 in(" + opu + ") or P.COLUMNA02 is null) AND (P.COLUMNA03='" + AcOwner + "' or P.COLUMNA03 is null) AND " + grdcol + "='" + sortByStatus + "' ";
                else
                    strQry = strQry + " AND  (P.COLUMNA02 in(" + opu + ") or P.COLUMNA02 is null) AND P.COLUMNA03='" + AcOwner + "' AND " + grdcol + "='" + sortByStatus + "' ";
            }
            else
            {
                if (itm != "" || UPC != "" || brand != "" || group != "" || family != "")
                {   //EMPHCS1741 rajasekhar reddy patakota 24/05/2016 Customer Wise price Adding in all transactions
				 if (frmID == 1582)
				     //EMPHCS1819 rajasekhar reddy patakota 21/09/2016 Info Grid Customisation for dynamic pages
                     strQry = strQry + " AND (P.COLUMNA02 in(" + opu + ") or P.COLUMNA02 is null) AND (P.COLUMNA03='" + AcOwner + "' or P.COLUMNA03 is null)";
                    else
					 //EMPHCS1819 rajasekhar reddy patakota 21/09/2016 Info Grid Customisation for dynamic pages
                     strQry = strQry + " AND (P.COLUMNA02 in(" + opu + ") or P.COLUMNA02 is null) AND P.COLUMNA03=" + AcOwner + "";
				}
                else
				{
                    if (frmID == 1582)
					    //EMPHCS1819 rajasekhar reddy patakota 21/09/2016 Info Grid Customisation for dynamic pages
                        strQry = strQry + " AND p.COLUMN02 != ''   AND (P.COLUMNA02 in(" + opu + ") or P.COLUMNA02 is null) AND (P.COLUMNA03='" + AcOwner + "' or P.COLUMNA03 is null)";
                    else
                        strQry = strQry + " AND p.COLUMN02 != ''   AND (P.COLUMNA02 in(" + opu + ") or P.COLUMNA02 is null) AND P.COLUMNA03='" + AcOwner + "'";
				 }
            }
            if (itemType != null)
            {
                if (frmID == 1261)
                {
                    var itemtype = itemType;
                    strQry = strQry + " AND " + gridsort + " p.COLUMN05='" + itemtype + "'";
                }
                else if (frmID == 1250)
                {
                    var itemtype = itemType;
                    strQry = strQry + " AND  p.COLUMN07='" + itemtype + "'";
                }
            }
            else { itemType = null; }
            if (sortByStatus == null)
            {
                if (frmID == 1273)
                {
                }
                else if (frmID == 1274)
                {
                }
                else if (frmID == 1277)
                {
                }
                else if (frmID == 1278)
                {
                }
                else if (frmID == 1349)
                {
					//EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
                    strQry = strQry + "AND p.COLUMN06='WITHDRAW'  ";
                }
                else if (frmID == 1350)
                {
                    strQry = strQry + "AND p.COLUMN06='DEPOSIT' ";
                }
                else if (frmID == 1351)
                {
                    strQry = strQry + "AND p.COLUMN06='TRANSFER' ";
                }
            }
            info.strQry = strQry;
            info.allCol = alCol;
            info.ob = ob;
            return info;

        }

        public FormBuildingEntities ReturnReport(int frmID)
        {
            FormBuildingEntities info = new FormBuildingEntities();
            if (frmID == 1365)
            {
                info.View = "Index";
                info.Controller = "EmpVsRole";
            }
            else if (frmID == 1383)
            {
                info.View = "SocialSites";
                info.Controller = "SendMailer";
            }
            //EMPHCS1723 : Creating Payment Term New form and displaying dropdown Default vales PT by GNANESHWAR ON 2/5/2016
            else if (frmID == 1580)
            {
                info.View = "Info";
                info.Controller = "PaymentTerms";
            }
            else if (frmID == 1389)
            {
                info.View = "AccountspayableLocation";
                info.Controller = "Report";
            }
            else if (frmID == 1390)
            {
                info.View = "AccountspayableDuedates";
                info.Controller = "Report";
            }

            else if (frmID == 1600)
            {
                info.View = "ResourseConsumptions";
                info.Controller = "Report";
            }

            else if (frmID == 1591)
            {
                info.View = "JobberIN";
                info.Controller = "Report";
            }

            else if (frmID == 1592)
            {
                info.View = "JobbingLedger";
                info.Controller = "Report";
            }
            //EMPHCS1801 rajasekhar reddy patakota 25/08/2016 Grey Report Creation in jobbing in module
            else if (frmID == 1610)
            {
                info.View = "GreyReport";
                info.Controller = "Report";
            }

            else if (frmID == 1618)
            {
                info.View = "GreyReportSummary";
                info.Controller = "Report";
            }
            else if (frmID == 1391)
            {
                info.View = "ProjectReceivableAccountReport";
                info.Controller = "Report";
            }
            else if (frmID == 1392)
            {
                info.View = "ProjectReceivableAccountDueDates";
                info.Controller = "Report";
            }
            else if (frmID == 1393)
            {
                info.View = "ProjectExpenses";
                info.Controller = "Report";
            }
            else if (frmID == 1394)
            {
                info.View = "ProjectBankReport";
                info.Controller = "Report";
            }
            else if (frmID == 1396)
            {
                info.View = "ARAgeing";
                info.Controller = "Report";
            }
           //EMPHCS1309 BY GNANESHWAR ON 28/10/2015	AR aging  And Customer Payment Reports Chages on due amount
		    else if (frmID == 1520)
            {
                info.View = "ARAgeingDetailes";
                info.Controller = "Report";
            }
            else if (frmID == 1397)
            {
                info.View = "APAgeing";
                info.Controller = "Report";
            }
            else if (frmID == 1398)
            {
                info.View = "GeneralLedger";
                info.Controller = "Report";
            }
            else if (frmID == 1399)
            {
                info.View = "ProfitAndLoss";
                info.Controller = "Report";
            }
           //EMPHCS1285 gnaneshwar on 7/10/2015  Creating Profitandloss By Project Report
		    else if (frmID == 1510)
            {
                info.View = "ProfitAndLossByProject";
                info.Controller = "Report";
            }
            else if (frmID == 1400)
            {
                info.View = "BalanceSheet";
                info.Controller = "Report";
            }
            else if (frmID == 1401)
            {
                info.View = "TrialBalance";
                info.Controller = "Report";
            }
            else if (frmID == 1402)
            {
                info.View = "VatInfo";
                info.Controller = "Report";
            }
            else if (frmID == 1403)
            {
                info.View = "SOByCustomer";
                info.Controller = "Report";
            }
            else if (frmID == 1636)
            {
                info.View = "SalesByProductDetailReport";
                info.Controller = "Report";
            }
            else if (frmID == 1387)
            {
                info.View = "Create";
                info.Controller = "AutoFieldConfig";
            }
            else if (frmID == 1384)
            {
                info.View = "Create";
                info.Controller = "TransactionMaster";
            }
            else if (frmID == 1385)
            {
                info.View = "ExpenseTracking";
                info.Controller = "Report";
            }
            else if (frmID == 1242)
            {
                info.View = "Info";
                info.Controller = "CompanyMaster";
            }
            else if (frmID == 1245)
            {
                info.View = "Index";
                info.Controller = "Subsidary";
            }
            else if (frmID == 1246)
            {
                info.View = "Index";
                info.Controller = "OperatingUnit";
            }
            else if (frmID == 1373)
            {
                info.View = "Create";
                info.Controller = "UserPreferences";
            }
            else if (frmID == 1377)
            {
                info.View = "FormBuild";
                info.Controller = "FormBuilding";
                //idi = null;
            }
            else if (frmID == 1243)
            {
                info.View = "Info";
                info.Controller = "CenterMaster";
            }
            else if (frmID == 1244)
            {
                info.View = "Info";
                info.Controller = "RoleMaster";
            }
            else if (frmID == 1289)
            {
                info.View = "Index";
                info.Controller = "LOVMaster1";
            }
            else if (frmID == 1290)
            {
                info.View = "Info";
                info.Controller = "LOVDetails";
            }
            else if (frmID == 1291)
            {
                info.View = "Index";
                info.Controller = "Report";
            }
            else if (frmID == 1294)
            {
                info.View = "TransactionReport";
                info.Controller = "Report";
            }
            else if (frmID == 1295)
            {
                info.View = "PayableAccountReport";
                info.Controller = "Report";
            }
            else if (frmID == 1296)
            {
                info.View = "ReceivableAccountReport";
                info.Controller = "Report";
            }
            else if (frmID == 1297)
            {
                info.View = "PaymentReport";
                info.Controller = "Report";
            }
            else if (frmID == 1298)
            {
                info.View = "EmployeeReport";
                info.Controller = "Report";
            }
            else if (frmID == 1300)
            {
                info.View = "PurchaseOrderSummary";
                info.Controller = "Report";
            }
            else if (frmID == 1331)
            {
                info.View = "VendorPayment";
                info.Controller = "Report";
            }
            else if (frmID == 1332)
            {
                info.View = "CustomerPayment";
                info.Controller = "Report";
            }
            else if (frmID == 1333)
            {
                info.View = "POByItem";
                info.Controller = "Report";
            }
            else if (frmID == 1334)
            {
                info.View = "SOByItem";
                info.Controller = "Report";
            }
            else if (frmID == 1335)
            {
                info.View = "POHistory";
                info.Controller = "Report";
            }
            else if (frmID == 1336)
            {
                info.View = "SOHistory";
                info.Controller = "Report";
            }
            else if (frmID == 1337)
            {
                info.View = "ItemOrderDetails";
                info.Controller = "Report";
            }
            else if (frmID == 1338)
            {
                info.View = "ItemFulfilment";
                info.Controller = "Report";
            }
            else if (frmID == 1342)
            {
                info.View = "VendorPaymentDues";
                info.Controller = "Report";
            }
            else if (frmID == 1343)
            {
                info.View = "CustomerPaymentDues";
                info.Controller = "Report";
            }
            else if (frmID == 1305)
            {
                info.View = "Top10Vendors";
                info.Controller = "Report";
            }


            else if (frmID == 1339)
            {
                info.View = "JobberPayment";
                info.Controller = "Report";
            }
            else if (frmID == 1340)
            {
                info.View = "JobberPaymentDues";
                info.Controller = "Report";
            }
            else if (frmID == 1341)
            {
                info.View = "StockOfJobber";
                info.Controller = "Report";
            }
            else if (frmID == 1344)
            {
                info.View = "InventoryByLoc";
                info.Controller = "Report";
            }

            else if (frmID == 1360)
            {
                info.View = "OpeningBalance";
                info.Controller = "Report";
            }
            else if (frmID == 1361)
            {
                info.View = "InventoryAsset";
                info.Controller = "Report";
            }

            else if (frmID == 1306)
            {
                info.View = "VendorRegister";
                info.Controller = "Report";
            }
            else if (frmID == 1307)
            {
                info.View = "PendingBills";
                info.Controller = "Report";
            }
            else if (frmID == 1308)
            {
                info.View = "PendingPurchaseOrders";
                info.Controller = "Report";
            }
            else if (frmID == 1301)
            {
                info.View = "PurchaseOrder";
                info.Controller = "Report";
            }
            else if (frmID == 1302)
            {
                info.View = "ItemReceipt";
                info.Controller = "Report";
            }
            else if (frmID == 1303)
            {
                info.View = "Bill";
                info.Controller = "Report";
            }
            else if (frmID == 1304)
            {
                info.View = "PayBill";
                info.Controller = "Report";
            }
            else if (frmID == 1310)
            {
                info.View = "salesOrderSummary";
                info.Controller = "Report";
            }

            else if (frmID == 1311)
            {
                info.View = "SalesOrder";
                info.Controller = "Report";
            }
            else if (frmID == 1312)
            {
                info.View = "ItemIssue";
                info.Controller = "Report";
            }
            else if (frmID == 1313)
            {
                info.View = "Invoice";
                info.Controller = "Report";
            }
            else if (frmID == 1314)
            {
                info.View = "Payment";
                info.Controller = "Report";
            }
            else if (frmID == 1315)
            {
                info.View = "Top10Customers";
                info.Controller = "Report";
            }
            else if (frmID == 1316)
            {
                info.View = "CustomerRegister";
                info.Controller = "Report";
            }
            else if (frmID == 1317)
            {
                info.View = "PendingInvoices";
                info.Controller = "Report";
            }
            else if (frmID == 1318)
            {
                info.View = "PendingSalesOrders";
                info.Controller = "Report";
            }
            else if (frmID == 1320)
            {
                info.View = "AccountsLedger";
                info.Controller = "Report";
            }
            else if (frmID == 1321)
            {//EMPHCS1338	Rename Stock Ledger as Current Inventory BY RAJ.jr
                info.View = "CurrentInventory";
                info.Controller = "Report";
            }
            //EMPHCS1848 : Creating Inventory report and stock register with out price by gnaneshwar on 30/12/2016
            else if (frmID == 1632)
            { 
                info.View = "StockRegister";
                info.Controller = "Report";
            }

            else if (frmID == 1633)
            { 
                info.View = "InventoryReport";
                info.Controller = "Report";
            }

            else if (frmID == 1325)
            {
                info.View = "JobberReport";
                info.Controller = "Report";
            }
            else if (frmID == 1326)
            {
                info.View = "BankRegister";
                info.Controller = "Report";
            }
            else if (frmID == 1345)
            {
                info.View = "CustomerVsSalesRep";
                info.Controller = "Report";
            }
            else if (frmID == 1346)
            {
                info.View = "CommissionPayment";
                info.Controller = "Report";
            }
            else if (frmID == 1347)
            {
                info.View = "CommissionPaymentDues";
                info.Controller = "Report";
            }
            else if (frmID == 1348)
            {
                info.View = "CommissionDues";
                info.Controller = "Report";
            }
            else if (frmID == 1356)
            {//EMPHCS1338	Rename Stock Details as Stock Ledger BY RAJ.jr
                info.View = "StockLedger";
                info.Controller = "Report";
            }
            else if (frmID == 1611)
            {
                info.View = "StockDetailes";
                info.Controller = "Report";
            }
           //EMPHCS1302 BY GNANESHWAR ON 14/10/2015 Craeting Report InvertoryByItem
		    else if (frmID == 1514)
            {
                info.View = "InventoryByItem";
                info.Controller = "Report";
            }
            else if (frmID == 1364)
            {
                info.View = "DayReport";
                info.Controller = "Report";
            }
            else if (frmID == 1370)
            {
                info.View = "Index";
                info.Controller = "SendMailer";
            }
            else if (frmID == 1371)
            {
                info.View = "SendSMS";
                info.Controller = "SendMailer";
            }
            else if (frmID == 1413)
            {
                info.View = "CustomerDiscount";
                info.Controller = "Report";
            }
            //EMPHCS1160	VAT Reports BY RAJ.Jr 1/10/2015
            else if (frmID == 1504)
            {
                info.View = "VATComputation";
                info.Controller = "Report";
            }
            //EMPHCS1210	Inventory by UOM Report BY RAJ.Jr 25/9/2015
            else if (frmID == 1502)
            {
                info.View = "InventoryByUOM";
                info.Controller = "Report";
            }
            else if (frmID == 1511)
            {
                info.View = "CustomerPaymentDetails";
                info.Controller = "Report";
            }
            
            else if (frmID == 1513)
            {
                info.View = "VendorPaymentDetails";
                info.Controller = "Report";
            }
            //else if (frmID == 1516)
            //{
            //    info.View = "FormBuild";
            //    info.Controller = "FormBuilding";
            //}
			//EMPHCS1551	Sales and Purchase summary and detail Reports By SRINI 5/1/2016
            else if (frmID == 1533)
            {
                info.View = "SalesByProductSummary";
                info.Controller = "Report";
            }
            else if (frmID == 1534)
            {
                info.View = "SalesByProductDetails";
                info.Controller = "Report";
            }
            //EMPHCS1556	PurchaseBySuppliersDetails & PurchaseBySuppliersSummary Reports BY RAJ.Jr
            else if (frmID == 1537)
            {
                info.View = "PurchasesBySuppliersDetails";
                info.Controller = "Report";
            }
            else if (frmID == 1538)
            {
                info.View = "PurchasesBySuppliersSummary";
                info.Controller = "Report";
            }
            else if (frmID == 1665)
            {
                info.View = "PurchaseRegister";
                info.Controller = "Report";
            }
            //EMPHCS1558 : SalesBycustomerDetails & SalesByCustomerSummary Reports By GNANESHWAR ON 9/2/2016
            else if (frmID == 1535)
            {
                info.View = "SalesByCustomerSummary";
                info.Controller = "Report";
            }
            //EMPHCS1558 : SalesBycustomerDetails & SalesByCustomerSummary Reports By GNANESHWAR ON 9/2/2016
            else if (frmID == 1536)
            {
                info.View = "SalesByCustomerDetails";
                info.Controller = "Report";
            }

            //CHNANGE BY GNANESHWAR ON 6/9/2016
            else if (frmID == 1612)
            {
                info.View = "SalesByCustomerDateWiseSummary";
                info.Controller = "Report";
            }
            else if (frmID == 1613)
            {
                info.View = "SalesByCustomerDateWiseDetails";
                info.Controller = "Report";
            }


            //EMPHCS1557 : PurchaseByProductDetails & PurchaseByProductSummary Reports By GNANESHWAR ON 9/2/2016
            else if (frmID == 1539)
            {
                info.View = "PurchaseByProductDetails";
                info.Controller = "Report";
            }
            //EMPHCS1557 : PurchaseByProductDetails & PurchaseByProductSummary Reports By GNANESHWAR ON 9/2/2016
            else if (frmID == 1540)
            {
                info.View = "PurchaseByProductSummary";
                info.Controller = "Report";
            }
			 //EMPHCS1562	New Reports For Returns BY RAJ.Jr
            else if (frmID == 1542)
            {
                info.View = "DebitMemoBySuppliersSummary";
                info.Controller = "Report";
            }
            else if (frmID == 1543)
            {
                info.View = "DebitMemoBySuppliersDetails";
                info.Controller = "Report";
            }
            else if (frmID == 1544)
            {
                info.View = "DebitMemoByProductDetails";
                info.Controller = "Report";
            }
            else if (frmID == 1545)
            {
                info.View = "DebitMemoByProductSummary";
                info.Controller = "Report";
            }
            else if (frmID == 1546)
            {
                info.View = "CreditMemoByCustomerSummary";
                info.Controller = "Report";
            }
            else if (frmID == 1547)
            {
                info.View = "CreditMemoByCustomerDetails";
                info.Controller = "Report";
            }
            else if (frmID == 1548)
            {
                info.View = "CreditMemoByProductDetails";
                info.Controller = "Report";
            }
            else if (frmID == 1549)
            {
                info.View = "CreditMemoByProductSummary";
                info.Controller = "Report";
            }
			//EMPHCS1580 rajasekhar reddy patakota 27/02/2016 Adding New Auditor Tax Reports
            else if (frmID == 1550)
            {
                info.View = "ProductsTaxRegister";
                info.Controller = "Report";
            }
            else if (frmID == 1551)
            {
                info.View = "SevicesTaxRegister";
                info.Controller = "Report";
            }
			//EMPHCS1586 rajasekhar reddy patakota 03/03/2016 Sales Tax Register Report Changes
            else if (frmID == 1563)
            {
                info.View = "PurchaseTaxRegister";
                info.Controller = "Report";
            }
            else if (frmID == 1564)
            {
                info.View = "PurchaseSevicesTaxRegister";
                info.Controller = "Report";
            }
            else if (frmID == 1565)
            {
                info.View = "SalesReturnTaxRegister";
                info.Controller = "Report";
            }
            else if (frmID == 1566)
            {
                info.View = "PurchaseReturnTaxRegister";
                info.Controller = "Report";
            }
            else if (frmID == 1552)
            {
                info.View = "TDSRegister";
                info.Controller = "Report";
            }
			//EMPHCS1606 rajasekhar reddy patakota 15/03/2016 ARAging Report Changes for rk
            else if (frmID == 1577)
            {
                info.View = "ARAgeingNew";
                info.Controller = "Report";
            }
			//EMPHCS1615 rajasekhar reddy patakota 16/03/2016 ARAging Details Report Changes for rk
            else if (frmID == 1578)
            {
                info.View = "ARAgeingDetailesNew";
                info.Controller = "Report";
            }
            else if (frmID == 1583)
            {
                info.View = "PurchaseTaxRegisterDetails";
                info.Controller = "Report";
            }
            else if (frmID == 1584)
            {
                info.View = "SalesTaxRegisterDetails";
                info.Controller = "Report";
            }
            // IP Config && Audit Information By Venkat
            else if (frmID == 1585)
            {
                info.View = "AuditInformation";
                info.Controller = "Report";
            }
			//EMPHCS1791	Resource Consumption prints and Reports By Raj.Jr
            else if (frmID == 1601)
            {
                info.View = "ResourceConsumptionSummary";
                info.Controller = "Report";
            }
            //EMPHCS1796 Production Entry Screen and Cost of Production Report Changes by gnaneshwar on 10/8/2016
            else if (frmID == 1607)
            {
                info.View = "ResourceConsumptionConsolidate";
                info.Controller = "Report";
            }            
			//EMPHCS1822 rajasekhar reddy patakota 30/09/2016 Opportunity Mail setup & Form Design Changes
			else if (frmID == 1620)
            {
                info.View = "OpporutinesReport";
                info.Controller = "Report";
            }
			//EMPHCS1830 rajasekhar reddy patakota 11/12/2016 Production Process in Job Order
            else if (frmID == 1627)
            {
                info.View = "JobOrderProcessing";
                info.Controller = "Report";
            }
			//EMPHCS1841 rajasekhar reddy patakota 07/12/2016 Cash Account Creation of type Cash for all bank related Transactions
            else if (frmID == 1628)
            {
                info.View = "CashRegister";
                info.Controller = "Report";
            }            
			else if (frmID == 1637)
            {
                info.View = "InvoiceBillDetails";
                info.Controller = "Report";
            }
            else if (frmID == 1639)
            {
                info.View = "ProfitAndLossByJobbing";
                info.Controller = "Report";
            }
            else if (frmID == 1640)
            {
                info.View = "ProfitAndLossByProduct";
                info.Controller = "Report";
            }
            else if (frmID == 1644)
            {
                info.View = "StockMoment";                
				info.Controller = "Report";
			}
            else if (frmID == 1650)
            {
                info.View = "StockRegisterKK";
                info.Controller = "Report";
            }
            else if (frmID == 1651)
            {
                info.View = "StockLedgerKK";
                info.Controller = "Report";
            }
            else if (frmID == 1652)
            {
                info.View = "GSTF";
                info.Controller = "Report";
            }
            else if (frmID == 1653)
            {
                info.View = "InventoryExpiry";
                info.Controller = "Report";
            }
            else if (frmID == 1654)
            {
                info.View = "BarcodeWiseStock";
                info.Controller = "Report";
            }
            else if (frmID == 1655)
            {
                info.View = "ProjectPriceAnalysis";
                info.Controller = "Report";
            }
            else if (frmID == 1660)
            {
                info.View = "SalesInvoiceRK";
                info.Controller = "Report";
            }
            else if (frmID == 1664)
            {
                info.View = "SalesRegister";
                info.Controller = "Report";
            }

            else if (frmID == 1661)
            {
                info.View = "StockSummary";
                info.Controller = "Report";
            }
            else if (frmID == 1662)
            {
                info.View = "StockDetailsByItem";
                info.Controller = "Report";
            }
            else if (frmID == 1663)
            {
                info.View = "MonthWiseSalesDetails";
                info.Controller = "Report";
            }
            return info;
        }

        public FormBuildingEntities ReturnLinks(int frmID)
        {
            FormBuildingEntities info = new FormBuildingEntities();

            if (frmID == 1251)
            {
                info.PForms = "Item Receipt"; info.PoNo = "ID";
            }
            else if (frmID == 1272) { info.PForms = "Bill"; info.PoNo = "Item Receipt#"; }
            //EMPHCS1146	Not displaying data in payment , customer payments BY RAJ.Jr 14/9/2015
            else if (frmID == 1273) { info.PForms = "Payment"; info.PoNo = "ID"; }
            else if (frmID == 1375) { info.PForms = "Sales Quotation"; info.PoNo = "ID"; }
            else if (frmID == 1275) { info.PForms = "Item Issue"; info.PoNo = "ID"; }
            else if (frmID == 1276) { info.PForms = "Invoice"; info.PoNo = "Item Issue#"; }
            //EMPHCS1146	Not displaying data in payment , customer payments BY RAJ.Jr 14/9/2015
            else if (frmID == 1277) { info.PForms = "Customer Payments"; info.PoNo = "ID"; }
            else if (frmID == 1283) { info.PForms = "Issue(Out)"; info.PoNo = "ID"; }
            else if (frmID == 1284) { info.PForms = "Receipt(Out)"; info.PoNo = "Item Issue#"; }
            else if (frmID == 1286) { info.PForms = "Bill to Jobber"; info.PoNo = "JoReciept#"; }
            else if (frmID == 1287) { info.PForms = "Jobber Payments"; info.PoNo = "Bill#"; }
            else if (frmID == 1328) { info.PForms = "Receipt"; info.PoNo = "ID"; }
            else if (frmID == 1329) { info.PForms = "Credit Memo"; info.PoNo = "ID"; }
            else if (frmID == 1353) { info.PForms = "Return Issue"; info.PoNo = "ID"; }
            else if (frmID == 1354) { info.PForms = "Debit Memo"; info.PoNo = "ID"; }
            //EMPHCS1814 rajasekhar reddy patakota 12/09/2016 Retail Changes for Sales Receipt screens
            else if (frmID == 1374) { info.PForms = "Quotation Comparison"; info.PoNo = "ID"; }
            else if (frmID == 1378) { info.PForms = "Project Work Order"; info.PoNo = "ID"; }
            else if (frmID == 1380) { info.PForms = "Material Dispatch"; info.PoNo = "ID"; }
            else if (frmID == 1380) { info.PForms = "Resourse Consumption"; info.PoNo = "ID"; }
			//EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
            else if (frmID == 1293) { info.PForms = "Expense Payment"; info.PoNo = "ID"; }
            else { info.PForms = null; info.PoNo = 0; }
            return info;
        }
        //set form link in webgrid for associated forms 
        public FormBuildingEntities FormBuildReturnLinks(int frmID)
        {
            FormBuildingEntities info = new FormBuildingEntities();

            if (frmID == 1272)
            {
                info.PForms = "Item Receipt"; info.PoNo = "ID";
            }
            else if (frmID == 1273) { info.PForms = "Bill"; info.PoNo = "Item Receipt#"; }
            else if (frmID == 1274) { info.PForms = "Payment"; info.PoNo = "Bill#"; }
            else if (frmID == 1376) { info.PForms = "Sales Quotation"; info.PoNo = "ID"; }
            else if (frmID == 1276) { info.PForms = "Item Issue"; info.PoNo = "ID"; }
            else if (frmID == 1277) { info.PForms = "Invoice"; info.PoNo = "Item Issue#"; }
            else if (frmID == 1278) { info.PForms = "Customer Payments"; info.PoNo = "Invoice#"; }
            else if (frmID == 1284) { info.PForms = "Job Order Issue"; info.PoNo = "ID"; }
            else if (frmID == 1286) { info.PForms = "Job Order Receipt"; info.PoNo = "Item Issue#"; }
            else if (frmID == 1287) { info.PForms = "Bill to Jobber"; info.PoNo = "JoReciept#"; }
            else if (frmID == 1288) { info.PForms = "Jobber Payments"; info.PoNo = "Bill#"; }
            else if (frmID == 1329) { info.PForms = "Receipt"; info.PoNo = "ID"; }
            else if (frmID == 1330) { info.PForms = "Credit Memo"; info.PoNo = "ID"; }
            else if (frmID == 1354) { info.PForms = "Return Issue"; info.PoNo = "ID"; }
            //EMPHCS1561 rajasekhar patakota 12/02/2016 latest bug fixes raised by sudheer in production
            else if (frmID == 1355) { info.PForms = "Debit Memo"; info.PoNo = "ID"; }
            //EMPHCS1814 rajasekhar reddy patakota 12/09/2016 Retail Changes for Sales Receipt screens
            else if (frmID == 1377) { info.PForms = "Quotation Comparison"; info.PoNo = "ID"; }
            else if (frmID == 1380) { info.PForms = "Project Work Order"; info.PoNo = "ID"; }
            else if (frmID == 1381) { info.PForms = "Resourse Consumption"; info.PoNo = "ID"; }
			//EMPHCS1422 rajasekhar reddy patakota 15/12/2015 Expense Booking and Expense Payment New Changes
            else if (frmID == 1293) { info.PForms = "Expense Payment"; info.PoNo = "ID"; }
            else { info.PForms = null; info.PoNo = 0; }
            return info;
        }
        //EMPHCS1200 rajasekhar reddy patakota 22/09/2015 Auto Feild Configuration Setup For Project and UOM at Account owner level
        public List<CONTABLE006> getAutoFeildDetails(DataTable adt)
        {
            List<CONTABLE006> itemdata = adt.AsEnumerable().Select(x => new CONTABLE006
            {
                COLUMN05 = ((x["COLUMN05"].ToString() == null || x["COLUMN05"].ToString() == "") ? null : (string)(x["COLUMN05"])),
                COLUMN06 = ((x["COLUMN06"].ToString() == null || x["COLUMN06"].ToString() == "") ? null : (string)(x["COLUMN06"])),
                COLUMN07 = ((x["COLUMN07"].ToString() == null || x["COLUMN07"].ToString() == "") ? null : (string)(x["COLUMN07"])),
                COLUMN08 = ((x["COLUMN08"].ToString() == null || x["COLUMN08"].ToString() == "") ? null : (string)(x["COLUMN08"])),
                COLUMN09 = ((x["COLUMN09"].ToString() == null || x["COLUMN09"].ToString() == "") ? null : (string)(x["COLUMN09"])),
                COLUMN10 = ((x["COLUMN10"].ToString() == null || x["COLUMN10"].ToString() == "") ? null : (string)(x["COLUMN10"])),
                COLUMN11 = ((x["COLUMN11"].ToString() == null || x["COLUMN11"].ToString() == "") ? null : (string)(x["COLUMN11"])),
                COLUMN12 = ((x["COLUMN12"].ToString() == null || x["COLUMN12"].ToString() == "") ? null : (string)(x["COLUMN12"])),
                COLUMN03 = ((x["COLUMN03"].ToString() == null || x["COLUMN03"].ToString() == "") ? null : (int?)(x["COLUMN03"])),
                COLUMN04 = ((x["COLUMN04"].ToString() == null || x["COLUMN04"].ToString() == "") ? null : (int?)(x["COLUMN04"])),
                COLUMN13 = ((x["COLUMN13"].ToString() == null || x["COLUMN13"].ToString() == "") ? null : (string)(x["COLUMN13"])),
                COLUMN14 = ((x["COLUMN14"].ToString() == null || x["COLUMN14"].ToString() == "") ? null : (string)(x["COLUMN14"])),
                COLUMN15 = ((x["COLUMN15"].ToString() == null || x["COLUMN15"].ToString() == "") ? null : (int?)(x["COLUMN15"]))
            }).ToList();
            return itemdata;
        }

        public FormBuildingEntities GetBillDetails(string billno, SqlConnection cn, string OPUnit, string AcOwner, object fname)
        {
            eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
            FormBuildingEntities info = new FormBuildingEntities();
            List<string> Oper = OPUnit.Split(',').ToList<string>();
            if (Oper.ElementAt(Oper.Count - 1) == "null")
                OPUnit = " (COLUMNA02 in(" + OPUnit + ") or COLUMNA02 is null) ";
            else
                OPUnit = " (COLUMNA02 in(" + OPUnit + ") or COLUMNA02 is null) ";
            int PurchaseOrderID = 0;
            string form = billno;
            SqlCommand cmdpo = new SqlCommand("select column06,column02 from putable005 where (column04='" + billno + "' or column02='" + billno + "')  AND " + OPUnit + " AND COLUMNA03='" + AcOwner + "'", cn);
            SqlDataAdapter dapo = new SqlDataAdapter(cmdpo);
            DataTable dtpo = new DataTable();
            dapo.Fill(dtpo); if (dtpo.Rows.Count > 0)
            {
                if (dtpo.Rows[0][0] != "")
                {
                    PurchaseOrderID = Convert.ToInt32(dtpo.Rows[0][0]);
                    billno = dtpo.Rows[0][1].ToString();
                }
            }
            info.ID = PurchaseOrderID;
            SqlCommand cmd = new SqlCommand("usp_PUR_TP_PAYBILL_HEADER_DATA", cn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            cmd.Parameters.Add(new SqlParameter("@Form", billno));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt1 = new DataTable();
            da.Fill(dt1);
            info.DataHead = dt1;
			//EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
            OPUnit = dt1.Rows[dt1.Rows.Count - 1]["COLUMN15"].ToString();
            SqlCommand cmd1 = new SqlCommand("usp_PUR_TP_PAYBILL_LINE_DATA", cn);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            cmd1.Parameters.Add(new SqlParameter("@Form", billno));
            //EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
			cmd1.Parameters.Add(new SqlParameter("@AcOwner", AcOwner));
            cmd1.Parameters.Add(new SqlParameter("@OPUNIT", OPUnit));
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da1.Fill(dt); 
			//EMPHCS1782 rajasekhar reddy patakota 09/07/2016 Credits Calculation in Customer and Vendor Payments Screen
            var Vendor = dt1.Rows[dt1.Rows.Count - 1]["COLUMN05"].ToString();
            SqlCommand cmdd = new SqlCommand("usp_PUR_TP_PAYMENT_LINE_CreditDATA", cn);
            cmdd.CommandType = CommandType.StoredProcedure;
            cmdd.Parameters.Add(new SqlParameter("@SalesOrderID", Vendor));
            cmdd.Parameters.Add(new SqlParameter("@AcOwner", AcOwner));
            cmdd.Parameters.Add(new SqlParameter("@OPUNIT", OPUnit));
            SqlDataAdapter dad = new SqlDataAdapter(cmdd);
            DataTable dtd = new DataTable();
            dad.Fill(dtd);
            if (dtd.Rows.Count > 0)
                dt.Merge(dtd, true);
            //Bill selection not getting return amount
			decimal damt = 0;
            decimal tamt = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                tamt += Convert.ToDecimal(dt.Rows[i]["COLUMN04"].ToString());
                damt += Convert.ToDecimal(dt.Rows[i]["COLUMN05"].ToString());
            }
            decimal DRetAmt = 0;
            decimal RDRetAmt = 0;
            decimal RetAmt = 0;
            decimal RDNextRowRetAmt = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["directReturn"].ToString() != "")
                {
                    DRetAmt = Convert.ToDecimal(dt.Rows[i]["directReturn"]) + RDRetAmt;
                    if (dt.Rows[i]["COLUMN16"].ToString() == "")
                        dt.Rows[i]["COLUMN16"] = 0;
                    RetAmt = Convert.ToDecimal(dt.Rows[i]["COLUMN16"]);
                    if (dt.Rows[i]["COLUMN16"].ToString() == "0")
                        dt.Rows[i]["COLUMN16"] = DRetAmt;
                    damt = Convert.ToDecimal(dt.Rows[i]["COLUMN05"]);
                    if ((DRetAmt + RetAmt) > damt)
                    {
                        RDRetAmt = (DRetAmt + RetAmt) - damt;
                        dt.Rows[i]["COLUMN16"] = ((DRetAmt + RetAmt) - RDRetAmt).ToString();
//EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016

                        if (dt.Rows[i]["COLUMN18"].ToString() == "")
                        {
                            dt.Rows[i]["COLUMN18"] = dt.Rows[i]["directReturnCol"];

                            if (dt.Rows[i]["COLUMN16"].ToString() == "0" || dt.Rows[i]["COLUMN16"].ToString() == "")
                            {
                                dt.Rows[i]["COLUMN18"] = 0;
                            }

                        }
                    }
                    else
                    {
                        dt.Rows[i]["COLUMN16"] = ((DRetAmt + RetAmt)).ToString();
                       //EMPHCS1598 : Checking parent record credit Delete condition when child record exist  by gnaneshwar on 12/3/2016
					    if (DRetAmt > 0)
                        {
                            if (dt.Rows[i]["COLUMN18"].ToString() == "")
                            {
                                dt.Rows[i]["COLUMN18"] = dt.Rows[i]["directReturnCol"];

                                if (dt.Rows[i]["COLUMN16"].ToString() == "0" || dt.Rows[i]["COLUMN16"].ToString() == "")
                                {
                                    dt.Rows[i]["COLUMN18"] = 0;
                                }

                            }
                            else
                            {
                                dt.Rows[i]["COLUMN18"] += "," + dt.Rows[i]["directReturnCol"];
                                if (dt.Rows[i]["COLUMN16"].ToString() == "0" || dt.Rows[i]["COLUMN16"].ToString() == "")
                                {
                                    dt.Rows[i]["COLUMN18"] = 0;
                                }
                            }
                        }
                    }
                    if (i + 1 < dt.Rows.Count)
                    {
                        dt.Rows[i + 1]["directReturn"] = RDRetAmt;
                        RDRetAmt = 0;
                    }
                    if (i + 2 < dt.Rows.Count)
                    {
                        dt.Rows[i + 2]["directReturn"] = 0;
                    }
                }
                if (dt.Rows[i]["COLUMN16"].ToString() != "")
                {
                    if (dt.Rows[i]["COLUMN16"].ToString() == dt.Rows[i]["COLUMN05"].ToString())
                    {
                        dt.Rows[i]["COLUMN06"] = 0;
                    }
                }
            }
            info.DataLine = dt;
            var formid = dc.CONTABLE0010.Where(a => a.COLUMN04 == fname).FirstOrDefault();
            var data = dc.CONTABLE006.Where(a => a.COLUMN03 == formid.COLUMN02).ToList();
            var itemdata = data.Where(a => a.COLUMN11 == "Item Level" && a.COLUMN07 != "N").ToList();
            var tblid = itemdata.Select(a => a.COLUMN04).Distinct().ToList();
			//EMPHCS1200 rajasekhar reddy patakota 22/09/2015 Auto Feild Configuration Setup For Project and UOM at Account owner level
			//EMPHCS1206 rajasekhar reddy patakota 24/09/2015 Operating Unit Location Trcking For Invoice and Bill
			//EMPHCS1307 rajasekhar reddy patakota 23/10/2015 Adding Lot No In Invoice And Bill
			//EMPHCS1534 rajasekhar reddy patakota 23/01/2015 Latest issues fixes raised by sudheer
            SqlCommand acmd = new SqlCommand(
                          "Select CONTABLE006.COLUMN05,CONTABLE006.COLUMN06,  iif((s.COLUMN05 is not null and CONTABLE006.COLUMN07!='N'), isnull( s1.COLUMN07,'N') ,CONTABLE006.COLUMN07)COLUMN07, CONTABLE006.COLUMN08,CONTABLE006.COLUMN09, CONTABLE006.COLUMN10,CONTABLE006.COLUMN11,CONTABLE006.COLUMN12,CONTABLE006.COLUMN03,CONTABLE006.COLUMN04,CONTABLE005.COLUMN06 DataType,CONTABLE006.COLUMN08,CONTABLE006.COLUMN13,CONTABLE006.COLUMN14,CONTABLE006.COLUMN15 From CONTABLE006  " +
                        "Inner Join CONTABLE005 on CONTABLE006.COLUMN04 = CONTABLE005.COLUMN03 " +
                        " left join SETABLE011 s on s.COLUMN04 = " + tblid[0] + " and CONTABLE006.COLUMN05 =s.COLUMN05 and s.COLUMN06 = " + formid.COLUMN02 + "  " +
                        " left join SETABLE012 s1 on s.COLUMN04 = s1.COLUMN04  and s1.COLUMN05 =s.COLUMN05 and s.COLUMN06 = s1.COLUMN06 and s1.COLUMNA03 = " + AcOwner + " " +
                        "where CONTABLE005.COLUMN03 =" + tblid[0] + " and CONTABLE006.COLUMN04 =" + tblid[0] + " and CONTABLE006.COLUMN05 = CONTABLE005.COLUMN04 and CONTABLE006.COLUMN03=" + formid.COLUMN02 + "   and CONTABLE006.COLUMN11 = 'Item Level'  and  iif((s.COLUMN05 is not null and CONTABLE006.COLUMN07!='N'), isnull( s1.COLUMN07,'N') ,CONTABLE006.COLUMN07)='Y'   ORDER BY CAST(CONTABLE006.COLUMN13 AS INT) ", cn);
            SqlDataAdapter ada = new SqlDataAdapter(acmd);
            DataTable adt = new DataTable();
            ada.Fill(adt);
            info.Datadt = adt;
			//EMPHCS1200 rajasekhar reddy patakota 22/09/2015 Auto Feild Configuration Setup For Project and UOM at Account owner level
            itemdata = getAutoFeildDetails(adt);
            info.Item = itemdata;
            return info;
        }
    }
}
