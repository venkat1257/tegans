﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace eBizSuiteAppBAL.Fombuilding
{
    public class FormBuildingEntities
    {
        public List<CONTABLE006> Item { get; set; }
        public DataTable DataHead { get; set; }
        public DataTable DataLine { get; set; }
        public DataTable Datadt { get; set; }
        public object PoNo { get; set; }
        public string PForms { get; set; }
        public string strQry { get; set; }
        public string Controller { get; set; }
        public string View { get; set; }
        public List<string> alCol { get; set; }
        public List<string> actCol { get; set; }
        public string allCol { get; set; }
        public string ob { get; set; }
        public System.Collections.Generic.List<string> statusdata { get; set; }
        public List<SelectListItem> Country = new List<SelectListItem>();
        public int tblID { get; set; }
        public int ID { get; set; }
    }
   //EMPHCS1781 Retail Creation of Customer and Item Master by gnaneshwar on 9/7/2016
    public class CustInsert
    {
        public int COLUMN01 { get; set; }
        public string COLUMN02 { get; set; }
        public Nullable<int> COLUMN03 { get; set; }
        public string COLUMN04 { get; set; }
        public string COLUMN05 { get; set; }
        public string COLUMN06 { get; set; }
        public string COLUMN07 { get; set; }
        public string COLUMN08 { get; set; }
        public string COLUMN09 { get; set; }
        public string COLUMN10 { get; set; }
        public string COLUMN11 { get; set; }
        public string COLUMN12 { get; set; }
        public string COLUMN13 { get; set; }
        public string COLUMN14 { get; set; }
        public string COLUMN15 { get; set; }
        public string COLUMN16 { get; set; }
        public string COLUMN17 { get; set; }
        public string COLUMN18 { get; set; }
        public string COLUMN19 { get; set; }
        public string COLUMN20 { get; set; }
        public string COLUMN21 { get; set; }
        public string COLUMN22 { get; set; }
        public Nullable<decimal> COLUMN23 { get; set; }
        public string COLUMN24 { get; set; }
        public Nullable<bool> COLUMN25 { get; set; }
        public Nullable<int> COLUMNA01 { get; set; }
        public Nullable<int> COLUMNA02 { get; set; }
        public Nullable<int> COLUMNA03 { get; set; }
        public Nullable<int> COLUMNA04 { get; set; }
        public Nullable<int> COLUMNA05 { get; set; }
        public Nullable<System.DateTime> COLUMNA06 { get; set; }
        public Nullable<System.DateTime> COLUMNA07 { get; set; }
        public Nullable<int> COLUMNA08 { get; set; }
        public Nullable<System.DateTime> COLUMNA09 { get; set; }
        public Nullable<System.DateTime> COLUMNA10 { get; set; }
        public Nullable<int> COLUMNA11 { get; set; }
        public Nullable<bool> COLUMNA12 { get; set; }
        public Nullable<bool> COLUMNA13 { get; set; }
        public string COLUMNB01 { get; set; }
        public string COLUMNB02 { get; set; }
        public string COLUMNB03 { get; set; }
        public string COLUMNB04 { get; set; }
        public string COLUMNB05 { get; set; }
        public string COLUMNB06 { get; set; }
        public Nullable<System.DateTime> COLUMNB07 { get; set; }
        public Nullable<System.DateTime> COLUMNB08 { get; set; }
        public Nullable<decimal> COLUMNB09 { get; set; }
        public Nullable<decimal> COLUMNB10 { get; set; }
        public Nullable<int> COLUMNB11 { get; set; }
        public Nullable<int> COLUMNB12 { get; set; }
        public string COLUMND01 { get; set; }
        public string COLUMND02 { get; set; }
        public string COLUMND03 { get; set; }
        public string COLUMND04 { get; set; }
        public string COLUMND05 { get; set; }
        public string COLUMND06 { get; set; }
        public Nullable<System.DateTime> COLUMND07 { get; set; }
        public Nullable<System.DateTime> COLUMND08 { get; set; }
        public Nullable<decimal> COLUMND09 { get; set; }
        public Nullable<int> COLUMND10 { get; set; }
        public string COLUMN26 { get; set; }
        public string COLUMN27 { get; set; }
        public string COLUMN28 { get; set; }
        public string COLUMN29 { get; set; }
        public string COLUMN30 { get; set; }
        public string COLUMN31 { get; set; }
        public Nullable<bool> column32 { get; set; }
        public string column33 { get; set; }
        public string COLUMN34 { get; set; }

        public string SCOLUMN06 { get; set; }
        public string SCOLUMN07 { get; set; }
        public string SCOLUMN08 { get; set; }
        public string SCOLUMN16 { get; set; }
        public string SCOLUMN11 { get; set; }
        public string SCOLUMN10 { get; set; }
        public string SCOLUMN12 { get; set; }
        public string SCOLUMN17 { get; set; }
        public string SCOLUMN18 { get; set; }
        public Nullable<bool> COLUMN40 { get; set; }

    }
}
