﻿using eBizSuiteAppDAL.classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace eBizSuiteAppBAL.Fombuilding
{
    public class DataAccess
    {
        public void Dateformat()
        {
            try
            {

                SqlConnection cn = new SqlConnection(Connection.Con);
                string str = "select COLUMN04,COLUMN10  from MYTABLE001 where COLUMN09=" + System.Web.HttpContext.Current.Session["eid"] + " and isnull(COLUMNA13,0)=0";
                SqlDataAdapter dad = new SqlDataAdapter(str, cn);
                DataTable dtd = new DataTable();
                dad.Fill(dtd); string DateFormat = null;
                if (dtd.Rows.Count > 0)
                {
                    DateFormat = dtd.Rows[0][0].ToString();
                    System.Web.HttpContext.Current.Session["FormatDate"] = dtd.Rows[0][0].ToString();
                }
                if (dtd.Rows[0][1].ToString() != "")
                    System.Web.HttpContext.Current.Session["ReportDate"] = dtd.Rows[0][1].ToString();
                else
                {
                    System.Web.HttpContext.Current.Session["ReportDate"] = "dd/mm/yy";
                    System.Web.HttpContext.Current.Session["FormatDate"] = "dd/MM/yyyy";
                }
            }

            catch (Exception ex)
            {
                var msg = "Failed........";
                System.Web.HttpContext.Current.Session["MessageFrom"] = msg + " Due to " + ex.Message;
                System.Web.HttpContext.Current.Session["SuccessMessageFrom"] = "fail";

            }
        }

        public DataTable GetGSTR1(string frDT, string toDT, string type, string GSTIN)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection cn = new SqlConnection(Connection.Con);
                SqlCommand cmd = new SqlCommand("GSTRETURN_PROC", cn);
                if (Convert.ToInt32(type)>99 )
                {
                    cmd.CommandText = "GSTRETURNBUSINESS_PROC";
                    cmd.Parameters.AddWithValue("@TYPE", type);
                }
                cmd.Parameters.AddWithValue("@FROMDT", frDT);
                cmd.Parameters.AddWithValue("@TODT", toDT);
                cmd.Parameters.AddWithValue("@GSTIN", GSTIN);
                cmd.Parameters.AddWithValue("@OPERATING", System.Web.HttpContext.Current.Session["OPunit"]);
                cmd.Parameters.AddWithValue("@OPUNIT", System.Web.HttpContext.Current.Session["OPunit"]);
                cmd.Parameters.AddWithValue("@AcOwner", System.Web.HttpContext.Current.Session["AcOwner"]);
                cmd.Parameters.AddWithValue("@FORMATDT", System.Web.HttpContext.Current.Session["FormatDate"]);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter dad = new SqlDataAdapter(cmd);
                dad.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                System.Web.HttpContext.Current.Session["MessageFrom"] = msg + " Due to " + ex.Message;
                System.Web.HttpContext.Current.Session["SuccessMessageFrom"] = "fail";
                return dt;

            }
        }

        public string GetHeader(string sec, string sub)
        {
            string html = "";
            if (sec == "1" && sub == "100")
            {
                html = "<thead><tr><th style='width:11%;vertical-align:top'>Summary For B2B(4)</th><th style='width:11%'></th><th style='width:7%'></th><th style='width:11%'></th>" +
                "<th style='width:11%'></th><th style='width:8%;vertical-align:top' ></th><th style='width:6%;vertical-align:top'></th><th style='width:11%;vertical-align:top'></th><th style='width:6%;vertical-align:top'></th><th style='width:11%;vertical-align:top'></th><th style='width:7%;vertical-align:top'></th>" +
                "</tr><tr><th>No. of Recipients</th><th >No of Invoices</th><th></th><th>Total Invoice Value</th>" +
                "<th></th><th></th><th></th>" +
                "<th ></th><th></th><th>Total Taxable Value</th><th>Total Cess</th></tr></thead>";
            }
            else if (sec == "1" && sub == "101")
            {
                html = "<thead><tr><th style='width:11%;vertical-align:top'>Summary For B2CL(5)</th><th style='width:11%'></th><th style='width:7%'></th><th style='width:11%'></th>" +
                "<th style='width:11%'></th><th style='width:8%;vertical-align:top' ></th><th style='width:6%;vertical-align:top'></th><th style='width:11%;vertical-align:top'>" +
                "</tr><tr><th >No of Invoices</th><th></th><th>Total Invoice Value</th>" +
                "<th></th><th></th>" +
                "<th>Total Taxable Value</th><th>Total Cess</th><th></th></tr></thead>";
            }
            else if (sec == "1" && sub == "102")
            {
                html = "<thead><tr><th style='width:11%;vertical-align:top'>Summary For B2CS(7)</th><th style='width:11%'></th><th style='width:7%'></th><th style='width:11%'></th>" +
                "<th style='width:11%'></th><th style='width:8%;vertical-align:top' ></th>" +
                "</tr><tr><th></th>" +
                "<th ></th><th></th><th>Total Taxable Value</th><th>Total Cess</th><th></th></tr></thead>";
            }
            else if (sec == "1" && sub == "103")
            {
                html = "<thead><tr><th style='width:10%;vertical-align:top'>Summary For CDNR(9B)</th><th style='width:10%'></th><th style='width:7%'></th><th style='width:10%'></th>" +
                "<th style='width:10%'></th><th style='width:8%;vertical-align:top' ></th><th style='width:6%;vertical-align:top'></th><th style='width:10%;vertical-align:top'></th><th style='width:6%;vertical-align:top'></th><th style='width:10%;vertical-align:top'></th><th style='width:7%;vertical-align:top'></th><th style='width:7%;vertical-align:top'></th><th></th>" +
                "</tr><tr><th>No. of Recipients</th><th >No of Invoices</th><th></th><th>No. of Notes/Vouchers</th><th></th><th></th><th></th><th></th><th>Total Note/Refund Voucher Value</th>" +
                "<th ></th><th>Total Taxable Value</th><th>Total Cess</th><th></th></tr></thead>";
            }
            else if (sec == "1" && sub == "104")
            {
                html = "<thead><tr><th style='width:10%;vertical-align:top'>Summary For CDNUR(9B)</th><th style='width:10%'></th><th style='width:7%'></th><th style='width:10%'></th>" +
                "<th style='width:10%'></th><th style='width:8%;vertical-align:top' ></th><th style='width:6%;vertical-align:top'></th><th style='width:10%;vertical-align:top'></th><th style='width:6%;vertical-align:top'></th><th style='width:10%;vertical-align:top'></th><th style='width:7%;vertical-align:top'></th><th style='width:7%;vertical-align:top'></th><th></th>" +
                "</tr><tr><th></th><th >No. of Notes/Vouchers</th><th></th><th></th><th>No of Invoices</th><th></th><th></th><th></th><th>Total Note Voucher Value</th>" +
                "<th ></th><th>Total Taxable Value</th><th>Total Cess</th><th></th></tr></thead>";
            }
            else if (sec == "1" && sub == "105")
            {
                html = "<thead><tr><th style='width:11%;vertical-align:top'>Summary For EXP(6)</th><th style='width:11%'></th><th style='width:7%'></th><th style='width:11%'></th>" +
                "<th style='width:11%'></th><th style='width:8%;vertical-align:top' ></th><th style='width:11%;vertical-align:top'></th><th style='width:6%;vertical-align:top'></th><th style='width:11%;vertical-align:top'></th>" +
                "</tr><tr><th></th><th >No of Invoices</th><th></th><th>Total Invoice Value</th>" +
                "<th></th><th>No. of Shipping Bill</th><th></th>" +
                "<th ></th><th>Total Taxable Value</th></tr></thead>";
            }
            else if (sec == "1" && sub == "106")
            {
                html = "<thead><tr><th style='width:10%;vertical-align:top'>Summary For Advance Adjusted (11B)</th><th style='width:10%'></th><th style='width:7%'></th><th style='width:10%'></th>" +
                "</tr><tr><th></th><th ></th><th>Total Advance Received</th><th>Total Cess</th></tr></thead>";
            }
            else if (sec == "1" && sub == "107")
            {
                html = "<thead><tr><th style='width:10%;vertical-align:top'>Summary For Advance Adjusted (11B)</th><th style='width:10%'></th><th style='width:7%'></th><th style='width:10%'></th>" +
                "</tr><tr><th></th><th ></th><th>Total Advance Adjusted</th><th>Total Cess</th></tr></thead>";
            }
            else if (sec == "1" && sub == "108")
            {
                html = "<thead><tr><th style='width:10%;vertical-align:top'>Summary For Nil rated, exempted and non GST outward supplies (8)</th><th style='width:10%'></th><th style='width:7%'></th><th style='width:10%'></th>" +
                "</tr><tr><th></th><th >Total Nil Rated Supplies</th><th>Total Exempted Supplies</th><th>Total Non-GST Supplies</th></tr></thead>";
            }
            else if (sec == "1" && sub == "109")
            {
                html = "<thead><tr><th style='width:11%;vertical-align:top'>Summary For HSN(12)</th><th style='width:11%'></th><th style='width:7%'></th><th style='width:11%'></th>" +
                "<th style='width:11%'></th><th style='width:8%;vertical-align:top' ></th><th style='width:6%;vertical-align:top'></th><th style='width:11%;vertical-align:top'></th><th style='width:6%;vertical-align:top'></th><th style='width:11%;vertical-align:top'></th>" +
                "</tr><tr><th>No. of HSN</th><th ></th><th></th><th></th>" +
                "<th>Total Value</th><th>Total Taxable Value</th><th>Total Integrated Tax</th>" +
                "<th >Total Central Tax</th><th>Total State/UT Tax</th><th>Total Cess</th></tr></thead>";
            }
            else if (sec == "1" && sub == "1")
            {
                html = "<thead><tr><th style='width:5%;vertical-align:top'   rowspan='2'>GSTIN/UIN</th><th style='width:36%' colspan='6'>Invoice</th><th style='width:9%' colspan='2'>IGST</th><th style='width:9%' colspan='2'>CGST</th>" +
                "<th style='width:9%' colspan='2'>SGST</th><th style='width:8%;vertical-align:top'   rowspan='2'>POS</th><th style='width:8%;vertical-align:top'   rowspan='2'>Reverse charge	</th><th style='width:8%;vertical-align:top'   rowspan='2'>Assessment</th><th style='width:8%;vertical-align:top'   rowspan='2'>GSTIN</th>" +
                "</tr><tr></th><th style='width:6%'>No.</th><th style='width:6%'>Date</th><th style='width:5.5%'>Value</th>" +
                "<th style='width:6%'>Goods/Services</th><th style='width:5.5%'>HSN/SAC</th><th style='width:7%'>Taxable value</th><th style='width:3%'>Rate</th><th style='width:6%'>Amt</th>" +
                "<th style='width:3%'>Rate</th><th style='width:6%'>Amt</th><th style='width:3%'>Rate</th><th style='width:6%'>Amt</th></tr></thead>";
            }
            else if (sec == "1" && sub == "2")
            {
                html = "<thead><tr><th style='width:4%;' colspan='2'>Original Invoice</th><th style='width:5%;vertical-align:top'   rowspan='2'>GSTIN/UIN</th><th style='width:36%' colspan='6'>Revised Invoice</th><th style='width:9%' colspan='2'>IGST</th><th style='width:9%' colspan='2'>CGST</th>" +
                "<th style='width:9%' colspan='2'>SGST</th><th style='width:7%;vertical-align:top'   rowspan='2'>POS</th><th style='width:7%;vertical-align:top'   rowspan='2'>Reverse charge	</th><th style='width:7%;vertical-align:top'   rowspan='2'>Assessment</th><th style='width:7%;vertical-align:top'   rowspan='2'>GSTIN</th>" +
                "</tr><tr><th>No.</th><th>Date</th><th style='width:6%'>No.</th><th style='width:6%'>Date</th><th style='width:5.5%'>Value</th>" +
                "<th style='width:6%'>Goods/Services</th><th style='width:5.5%'>HSN/SAC</th><th style='width:7%'>Taxable value</th><th style='width:3%'>Rate</th><th style='width:6%'>Amt</th>" +
                "<th style='width:3%'>Rate</th><th style='width:6%'>Amt</th><th style='width:3%'>Rate</th><th style='width:6%'>Amt</th></tr></thead>";
            }
            else if (sec == "1" && sub == "3")
            {
                html = "<thead><tr><th style='width:4%;vertical-align:top' rowspan='2'>Recipient’s State code</th><th style='width:5%;vertical-align:top'   rowspan='2'>Name of the recipient</th><th style='width:36%' colspan='6'>Invoice</th><th style='width:9%' colspan='2'>IGST</th>" +
                "<th style='width:7%;vertical-align:top'   rowspan='2'>POS</th><th style='width:7%;vertical-align:top'   rowspan='2'>Assessment</th>" +
                "</tr><tr><th style='width:6%'>No.</th><th style='width:6%'>Date</th><th style='width:5.5%'>Value</th>" +
                "<th style='width:6%'>Goods/Services</th><th style='width:5.5%'>HSN/SAC</th><th style='width:7%'>Taxable value</th><th style='width:3%'>Rate</th><th style='width:6%'>Amt</th>" +
                "</tr></thead>";
            }
            else if (sec == "1" && sub == "4")
            {
                html = "<thead><tr><th style='width:4%;' colspan='2'>Original Invoice</th><th style='width:4%;vertical-align:top' rowspan='2'>Recipient’s State code</th><th style='width:5%;vertical-align:top'   rowspan='2'>Name of the recipient</th><th style='width:36%' colspan='6'>Revised Invoice</th><th style='width:9%' colspan='2'>IGST</th>" +
                "<th style='width:7%;vertical-align:top'   rowspan='2'>POS</th><th style='width:7%;vertical-align:top'   rowspan='2'>Assessment</th>" +
                "</tr><tr><th>No.</th><th>Date</th><th style='width:6%'>No.</th><th style='width:6%'>Date</th><th style='width:5.5%'>Value</th>" +
                "<th style='width:6%'>Goods/Services</th><th style='width:5.5%'>HSN/SAC</th><th style='width:7%'>Taxable value</th><th style='width:3%'>Rate</th><th style='width:6%'>Amt</th>" +
                "</tr></thead>";
            }
            else if (sec == "1" && sub == "5")
            {
                html = "<thead><tr><th style='width:4%;vertical-align:top' rowspan='2'>Goods/Services</th><th style='width:4%;vertical-align:top' rowspan='2'>HSN/SAC</th><th style='width:5%;vertical-align:top'   rowspan='2'>State code</th><th style='width:5%;vertical-align:top'   rowspan='2'>Aggregate Taxable Value</th><th style='width:9%' colspan='2'>IGST</th><th style='width:9%' colspan='2'>CGST</th><th style='width:9%' colspan='2'>SGST</th>" +
                "<th style='width:7%;vertical-align:top'   rowspan='2'>Assessment</th>" +
                "</tr><tr><th style='width:3%'>Rate</th><th style='width:6%'>Amt</th><th style='width:3%'>Rate</th><th style='width:6%'>Amt</th><th style='width:3%'>Rate</th><th style='width:6%'>Amt</th>" +
                "</tr></thead>";
            }
            else if (sec == "1" && sub == "6")
            {

                html = "<thead><tr><th style='width:12%;vertical-align:top' colspan='4'>Original Details</th><th style='width:9%;vertical-align:top' colspan='3'>Revised Details</th><th style='width:5%;vertical-align:top'   rowspan='2'>Aggregate Taxable Value</th><th style='width:9%' colspan='2'>IGST</th><th style='width:9%' colspan='2'>CGST</th><th style='width:9%' colspan='2'>SGST</th>" +
                "<th style='width:7%;vertical-align:top'   rowspan='2'>Assessment</th>" +
                "</tr><tr><th>Month(Tax Period)</th><th>Goods/Services</th><th>HSN/SAC</th><th>State Code</th><th>Goods/Services</th><th>HSN/SAC</th><th>State Code</th><th style='width:3%'>Rate</th><th style='width:6%'>Amt</th><th style='width:3%'>Rate</th><th style='width:6%'>Amt</th><th style='width:3%'>Rate</th><th style='width:6%'>Amt</th>" +
                "</tr></thead>";
            }
            return html;
        }

        public string GetBody(DataTable dt, string req, string sub)
        {
            string htmlstring = "";
            for (int g = 0; g < dt.Rows.Count; g++)
            {
                if (sub == "5")
                {
                    htmlstring += "<tr><td>" + dt.Rows[g][3] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][4] + "</td>";
                    htmlstring += "<td></td>";
                    htmlstring += "<td>" + dt.Rows[g][5] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][6] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][7] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][8] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][9] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][10] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][11] + "</td><td></td></tr>";
                }
                else if (sub == "6")
                {
                    htmlstring += "<tr><td></td>";
                    htmlstring += "<td>" + dt.Rows[g][3] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][4] + "</td>";
                    htmlstring += "<td></td>";
                    htmlstring += "<td>" + dt.Rows[g][3] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][4] + "</td>";
                    htmlstring += "<td></td>";
                    htmlstring += "<td>" + dt.Rows[g][5] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][6] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][7] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][8] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][9] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][10] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][11] + "</td><td></td></tr>";
                }
                else if (sub == "100")
                {
                    htmlstring += "<tr><td>" + dt.Rows[g][0] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][1] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][2] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][3] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][4] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][5] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][6] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][7] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][8] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][9] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][10] + "</td></tr>";
                }
                else if (sub == "101")
                {
                    htmlstring += "<tr>";
                    htmlstring += "<td>" + dt.Rows[g][0] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][1] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][2] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][3] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][4] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][5] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][6] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][7] + "</td></tr>";
                }
                else if (sub == "102")
                {
                    htmlstring += "<tr>";
                    htmlstring += "<td>" + dt.Rows[g][0] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][1] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][2] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][3] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][4] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][5] + "</td></tr>";
                }
                else if (sub == "103")
                {
                    htmlstring += "<tr><td>" + dt.Rows[g][0] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][1] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][2] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][3] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][4] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][5] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][6] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][7] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][8] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][9] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][10] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][11] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][12] + "</td></tr>";
                }
                else if (sub == "104")
                {
                    htmlstring += "<tr><td>" + dt.Rows[g][0] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][1] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][2] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][3] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][4] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][5] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][6] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][7] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][8] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][9] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][10] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][11] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][12] + "</td></tr>";
                }
                else if (sub == "105")
                {
                    htmlstring += "<tr><td>" + dt.Rows[g][0] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][1] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][2] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][3] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][4] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][5] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][6] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][7] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][8] + "</td></tr>";
                }
                else if (sub == "106" || sub == "108")
                {
                    htmlstring += "<tr><td>" + dt.Rows[g][0] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][1] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][2] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][3] + "</td></tr>";
                }
                else if (sub == "107")
                {
                    htmlstring += "<tr><td>" + dt.Rows[g][0] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][1] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][2] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][3] + "</td></tr>";
                }
                else if (sub == "109")
                {
                    htmlstring += "<tr><td>" + dt.Rows[g][0] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][1] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][2] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][3] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][4] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][5] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][6] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][7] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][8] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][9] + "</td></tr>";
                }
                else
                {
                    if (req != "3")
                    {
                        htmlstring += "<tr><td></td>";
                        if (sub == "3") htmlstring += "<td></td>";
                        if (sub == "2" || sub == "4") htmlstring += "<td></td><td></td>";
                        htmlstring += "<td>" + dt.Rows[g][0] + "</td>";
                        htmlstring += "<td>" + dt.Rows[g][1] + "</td>";
                        htmlstring += "<td>" + dt.Rows[g][2] + "</td>";
                        htmlstring += "<td>" + dt.Rows[g][3] + "</td>";
                        htmlstring += "<td>" + dt.Rows[g][4] + "</td>";
                        htmlstring += "<td>" + dt.Rows[g][5] + "</td>";
                        htmlstring += "<td>" + dt.Rows[g][6] + "</td>";
                        htmlstring += "<td>" + dt.Rows[g][7] + "</td>";
                        if (sub != "3" && sub != "4")
                        {
                            htmlstring += "<td>" + dt.Rows[g][8] + "</td>";
                            htmlstring += "<td>" + dt.Rows[g][9] + "</td>";
                            htmlstring += "<td>" + dt.Rows[g][10] + "</td>";
                            htmlstring += "<td>" + dt.Rows[g][11] + "</td>";
                            htmlstring += "<td></td>";
                            htmlstring += "<td></td>";
                        } if (sub == "4")
                        {
                            htmlstring += "<td></td>";
                        }
                        htmlstring += "<td></td>";
                        if (req == "2")
                        {
                            htmlstring += "<td></td>";
                            htmlstring += "<td></td>";
                            htmlstring += "<td></td>";
                            htmlstring += "<td></td>";
                        }
                        htmlstring += "<td></td></tr>";
                    }
                    else if (req == "3")
                    {
                        htmlstring += "<tr><td>" + dt.Rows[g][4] + "</td>";
                        htmlstring += "<td>" + dt.Rows[g][6] + "</td>";
                        htmlstring += "<td>" + dt.Rows[g][5] + "</td>";
                        htmlstring += "<td>" + dt.Rows[g][7] + "</td></tr>";
                    }
                }
            }
            return htmlstring;
        }

        public DataTable GetGSTR3B(string frDT, string toDT,int i)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlConnection cn = new SqlConnection(Connection.Con);
                SqlCommand cmd = new SqlCommand("USP_PROC_GSTR3B", cn);
                cmd.Parameters.AddWithValue("@FROMDT", frDT);
                cmd.Parameters.AddWithValue("@TODT", toDT);
                cmd.Parameters.AddWithValue("@OPERATING", System.Web.HttpContext.Current.Session["OPunit"]);
                cmd.Parameters.AddWithValue("@OPUNIT", System.Web.HttpContext.Current.Session["OPunit"]);
                cmd.Parameters.AddWithValue("@AcOwner", System.Web.HttpContext.Current.Session["AcOwner"]);
                cmd.Parameters.AddWithValue("@FORMATDT", System.Web.HttpContext.Current.Session["FormatDate"]);
                cmd.Parameters.AddWithValue("@SEC", i);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter dad = new SqlDataAdapter(cmd);
                dad.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                var msg = "Failed........";
                System.Web.HttpContext.Current.Session["MessageFrom"] = msg + " Due to " + ex.Message;
                System.Web.HttpContext.Current.Session["SuccessMessageFrom"] = "fail";
                return dt;

            }
        }

        public string GetBody3B(DataTable dt,string sub,string req)
        {
            string htmlstring = "";
            for (int g = 0; g < dt.Rows.Count; g++)
            {
                if (sub == "1")
                {
                    htmlstring += "<tr><td colspan=2>" + dt.Rows[g][0] + "</td>";
                    htmlstring += "<td colspan=2>" + dt.Rows[g][1] + "</td>";
                    htmlstring += "<td colspan=2>" + dt.Rows[g][2] + "</td>";
                    htmlstring += "<td colspan=2>" + dt.Rows[g][3] + "</td>";
                    htmlstring += "<td colspan=2>" + dt.Rows[g][4] + "</td>";
                    htmlstring += "<td colspan=2>" + dt.Rows[g][5] + "</td></tr>";
                }
                else if (sub == "2")
                {
                    htmlstring += "<tr><td  colspan=3>" + dt.Rows[g][0] + "</td>";
                    htmlstring += "<td  colspan=3>" + dt.Rows[g][1] + "</td>";
                    htmlstring += "<td  colspan=3>" + dt.Rows[g][2] + "</td>";
                    htmlstring += "<td  colspan=3>" + dt.Rows[g][3] + "</td></tr>";
                }
                else if (sub == "3")
                {
                    htmlstring += "<tr><td colspan=3>" + dt.Rows[g][0] + "</td>";
                    htmlstring += "<td colspan=2>" + dt.Rows[g][1] + "</td>";
                    htmlstring += "<td colspan=3>" + dt.Rows[g][2] + "</td>";
                    htmlstring += "<td colspan=2>" + dt.Rows[g][3] + "</td>";
                    htmlstring += "<td colspan=2>" + dt.Rows[g][4] + "</td></tr>";
                }
                else if (sub == "4")
                {
                    htmlstring += "<tr><td colspan=4>" + dt.Rows[g][0] + "</td>";
                    htmlstring += "<td colspan=4>" + dt.Rows[g][1] + "</td>";
                    htmlstring += "<td colspan=4>" + dt.Rows[g][2] + "</td></tr>";
                }
                else if (sub == "5")
                {
                    htmlstring += "<tr><td colspan=2>" + dt.Rows[g][0] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][1] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][2] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][3] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][4] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][5] + "</td>";
                    htmlstring += "<td colspan=2>" + dt.Rows[g][6] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][7] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][8] + "</td>";
                    htmlstring += "<td>" + dt.Rows[g][9] + "</td></tr>";
                }
                else if (sub == "6")
                {
                    htmlstring += "<tr><td colspan=3>" + dt.Rows[g][0] + "</td>";
                    htmlstring += "<td colspan=3>" + dt.Rows[g][1] + "</td>";
                    htmlstring += "<td colspan=3>" + dt.Rows[g][2] + "</td>";
                    htmlstring += "<td colspan=3>" + dt.Rows[g][3] + "</td></tr>";
                }
            }
            return htmlstring;
        }

        public DataTable ConvertHTMLTablesToDataTable(string HTML)
        {


            DataTable dt = null;
            DataRow dr = null;
            DataColumn dc = null;
            string TableExpression = "<table[^>]*>(.*?)</table>";
            string HeaderExpression = "<th[^>]*>(.*?)</th>";
            string RowExpression = "<tr[^>]*>(.*?)</tr>";
            string ColumnExpression = "<td[^>]*>(.*?)</td>";
            bool HeadersExist = false;
            int iCurrentColumn = 0;
            int iCurrentRow = 0;

            // Get a match for all the tables in the HTML    
            MatchCollection Tables = Regex.Matches(HTML, TableExpression, RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase);

            // Loop through each table element    
            foreach (Match Table in Tables)
            {

                // Reset the current row counter and the header flag    
                iCurrentRow = 0;
                HeadersExist = false;

                // Add a new table to the DataSet    
                dt = new DataTable();

                // Create the relevant amount of columns for this table (use the headers if they exist, otherwise use default names)    
                if (Table.Value.Contains("<th"))
                {
                    // Set the HeadersExist flag    
                    HeadersExist = true;

                    // Get a match for all the rows in the table    
                    MatchCollection Headers = Regex.Matches(Table.Value.Replace("<thead><tr>", "").Replace("</tr><tr>", ""), HeaderExpression, RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase);

                    // Loop through each header element    
                    foreach (Match Header in Headers)
                    {
                        //dt.Columns.Add(Header.Groups(1).ToString); 
                        if (Header.Groups[1].ToString() != "")
                            dt.Columns.Add(Header.Groups[1].ToString());

                    }
                }
                else
                {
                    //for (int iColumns = 1; iColumns <= Regex.Matches(Regex.Matches(Regex.Matches(Table.Value, TableExpression, RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase)[0].ToString(), RowExpression, RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase)[0].ToString(), ColumnExpression, RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase).Count; iColumns++)
                    //{
                    //    dt.Columns.Add("Column " + iColumns);
                    //}
                }

                // Get a match for all the rows in the table    
                MatchCollection Rows = Regex.Matches(Table.Value, RowExpression, RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase);

                // Loop through each row element    
                foreach (Match Row in Rows)
                {

                    // Only loop through the row if it isn't a header row    
                    if (!(iCurrentRow == 0 & HeadersExist == true))
                    {

                        // Create a new row and reset the current column counter    
                        dr = dt.NewRow();
                        iCurrentColumn = 0;

                        // Get a match for all the columns in the row    
                        MatchCollection Columns = Regex.Matches(Row.Value, ColumnExpression, RegexOptions.Multiline | RegexOptions.Singleline | RegexOptions.IgnoreCase);

                        // Loop through each column element    
                        foreach (Match Column in Columns)
                        {

                            DataColumnCollection columns = dt.Columns;

                            //if (!columns.Contains("Column " + iCurrentColumn))
                            //{
                            //    //Add Columns  
                            //    dt.Columns.Add("Column " + iCurrentColumn);
                            //}
                            // Add the value to the DataRow  
                            if (iCurrentColumn < dt.Columns.Count)
                                dr[iCurrentColumn] = Column.Groups[1].ToString();
                            // Increase the current column    
                            iCurrentColumn += 1;

                        }

                        // Add the DataRow to the DataTable    
                        dt.Rows.Add(dr);

                    }

                    // Increase the current row counter    
                    iCurrentRow += 1;
                }


            }

            return (dt);

        }

    }
}
