﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eBizSuiteAppModel.Tree
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class EbizsuiteDbTreeEntities : DbContext
    {
        public EbizsuiteDbTreeEntities()
            : base("name=EbizsuiteDbTreeEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<CustomMenuMaster> CustomMenuMasters { get; set; }
        public DbSet<Field_Master> Field_Master { get; set; }
        public DbSet<Form_Master> Form_Master { get; set; }
        public DbSet<MenuMaster> MenuMasters { get; set; }
        public DbSet<Table_Mapping> Table_Mapping { get; set; }
        public DbSet<TabMaster> TabMasters { get; set; }
        public DbSet<Tbl_Emp> Tbl_Emp { get; set; }
    }
}
