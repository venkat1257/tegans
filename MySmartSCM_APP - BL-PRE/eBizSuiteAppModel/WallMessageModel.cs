﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace eBizSuiteAppModel
{
    public class WallMessageModel
    {
        public string name { get; set; }
        public string link { get; set; }
        public string caption { get; set; }
        public string description { get; set; }
        public string source { get; set; }
        public string actions { get; set; }
        public string privacy { get; set; }
        [Required(ErrorMessage = "Please enter somthing", AllowEmptyStrings = false)]
        public string message { get; set; }
    }
}
