﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eBizSuiteAppModel
{
    public class MethodResult
    {
        public string errorMessage;
        public ReturnCode returnCode;

        public enum ReturnCode
        {
            Unknown = 0,
            Success = 1,
            Failure
        }
    }
}
