//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eBizSuiteAppModel.Table
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_EmpAddress
    {
        public int ADID { get; set; }
        public int EmployeeID { get; set; }
        public Nullable<int> Form_Id { get; set; }
        public Nullable<long> Primary_point_of_contact { get; set; }
        public string Addressee { get; set; }
        public Nullable<long> Phone_No { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public Nullable<int> Zip { get; set; }
        public string Country { get; set; }
        public string Default_shipping { get; set; }
        public string Home { get; set; }
        public string Office { get; set; }
    }
}
