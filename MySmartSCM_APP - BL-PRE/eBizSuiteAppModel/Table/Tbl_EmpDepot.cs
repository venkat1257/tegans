//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eBizSuiteAppModel.Table
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_EmpDepot
    {
        public int EDID { get; set; }
        public int EmployeeID { get; set; }
        public Nullable<int> Form_Id { get; set; }
        public string Depotexpenes { get; set; }
        public string Labour { get; set; }
        public Nullable<int> Work_Hours { get; set; }
    }
}
