﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eBizSuiteAppModel
{
    public class FacebookLoginModel
    {
        public string uid { get; set; }
        public string accessToken { get; set; }
    }
}
