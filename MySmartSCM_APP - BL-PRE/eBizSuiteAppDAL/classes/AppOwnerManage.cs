﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eBizSuiteAppModel.Table;

namespace eBizSuiteDAL.classes
{
  public class AppOwnerManage
    {
      public eBizSuiteTableEntities entities = new eBizSuiteTableEntities();

      public IEnumerable<tbl_AppOwnerMaster> GetAppOwnerMaster()
        {
            return entities.tbl_AppOwnerMaster.ToList();
        }

      public void Add(tbl_AppOwnerMaster register)
        {
            tbl_AppOwnerMaster sysRegister = new tbl_AppOwnerMaster();

            sysRegister.ApplicationOwnerId = register.ApplicationOwnerId;
            sysRegister.AppOwnerUserName = register.AppOwnerUserName;
            sysRegister.CompanyName = register.CompanyName;

            entities.tbl_AppOwnerMaster.Add(sysRegister);
            entities.SaveChanges();
        }

        public bool IsUserLoginIDExist(int userId)
        {
            return (from o in entities.tbl_AppOwnerMaster where o.ApplicationOwnerId == userId select o).Any();
        }

        public tbl_AppOwnerMaster GetEmployeeDetail(int empID)
        {

            return entities.tbl_AppOwnerMaster.Where(m => m.ApplicationOwnerId == empID).FirstOrDefault();
        }

        public bool UpdateAppOwnerMaster(tbl_AppOwnerMaster register)
        {
            try
            {
                tbl_AppOwnerMaster sysRegister = entities.tbl_AppOwnerMaster.Where(m => m.ApplicationOwnerId == register.ApplicationOwnerId).FirstOrDefault();

                sysRegister.ApplicationOwnerId = register.ApplicationOwnerId;
                sysRegister.AppOwnerUserName = register.AppOwnerUserName;
                sysRegister.CompanyName = register.CompanyName;



                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                return false;
            }
        }

        public bool DeleteAppOwnerMaster(int mCustID)
        {
            try
            {
                tbl_AppOwnerMaster data = entities.tbl_AppOwnerMaster.Where(m => m.ApplicationOwnerId == mCustID).FirstOrDefault();
                entities.tbl_AppOwnerMaster.Remove(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                return false;
            }
        }
    }
}
