﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eBizSuiteAppModel.Table;

namespace eBizSuiteDAL.classes
{
   public class ModuleMasterManage
    {
       public eBizSuiteTableEntities entities = new eBizSuiteTableEntities();

        public IEnumerable<CONTABLE001> GetModuleMaster()
        {
            return entities.CONTABLE001.ToList();
        }

        public void Add(CONTABLE001 register)
        {
            CONTABLE001 sysRegister = new CONTABLE001();

            sysRegister.COLUMN02 = register.COLUMN02;
            sysRegister.COLUMN03 = register.COLUMN03;
            sysRegister.COLUMN04 = register.COLUMN04;
            sysRegister.COLUMN05 = register.COLUMN05;
            sysRegister.COLUMN06 = register.COLUMN06;

            entities.CONTABLE001.Add(sysRegister);
            entities.SaveChanges();
        }

        public bool IsUserLoginIDExist(int userId)
        {
            return (from o in entities.CONTABLE001 where o.COLUMN02 == userId select o).Any();
        }

        public CONTABLE001 GetEmployeeDetail(int empID)
        {

            return entities.CONTABLE001.Where(m => m.COLUMN02 == empID).FirstOrDefault();
        }

        public bool UpdateModuleMaster(CONTABLE001 register)
        {
            try
            {
                CONTABLE001 sysRegister = entities.CONTABLE001.Where(m => m.COLUMN02 == register.COLUMN02).FirstOrDefault();
                sysRegister.COLUMN02 = register.COLUMN02;
                sysRegister.COLUMN03 = register.COLUMN03;
                sysRegister.COLUMN04 = register.COLUMN04;
                sysRegister.COLUMN05 = register.COLUMN05;
                sysRegister.COLUMN06 = register.COLUMN06;

                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                return false;
            }
        }

        public bool DeleteModuleMaster(int mCustID)
        {
            try
            {
                CONTABLE001 data = entities.CONTABLE001.Where(m => m.COLUMN02 == mCustID).FirstOrDefault();
                entities.CONTABLE001.Remove(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                return false;
            }
        }
    }
}
