﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;
using System.Data;
using System.ComponentModel.DataAnnotations;
using eBizSuiteAppModel.Table;
using eBizSuiteAppDAL.classes;
using System.Web.Mvc;
using System.Drawing.Imaging;
namespace eBizSuiteAppDAL.classes
{
   
    public class LogWriter
    {
        eBizSuiteTableEntities dc = new eBizSuiteTableEntities();
        public FileStreamResult CreateFile(string data)
        {
            var byteArray = Encoding.ASCII.GetBytes(data);
            var stream = new MemoryStream(byteArray);

            return new FileStreamResult(stream, "text/plain");
        }
        public void CreateFile(string path, string file, string data)
        {
            var imagepath = dc.CONTABLE026.Where(a => a.COLUMN02 == "1000").FirstOrDefault();
            DateTime dt = DateTime.Now;
            if (!Directory.Exists(path + "/" + dt.Year))
                Directory.CreateDirectory(path + dt.Year);
            if (!Directory.Exists(path + "/" + dt.Year + "/" + dt.Month))
                Directory.CreateDirectory(path + "/" + dt.Year + "/" + dt.Month);
            if (!Directory.Exists(path + "/" + dt.Year + "/" + dt.Month + "/" + dt.Day))
                Directory.CreateDirectory(path + "/" + dt.Year + "/" + dt.Month + "/" + dt.Day);
            string fp = path + "/" + dt.Year + "/" + dt.Month + "/" + dt.Day + "/" + file + ".txt";
            if (!File.Exists(fp))
            {
                string pathh = Path.Combine(HttpContext.Current.Server.MapPath("~/"), file + ".txt");
                var byteArray = Encoding.ASCII.GetBytes(data);
                var stream = new MemoryStream(byteArray);
                using (FileStream fs1 = new FileStream(path + "/" + dt.Year + "/" + dt.Month + "/" + dt.Day + "/" + file + ".txt", FileMode.OpenOrCreate, FileAccess.Write))
                {
                    //FileStream fs1 = new FileStream(path + "/" + dt.Year + "/" + dt.Month + "/" + dt.Day + "/" + file + ".txt", FileMode.OpenOrCreate, FileAccess.Write);
                    using (StreamWriter writer = new StreamWriter(fs1))
                    {
                        writer.WriteLine();
                        writer.Write(data + " " + dt);
                        writer.Close();
                    }
                }
            }
            else
            {
                using (TextWriter tw = new StreamWriter(fp, true))
                {
                    tw.WriteLine(data + " " + dt);
                    tw.Close();
                }
            }
        }

        public string ImageFile(string formname, string modulename)
        {
            var path = dc.CONTABLE026.Where(a => a.COLUMN02 == "1000").FirstOrDefault().COLUMN04.ToString();
            if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Upload/Images"))))
                Directory.CreateDirectory(Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Upload/Images")));
            if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Upload/Images/" + modulename))))
                Directory.CreateDirectory(Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Upload/Images/" + modulename)));
            if (!Directory.Exists(Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Upload/Images/" + modulename + "/" + formname))))
                Directory.CreateDirectory(Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Upload/Images/" + modulename + "/" + formname)));
            return Path.Combine(HttpContext.Current.Server.MapPath("~/Content/Upload/Images/" + modulename + "/" + formname));
        }
    }
}
