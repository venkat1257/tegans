﻿using eBizSuiteAppModel.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace eBizSuiteDAL.classes
{
    public class RoleMasterManage
    {
        public eBizSuiteTableEntities entities = new eBizSuiteTableEntities();

        public IEnumerable<CONTABLE012> GetRoleMaster()
        {
            return entities.CONTABLE012.ToList();
        }

        public void Add(CONTABLE012 register)
        {
            CONTABLE012 sysRegister = new CONTABLE012();

            sysRegister.COLUMN02 = register.COLUMN02;
            sysRegister.COLUMN03 = register.COLUMN03;
            sysRegister.COLUMN04 = register.COLUMN04;
            sysRegister.COLUMN05 = register.COLUMN05;
            sysRegister.COLUMN06 = register.COLUMN06;
            sysRegister.COLUMN07 = register.COLUMN07;

            entities.CONTABLE012.Add(sysRegister);
            entities.SaveChanges();
        }

        public bool IsUserLoginIDExist(int userId)
        {
            return (from o in entities.CONTABLE012 where o.COLUMN02 == userId select o).Any();
        }

        public CONTABLE012 GetEmployeeDetail(int empID)
        {

            return entities.CONTABLE012.Where(m => m.COLUMN02 == empID).FirstOrDefault();
        }

        public bool UpdateRoleMaster(CONTABLE012 register)
        {
            try
            {
                CONTABLE012 sysRegister = entities.CONTABLE012.Where(m => m.COLUMN02 == register.COLUMN02).FirstOrDefault();

                sysRegister.COLUMN02 = register.COLUMN02;
                sysRegister.COLUMN03 = register.COLUMN03;
                sysRegister.COLUMN04 = register.COLUMN04;
                sysRegister.COLUMN05 = register.COLUMN05;
                sysRegister.COLUMN06 = register.COLUMN06;
                sysRegister.COLUMN07 = register.COLUMN07;



                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                return false;
            }
        }

        //public bool DeleteRoleMaster(int mCustID)
        //{
        //    try
        //    {
        //        //CONTABLE012 data = entities.CONTABLE012.Where(m => m.COLUMN02 == mCustID).FirstOrDefault();
        //         CONTABLE012 data = entities.CONTABLE012.Find(mCustID);
        //        entities.CONTABLE012.Remove(data);
        //        entities.SaveChanges();
        //        return true;
        //    }
        //    catch (Exception mex)
        //    {
        //        return false;
        //    }
        //}

    }
}
