﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using eBizSuiteAppModel.Table;


namespace eBizSuiteDAL
{
    public class AdminSesurityAnswerManage
    {
        eBizSuiteTableEntities dbContext = new eBizSuiteTableEntities();

        public void Add(CONTABLE019 sa, int userId)
        {


            CONTABLE019 dbSecAns = new CONTABLE019();
            var col02 = dbContext.CONTABLE019.OrderByDescending(a => a.COLUMN02).ToList();
            var col2data = col02.Select(a => a.COLUMN02).FirstOrDefault();
            if (col2data == null)
            {
                dbSecAns.COLUMN02 = 100;
            }
            else
            {
                dbSecAns.COLUMN02 = (col2data + 1);
            }

            CONTABLE018 dcQ = new CONTABLE018();
            var tid = dbContext.CONTABLE018.Where(a => a.COLUMN03 == sa.COLUMN04).Select(a => a.COLUMN02).ToList();



            dbSecAns.COLUMN03 = userId;
            dbSecAns.COLUMN04 = sa.COLUMN04;
            dbSecAns.COLUMN05 = sa.COLUMN05;
            dbSecAns.COLUMN06 = sa.COLUMN06;
            dbSecAns.COLUMN07 = sa.COLUMN07;
            dbSecAns.COLUMN08 = sa.COLUMN08;
            dbSecAns.COLUMN09 = sa.COLUMN09;
            dbContext.CONTABLE019.Add(dbSecAns);
            dbContext.SaveChanges();
        }

        public bool IsUserLoginIDExist(int userId1)
        {
            return (from o in dbContext.CONTABLE019 where o.COLUMN03 == userId1 select o).Any();
        }
    }
}
