﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using eBizSuiteAppModel.Table;

namespace eBizSuiteDAL.classes
{
    public class EmployeeMasterManage
    {
        public eBizSuiteTableEntities entities = new eBizSuiteTableEntities();

        public IEnumerable<tbl_EmployeeMaster> GetEmployeeMaster()
        {
            return entities.tbl_EmployeeMaster.ToList();
        }

        public void Add(tbl_EmployeeMaster register)
        {
            tbl_EmployeeMaster sysRegister = new tbl_EmployeeMaster();

            sysRegister.EmployeeID = register.EmployeeID;
            sysRegister.EmployeeName = register.EmployeeName;
            sysRegister.CompanyName = register.CompanyName;
            sysRegister.TypeOfBusines = register.TypeOfBusines;
            sysRegister.IsActive = Convert.ToString(register.IsActive);
            sysRegister.Email = register.Email;
            if (register.Password == "true")
            {
                sysRegister.Password = "eBizSuite";
            }
            else { sysRegister.Password = "NULL"; }

            entities.tbl_EmployeeMaster.Add(sysRegister);
            entities.SaveChanges();
        }

        public bool IsUserLoginIDExist(int userId)
        {
            return (from o in entities.tbl_EmployeeMaster where o.EmployeeID == userId select o).Any();
        }

        public tbl_EmployeeMaster GetEmployeeDetail(int empID)
        {

            return entities.tbl_EmployeeMaster.Where(m => m.EmployeeID == empID).FirstOrDefault();
        }

        public bool UpdateEmployeeMaster(tbl_EmployeeMaster register)
        {
            try
            {
                tbl_EmployeeMaster sysRegister = entities.tbl_EmployeeMaster.Where(m => m.EmployeeID == register.EmployeeID).FirstOrDefault();

                sysRegister.EmployeeID = register.EmployeeID;
                sysRegister.EmployeeName = register.EmployeeName;
                sysRegister.CompanyName = register.CompanyName;



                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                return false;
            }
        }

        public bool DeleteEmployeeMaster(int mCustID)
        {
            try
            {
                tbl_EmployeeMaster data = entities.tbl_EmployeeMaster.Where(m => m.EmployeeID == mCustID).FirstOrDefault();
                entities.tbl_EmployeeMaster.Remove(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                return false;
            }
        }

    }

    public class EmployeMaster
    {
        public int EmployeeID { get; set; }

        [Required(ErrorMessage = "Please provide EmployeeName", AllowEmptyStrings = false)]
        public string EmployeeName { get; set; }

        [Required(ErrorMessage = "Please provide CompanyName", AllowEmptyStrings = false)]
        public string CompanyName { get; set; }
        [Required(ErrorMessage = "Please provide TypeOfBusines", AllowEmptyStrings = false)]
        public string TypeOfBusines { get; set; }

        public bool IsActive { get; set; }

        [Required(ErrorMessage = "Please provide Email ID", AllowEmptyStrings = false)]
        [RegularExpression(@"^([0-9a-zA-Z]([\+\-_\.][0-9a-zA-Z]+)*)+@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,3})$",
        ErrorMessage = "Please provide valid email id")]
        public string Email { get; set; }

        
        [Display(Name = "IS Login required ?")]
        public bool Password { get; set; }


    }
}
