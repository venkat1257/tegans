﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eBizSuiteAppModel.Table;

namespace eBizSuiteDAL
{
    public class RegisterManage
    {
        public eBizSuiteTableEntities dbUser = new eBizSuiteTableEntities();
        public eBizSuiteTableEntities dbRegister = new eBizSuiteTableEntities();
        public void Add(tbl_AdminRegistration register)
        {

            tbl_AdminRegistration sysRegister = new tbl_AdminRegistration();
            sysRegister.UserID = register.UserID;
            sysRegister.FullName = register.FullName;
            sysRegister.UserName = register.UserName;
            sysRegister.Password = register.Password;
            sysRegister.EmailID = register.EmailID;


            tbl_AdminLogOn sysUser = new tbl_AdminLogOn();
            sysUser.UserID = register.UserID;
            sysUser.UserName = register.UserName;
            sysUser.Password = register.Password;

            dbRegister.tbl_AdminRegistration.Add(sysRegister);
            dbRegister.SaveChanges();

            dbUser.tbl_AdminLogOn.Add(sysUser);
            dbUser.SaveChanges();


        }
        public bool IsUserLoginIDExist(int userId)
        {
            return (from o in dbRegister.tbl_AdminRegistration where o.UserID == userId select o).Any();
        }

    }
}
