﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eBizSuiteAppModel.Table;

namespace eBizSuiteDAL.classes
{
    public class CenterMasterManage

    {
        public eBizSuiteTableEntities entities = new eBizSuiteTableEntities();

       public IEnumerable<CONTABLE011> GetCenterMaster()
        {
            return entities.CONTABLE011.ToList();
        }

        public void Add(CONTABLE011 register)
        {
            CONTABLE011 sysRegister = new CONTABLE011();

            sysRegister.COLUMN02 = register.COLUMN02;
            sysRegister.COLUMN03 = register.COLUMN03;
            sysRegister.COLUMN04 = register.COLUMN04;
            sysRegister.COLUMN05 = register.COLUMN05;
            sysRegister.COLUMN06 = register.COLUMN06;

            entities.CONTABLE011.Add(sysRegister);
            entities.SaveChanges();
        }

        public bool IsUserLoginIDExist(int userId)
        {
            return (from o in entities.CONTABLE011 where o.COLUMN02 == userId select o).Any();
        }

        public CONTABLE011 GetEmployeeDetail(int empID)
        {

            return entities.CONTABLE011.Where(m => m.COLUMN02 == empID).FirstOrDefault();
        }

        public int?[] GetForms(int mID)
        {
            var str = entities.CONTABLE003.Where(a => a.COLUMN04 == mID).Select(q => q.COLUMN05).ToArray();
            var strID = entities.CONTABLE003.Where(a =>str.Contains( a.COLUMN05 ) && a.COLUMN04==101).Select(q => q.COLUMN02).ToArray();
            return strID;
                
        }

        public bool UpdateCenterMaster(CONTABLE011 register)
        {
            try
            {
                CONTABLE011 sysRegister = entities.CONTABLE011.Where(m => m.COLUMN02 == register.COLUMN02).FirstOrDefault();

                sysRegister.COLUMN02 = register.COLUMN02;
                sysRegister.COLUMN03 = register.COLUMN03;
                sysRegister.COLUMN04 = register.COLUMN04;
                sysRegister.COLUMN05 = register.COLUMN05;
                sysRegister.COLUMN06 = register.COLUMN06;


                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                return false;
            }
        }

        //public bool DeleteCenterMaster(int mCustID)
        //{
        //    try
        //    {
        //        //CONTABLE011 data = entities.CONTABLE011.Where(m => m.COLUMN02 == mCustID).FirstOrDefault();
        //         CONTABLE011 data = entities.CONTABLE011.Find(mCustID);
        //        entities.CONTABLE011.Remove(data);
        //        entities.SaveChanges();
        //        return true;
        //    }
        //    catch (Exception mex)
        //    {
        //        return false;
        //    }
        //}

    }
}
