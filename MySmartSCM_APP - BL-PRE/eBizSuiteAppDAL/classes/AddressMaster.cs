﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace eBizSuiteAppDAL.classes
{   //ItemRecp
    public class AddressMaster
    {
        public int Check(int PurchaseOrderID)
        {
            int order = 0;
            SqlConnection conn1 = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn1.Open();
            SqlCommand cmd1 = new SqlCommand("usp_PUR_TR_CHECK", conn1);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            //conn1.Close();
            if (dt1.Rows.Count > 0)
            {
                order = Convert.ToInt32(dt1.Rows[0]["COLUMN03"].ToString());
                return order;
            }
            return PurchaseOrderID;
        }

        public string GetAddress(int PurchaseOrderID)
        {
            string collist = null;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand cmd = new SqlCommand("usp_PUR_TP_Address", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            //conn.Close();
            if (dt.Rows.Count > 0)
            {

                if (dt.Rows[0]["COLUMN05"].ToString() == "" || dt.Rows[0]["COLUMN05"].ToString() == null)
                    collist += "";
                else
                    collist = dt.Rows[0]["COLUMN05"].ToString();
                if (dt.Rows[0]["COLUMN06"].ToString() == "" || dt.Rows[0]["COLUMN06"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN06"].ToString();
                if (dt.Rows[0]["COLUMN07"].ToString() == "" || dt.Rows[0]["COLUMN07"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN07"].ToString();
                if (dt.Rows[0]["COLUMN08"].ToString() == "" || dt.Rows[0]["COLUMN08"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN08"].ToString();
                if (dt.Rows[0]["COLUMN09"].ToString() == "" || dt.Rows[0]["COLUMN09"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN09"].ToString();
                if (dt.Rows[0]["COLUMN10"].ToString() == "" || dt.Rows[0]["COLUMN10"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN10"].ToString();
                if (dt.Rows[0]["COLUMN11"].ToString() == "" || dt.Rows[0]["COLUMN11"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN11"].ToString();
                if (dt.Rows[0]["COLUMN15"].ToString() == "" || dt.Rows[0]["COLUMN15"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN15"].ToString();
                if (collist != null && collist != "")
                {
                    Array las = new Array[7];
                    las = collist.Split(','); collist = "";
                    for (int i = 0; i < las.Length; i++)
                    {
                        if (i == las.Length - 1)
                            collist += las.GetValue(i) + ".";
                        else if (las.GetValue(i) == "")
                            collist += "";
                        else
                            collist += las.GetValue(i) + ",\r\n";
                    }
                }


            }
            return collist;
        }

        public string GetAddress1(int PurchaseOrderID)
        {
            string collist = null;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand cmd = new SqlCommand("usp_PUR_TR_Address1", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            //conn.Close();
            if (dt.Rows.Count > 0)
            {
                collist = dt.Rows[0]["COLUMN05"].ToString();
                collist += "," + "\r\n" + dt.Rows[0]["COLUMN06"].ToString() + "," + "\r\n" + dt.Rows[0]["COLUMN07"].ToString() + "," + "\r\n" + dt.Rows[0]["COLUMN08"].ToString() + "," + "\r\n" + dt.Rows[0]["COLUMN09"].ToString() + "," + "\r\n" + dt.Rows[0]["COLUMN10"].ToString() + "," + "\r\n" + dt.Rows[0]["COLUMN11"].ToString() + ".";
            } return collist;
        }

        public string GetVendorAddress(int VendorID)
        {
            string collist = null;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand cmd = new SqlCommand("usp_PUR_TP_V_Address", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@VendorID", VendorID));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            //conn.Close();
            if (dt.Rows.Count > 0)
            {

                if (dt.Rows[0]["COLUMN06"].ToString() == "" || dt.Rows[0]["COLUMN06"].ToString() == null)
                    collist += "";
                else
                    collist = dt.Rows[0]["COLUMN06"].ToString();
                if (dt.Rows[0]["COLUMN07"].ToString() == "" || dt.Rows[0]["COLUMN07"].ToString() == null)
                    collist += "";
                else
                    collist += " " + dt.Rows[0]["COLUMN07"].ToString();
                if (dt.Rows[0]["COLUMN08"].ToString() == "" || dt.Rows[0]["COLUMN08"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN08"].ToString();
                if (dt.Rows[0]["COLUMN09"].ToString() == "" || dt.Rows[0]["COLUMN09"].ToString() == null)
                    collist += "";
                else
                    collist += " " + dt.Rows[0]["COLUMN09"].ToString();
                if (dt.Rows[0]["COLUMN10"].ToString() == "" || dt.Rows[0]["COLUMN10"].ToString() == null)
                    collist += "";
                else
                    collist += " " + dt.Rows[0]["COLUMN10"].ToString();
                if (dt.Rows[0]["COLUMN11"].ToString() == "" || dt.Rows[0]["COLUMN11"].ToString() == null)
                    collist += "";
                else
                    collist += " " + dt.Rows[0]["COLUMN11"].ToString();
                if (dt.Rows[0]["COLUMN12"].ToString() == "" || dt.Rows[0]["COLUMN12"].ToString() == null)
                    collist += "";
                else
                    collist += " " + dt.Rows[0]["COLUMN12"].ToString();
                if (dt.Rows[0]["COLUMN16"].ToString() == "" || dt.Rows[0]["COLUMN16"].ToString() == null)
                    collist += "";
                else
                    collist += " " + dt.Rows[0]["COLUMN16"].ToString();
                if (collist != null && collist != "")
                {
                    Array las = new Array[7];
                    las = collist.Split(' '); collist = "";
                    for (int i = 0; i < las.Length; i++)
                    {
                        if (i == las.Length - 1)
                            collist += las.GetValue(i) + ".";
                        else if (las.GetValue(i) == "")
                            collist += "";
                        else
                            collist += las.GetValue(i) + " \r\n";
                    }
                }


            }

            return collist;
        }

        public string GetOrderQty1(int PurchaseOrderID)
        {
            string collist = "0";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand cmd = new SqlCommand("usp_PUR_TR_GetOrderQty1", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            //conn.Close();
            collist = dt.Rows[0]["COLUMN08"].ToString();

            return collist;
        }

        public string GetQty1(int PurchaseOrderID)
        {
            string collist = "0";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand cmd = new SqlCommand("usp_PUR_TR_GetQty1", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            //conn.Close();
            if (dt.Rows.Count > 0)
            {
                collist = dt.Rows[0]["COLUMN08"].ToString();
            }
            return collist;
        }

        public string GetReceivedQty1(int PurchaseOrderID)
        {
            string collist = "0";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand cmd = new SqlCommand("usp_PUR_TR_GetReceivedQty1", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            //conn.Close();
            if (dt.Rows.Count > 0)
            {
                collist = dt.Rows[0]["COLUMN09"].ToString();
            } return collist;
        }

        public string GetItem1(int PurchaseOrderID)
        {
            string collist = null;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand cmd = new SqlCommand("usp_PUR_TR_GetItem1", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            //conn.Close();
            if (dt.Rows.Count > 0)
            {
                collist = dt.Rows[0]["COLUMN03"].ToString();
            }
            return collist;
        }

        public string GetItemDescription1(int PurchaseOrderID)
        {
            string collist = null;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand cmd = new SqlCommand("usp_PUR_TR_GetItemDescription1", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            //conn.Close();
            if (dt.Rows.Count > 0)
            {
                collist = dt.Rows[0]["COLUMN04"].ToString();
            }
            return collist;
        }

        public string GetUnits1(int PurchaseOrderID)
        {
            string collist = "0";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand cmd = new SqlCommand("usp_PUR_TR_GetUnits1", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            //conn.Close();
            if (dt.Rows.Count > 0)
            {
                collist = dt.Rows[0]["COLUMN05"].ToString();
            }
            return collist;
        }

        public string GetPrice1(int PurchaseOrderID)
        {
            string collist = "0";
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand cmd = new SqlCommand("usp_PUR_TR_GetPrice1", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            //conn.Close();
            if (dt.Rows.Count > 0)
            {
                collist = dt.Rows[0]["COLUMN10"].ToString();
            }
            return collist;
        }

        public string GetOperatingUnit1(int PurchaseOrderID)
        {
            string collist = null;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand cmd = new SqlCommand("usp_PUR_TR_GetOperatingUnit1", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            //conn.Close();
            if (dt.Rows.Count > 0)
            {
                collist = dt.Rows[0]["COLUMN12"].ToString();
            }
            return collist;
        }

        public string GetSubsidary1(int PurchaseOrderID)
        {
            string collist = null;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand cmd = new SqlCommand("usp_PUR_TR_GetSubsidary1", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            //conn.Close();
            if (dt.Rows.Count > 0)
            {
                collist = dt.Rows[0]["COLUMN13"].ToString();
            }
            return collist;
        }

        public string GetClassification1(int PurchaseOrderID)
        {
            string collist = null;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand cmd = new SqlCommand("usp_PUR_TR_GetClassification1", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            //conn.Close();
            if (dt.Rows.Count > 0)
            {
                collist = dt.Rows[0]["COLUMN14"].ToString();
            }
            return collist;
        }

        public string GetDepartment1(int PurchaseOrderID)
        {
            string collist = null;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand cmd = new SqlCommand("usp_PUR_TR_GetDepartment1", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            //conn.Close();
            if (dt.Rows.Count > 0)
            {
                collist = dt.Rows[0]["COLUMN15"].ToString();
            }
            return collist;
        }

        //public int Check(int SalesOrderID)
        //{
        //    int order = 0;
        //    SqlConnection conn1 = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        //    conn1.Open();
        //    SqlCommand cmd1 = new SqlCommand("usp_PUR_TR_CHECK", conn1);
        //    cmd1.CommandType = CommandType.StoredProcedure;
        //    cmd1.Parameters.Add(new SqlParameter("@SalesOrderID", SalesOrderID));
        //    SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
        //    DataTable dt1 = new DataTable();
        //    da1.Fill(dt1);
        //    conn1.Close();
        //    if (dt1.Rows.Count > 0)
        //    {
        //        order =Convert.ToInt32( dt1.Rows[0]["COLUMN06"]);
        //        return order;
        //    }
        //    return SalesOrderID;
        //}


        //public string GetAddress(int PurchaseOrderID)
        //{
        //    string collist = null;
        //    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        //    conn.Open();
        //    SqlCommand cmd = new SqlCommand("usp_PUR_TR_Address", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
        //    SqlDataAdapter da = new SqlDataAdapter(cmd);
        //    DataTable dt = new DataTable();
        //    da.Fill(dt);
        //    conn.Close();
        //    if (dt.Rows.Count > 0)
        //    {

        //        if (dt.Rows[0]["COLUMN05"].ToString() == "")
        //        {
        //            collist = dt.Rows[0]["COLUMN05"].ToString();
        //            collist += "";
        //        }
        //        else
        //        {
        //            collist = dt.Rows[0]["COLUMN05"].ToString();
        //            collist += "," + "\r\n";
        //        }

        //        if (dt.Rows[0]["COLUMN06"].ToString() == "")
        //        {
        //            collist += dt.Rows[0]["COLUMN06"].ToString();
        //            collist += "";
        //        }
        //        else
        //        {
        //            collist += dt.Rows[0]["COLUMN06"].ToString();
        //            collist += "," + "\r\n";
        //        }
        //        if (dt.Rows[0]["COLUMN09"].ToString() == "")
        //        {
        //            collist += dt.Rows[0]["COLUMN09"].ToString();
        //            collist += "";
        //        }
        //        else
        //        {
        //            collist += dt.Rows[0]["COLUMN09"].ToString();
        //            collist += "," + "\r\n";
        //        }
        //        if (dt.Rows[0]["COLUMN10"].ToString() == "")
        //        {
        //            collist += dt.Rows[0]["COLUMN10"].ToString();
        //            collist += "";
        //        }
        //        else
        //        {
        //            collist += dt.Rows[0]["COLUMN10"].ToString();
        //            collist += "," + "\r\n";
        //        }
        //        if (dt.Rows[0]["COLUMN11"].ToString() == "")
        //        {
        //            collist += dt.Rows[0]["COLUMN11"].ToString();
        //            collist += "";
        //        }
        //        else
        //        {
        //            collist += dt.Rows[0]["COLUMN11"].ToString() + ".";

        //        }



        //    }
        //    return collist;
        //}

        //public string GetAddress1(int PurchaseOrderID)
        //{
        //    string collist = null;
        //    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
        //    conn.Open();
        //    SqlCommand cmd = new SqlCommand("usp_PUR_TR_Address1", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.Add(new SqlParameter("@PurchaseOrderID", PurchaseOrderID));
        //    SqlDataAdapter da = new SqlDataAdapter(cmd);
        //    DataTable dt = new DataTable();
        //    da.Fill(dt);
        //    conn.Close();
        //    if (dt.Rows.Count > 0)
        //    {
        //        collist = dt.Rows[0]["COLUMN05"].ToString();
        //        collist += "," + "\r\n" + dt.Rows[0]["COLUMN06"].ToString() + "," + "\r\n" + dt.Rows[0]["COLUMN07"].ToString() + "," + "\r\n" + dt.Rows[0]["COLUMN08"].ToString() + "," + "\r\n" + dt.Rows[0]["COLUMN09"].ToString() + "," + "\r\n" + dt.Rows[0]["COLUMN10"].ToString() + "," + "\r\n" + dt.Rows[0]["COLUMN11"].ToString() + ".";
        //    } return collist;
        //}
        //II adres

        public string GetAdrs(int SalesOrderID)
        {
            string collist = null;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand cmd = new SqlCommand("usp_SAL_TP_Address", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@SalesOrderID", SalesOrderID));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            //conn.Close();
            if (dt.Rows.Count > 0)
            {

                if (dt.Rows[0]["COLUMN06"].ToString() == "" || dt.Rows[0]["COLUMN06"].ToString() == null)
                    collist += "";
                else
                    collist = dt.Rows[0]["COLUMN06"].ToString();
                if (dt.Rows[0]["COLUMN07"].ToString() == "" || dt.Rows[0]["COLUMN07"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN07"].ToString();
                if (dt.Rows[0]["COLUMN08"].ToString() == "" || dt.Rows[0]["COLUMN08"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN08"].ToString();
                if (dt.Rows[0]["COLUMN10"].ToString() == "" || dt.Rows[0]["COLUMN10"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN10"].ToString();
                if (dt.Rows[0]["COLUMN11"].ToString() == "" || dt.Rows[0]["COLUMN11"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN11"].ToString();
                if (dt.Rows[0]["COLUMN12"].ToString() == "" || dt.Rows[0]["COLUMN12"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN12"].ToString();
                if (dt.Rows[0]["COLUMN16"].ToString() == "" || dt.Rows[0]["COLUMN16"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN16"].ToString();
                if (collist != null && collist != "")
                {
                    Array las = new Array[7];
                    las = collist.Split(','); collist = "";
                    for (int i = 0; i < las.Length; i++)
                    {
                        if (i == las.Length - 1)
                            collist += las.GetValue(i) + ".";
                        else if (las.GetValue(i) == "")
                            collist += "";
                        else
                            collist += las.GetValue(i) + ",\r\n";
                    }
                }


            }
            return collist;
        }

        public string GetOppUnitAdrs(int opunit)
        {
            string collist = null;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand cmd = new SqlCommand("usp_SAL_TP_oppUnitAddress", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@oppUnit", opunit));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            //conn.Close();
            if (dt.Rows.Count > 0)
            {

                if (dt.Rows[0]["COLUMN12"].ToString() == "" || dt.Rows[0]["COLUMN12"].ToString() == null)
                    collist += "";
                else
                    collist = dt.Rows[0]["COLUMN12"].ToString();

                if (dt.Rows[0]["COLUMN13"].ToString() == "" || dt.Rows[0]["COLUMN13"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN13"].ToString();

                if (dt.Rows[0]["COLUMN14"].ToString() == "" || dt.Rows[0]["COLUMN14"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN14"].ToString();

                if (dt.Rows[0]["COLUMN15"].ToString() == "" || dt.Rows[0]["COLUMN15"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN15"].ToString();

                if (dt.Rows[0]["COLUMN16"].ToString() == "" || dt.Rows[0]["COLUMN16"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN16"].ToString();

                //if (dt.Rows[0]["COLUMN24"].ToString() == "" || dt.Rows[0]["COLUMN24"].ToString() == null)
                //    collist += "";
                //else
                //    collist += "," + dt.Rows[0]["COLUMN24"].ToString();

                //if (dt.Rows[0]["COLUMN16"].ToString() == "" || dt.Rows[0]["COLUMN16"].ToString() == null)
                //    collist += "";
                //else
                //    collist += "," + dt.Rows[0]["COLUMN16"].ToString();

                if (collist != null && collist != "")
                {
                    Array las = new Array[7];
                    las = collist.Split(','); collist = "";
                    for (int i = 0; i < las.Length; i++)
                    {
                        if (i == las.Length - 1)
                            collist += las.GetValue(i) + ".";
                        else if (las.GetValue(i) == "")
                            collist += "";
                        else
                            collist += las.GetValue(i) + "\r\n";
                    }
                }


            }
            return collist;
        }

        public string GetCustAdrs(int CustomerId)
        {
            string collist = null; string coldata = null;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand cmd = new SqlCommand("usp_SAL_TP_C_Address", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@CustomerId", CustomerId));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            //conn.Close();
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                coldata = dt.Rows[0][i].ToString();
                if (coldata == "" || coldata == null)
                    collist += "";
                else
                    collist += dt.Rows[0][i].ToString() + " \r\n";
            }
            if (dt.Columns.Count > 0)
            { collist = collist.Trim(); }
            return collist;
        }
        public string GetEmpAdrs(string EmpId)
        {
            string collist = null; string coldata = null;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            SqlCommand cmd = new SqlCommand("usp_EMP_TP_EmpAddress", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@EmpId", EmpId));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    coldata = dt.Rows[0][i].ToString();
                    if (coldata == "" || coldata == null)
                        collist += "";
                    else
                        collist += dt.Rows[0][i].ToString() + " \r\n";
                }
                if (dt.Columns.Count > 0)
                { collist = collist.Trim(); }
            }
            return collist;
        }
        //EMPHCS1784	Job Order for IN Process 15/7/2016
        public string GetJORINAddess(int SalesOrderID)
        {
            string collist = null;
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlcon"].ToString());
            //conn.Open();
            SqlCommand cmd = new SqlCommand("usp_SAL_TP_JORINAddess", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@SalesOrderID", SalesOrderID));
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            //conn.Close();
            if (dt.Rows.Count > 0)
            {

                if (dt.Rows[0]["COLUMN06"].ToString() == "" || dt.Rows[0]["COLUMN06"].ToString() == null)
                    collist += "";
                else
                    collist = dt.Rows[0]["COLUMN06"].ToString();
                if (dt.Rows[0]["COLUMN07"].ToString() == "" || dt.Rows[0]["COLUMN07"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN07"].ToString();
                if (dt.Rows[0]["COLUMN08"].ToString() == "" || dt.Rows[0]["COLUMN08"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN08"].ToString();
                if (dt.Rows[0]["COLUMN10"].ToString() == "" || dt.Rows[0]["COLUMN10"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN10"].ToString();
                if (dt.Rows[0]["COLUMN11"].ToString() == "" || dt.Rows[0]["COLUMN11"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN11"].ToString();
                if (dt.Rows[0]["COLUMN12"].ToString() == "" || dt.Rows[0]["COLUMN12"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN12"].ToString();
                if (dt.Rows[0]["COLUMN16"].ToString() == "" || dt.Rows[0]["COLUMN16"].ToString() == null)
                    collist += "";
                else
                    collist += "," + dt.Rows[0]["COLUMN16"].ToString();
                if (collist != null && collist != "")
                {
                    Array las = new Array[7];
                    las = collist.Split(','); collist = "";
                    for (int i = 0; i < las.Length; i++)
                    {
                        if (i == las.Length - 1)
                            collist += las.GetValue(i) + ".";
                        else if (las.GetValue(i) == "")
                            collist += "";
                        else
                            collist += las.GetValue(i) + ",\r\n";
                    }
                }


            }
            return collist;
        }
}
}
