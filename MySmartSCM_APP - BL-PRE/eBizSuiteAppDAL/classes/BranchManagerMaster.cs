﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eBizSuiteAppModel.Table;

namespace eBizSuiteDAL.classes
{
   public class BranchManagerMaster
    {
       public eBizSuiteTableEntities entities = new eBizSuiteTableEntities();

       public IEnumerable<tbl_BranchMaster> GetBranchMaster()
        {
            return entities.tbl_BranchMaster.ToList();
        }

       public void Add(tbl_BranchMaster register)
        {
            tbl_BranchMaster sysRegister = new tbl_BranchMaster();

            sysRegister.BranchId = register.BranchId;
            sysRegister.BranchName = register.BranchName;
            sysRegister.BranchDesc = register.BranchDesc;

            entities.tbl_BranchMaster.Add(sysRegister);
            entities.SaveChanges();
        }

        public bool IsUserLoginIDExist(int userId)
        {
            return (from o in entities.tbl_BranchMaster where o.BranchId == userId select o).Any();
        }

        public tbl_BranchMaster GetEmployeeDetail(int empID)
        {

            return entities.tbl_BranchMaster.Where(m => m.BranchId == empID).FirstOrDefault();
        }

        public bool UpdateBranchMaster(tbl_BranchMaster register)
        {
            try
            {
                tbl_BranchMaster sysRegister = entities.tbl_BranchMaster.Where(m => m.BranchId == register.BranchId).FirstOrDefault();

                sysRegister.BranchId = register.BranchId;
                sysRegister.BranchName = register.BranchName;
                sysRegister.BranchDesc = register.BranchDesc;



                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                return false;
            }
        }

        public bool DeleteBranchMaster(int mCustID)
        {
            try
            {
                tbl_BranchMaster data = entities.tbl_BranchMaster.Where(m => m.BranchId == mCustID).FirstOrDefault();
                entities.tbl_BranchMaster.Remove(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                return false;
            }
        }

    }
}
