﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using eBizSuiteAppModel.Table;

namespace eBizSuiteDAL.classes
{
  public class CompanyMasterManage
    {
      public eBizSuiteTableEntities entities = new eBizSuiteTableEntities();

      public IEnumerable<tbl_CompanyMaster> GetCompanyMaster()
        {
            return entities.tbl_CompanyMaster.ToList();
        }

      public void Add(tbl_CompanyMaster register)
      {
          tbl_CompanyMaster sysRegister = new tbl_CompanyMaster();

          sysRegister.CompanyId = register.CompanyId;
          sysRegister.CompanyName = register.CompanyName;
          sysRegister.CompanyDesc = register.CompanyDesc;

          entities.tbl_CompanyMaster.Add(sysRegister);

          entities.SaveChanges();
      }

        public bool IsUserLoginIDExist(int userId)
        {
            return (from o in entities.tbl_CompanyMaster where o.CompanyId == userId select o).Any();
        }

        public tbl_CompanyMaster GetEmployeeDetail(int empID)
        {

            return entities.tbl_CompanyMaster.Where(m => m.CompanyId == empID).FirstOrDefault();
        }

        public bool UpdateCompanyMaster(tbl_CompanyMaster register)
        {
            try
            {
                tbl_CompanyMaster sysRegister = entities.tbl_CompanyMaster.Where(m => m.CompanyId == register.CompanyId).FirstOrDefault();

                sysRegister.CompanyId = register.CompanyId;
                sysRegister.CompanyName = register.CompanyName;
                sysRegister.CompanyDesc = register.CompanyDesc;



                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                return false;
            }
        }

        public bool DeleteCompanyMaster(int mCustID)
        {
            try
            {
                tbl_CompanyMaster data = entities.tbl_CompanyMaster.Where(m => m.CompanyId == mCustID).FirstOrDefault();
                entities.tbl_CompanyMaster.Remove(data);
                entities.SaveChanges();
                return true;
            }
            catch (Exception mex)
            {
                return false;
            }
        }

    }
}
